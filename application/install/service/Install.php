<?php
/**
 *  ==================================================================
 *        文 件 名: Install.php
 *        概    要: 安装程序服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/13 12:42
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\install\service;

use builder\KeFormBuilder;
use database\Sql;

/**
 * Class Install - 安装程序服务层
 * @package app\install\service
 */
class Install extends Base {
    
    /**
     * 系统环境检测
     * @return array 返回检测结果
     */
    public function envCheck() {
        $return = [];
        session('error', false);
        $items = [
            'os'     => ['操作系统', '不限制', '类Unix', PHP_OS, 'check'],
            'php'    => ['PHP版本', '5.6', '5.6+', PHP_VERSION, 'check'],
            'upload' => ['附件上传', '不限制', '2M+', '未知', 'check'],
            'gd'     => ['GD库', '2.0', '2.0+', '未知', 'check'],
            'disk'   => ['磁盘空间', '100M', '不限制', '未知', 'check'],
        ];
        
        // PHP环境检测
        if ($items['php'][3] < $items['php'][1]) {
            $items['php'][4] = 'times text-warning';
            $return[] = [
                'status' => -1,
                'msg'    => 'PHP版本过低',
            ];
            session('error', $return);
        }
        
        // 附件上传检测
        if (@ini_get('file_uploads')) {
            $items['upload'][3] = ini_get('upload_max_filesize');
        }
        
        
        // GD库检测
        $gd = function_exists('gd_info') ? gd_info() : [];
        if (empty($gd['GD Version'])) {
            $items['gd'][3] = '未安装';
            $items['gd'][4] = 'times text-warning';
            $return[] = [
                'status' => -2,
                'msg'    => '未安装GD库',
            ];
            session('error', $return);
        } else {
            $items['gd'][3] = $gd['GD Version'];
        }
        
        // 磁盘空间检测
        if (function_exists('disk_free_space')) {
            $disk_size = floor(disk_free_space(BASE_ROOT) / (1024 * 1024));
            $items['disk'][3] = $disk_size . 'M';
            if ($disk_size < 100) {
                $items['disk'][4] = 'times text-warning';
                $return[] = [
                    'status' => -3,
                    'msg'    => '磁盘空间不足',
                ];
                session('error', $return);
            }
        }
        return $items;
    }
    
    /**
     * 目录、文件权限检测
     * @return array 返回检测结果
     */
    public function dirCheck() {
        $items = [
            ['dir', '可写', 'check', 'application'],
            ['dir', '可写', 'check', 'config'],
            ['dir', '可写', 'check', 'data'],
            ['dir', '可写', 'check', 'html'],
            ['dir', '可写', 'check', 'plugins'],
            ['dir', '可写', 'check', PUBLIC_NAME . '/static'],
            ['dir', '可写', 'check', PUBLIC_NAME . '/uploads'],
            ['dir', '可写', 'check', 'runtime'],
        ];
        
        foreach ($items as &$val) {
            $item = BASE_ROOT . $val[3];
            if ('dir' == $val[0]) {
                if (!is_writable($item)) {
                    if (is_dir($item)) {
                        $val[1] = '可读';
                        $val[2] = 'times text-warning';
                        session('error', true);
                    } else {
                        $val[1] = '不存在';
                        $val[2] = 'times text-warning';
                        session('error', true);
                    }
                }
            } else {
                if (file_exists($item)) {
                    if (!is_writable($item)) {
                        $val[1] = '不可写';
                        $val[2] = 'times text-warning';
                        session('error', true);
                    }
                } else {
                    if (!is_writable(dirname($item))) {
                        $val[1] = '不存在';
                        $val[2] = 'times text-warning';
                        session('error', true);
                    }
                }
            }
        }
        
        return $items;
    }
    
    /**
     * 函数、扩展检测
     * @return array 返回检测结果
     */
    public function funCheck() {
        $items = array(
            ['pdo', '支持', 'check', '类'],
            ['pdo_mysql', '支持', 'check', '模块'],
            ['fileinfo', '支持', 'check', '模块'],
            ['curl', '支持', 'check', '模块'],
            ['file_get_contents', '支持', 'check', '函数'],
            ['mb_strlen', '支持', 'check', '函数'],
        );
        
        foreach ($items as &$val) {
            if (('类' == $val[3] && !class_exists($val[0]))
                || ('模块' == $val[3] && !extension_loaded($val[0]))
                || ('函数' == $val[3] && !function_exists($val[0]))
            ) {
                $val[1] = '不支持';
                $val[2] = 'times text-warning';
                session('error', true);
            }
        }
        
        return $items;
    }
    
    /**
     * 写入数据库配置文件
     * @param $config - 配置信息
     * @return bool - 写入结果
     */
    public function setConfig($config) {
        if (!is_array($config)) {
            return false;
        }
        //读取配置内容
        $conf = file_get_contents(APP_PATH . 'install/data/database.tpl');
        // 替换配置项
        foreach ($config as $name => $value) {
            $conf = str_replace("[{$name}]", $value, $conf);
        }
        $conf = str_replace(['[atime]', '[utime]'], [time(), time()], $conf);
        //写入应用配置文件
        if (file_put_contents(BASE_ROOT . 'config' . DIRECTORY_SEPARATOR . 'database.php', $conf)) {
            return true;
        } else {
            session('error', true);
            return false;
        }
    }
    
    /**
     * 创建数据表
     * @param string $prefix - 表前缀
     * @param array $replaceArray - 替换数组
     * @return bool
     */
    public function createTables($prefix = '', $replaceArray = []) {
        $orginal = config('original_table_prefix');
        $replaceArray = array_merge($replaceArray, ["{$orginal}" => "{$prefix}"]);
        $sql1 = Sql::getSqlFromFile(realpath(APP_PATH . 'install/data/skcloud.sql'), false, $replaceArray);
        $sql2 = Sql::getSqlFromFile(realpath(APP_PATH . 'install/data/install.sql'), false, $replaceArray);
        if (!$sql1 || !$sql2) {
            return false;
        }
        $sql = array_merge($sql1, $sql2);
        //halt($sql);
        // 开始安装
        $db = db()->connect(session('db_config'));
        $true = 0;
        $false = 0;
        foreach ($sql as $value) {
            $value = trim($value);
            if (empty($value)) continue;
            if (false !== $db->execute($value)) {
                $true++;
            } else {
                $false++;
                session('error', true);
            }
        }
        return $false == 0 ? true : false;
    }
    
    /**
     * 生产随机字符串
     * @param int $length - 指定生产字符串的长度
     * @param string $type - 指定生产字符串的类型（all-全部，num-纯数字，letter-纯字母）
     * @return null|string
     */
    public function cmRound($length = 4, $type = 'all') {
        $str = '';
        $strUp = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $strLow = 'abcdefghijklmnopqrstuvwxyz';
        $number = '0123456789';
        switch ($type) {
            case 'num':
                $strPol = $number;
                break;
            case 'letter':
                $strPol = $strUp . $strLow;
                break;
            default:
                $strPol = $strUp . $number . $strLow;
        }
        $max = strlen($strPol) - 1;
        for ($i = 0; $i < $length; $i++) {
            $str .= $strPol[rand(0, $max)];
        }
        return $str;
    }
    
    /**
     * 创建数据库连接表单
     * @return mixed
     */
    public function databaseFormHtml() {
        $hostValidate = [
            'notEmpty' => ['message' => '数据库HOST不能为空']
        ];
        $dbValidate = [
            'notEmpty' => ['message' => '数据库名称不能为空']
        ];
        $userValidate = [
            'notEmpty' => ['message' => '数据库用户名不能为空']
        ];
        $psdValidate = [
            'notEmpty' => ['message' => '数据库密码不能为空']
        ];
        $portValidate = [
            'notEmpty' => ['message' => '数据库端口号不能为空']
        ];
        $fixValidate = [];
        $form = KeFormBuilder::makeForm(url('install/index/dbInfoCheck'), 4, [], 'db_info')
            ->addRadio('type', 'mysql', ['mysql' => 'MySQL'], '数据库类型')
            ->addText('hostname', '127.0.0.1', '数据库地址', $hostValidate)
            ->addText('database', 'skcloud', '数据库名称', $dbValidate)
            ->addText('username', 'root', '数据库用户名', $userValidate)
            ->addPassword('psd', '', '数据库密码', $psdValidate)
            ->addText('hostport', '3306', '数据库端口', $portValidate)
            ->addText('prefix', 'skc_', '数据表前缀', $fixValidate)
            ->addHidden('engine', 'InnoDB')
            //->addRadio('engine', 'InnoDB', ['InnoDB' => 'InnoDB', 'MyISAM' => 'MyISAM'], '数据表引擎')
            ->addRadio('cover', 1, ['1' => '不覆盖', '2' => '覆盖', '3' => '删除同名数据库'], '是否覆盖同名数据库')
            ->validateForm(url('install/index/step6'))
            ->returnForm();
        return $form;
    }
    
    /**
     * 创建管理员信息表单
     * @return mixed
     */
    public function administratorsFormHtml() {
        $usernameValidate = [
            'notEmpty' => ['message' => '管理员账号不能为空'],
            'regexp'   => [
                'regexp'  => '/^[a-zA-Z0-9_]+$/',
                'message' => '账号只能由字母/数字/下划线组成'
            ]
        ];
        $emailValidate = [
            'notEmpty'     => ['message' => '管理员邮箱不能为空'],
            'emailAddress' => ['message' => '请输入正确的邮箱格式']
        ];
        $psdValidate = [
            'notEmpty'  => ['message' => '管理员密码不能为空'],
            'different' => [
                'field'   => 'username',
                'message' => '密码不能和用户名相同'
            ]
        ];
        $psd2Validate = [
            'notEmpty'  => ['message' => '重复密码不能为空'],
            'different' => [
                'field'   => 'username',
                'message' => '密码不能和用户名相同'
            ],
            'identical' => [
                'field'   => 'password',
                'message' => '两次密码输入不一致'
            ]
        ];
        $emailValue = 'mail@' . str_replace(request()->scheme() . '://', '', request()->domain());
        $form = KeFormBuilder::makeForm(url('install/index/userInfoCheck'), 4, [], 'user_info')
            ->addText('username', 'admin', '设置管理员账号', $usernameValidate)
            ->addText('email', $emailValue, '设置管理员邮箱', $emailValidate)
            ->addPassword('password', '', '设置管理员密码', $psdValidate)
            ->addPassword('password2', '', '重复管理员密码', $psd2Validate)
            ->validateForm(url('install/index/complete'))
            ->returnForm();
        return $form;
    }
}