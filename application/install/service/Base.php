<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 安装模块服务层基类
 *        作    者: IT小强
 *        创建时间: 2017/9/27 0:35
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\install\service;

/**
 * Class Base - 安装模块服务层基类
 * @package app\install\service
 */
class Base extends \app\common\service\Base {
    
}