<?php
/**
 *  ==================================================================
 *        文 件 名: Install.php
 *        概    要: 安装程序验证器
 *        作    者: IT小强
 *        创建时间: 2017/9/13 17:59
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\install\validate;

/**
 * Class Install - 安装程序验证器
 * @package app\install\validate
 */
class Install extends Base {
    /**
     * @var array - 验证规则
     */
    protected $rule = [
        // 数据库连接
        'type|数据库类型'      => 'require|eq:mysql',
        'hostname|数据库地址'  => 'require',
        'database|数据库名称'  => 'require|alphaDash',
        'username|数据库用户名' => 'require|alphaDash',
        'psd|数据库用户名'      => 'require',
        'hostport|数据库端口'  => 'require|integer',
        'prefix|数据表前缀'    => 'require|alphaDash',
        'engine|数据表引擎'    => 'require|in:InnoDB,MyISAM',
        'cover|是否覆盖同名数据库' => 'require|in:1,2,3',
        
        // 管理员账号信息
        'username|登录账号'   => 'require|alphaDash',
        'email|管理员邮箱'     => 'require|email',
        'password|登录密码'   => 'require|min:8|max:18',
        'password2|重复密码'  => 'require|confirm:password|min:8|max:18',
    ];
    
    /**
     * @var array - 验证提示信息
     */
    protected $message = [
    
    ];
    
    /**
     * @var array - 验证场景
     */
    protected $scene = [
        'db'   => ['type', 'hostname', 'database', 'username', 'hostport', 'prefix', 'engine', 'cover', 'psd'],
        'info' => ['username', 'email', 'password', 'password2']
    ];
}