SET FOREIGN_KEY_CHECKS = 0;

-- -----------------------------
-- Table structure for `skc_sys_concate`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_concate`;
CREATE TABLE `skc_sys_concate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `en_name` varchar(100) NOT NULL COMMENT '英文别名',
  `name` varchar(30) NOT NULL COMMENT '分类名称',
  `atime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `utime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `en_name` (`en_name`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_concate_pid_i` (`pid`),
  KEY `sys_concate_show_i` (`show`),
  KEY `sys_concate_enable_i` (`enable`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='配置分类表';

-- -----------------------------
-- Records of `skc_sys_concate`
-- -----------------------------
INSERT INTO `skc_sys_concate` VALUES ('1', '0', 'default', '基本配置', unix_timestamp(now()), unix_timestamp(now()), '1', '1', '1');
INSERT INTO `skc_sys_concate` VALUES ('2', '0', 'seo', 'SEO配置', unix_timestamp(now()), unix_timestamp(now()), '3', '1', '1');
INSERT INTO `skc_sys_concate` VALUES ('3', '0', 'backup', '备份还原', unix_timestamp(now()), unix_timestamp(now()), '4', '1', '1');
INSERT INTO `skc_sys_concate` VALUES ('4', '0', 'system', '系统配置', unix_timestamp(now()), unix_timestamp(now()), '2', '1', '1');
INSERT INTO `skc_sys_concate` VALUES ('5', '0', 'upload', '上传配置', unix_timestamp(now()), unix_timestamp(now()), '5', '1', '1');

-- -----------------------------
-- Table structure for `skc_sys_module`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_module`;
CREATE TABLE `skc_sys_module` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) NOT NULL COMMENT '模块标识',
  `title` varchar(200) DEFAULT '' COMMENT '模块名称',
  `des` varchar(255) DEFAULT '' COMMENT '模块描述',
  `ico` varchar(30) DEFAULT 'bars' COMMENT '模块图标',
  `version` varchar(20) NOT NULL DEFAULT '1.0.0' COMMENT '模块版本',
  `author` varchar(20) NOT NULL DEFAULT '' COMMENT '模块作者',
  `author_url` varchar(255) NOT NULL DEFAULT '' COMMENT '模块作者主页',
  `update_url` varchar(255) NOT NULL DEFAULT '' COMMENT '模块更新链接',
  `atime` int(10) DEFAULT '0' COMMENT '添加时间',
  `utime` int(10) DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `sub_name` varchar(30) NOT NULL COMMENT '多模块时，二级模块名称',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_module_show_i` (`show`),
  KEY `sys_module_enable_i` (`enable`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统模块表';

-- -----------------------------
-- Records of `skc_sys_module`
-- -----------------------------
INSERT INTO `skc_sys_module` VALUES ('1', 'admin', '系统', '配置管理、菜单管理、备份还原、插件管理', 'fa fa-cog', '1.0.0', 'IT小强', 'http://www.xqitw.com', 'http://www.xqitw.com', unix_timestamp(now()), unix_timestamp(now()), '1', '', '1', '1');
INSERT INTO `skc_sys_module` VALUES ('2', 'user', '用户', '个人中心、权限管理、角色管理、用户管理', 'fa fa-user', '1.0.0', 'IT小强', 'http://www.xqitw.com', 'http://www.xqitw.com', unix_timestamp(now()), unix_timestamp(now()), '2', '', '1', '1');

-- -----------------------------
-- Table structure for `skc_sys_log`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_log`;
CREATE TABLE `skc_sys_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `aid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '行为id',
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行用户id',
  `aip` varchar(30) NOT NULL DEFAULT '127.0.0.1' COMMENT '执行行为者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '触发行为的表',
  `rid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '触发行为的数据id',
  `remark` longtext NOT NULL COMMENT '日志备注',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `atime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行行为的时间',
  PRIMARY KEY (`id`),
  KEY `sys_log_ip_i` (`aip`),
  KEY `sys_log_id_i` (`aid`),
  KEY `sys_log_uid_i` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统日志表';

-- -----------------------------
-- Table structure for `skc_sys_layout`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_layout`;
CREATE TABLE `skc_sys_layout` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '外键->用户表',
  `boxed_layout` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '是否为留白布局 1-->是，2-->否',
  `boxed_layout_bg` varchar(255) NOT NULL DEFAULT '' COMMENT '留白布局背景',
  `enable_animations` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否启用动画 1-->是，2-->否',
  `transitions` varchar(255) NOT NULL DEFAULT 'effect' COMMENT '动画过度效果',
  `fixed_navbar` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否固定导航 1-->是，2-->否',
  `fixed_footer` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否固定页脚 1-->是，2-->否',
  `fixed_nav` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否固左侧菜单 1-->是，2-->否',
  `widget_profil` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '是否显示个人信息 1-->是，2-->否',
  `shortcut_buttons` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '是否显示快捷按钮 1-->是，2-->否',
  `collapsed_mode` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '后台菜单是否折叠 1-->折叠，2-->后台不折叠',
  `off_canvas` varchar(255) NOT NULL DEFAULT 'none' COMMENT '滑动导航设置',
  `aside_visible` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '右侧工具栏是否显示 1-->显示，2-->不显示',
  `aside_fixed` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '右侧工具栏是否固定 1-->固定，2-->不固定',
  `aside_float` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '右侧工具栏是否浮动显示 1-->浮动，2-->不浮动',
  `aside_left` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '右侧工具栏是否显示左侧 1-->左侧显示，2-->右侧显示',
  `aside_dark` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '右侧工具栏是否启用暗色背景 1-->暗，2-->亮',
  `color` varchar(255) NOT NULL DEFAULT '' COMMENT '配色方案',
  `atime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `utime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  KEY `sys_layout_uid_i` (`uid`),
  KEY `sys_layout_show_i` (`show`),
  KEY `sys_layout_enable_i` (`enable`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='后台布局表';

-- -----------------------------
-- Records of `skc_sys_layout`
-- -----------------------------
INSERT INTO `skc_sys_layout` VALUES ('1', '1', '2', '', '1', 'effect', '1', '1', '1', '1', '2', '2', 'none', '2', '1', '1', '2', '2', '', unix_timestamp(now()), unix_timestamp(now()), '0', '1', '1');

-- -----------------------------
-- Table structure for `skc_sys_attachment`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_attachment`;
CREATE TABLE `skc_sys_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `module` varchar(32) NOT NULL DEFAULT '' COMMENT '模块名，由哪个模块上传的',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图路径',
  `ext` varchar(30) NOT NULL DEFAULT '' COMMENT '文件类型',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `driver` varchar(16) NOT NULL DEFAULT 'local' COMMENT '上传驱动',
  `download` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `atime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `utime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件表';

-- -----------------------------
-- Table structure for `skc_sys_plugin`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_plugin`;
CREATE TABLE `skc_sys_plugin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) NOT NULL COMMENT '插件名称',
  `title` varchar(100) NOT NULL COMMENT '插件标题',
  `ico` varchar(30) DEFAULT 'bars' COMMENT '插件图标',
  `describe` varchar(200) NOT NULL DEFAULT '' COMMENT '插件描述',
  `version` varchar(20) NOT NULL DEFAULT '1.0.0' COMMENT '插件版本',
  `author` varchar(20) NOT NULL DEFAULT '' COMMENT '插件作者',
  `author_url` varchar(255) NOT NULL DEFAULT '' COMMENT '插件作者主页',
  `update_url` varchar(255) NOT NULL DEFAULT '' COMMENT '插件更新链接',
  `hook` text NOT NULL COMMENT '插件列表',
  `atime` int(10) DEFAULT '0' COMMENT '添加时间',
  `utime` int(10) DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->有后台配置，2-->无后台配置',
  `manage` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->有后台管理，2-->无后台管理',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_plugin_show_i` (`show`),
  KEY `sys_plugin_enable_i` (`enable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统插件表';

-- -----------------------------
-- Table structure for `skc_sys_plugincon`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_plugincon`;
CREATE TABLE `skc_sys_plugincon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属插件ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置项名称',
  `describe` varchar(200) NOT NULL DEFAULT '' COMMENT '配置项描述',
  `value` text NOT NULL COMMENT '配置项值',
  `type` varchar(255) NOT NULL DEFAULT 'hidden' COMMENT '配置类型',
  `list` text NOT NULL COMMENT '配置可选项',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '配置提示语',
  `atime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `utime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  KEY `sys_plugincon_name_i` (`name`),
  KEY `plugin_config_ibfk_1` (`pid`),
  KEY `sys_plugincon_enable_i` (`enable`),
  CONSTRAINT `plugin_config_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `skc_sys_plugin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='插件配置表';

-- -----------------------------
-- Table structure for `skc_sys_role`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_role`;
CREATE TABLE `skc_sys_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(30) NOT NULL COMMENT '角色名称',
  `describe` varchar(200) NOT NULL DEFAULT '' COMMENT '角色描述',
  `power` varchar(300) NOT NULL DEFAULT '' COMMENT '权限列表 eg： 1,2,5,8',
  `atime` int(10) DEFAULT '0' COMMENT '添加时间',
  `utime` int(10) DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_role_pid_i` (`pid`),
  KEY `sys_role_show_i` (`show`),
  KEY `sys_role_enable_i` (`enable`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- -----------------------------
-- Records of `skc_sys_role`
-- -----------------------------
INSERT INTO skc_sys_role VALUES ('1', '0', '默认角色', '暂无任何权限', '', unix_timestamp(now()), unix_timestamp(now()), '1', '1', '1');
INSERT INTO skc_sys_role VALUES ('2', '0', '超级管理', '拥有所有权限', '', unix_timestamp(now()), unix_timestamp(now()), '2', '1', '1');

-- -----------------------------
-- Table structure for `skc_sys_user`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_user`;
CREATE TABLE `skc_sys_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `rid` int(11) unsigned NOT NULL COMMENT '用户角色ID 外键->admin_role表',
  `t_name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `age` int(3) DEFAULT NULL COMMENT '用户年龄',
  `n_name` varchar(20) DEFAULT NULL COMMENT '用户昵称',
  `portrait` varchar(100) DEFAULT NULL COMMENT '用户头像',
  `email` varchar(30) DEFAULT NULL COMMENT 'Email地址',
  `birthday` varchar(10) DEFAULT '0' COMMENT '用户生日',
  `qq` varchar(20) DEFAULT NULL COMMENT 'QQ账号',
  `phone` varchar(11) DEFAULT NULL COMMENT '用户手机',
  `sex` tinyint(1) unsigned DEFAULT '1' COMMENT '用户性别 1-->保密 2-->男 3-->保密',
  `username` varchar(60) NOT NULL COMMENT '用户名(登录账号)',
  `password` varchar(255) NOT NULL COMMENT '登录密码',
  `add_time` int(10) unsigned DEFAULT '0' COMMENT '注册时间',
  `last_time` int(10) unsigned DEFAULT '0' COMMENT '上次登录时间',
  `address` varchar(255) DEFAULT NULL COMMENT '用户地址',
  `describe_subject` varchar(100) DEFAULT NULL COMMENT '用户个人说明标题',
  `describe_content` text COMMENT '用户个人说明内容',
  `salt` varchar(10) NOT NULL DEFAULT '' COMMENT '密码加/解密 密钥',
  `del` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->未删除，2-->删除',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `sys_user_t_name` (`t_name`),
  KEY `sys_user_email` (`email`),
  KEY `sys_user_qq` (`qq`),
  KEY `sys_user_phone` (`phone`),
  KEY `skc_user_ibfk_1` (`rid`),
  KEY `sys_user_show_i` (`show`),
  KEY `sys_user_enable_i` (`enable`),
  KEY `sys_user_del_i` (`del`),
  CONSTRAINT `skc_user_ibfk_1` FOREIGN KEY (`rid`) REFERENCES `skc_sys_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- -----------------------------
-- Records of `skc_sys_user`
-- -----------------------------
INSERT INTO skc_sys_user VALUES ('1', '2', 'SKCloud管理员', '25', 'SKCloud管理员', '', '[email]', 0, '', '', '1', '[username]', '[password]', unix_timestamp(now()), unix_timestamp(now()), '', 'SKCloudPHP管理员', 'SKCloudPHP管理员', '[salt]', '1', '1', '1', '1');

SET FOREIGN_KEY_CHECKS = 1;
