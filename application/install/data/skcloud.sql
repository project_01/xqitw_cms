-- -----------------------------
-- MySQL Data Transfer
--
-- Host     : 58f4cf1a73eeb.sh.cdb.myqcloud.com
-- Port     : 3534
-- Database : skcloudphp100
--
-- Part : #1
-- Date : 2017-10-02 21:47:58
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `skc_sys_action`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_action`;
CREATE TABLE `skc_sys_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `mid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '外键->模块表',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '行为唯一标识',
  `title` varchar(80) NOT NULL DEFAULT '' COMMENT '行为标题',
  `remark` varchar(128) NOT NULL DEFAULT '' COMMENT '行为描述',
  `rule` text NOT NULL COMMENT '行为规则',
  `log` text NOT NULL COMMENT '日志规则',
  `atime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `utime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_action_ibfk_1` (`mid`),
  KEY `sys_action_enable_i` (`enable`),
  CONSTRAINT `sys_action_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `skc_sys_module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统行为表';

-- -----------------------------
-- Records of `skc_sys_action`
-- -----------------------------
INSERT INTO `skc_sys_action` VALUES ('1', '2', 'user_login_success', '登录成功', '管理员后台登录记录-登录成功', ' ', '登录日志：[details]', '1502674999', '1502936670', '1', '1');
INSERT INTO `skc_sys_action` VALUES ('2', '2', 'user_login_error', '登录失败', '管理员后台登录记录-登录失败', ' ', '  登录日志：[details]', '1502936745', '1502936745', '2', '1');

-- -----------------------------
-- Table structure for `skc_sys_config`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_config`;
CREATE TABLE `skc_sys_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '外键->分类表',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置项名称',
  `describe` varchar(200) DEFAULT '' COMMENT '配置项描述',
  `value` text NOT NULL COMMENT '配置项值',
  `type` varchar(255) NOT NULL DEFAULT 'text' COMMENT '配置类型',
  `list` varchar(255) NOT NULL DEFAULT '' COMMENT '配置可选项',
  `tip` varchar(255) DEFAULT '' COMMENT '配置提示语',
  `atime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `utime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_config_ibfk_1` (`cid`),
  KEY `sys_config_show_i` (`show`),
  KEY `sys_config_enable_i` (`enable`),
  CONSTRAINT `sys_config_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `skc_sys_concate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- -----------------------------
-- Records of `skc_sys_config`
-- -----------------------------
INSERT INTO `skc_sys_config` VALUES ('1', '1', 'is_login_captcha', '后台登录是否验证', '2', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '开启此项后，每次登陆后台都需要输入正确的验证码', '1491307501', '1506699758', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('2', '2', 'web_title', '站点SEO标题', 'SKCloudPHP后台管理系统', 'text', '', '', '1491307501', '1491307501', '1', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('3', '2', 'web_tags', '站点SEO关键词', '后台系统,响应式后台,多功能后台,快速开发框架', 'tags', '', '', '1491307501', '1491307501', '3', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('4', '4', 'app_trace', '是否开启页面Trace', '2', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '此配置用于开发时调试。开启后，可以在浏览器控制台输出调试选项。（一般情况下无需开启）', '1490880592', '1490880592', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('5', '3', 'data_backup_part_size', '数据库备份卷大小', '20971520', 'text', '', '', '1491277290', '1491277290', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('6', '3', 'data_backup_compress', '数据库备份文件是否启用压缩', '2', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '', '1491277388', '1491277388', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('7', '3', 'data_backup_compress_level', '数据库备份文件压缩级别', '1', 'radio', 'a:3:{i:1;s:6:\"最低\";i:4;s:6:\"一般\";i:9;s:6:\"最高\";}', '', '1491277484', '1491277484', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('8', '1', 'is_admin_reg', '是否开启后台注册', '1', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '', '1491741270', '1491741270', '3', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('9', '1', 'is_minify', '是否开启Minify压缩', '1', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '开启此项后，系统会将js、css文件压缩合拼，可以减少请求次数，加快网站加载速度', '1491799721', '1491799721', '4', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('10', '2', 'web_admin_logo', '网站LOGO图片', '', 'file', '', '', '1491817381', '1491817381', '5', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('11', '2', 'web_title_delimiter', '站点标题分隔符', '-', 'radio', 'a:3:{s:1:\"-\";s:9:\"破折号\";s:1:\"_\";s:9:\"下划线\";s:1:\"|\";s:6:\"竖线\";}', '', '1491825852', '1491825852', '6', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('12', '2', 'web_title_delimiter_space', '分隔符是否留空', '1', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '', '1491826161', '1491826161', '7', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('13', '4', 'is_ssl', '是否开启强制SSL', '2', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '开启强制SSL，网站会自动将http网站转换为https。（请确保您的域名支持https再开启此项）', '1492090135', '1492090135', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('14', '4', 'web_cache_type', '清除缓存类型', 'a:3:{i:0;s:5:\"cache\";i:1;s:4:\"temp\";i:2;s:3:\"log\";}', 'checkbox', 'a:3:{s:5:\"cache\";s:12:\"缓存文件\";s:4:\"temp\";s:18:\"模板编译文件\";s:3:\"log\";s:18:\"系统日志文件\";}', '', '1492259016', '1492259016', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('15', '1', 'default_home_module', '指定前台默认模块', 'index/index/index', 'text', '', '', '1494145152', '1494145152', '6', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('16', '4', 'is_html_cache', '是否开启静态页面缓存', '1', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '', '1494154331', '1494154331', '0', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('17', '1', 'index_on_off', '是否开启前台站点', '1', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '', '1494690431', '1494690431', '1', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('18', '2', 'web_sub_title', '站点二级标题', '极速、稳定的后台开发框架', 'text', '', '', '1495861522', '1495861522', '2', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('19', '2', 'web_describe', '站点SEO描述', 'SKCloudPHP后台管理系统，极速、稳定的后台开发框架', 'textarea', '', '', '1495861610', '1495861610', '4', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('20', '4', 'hide_index', '是否隐藏入口index.php', '2', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '', '1496552472', '0', '3', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('21', '1', 'default_admin_module', '指定后台默认模块', 'admin/index/index', 'text', '', '', '1499515476', '0', '4', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('22', '5', 'upload_file_type', '允许上传的文件类型', 'doc,docx,xls', 'tags', '', '请填写允许上传的文件后缀，不填写则不限制文件类型。例如：doc,zip', '1499907954', '0', '1', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('23', '5', 'upload_file_size', '文件上传大小限制', '0', 'text', '', '文件允许上传的最大值,单位：kb,如果不限制大小，请输入0', '1499909080', '0', '2', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('24', '1', 'admin_login_name', '指定后台登录入口', 'admin-login', 'text', '', '例如：后台登录入口为http://www.gzyundb.com/skc-login.html,则填写skc-login即可', '1500179797', '0', '5', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('25', '1', 'is_hiden_admin_login_path', '是否隐藏后台登录入口', '2', 'switch', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '若启用隐藏登录入口，则必须输入正确的登录地址，才会显示登录页面，否则一律跳转到网站首页', '1500180463', '0', '6', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('26', '1', 'active_module', '设置后台模块展示数量', '4', 'text', '', '', '1506697655', '1506745757', '9', '1', '1');
INSERT INTO `skc_sys_config` VALUES ('27', '4', 'session_life_time', '登录有效时间', '1800', 'text', '', '单位：秒。设置为0表示不限制登录有效时间。', '1506745606', '1506745606', '6', '1', '1');

-- -----------------------------
-- Table structure for `skc_sys_power`
-- -----------------------------
DROP TABLE IF EXISTS `skc_sys_power`;
CREATE TABLE `skc_sys_power` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(30) NOT NULL COMMENT '权限名称',
  `mid` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '模块ID',
  `ico` varchar(30) DEFAULT 'bars' COMMENT '权限图标',
  `controller` varchar(20) NOT NULL DEFAULT 'index' COMMENT '控制器识别名',
  `action` varchar(20) NOT NULL DEFAULT 'index' COMMENT '方法识别名',
  `order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->系统权限，2-->用户自定义权限',
  `atime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `utime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->显示，2-->隐藏',
  `enable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->启用，2-->禁用',
  `debug` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-->仅调试模式显示，2-->任何时候都显示',
  PRIMARY KEY (`id`),
  KEY `sys_power_name_i` (`name`),
  KEY `sys_power_pid_i` (`pid`),
  KEY `sys_power_ibfk_1` (`mid`),
  KEY `sys_power_type_i` (`type`),
  KEY `sys_power_show_i` (`show`),
  KEY `sys_power_enable_i` (`enable`),
  CONSTRAINT `sys_power_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `skc_sys_module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COMMENT='系统权限表';

-- -----------------------------
-- Records of `skc_sys_power`
-- -----------------------------
INSERT INTO `skc_sys_power` VALUES ('1', '0', '系统首页', '1', 'fa fa-cog', 'admin-Index', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('2', '1', '删除静态页面', '1', 'fa fa-trash-o', 'admin-Index', 'rmHtml', '1', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('3', '1', '删除缓存', '1', 'fa fa-trash-o', 'admin-Index', 'rmCache', '2', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('4', '1', '设置后台布局', '1', 'fa fa-expand', 'admin-Index', 'setLayout', '3', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('6', '0', '系统设置', '1', 'fa fa-wrench', 'admin-Config', 'lists', '2', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('7', '6', '配置列表', '1', 'fa fa-folder-open', 'admin-Config', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('8', '6', '添加配置', '1', 'fa fa-send-o', 'admin-Config', 'add', '2', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('9', '6', '修改配置', '1', 'fa fa-edit', 'admin-Config', 'edit', '3', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('10', '6', '检查配置分类是否存在', '1', 'fa fa-check-circle', 'admin-Config', 'idUniqueCheck', '5', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('11', '0', '菜单管理', '1', 'fa fa-bars', 'admin-Menu', 'default', '6', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('12', '11', '菜单列表', '1', 'fa fa-folder-open-o', 'admin-Menu', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('13', '11', '添加菜单', '1', 'fa fa-send-o', 'admin-Menu', 'add', '2', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('14', '11', '编辑菜单', '1', 'fa fa-edit', 'admin-Menu', 'edit', '3', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('15', '11', '删除菜单', '1', 'fa fa-trash-o', 'admin-Menu', 'delete', '4', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('16', '11', '快速修改菜单项', '1', 'fa fa-rocket', 'admin-Menu', 'updateField', '5', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('17', '11', '获取菜单列表', '1', 'fa fa-expand', 'admin-Menu', 'getMenuListByMid', '6', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('18', '11', '获取控制器', '1', 'fa fa-expand', 'admin-Menu', 'getController', '7', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('19', '11', '获取操作', '1', 'fa fa-expand', 'admin-Menu', 'getAction', '8', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('20', '0', '数据管理', '1', 'fa fa-database', 'admin-Database', 'default', '5', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('21', '20', '备份数据', '1', 'fa fa-copy', 'admin-Database', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('22', '20', '还原数据', '1', 'fa fa-reply', 'admin-Database', 'reply', '2', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('23', '20', '导入数据', '1', 'fa fa-arrow-up', 'admin-Database', 'import', '3', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('24', '20', '导出数据', '1', 'fa fa-arrow-down', 'admin-Database', 'export', '4', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('25', '20', '修复数据表', '1', 'fa fa-wrench', 'admin-Database', 'repair', '5', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('26', '20', '优化数据表', '1', 'fa fa-gears', 'admin-Database', 'optimize', '6', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('27', '20', '删除备份文件', '1', 'fa fa-trash', 'admin-Database', 'delete', '7', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('28', '0', '模块管理', '1', 'fa fa-windows', 'admin-Module', 'default', '3', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('29', '28', '模块列表', '1', 'fa fa-folder-open-o', 'admin-Module', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('30', '28', '模块详情', '1', 'fa fa-eye', 'admin-Module', 'detail', '2', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('31', '28', '上传模块', '1', 'fa fa-upload', 'admin-Module', 'upload', '3', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('32', '28', '启用模块', '1', 'fa fa-arrow-right', 'admin-Module', 'enable', '4', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('33', '28', '禁用模块', '1', 'fa fa-arrow-left', 'admin-Module', 'disable', '5', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('34', '28', '安装模块', '1', 'fa fa-plus', 'admin-Module', 'install', '6', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('35', '28', '卸载模块', '1', 'fa fa-trash-o', 'admin-Module', 'uninstall', '7', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('36', '0', '插件管理', '1', 'fa fa-puzzle-piece', 'admin-Plugin', 'default', '4', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('37', '36', '插件列表', '1', 'fa fa-folder-open', 'admin-Plugin', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('38', '36', '上传插件', '1', 'fa fa-upload', 'admin-Plugin', 'upload', '2', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('39', '36', '查看插件详情', '1', 'fa fa-eye', 'admin-Plugin', 'detail', '3', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('40', '36', '插件编辑', '1', 'fa fa-edit', 'admin-Plugin', 'edit', '4', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('41', '36', '插件管理', '1', 'fa fa-gear', 'admin-Plugin', 'manage', '5', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('42', '36', '安装插件', '1', 'fa fa-forward', 'admin-Plugin', 'install', '6', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('43', '36', '卸载插件', '1', 'fa fa-backward', 'admin-Plugin', 'uninstall', '7', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('44', '36', '启用插件', '1', 'fa fa-check-circle', 'admin-Plugin', 'enable', '8', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('45', '36', '禁用插件', '1', 'fa fa-minus-circle', 'admin-Plugin', 'disable', '9', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('46', '0', '行为管理', '1', 'fa fa-retweet', 'admin-Action', 'default', '7', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('47', '46', '行为列表', '1', 'fa fa-folder-open-o', 'admin-Action', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('48', '46', '添加行为', '1', 'fa fa-send-o', 'admin-Action', 'add', '2', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('49', '46', '编辑行为', '1', 'fa fa-edit', 'admin-Action', 'edit', '4', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('50', '46', '删除行为', '1', 'fa fa-trash-o', 'admin-Action', 'delete', '5', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('51', '46', '快速修改字段', '1', 'fa fa-rocket', 'admin-Action', 'updateField', '6', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('52', '0', '系统日志', '1', 'fa fa-exclamation-circle', 'admin-Log', 'index', '8', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('53', '52', '系统日志详情', '1', 'fa fa-folder-open-o', 'admin-Log', 'detail', '1', '1', '1506689466', '1506689466', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('54', '52', '删除日志', '1', 'fa fa-trash-o', 'admin-Log', 'delete', '2', '1', '1506689466', '1506689466', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('55', '0', '个人信息', '2', 'fa fa-user', 'user-Index', 'index', '1', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('56', '55', '快速修改字段', '2', 'fa fa-rocket', 'user-Index', 'updateField', '1', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('57', '55', '修改信息', '2', 'fa fa-user-plus', 'user-Index', 'saveUserInfo', '2', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('58', '55', '修改密码', '2', 'fa fa-unlock', 'user-Index', 'savePassword', '3', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('59', '55', '更换头像', '2', 'fa fa-user', 'user-Index', 'updateAvatar', '4', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('60', '0', '权限管理', '2', 'fa fa-key', 'user-Power', 'default', '4', '1', '1506689466', '1506689466', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('61', '60', '权限列表', '2', 'fa fa-folder-open', 'user-Power', 'index', '1', '1', '1506689467', '1506689467', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('62', '60', '添加权限', '2', 'fa fa-send', 'user-Power', 'add', '2', '1', '1506689467', '1506689467', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('63', '60', '编辑权限', '2', 'fa fa-edit', 'user-Power', 'edit', '4', '1', '1506689467', '1506689467', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('64', '60', '删除权限', '2', 'fa fa-trash', 'user-Power', 'delete', '5', '1', '1506689467', '1506689467', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('65', '60', '权限排序', '2', 'fa fa-expand', 'user-Power', 'order', '6', '1', '1506689467', '1506689467', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('66', '60', 'ajax获取控制器名', '2', 'fa fa-minus', 'user-Power', 'getController', '7', '1', '1506689467', '1506689467', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('67', '60', 'ajax获取操作名', '2', 'fa fa-minus', 'user-Power', 'getAction', '8', '1', '1506689467', '1506689467', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('68', '0', '角色管理', '2', 'fa fa-group', 'user-Role', 'index', '3', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('69', '68', '快速修改字段', '2', 'fa fa-rocket', 'user-Role', 'updateField', '1', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('70', '68', '角色列表', '2', 'fa fa-group', 'user-Role', 'index', '2', '1', '1506689467', '1506689467', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('71', '68', '添加角色', '2', 'fa fa-send', 'user-Role', 'add', '3', '1', '1506689467', '1506689467', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('72', '68', '修改角色', '2', 'fa fa-edit', 'user-Role', 'edit', '4', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('73', '68', '删除角色', '2', 'fa fa-trash-o', 'user-Role', 'delete', '5', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('74', '0', '用户管理', '2', 'fa fa-user-circle-o', 'user-User', 'default', '2', '1', '1506689466', '1506689466', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('75', '74', '用户列表', '2', 'fa fa-folder-open', 'user-User', 'index', '1', '1', '1506689467', '1506689467', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('76', '74', '添加用户', '2', 'fa fa-send', 'user-User', 'add', '2', '1', '1506689467', '1506689467', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('77', '74', '编辑用户', '2', 'fa fa-edit', 'user-User', 'edit', '3', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('78', '74', '删除用户', '2', 'fa fa-trash-o', 'user-User', 'delete', '4', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('79', '74', '快速修改字段', '2', 'fa fa-rocket', 'user-User', 'updateField', '5', '1', '1506689467', '1506689467', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('80', '6', '删除配置', '1', 'fa fa-trash-o', 'admin-Config', 'delete', '4', '1', '0', '0', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('81', '60', '导出权限', '2', 'fa fa-share', 'user-Power', 'export', '3', '1', '0', '0', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('82', '46', '导出行为', '1', 'fa fa-share', 'admin-Action', 'export', '3', '1', '0', '0', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('83', '28', '快速修改字段', '1', 'fa fa-rocket', 'admin-Module', 'updateField', '8', '1', '0', '0', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('84', '36', '快速修改字段', '1', 'fa fa-rocket', 'admin-Plugin', 'updateField', '10', '1', '0', '0', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('104', '0', '门户首页', '10', 'fa fa-window-maximize', 'cms-admin-Index', 'index', '1', '1', '1506843858', '1506843858', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('105', '0', '导航管理', '10', 'fa fa-navicon', 'cms-admin-Nav', 'default', '3', '1', '1506843858', '1506843858', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('106', '105', '导航列表', '10', 'fa fa-folder-open-o', 'cms-admin-Nav', 'index', '1', '1', '1506843858', '1506843858', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('107', '105', '添加导航', '10', 'fa fa-plus', 'cms-admin-Nav', 'add', '2', '1', '1506843858', '1506843858', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('108', '105', '编辑导航', '10', 'fa fa-edit', 'cms-admin-Nav', 'edit', '3', '1', '1506843858', '1506843858', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('109', '105', '删除导航', '10', 'fa fa-trash-o', 'cms-admin-Nav', 'delete', '4', '1', '1506843858', '1506843858', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('110', '105', '快速修改字段', '10', 'fa fa-rocket', 'cms-admin-Nav', 'updateField', '5', '1', '1506843858', '1506843858', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('111', '105', '导航分类管理', '10', 'fa fa-navicon', 'cms-admin-Navcate', 'default', '6', '1', '1506843858', '1506843858', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('112', '111', '导航分类列表', '10', 'fa fa-folder-open-o', 'cms-admin-Navcate', 'index', '1', '1', '1506843858', '1506843858', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('113', '111', '添加导航分类', '10', 'fa fa-plus', 'cms-admin-Navcate', 'add', '2', '1', '1506843858', '1506843858', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('114', '111', '编辑导航分类', '10', 'fa fa-edit', 'cms-admin-Navcate', 'edit', '3', '1', '1506843858', '1506843858', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('115', '111', '删除导航分类', '10', 'fa fa-trash-o', 'cms-admin-Navcate', 'delete', '4', '1', '1506843858', '1506843858', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('116', '111', '快速修改字段', '10', 'fa fa-rocket', 'cms-admin-Navcate', 'updateField', '5', '1', '1506843858', '1506843858', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('117', '0', '模型管理', '10', 'fa fa-th-large', 'cms-admin-Model', 'default', '4', '1', '1506843858', '1506843858', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('118', '117', '内容模型列表', '10', 'fa fa-folder-open-o', 'cms-admin-Model', 'index', '1', '1', '1506843858', '1506843858', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('119', '117', '新增内容模型', '10', 'fa fa-plus', 'cms-admin-Model', 'add', '2', '1', '1506843858', '1506843858', '1', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('120', '117', '编辑内容模型', '10', 'fa fa-edit', 'cms-admin-Model', 'edit', '3', '1', '1506843858', '1506843858', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('121', '117', '删除内容模型', '10', 'fa fa-trash-o', 'cms-admin-Model', 'delete', '4', '1', '1506843858', '1506843858', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('122', '0', '字段管理', '10', 'fa fa-th-list', 'cms-admin-Field', 'default', '5', '1', '0', '0', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('123', '122', '字段列表', '10', 'fa fa-folder-open-o', 'cms-admin-Field', 'index', '1', '1', '0', '0', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('124', '122', '新增字段', '10', 'fa fa-plus', 'cms-admin-Field', 'add', '2', '1', '0', '0', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('125', '122', '修改字段', '10', 'fa fa-edit', 'cms-admin-Field', 'edit', '3', '1', '0', '0', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('126', '122', '删除字段', '10', 'fa fa-trash-o', 'cms-admin-Field', 'delete', '4', '1', '0', '0', '2', '1', '1');
INSERT INTO `skc_sys_power` VALUES ('127', '0', '文档管理', '10', 'fa fa-book', 'cms-admin-Doc', 'default', '2', '1', '0', '0', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('128', '127', '文档列表', '10', 'fa fa-folder-open-o', 'cms-admin-Doc', 'index', '1', '1', '0', '0', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('129', '127', '新增文档', '10', 'fa fa-plus', 'cms-admin-Doc', 'add', '2', '1', '0', '0', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('130', '127', '编辑文档', '10', 'fa fa-edit', 'cms-admin-Doc', 'edit', '3', '1', '0', '0', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('131', '127', '删除文档', '10', 'fa fa-trash-o', 'cms-admin-Doc', 'delete', '4', '1', '0', '0', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('132', '127', '快速修改字段', '10', 'fa fa-rocket', 'cms-admin-Doc', 'updateField', '5', '1', '0', '0', '2', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('133', '127', '文档分类管理', '10', 'fa fa-bars', 'cms-admin-Category', 'default', '6', '1', '0', '0', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('134', '133', '文档分类列表', '10', 'fa fa-folder-open-o', 'cms-admin-Category', 'index', '1', '1', '0', '0', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('135', '133', '新增文档分类', '10', 'fa fa-plus', 'cms-admin-Category', 'add', '2', '1', '0', '0', '1', '1', '2');
INSERT INTO `skc_sys_power` VALUES ('136', '133', '编辑文档分类', '10', 'fa fa-edit', 'cms-admin-Category', 'edit', '3', '1', '0', '0', '2', '1', '2');


SET FOREIGN_KEY_CHECKS = 1;