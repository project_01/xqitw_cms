<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 安装模块控制器基类
 *        作    者: IT小强
 *        创建时间: 2017-09-13 下午12:37
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\install\controller;

use app\install\service\Install;


/**
 * Class Base - 安装模块控制器基类
 * @package app\install\controller
 */
abstract class Base extends \app\common\controller\Base {
    
    /**
     * @var \app\install\service\Install - 服务层
     */
    protected $service = null;
    
    /**
     * @var string - 数据库配置文件路径
     */
    protected $databasePath = BASE_ROOT . 'config' . DIRECTORY_SEPARATOR . 'database.php';
    
    /**
     * 初始化操作
     */
    protected function initialize() {
        if (!$this->reinstallCheck()) {
            abort('404', '不允许重新安装');
            exit();
        }
        parent::initialize();
        self::$sysName = config('sys_name');
        self::$sysVersion = config('sys_version');
        $assign = [
            'static'   => BASE_URL . 'static',
            'sys_copy' => config('sys_copy'),
            'sys_link' => config('sys_link'),
        ];
        $this->assign($assign);
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new Install();
    }
    
    /**
     * 检查是否允许重新安装
     * @return bool
     */
    protected function reinstallCheck() {
        $reinstall = config('reinstall');
        if (!$reinstall && is_file($this->databasePath) && is_file(INSTALL_LOCK_PATH)) {
            return false;
        }
        return true;
    }
}