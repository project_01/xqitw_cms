<?php
/**
 *  ==================================================================
 *        文 件 名: Index.php
 *        概    要: 安装模块默认控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/13 17:57
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\install\controller;

/**
 * Class Index - 安装模块默认控制器
 * @package app\install\controller
 */
class Index extends Base {
    
    /**
     * 安装程序首页
     */
    public function index() {
        if (is_file($this->databasePath)) {
            // 已经安装过了 执行更新程序
            session('reinstall', true);
            $next = '重新安装';
        } else {
            session('reinstall', false);
            $next = '开始安装';
        }
        session('step', 1);
        session('error', false);
        $title = '安装程序 - ' . self::$sysName . ' V' . self::$sysVersion;
        $assign_push = ['title' => $title, 'next' => $next];
        $this->assign($assign_push);
        return $this->fetch();
    }
    
    /**
     * 安装步骤2 - 系统环境 检测
     */
    public function step2() {
        // 来路页面检测
        $checkPathArray = [1, 2, 3];
        if (!in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/index');
            exit();
        }
        // 检测安装是否出错
        $checkPathArray = [2, 3];
        if (session('error') && !in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/index');
            exit();
        }
        // 判断是否为重新安装
        if (session('reinstall')) {
            $this->redirect('install/index/step5');
            exit();
        }
        session('step', 2);
        $env = $this->service->envCheck();
        if (session('error') == false) {
            $check = '1';
        } else {
            $check = '0';
        }
        $title = '环境检测 - ' . self::$sysName . ' V' . self::$sysVersion;
        $assign_push = ['env' => $env, 'title' => $title, 'check' => $check];
        $this->assign($assign_push);
        return $this->fetch();
    }
    
    /**
     * 安装步骤3 - 目录、文件 权限检测
     */
    public function step3() {
        // 来路页面检测
        $checkPathArray = [2, 3, 4];
        if (!in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/index');
            exit();
        }
        // 检测安装是否出错
        $checkPathArray = [3, 4];
        if (session('error') && !in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/step2');
            exit();
        }
        session('step', 3);
        $dir = $this->service->dirCheck();
        if (session('error') == false) {
            $check = '1';
        } else {
            $check = '0';
        }
        $title = '目录、文件权限检测 - ' . self::$sysName . ' V' . self::$sysVersion;
        $assign_push = ['dir' => $dir, 'title' => $title, 'check' => $check];
        $this->assign($assign_push);
        return $this->fetch();
    }
    
    /**
     * 安装步骤4 - 函数、扩展检测
     */
    public function step4() {
        // 来路页面检测
        $checkPathArray = [3, 4, 5];
        if (!in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/index');
            exit();
        }
        // 检测安装是否出错
        $checkPathArray = [4, 5];
        if (session('error') && !in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/step3');
            exit();
        }
        session('step', 4);
        $fun = $this->service->funCheck();
        if (session('error') == false) {
            $check = '1';
        } else {
            $check = '0';
        }
        $title = '函数、扩展检测 - ' . self::$sysName . ' V' . self::$sysVersion;
        $assign_push = ['fun' => $fun, 'title' => $title, 'check' => $check];
        $this->assign($assign_push);
        return $this->fetch();
    }
    
    /**
     * 安装步骤5 - 填写数据库信息
     * @return mixed
     */
    public function step5() {
        // 来路页面检测
        $checkPathArray = [1, 4, 5, 6];
        if (!in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/index');
            exit();
        }
        // 检测安装是否出错
        $checkPathArray = [5, 6];
        if (session('error') && !in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/step4');
            exit();
        }
        session('step', 5);
        $title = '创建数据库 - ' . self::$sysName . ' V' . self::$sysVersion;
        $assign_push = [
            'title' => $title,
            'form'  => $this->service->databaseFormHtml(),
            'check' => 1
        ];
        $this->assign($assign_push);
        return $this->fetch();
    }
    
    /**
     * 安装步骤6 - 设置超级管理员账号并创建数据表
     * @return mixed
     */
    public function step6() {
        // 来路页面检测
        $checkPathArray = [5, 6];
        if (!in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/index');
            exit();
        }
        // 检测安装是否出错
        $checkPathArray = [6];
        if (session('error') && !in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/step5');
            exit();
        }
        session('step', 6);
        $title = '设置超级管理员账号 - ' . self::$sysName . ' V' . self::$sysVersion;
        $assign_push = [
            'title' => $title,
            'form'  => $this->service->administratorsFormHtml(),
            'check' => 1
        ];
        $this->assign($assign_push);
        return $this->fetch();
    }
    
    /**
     * 完成安装
     */
    public function complete() {
        // 来路页面检测
        $checkPathArray = [6];
        if (!in_array(session('step'), $checkPathArray)) {
            $this->redirect('install/index/index');
            exit();
        }
        // 检测安装是否出错
        if (session('error') && session('step') != 7) {
            $this->redirect('install/index/step6');
            exit();
        }
        $installData = [
            'install' => 'lock',
            'time'    => date('Y-m-d H:i:s'),
            'version' => config('sys_version')
        ];
        $installData = json_encode($installData);
        $write = file_put_contents(INSTALL_LOCK_PATH, $installData);
        if ($write === false) {
            $this->error('写入锁文件失败,请坚持目录权限');
            return false;
        }
        session('step', 7);
        $title = '程序安装完成 - ' . self::$sysName . ' V' . self::$sysVersion;
        $assign_push = ['title' => $title];
        $this->assign($assign_push);
        return $this->fetch();
    }
    
    /**
     * AJAX（POST）- 数据库配置信息检测，建库
     */
    public function dbInfoCheck() {
        // 来路页面检测
        if (session('step') != 5) {
            return false;
        }
        if (!request()->isAjax()) {
            $this->error('提交方式有误!');
        }
        $db = request()->post();
        // 检测数据库配置
        $validate = validate('install');
        $checkDbInfo = $validate->scene('db')->check($db);
        if (!$checkDbInfo) {
            $this->error($validate->getError());
            return false;
        }
        $db['password'] = $db['psd'];
        unset($db['psd']);
        // 保存数据库配置
        $db_config = $db;
        session('db_config', $db_config);
        // 防止不存在的数据库导致连接数据库失败
        $db_name = $db['database'];
        unset($db['database']);
        
        // 创建数据库连接
        $db_instance = db()->connect($db);
        
        // 检测数据库连接
        try {
            $db_instance->execute('select version()');
        } catch (\Exception $e) {
            $this->error('数据库连接失败，请检查数据库连接信息是否正确！');
        }
        
        // 用户选择不覆盖情况下检测是否已存在数据库
        $cover = intval($db['cover']);
        // 检测是否已存在数据库
        $sql = 'SELECT * FROM information_schema.schemata WHERE schema_name =\'' . $db_name . '\'';
        $db_isExist = $db_instance->execute($sql);
        if ($cover == 1 && $db_isExist) {  //用户选择不覆盖
            $this->error('该数据库已存在，请更换名称！如需覆盖，请选中覆盖按钮！');
            return false;
        } else if ($cover == 3 && $db_isExist) {    // 用户选择删除原数据库
            $sql = "DROP DATABASE IF EXISTS `{$db_name}`";
            $del_db = $db_instance->execute($sql);
            if ($del_db === false) {
                $this->error($db_instance->getError());
                return false;
            }
        }
        // 创建数据库
        $sql = "CREATE DATABASE IF NOT EXISTS `{$db_name}` DEFAULT CHARACTER SET utf8";
        if (!$db_instance->execute($sql)) {
            $this->error($db_instance->getError());
            return false;
        }
        if (!$this->service->setConfig($db_config)) {
            $this->error('写如配置文件失败');
        } else {
            $this->success('数据库创建成功', url('install/index/step6'));
        }
        return true;
    }
    
    /**
     * AJAX(POST) - 超级管理员信息检测，建表
     */
    public function userInfoCheck() {
        // 来路页面检测
        if (session('step') != 6) {
            return false;
        }
        // 检测是否为AJAX提交
        if (!request()->isAjax()) {
            $this->error('提交方式有误!');
            return false;
        }
        // 表单信息验证
        $data = request()->post();
        $validate = validate('install');
        $checkInfo = $validate->scene('info')->check($data);
        if (!$checkInfo) {
            $this->error($validate->getError());
            return false;
        }
        // 替换SQL文件
        $replaceArray = [
            '[email]'    => $data['email'],
            '[username]' => $data['username'],
            '[password]' => password_hash($data['password'], PASSWORD_DEFAULT),
            '[salt]'     => $this->service->cmRound(6),
        ];
        // 写入SQL文件
        $db_config = session('db_config');
        if (!$this->service->createTables($db_config['prefix'], $replaceArray)) {
            $this->error('创建数据表失败!');
        } else {
            $this->success('创建数据表成功!', url('install/index/complete'));
        }
        return true;
    }
    
    /**
     * 不存在的URL全部跳转到首页
     */
    public function _empty() {
        $this->redirect('install/index/index');
        exit();
    }
}