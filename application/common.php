<?php
/**
 *  ==================================================================
 *        文 件 名: common.php
 *        概    要: 应用公共文件
 *        作    者: IT小强
 *        创建时间: 2017/9/27 20:26
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

if (!function_exists('table_exist')) {
    /**
     * 检查数据表是否存在
     * @param string $table - 表名
     * @return bool
     */
    function table_exist($table) {
        return true == db()->query("SHOW TABLES LIKE '{$table}'");
    }
}

if (!function_exists('array_serialize')) {
    
    /**
     * 数组序列化url字符串
     * @param $array - 数组（键值对）
     * @return string
     */
    function array_serialize($array) {
        $str = '';
        if (!is_array($array) || count($array) < 1) {
            return $str;
        }
        foreach ($array as $k => $v) {
            $str .= (empty($str)) ? '?' : '&';
            $str .= $k . '=' . $v;
        }
        return $str;
    }
}

if (!function_exists('download_file')) {
    /**
     * 文件下载
     * @param string - $fileName - 下载显示文件名
     * @param string - $filePath - 文件路径
     * @param string - $ext - 文件后缀名
     * @return bool
     */
    function download_file($fileName, $filePath, $ext = '') {
        set_time_limit(0);
        //用以解决中文不能显示出来的问题
        $fileName = iconv('utf-8', 'gb2312', $fileName);
        if (!empty($ext)) {
            $fileName = $fileName . '.' . $ext;
        }
        $file = realpath($filePath . DIRECTORY_SEPARATOR . $fileName);
        
        //首先要判断给定的文件存在与否
        if (!is_file($file)) {
            return false;
        }
        // 获取文件大小
        $fileSize = filesize($file);
        //下载文件需要用到的头
        header('Content-type: application/octet-stream');
        header('Accept-Ranges: bytes');
        header('Accept-Length:' . $fileSize);
        header('Content-Disposition: attachment; filename=' . $fileName);
        readfile($file);
        exit();
    }
}

if (!function_exists('underline_to_hump')) {
    
    /**
     * 下划线命名转驼峰命名
     * @param $str - 下划线命名字符串
     * @param $is_first - 是否为大驼峰（即首字母也大写）
     * @return mixed
     */
    function underline_to_hump($str, $is_first = false) {
        $str = preg_replace_callback('/([\-\_]+([a-z]{1}))/i', function ($matches) {
            return strtoupper($matches[2]);
        }, $str);
        if ($is_first) {
            $str = ucfirst($str);
        }
        return $str;
    }
}

if (!function_exists('hump_to_underline')) {
    
    /**
     * 驼峰命名转下划线命名
     * @param $str
     * @return mixed
     */
    function hump_to_underline($str) {
        $str = preg_replace_callback('/([A-Z]{1})/', function ($matches) {
            return '_' . strtolower($matches[0]);
        }, $str);
        $str = preg_replace('/^\_/', '', $str);
        return $str;
    }
}

if (!function_exists('check_password')) {
    /**
     * 检查密码是否正确
     * @param $password - 明文
     * @param $hash - 密文
     * @return bool
     */
    function check_password($password, $hash) {
        return password_verify($password, $hash);
    }
}

if (!function_exists('get_client_ip')) {
    
    /**
     * 获取客户端IP地址
     * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
     * @return mixed
     */
    function get_client_ip($type = 0, $adv = true) {
        return request()->ip($type, $adv);
    }
}

if (!function_exists('cm_explode')) {
    
    /**
     * 字符传按分隔符转为数组
     * @param $string - 指定需要分割的字符串
     * @param $delimiter - 分割符
     * @return array
     */
    function cm_explode($string, $delimiter = ',') {
        if (!$string || !is_string($string)) {
            return [];
        }
        if (!strpos($string, $delimiter)) {
            return ['0' => $string];
        }
        $tempArr = explode($delimiter, $string);
        $returnData = [];
        foreach ($tempArr as $k => $v) {
            $returnData[strval($k)] = $v;
        }
        return $returnData;
    }
}

if (!function_exists('cm_round')) {
    /**
     * 生产随机字符串
     * @param int $length - 指定生产字符串的长度
     * @param string $type - 指定生产字符串的类型（all-全部，num-纯数字，letter-纯字母）
     * @return null|string
     */
    function cm_round($length = 4, $type = 'all') {
        $str = '';
        $strUp = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $strLow = 'abcdefghijklmnopqrstuvwxyz';
        $number = '0123456789';
        switch ($type) {
            case 'num':
                $strPol = $number;
                break;
            case 'letter':
                $strPol = $strUp . $strLow;
                break;
            default:
                $strPol = $strUp . $number . $strLow;
        }
        $max = strlen($strPol) - 1;
        for ($i = 0; $i < $length; $i++) {
            $str .= $strPol[mt_rand(0, $max)];
        }
        return $str;
    }
}

if (!function_exists('cm_array_merge')) {
    
    /**
     * 数组合并，可传入多个数组
     * @param $array1 - 第一个参数必须，参数必须为数组
     * @return array
     */
    function cm_array_merge($array1) {
        $returnArray = [];
        $args = func_get_args();
        foreach ($args as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $sk => $sv) {
                    $returnArray[$sk] = $sv;
                }
            }
        }
        return $returnArray;
    }
}

if (!function_exists('format_array')) {
    /**
     * 格式化数组，可以指定数组的键和值
     * @param $array - 原始数组
     * @param $key - 键
     * @param $value - 值
     * @param $delimiter - 分隔符，二级数组元素为字符串时
     * @return array -返回格式化后的数组
     */
    function format_array($array, $key = 'id', $value = 'name', $delimiter = ':') {
        $returnArray = [];
        if (!$array || !is_array($array) || count($array) < 1) {
            return $returnArray;
        }
        foreach ($array as $item) {
            if (is_string($item) && strpos($item, $delimiter)) {
                $tempArr = explode($delimiter, $item);
                $newKey = $tempArr[0];
                $newValue = $tempArr[1];
                $returnArray[$newKey] = $newValue;
            } else if (isset($item[$key]) && isset($item[$value])) {
                $newKey = $item[$key];
                $newValue = $item[$value];
                $returnArray[$newKey] = $newValue;
            } else {
                break;
            }
        }
        return $returnArray;
    }
}

if (!function_exists('get_select_list')) {
    
    /**
     * 获取下拉列表数据
     * @param $data - 原始数组
     * @param string $key - value值
     * @param string $value - 名称
     * @param string $title - 是否添加标题
     * @return string
     */
    function get_select_list($data, $key = 'id', $value = 'name', $title = '') {
        $source = '[';
        $source .= empty($title) ? '' : '{value:0,text:\'' . $title . '\'}';
        if ($data && is_array($data) && count($data) >= 1) {
            foreach ($data as $vo) {
                $source .= ($source === '[') ? '' : ',';
                $source .= '{value:\'' . $vo[$key] . '\',text:\'' . $vo[$value] . '\'}';
            }
        }
        $source .= ']';
        return $source;
    }
}

if (!function_exists('get_array_data')) {
    /**
     * 获取数组下标对应值，不存在时返回指定的默认值
     * @param $name - 下标（键名）
     * @param $data - 原始数组
     * @param string $default - 指定默认值
     * @return mixed
     */
    function get_array_data($name, $data, $default = '') {
        $value = isset($data[$name]) ? $data[$name] : $default;
        return $value;
    }
}

if (!function_exists('hook')) {
    /**
     * 监听钩子
     * @param string $tag 标签名称
     * @param mixed $params 传入参数
     * @param bool $once 只获取一个有效返回值
     * @return mixed
     */
    function hook($tag, $params = null, $once = true) {
        return \think\facade\Hook::listen($tag, $params, $once);
    }
}

if (!function_exists('hook_add')) {
    
    /**
     * 动态添加行为扩展到某个标签
     * @param $tag - 标签名称
     * @param $behavior - 行为名称
     * @param bool $first - 是否放到开头执行
     */
    function hook_add($tag, $behavior, $first = false) {
        \think\facade\Hook::add($tag, $behavior, $first);
    }
}

if (!function_exists('print_script')) {
    
    /**
     * @param $name - 脚本名称（xxx.js/xxx.css）
     * @param bool $is_min - 是否有minify压缩
     * @param bool $is_check - 是否检测加载状态
     * @param $min_path - 压缩路径
     * @param $file_path - 源文件路径
     * @param $cache_time - 压缩文件失效时间
     * @return string - 返回要输出的脚本（js/css）
     */
    function print_script($name, $is_min = true, $is_check = false, $min_path = '', $file_path = '', $cache_time = 0) {
        $script = '';
        $check = ($is_check == false) ? true : boolval(config($is_check));
        if (!$check) {
            return $script;
        }
        $fileGroupName = preg_replace('/(.*?)((\.)(js|css))$/', '\\1_\\4', $name);
        $extension = preg_replace('/(.*?)((\.)(js|css))$/', '\\4', $name);
        if ($extension == 'js') {
            if ($is_min) {
                $min = minify($name, $min_path, $file_path, $cache_time);
                $script .= '<script type="text/javascript" src="' . $min . '"></script>';
            } else {
                $src = config('minify.' . $fileGroupName);
                if (is_array($src) && count($src) >= 1) {
                    foreach ($src as $v) {
                        $script .= '<script type="text/javascript" src="__STATIC__/' . $v . '"></script>';
                    }
                }
            }
        } else if ($extension = 'css') {
            if ($is_min) {
                $min = minify($name, $min_path, $file_path, $cache_time);
                $script .= '<link rel="stylesheet" type="text/css" href="' . $min . '">';
            } else {
                $src = config('minify.' . $fileGroupName);
                if (is_array($src) && count($src) >= 1) {
                    foreach ($src as $v) {
                        $script .= '<link rel="stylesheet" type="text/css" href="__STATIC__/' . $v . '">';
                    }
                }
            }
        }
        return $script;
    }
}

if (!function_exists('minify')) {
    /**
     * 压缩合并静态资源
     * @param $name - 压缩后的文件名称
     * @param $min_path - 压缩路径
     * @param $file_path - 源文件路径
     * @param $cache_time - 压缩文件失效时间
     * @return bool
     */
    function minify($name, $min_path = '', $file_path = '', $cache_time = 0) {
        set_time_limit(0);
        $filePath = empty($file_path) ? BASE_ROOT . PUBLIC_NAME . '/static/' : $file_path;
        $minPath = empty($min_path) ? config('minify.min_path') : $filePath . $min_path;
        $minUrl = empty($min_path) ? config('minify.min_url') : BASE_URL . 'static/' . $min_path;
        $config = [
            'file_path'  => $filePath,
            'min_path'   => $minPath,
            'min_url'    => $minUrl,
            'cache_time' => intval($cache_time),
        ];
        $fileGroupName = preg_replace('/(.*?)((\.)(js|css))$/', '\\1_\\4', $name);
        $file = config('minify.' . $fileGroupName);
        if (!$file) {
            return false;
        }
        $type = 'g';
        $min = \minify\Minify::setConfig($config)->min($name, $file, $type);
        return $min;
    }
}

if (!function_exists('add_action_log')) {
    /**
     * 记录行为日志，并执行该行为的规则
     * @param string $action 行为标识
     * @param string $model 触发行为的模型名
     * @param string $record_id 触发行为的记录id
     * @param int $user_id 执行行为的用户id
     * @param string $details 详情
     * @return bool|string
     */
    function add_action_log($action, $model, $record_id = '', $user_id = 0, $details = '') {
        $user_id = intval($user_id);
        if (strpos($action, '.')) {
            list($module, $action) = explode('.', $action);
        } else {
            $module = request()->module();
        }
        $where = [
            ['name', 'EQ', strtolower($module)]
        ];
        $mid = db('sys_module')->where($where)->value('id');
        // 查询行为,判断是否执行
        $where = [
            ['enable', 'EQ', 1],
            ['name', 'EQ', $action],
            ['mid', 'EQ', $mid],
        ];
        $action_info = db('sys_action')->where($where)->find();
        if (!$action_info) {
            return '该行为不存在或被被禁用';
        }
        
        // 插入行为日志
        $data = [
            'aid'   => $action_info['id'],
            'uid'   => $user_id,
            'aip'   => get_client_ip(),
            'model' => $model,
            'rid'   => $record_id,
            'atime' => time()
        ];
        
        // 解析日志规则,生成日志备注
        if (!empty(trim($action_info['log']))) {
            if (preg_match_all('/\[(\S+?)\]/', $action_info['log'], $match)) {
                $log = [
                    'user'    => $user_id,
                    'record'  => $record_id,
                    'model'   => $model,
                    'time'    => request()->time(),
                    'data'    => ['user' => $user_id, 'model' => $model, 'record' => $record_id, 'time' => time()],
                    'details' => $details
                ];
                
                $replace = [];
                foreach ($match[1] as $value) {
                    $param = explode('|', $value);
                    if (isset($param[1])) {
                        $replace[] = call_user_func($param[1], $log[$param[0]]);
                    } else {
                        $replace[] = $log[$param[0]];
                    }
                }
                
                $data['remark'] = str_replace($match[0], $replace, $action_info['log']);
            } else {
                $data['remark'] = $action_info['log'];
            }
        } else {
            // 未定义日志规则，记录操作url
            $data['remark'] = '操作url：' . request()->server('REQUEST_URI');
        }
        
        // 保存日志
        db('sys_log')->insert($data);
        
        if (!empty(trim($action_info['rule']))) {
            // 解析行为
            $rules = parse_action($action, $user_id);
            // 执行行为
            $res = execute_action($rules, $action_info['id'], $user_id);
            if (!$res) {
                return '执行行为失败';
            }
        }
        return true;
    }
}

if (!function_exists('parse_action')) {
    /**
     * 解析行为规则
     * 规则定义  table:$table|field:$field|condition:$condition|rule:$rule[|cycle:$cycle|max:$max][;......]
     * 规则字段解释：table->要操作的数据表，不需要加表前缀；
     *            field->要操作的字段；
     *            condition->操作的条件，目前支持字符串，默认变量{$self}为执行行为的用户
     *            rule->对字段进行的具体操作，目前支持四则混合运算，如：1+score*2/2-3
     *            cycle->执行周期，单位（小时），表示$cycle小时内最多执行$max次
     *            max->单个周期内的最大执行次数（$cycle和$max必须同时定义，否则无效）
     * 单个行为后可加 ； 连接其他规则
     * @param string $action 行为id或者name
     * @param int $self 替换规则里的变量为执行用户的id
     * @return boolean|array: false解析出错 ， 成功返回规则数组
     */
    function parse_action($action, $self) {
        // 参数支持id或者name
        if (is_numeric($action)) {
            $map = [['id', 'EQ', $action]];
        } else {
            $map = [['name', 'EQ', $action]];
        }
        
        // 查询行为信息
        $info = db('sys_action')->where($map)->find();
        if (!$info || $info['enable'] != 1) {
            return false;
        }
        
        // 解析规则:table:$table|field:$field|condition:$condition|rule:$rule[|cycle:$cycle|max:$max][;......]
        $rule = $info['rule'];
        $rule = str_replace('{$self}', $self, $rule);
        $rules = explode(';', $rule);
        $return = [];
        foreach ($rules as $key => &$rule) {
            $rule = explode('|', $rule);
            foreach ($rule as $k => $fields) {
                $field = empty($fields) ? array() : explode(':', $fields);
                if (!empty($field)) {
                    $return[$key][$field[0]] = $field[1];
                }
            }
            // cycle(检查周期)和max(周期内最大执行次数)必须同时存在，否则去掉这两个条件
            if (!isset($return[$key]['cycle']) || !isset($return[$key]['max'])) {
                unset($return[$key]['cycle'], $return[$key]['max']);
            }
        }
        
        return $return;
    }
}

if (!function_exists('execute_action')) {
    /**
     * 执行行为
     * @param array|bool $rules 解析后的规则数组
     * @param int $action_id 行为id
     * @param array $user_id 执行的用户id
     * @return boolean false 失败 ， true 成功
     */
    function execute_action($rules = false, $action_id = null, $user_id = null) {
        if (!$rules || empty($action_id) || empty($user_id)) {
            return false;
        }
        
        $return = true;
        foreach ($rules as $rule) {
            // 检查执行周期
            $map = ['aid' => $action_id, 'uid' => $user_id];
            $map['atime'] = ['gt', time() - intval($rule['cycle']) * 3600];
            $exec_count = db('sys_log')->where($map)->count();
            if ($exec_count > $rule['max']) {
                continue;
            }
            
            // 执行数据库操作
            $field = $rule['field'];
            $res = db($rule['table'])->where($rule['condition'])->setField($field, array('exp', $rule['rule']));
            
            if (!$res) {
                $return = false;
            }
        }
        return $return;
    }
}

if (!function_exists('is_mobile')) {
    
    /**
     * 检测设备是否为手机
     * @return bool
     */
    function is_mobile() {
        return request()->isMobile();
    }
}

if (!function_exists('get_file_path')) {
    
    /**
     * 获取附件文件路径
     * @param $id - 附件ID
     * @param string $value - 路径字段名称
     * @param string $default - 默认值
     * @param null $dbName - 数据表名称（不能前缀）
     * @return array|string
     */
    function get_file_path($id, $value = '', $default = '', $dbName = NULL) {
        $dbName = ($dbName == NULL) ? 'sys_attachment' : $dbName;
        $value = empty($value) ? 'path' : $value;
        
        if (!$id || empty($id)) {
            return $default;
        }
        if (is_string($id)) {
            $id = explode(',', $id);
        }
        if (!is_array($id) || count($id) < 1) {
            return $default;
        }
        $val = [];
        $db = db($dbName);
        foreach ($id as $v) {
            $temp = $db->where([['id', 'EQ', $v]])->value($value);
            $val[] = empty($temp) ? $default : $temp;
        }
        $count = count($val);
        if ($count < 1) {
            return $default;
        } else if ($count == 1) {
            return $val[0];
        } else {
            return $val;
        }
    }
}

if (!function_exists('get_controller_name')) {
    /**
     * 获取控制器名称
     * @param string $moduleName - 当前模块名称
     * @param string $odd - 是否为单模块
     * @return array
     */
    function get_controller_name($moduleName = 'admin', $odd = '') {
        $subPath = empty($odd) ? '' : $odd . '/';
        $path = APP_PATH . $moduleName . '/' . 'controller' . '/' . $subPath;
        $dir = \files\File::get_dirs($path);
        $returnData = [];
        if (!isset($dir['file']) || !is_array($dir['file']) || count($dir['file']) < 1) {
            return $returnData;
        }
        
        foreach ($dir['file'] as $k => $v) {
            $fileName = strtolower(str_replace('.php', '', $v));
            if ($fileName != 'base' && $fileName != 'common') {
                $subPath = str_replace('/', '-', $subPath);
                $tempArr = [
                    'value' => $moduleName . '-' . $subPath . str_replace('.php', '', $v),
                    'desc'  => $fileName
                ];
                $returnData[] = $tempArr;
            }
        }
        return $returnData;
    }
}

if (!function_exists('get_allow_data')) {
    /**
     * 获取允许字段列表
     * @param $field - 允许字段
     * @param $data - 原始数组
     * @return array
     */
    function get_allow_data($field, $data) {
        $returnData = [];
        if (!$data || !is_array($data) || count($data) < 1) {
            return $returnData;
        }
        if (is_string($field)) {
            $field = explode(',', $field);
        }
        if (!is_array($field) || count($field) < 1) {
            return $data;
        }
        foreach ($data as $k => $v) {
            if (in_array($k, $field)) {
                $returnData[$k] = $v;
            }
        }
        return $returnData;
    }
}

if (!function_exists('captcha_src')) {
    /**
     * 输出验证码图像
     * @param string $id
     * @return string
     */
    function captcha_src($id = '') {
        $captchaSrc = url('/captcha' . ($id ? "/{$id}" : ''));
        return $captchaSrc;
    }
}

if (!function_exists('get_config_level_one')) {
    /**
     * 获取一级配置
     * @param $name - 一级配置名称
     * @return mixed
     */
    function get_config_level_one($name) {
        return get_array_data($name, config(), []);
    }
}

if (!function_exists('get_plugin_class')) {
    /**
     * 获取插件类名
     * @param string $name - 插件名
     * @param bool $is_new - 是否实例化
     * @return mixed
     */
    function get_plugin_class($name, $is_new = true) {
        $plugin = 'plugins\\' . hump_to_underline($name) . '\\' . underline_to_hump($name, true);
        return ($is_new === true) ? (new $plugin(request(), app())) : $plugin;
    }
}

if (!function_exists('get_template_dir')) {
    
    /**
     * 获取指定文件夹下模板目录
     * @param $path - 路径
     * @param string $unset - 要剔除的目录名称
     * @return array
     */
    function get_template_dir($path, $unset = '') {
        if (is_string($unset)) {
            $unset = explode(',', $unset);
        }
        if (!is_array($unset)) {
            $unset = [];
        }
        $templateArr = \files\File::get_dirs($path);
        $templateList = [];
        if (isset($templateArr['dir']) && is_array($templateArr['dir'])) {
            foreach ($templateArr['dir'] as $v) {
                if ($v == '.' || $v == '..') {
                    continue;
                }
                if (!in_array($v, $unset)) {
                    $templateList[] = $v;
                }
            }
        }
        return $templateList;
    }
}

if (!function_exists('get_template_file')) {
    
    /**
     * 获取指定文件夹下模板页面
     * @param $path - 路径
     * @param string $unset - 要剔除的页面
     * @return array
     */
    function get_template_file($path, $unset = '') {
        if (is_string($unset)) {
            $unset = explode(',', $unset);
        }
        if (!is_array($unset)) {
            $unset = [];
        }
        $search = '.' . config('template.view_suffix');
        $templateArr = \files\File::get_dirs($path);
        $templateList = [];
        if (isset($templateArr['file']) && is_array($templateArr['file'])) {
            foreach ($templateArr['file'] as $v) {
                $fileName = str_replace($search, '', $v);
                if (!in_array($fileName, $unset)) {
                    $tempArr = ['name' => $fileName, 'all_name' => $v];
                    $templateList[] = $tempArr;
                }
            }
        }
        return $templateList;
    }
}
