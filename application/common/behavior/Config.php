<?php

/**
 *  ==================================================================
 *        文 件 名: Config.php
 *        概    要: 初始化配置信息行为
 *        作    者: IT小强
 *        创建时间: 2017/9/12 0:20
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\behavior;

/**
 * Class Config - 初始化配置信息行为
 * @package app\common\behavior
 */
class Config {
    
    /**
     * 应用初始化行为
     */
    public function appInit() {
        
        // 如果为安装模块直接返回
        if (defined('BIND_MODULE') && BIND_MODULE === 'install') {
            // 安装模块路由配置
            $this->installRoute();
            return true;
        }
        // 设置系统配置项
        $this->setAdminConfig();
        
        // 加载插件钩子
        $this->setPlugins();
        
        // 路由配置
        $this->route();
        
        // 设置模板替换变量
        $this->setViewReplaceStr();
        
        // 入口文件index.php强制显示
        $this->isShowIndex();
        
        // 强制跳转ssl
        $this->isHttps();
        
        return true;
    }
    
    /**
     * 设置模板替换变量
     */
    private function setViewReplaceStr() {
        $view_replace_str = [
            '__STATIC__'  => BASE_URL . 'static',
            '__ASSETS__'  => BASE_URL . 'static/assets',
            '__UPLOADS__' => BASE_URL . 'uploads',
            '___STATIC'   => '__STATIC__',
            '___UPLOADS'  => '__UPLOADS__',
            '___ASSETS'   => '__ASSETS__',
        ];
        config('view_replace_str', $view_replace_str);
    }
    
    /**
     * 加载系统设置
     */
    private function setAdminConfig() {
        $cacheName = 'system_config';
        if (cache($cacheName) === false) {
            $configWhere = ['enable' => 1];
            $configField = 'name,value,type';
            $configList = db('sys_config')->where($configWhere)->field($configField)->select();
            cache($cacheName, $configList, 0);
        } else {
            $configList = cache($cacheName);
        }
        foreach ($configList as $v) {
            $configType = $v['type'];
            $configName = $v['name'];
            $configValue = $v['value'];
            if ($configType == 'switch') {
                $configValue = ($configValue == 1) ? true : false;
            }
            config($configName, $configValue);
        }
    }
    
    /**
     * 入口文件index.php强制显示
     */
    private function isShowIndex() {
        $root_url = HIDE_APP ? BASE_URL : str_replace(PUBLIC_NAME . '/', '', BASE_URL);
        if (!config('hide_index')) {
            app('url')->root($root_url . 'index.php');
        } else {
            app('url')->root(preg_replace('/\/$/', '', $root_url));
        }
    }
    
    /**
     * 强制跳转ssl
     */
    private function isHttps() {
        // 当前域名
        $serverName = request()->domain();
        // 当前URl
        $requestUrl = request()->url();
        // HTTPS状态
        $ssl = request()->isSsl();
        // 当前页面完整路径
        $redirectUrl = 'http' . ($ssl ? 's' : '') . '://' . $serverName . $requestUrl;
        
        if (!defined('PAGE_URL')) {
            define('PAGE_URL', $redirectUrl);
        }
        if (config('is_ssl') == true && !$ssl) {
            $redirectUrl = 'https://' . $serverName . $requestUrl;
            echo '<script type="text/javascript">window.location.href="' . $redirectUrl . '";</script>';
            exit();
        }
    }
    
    /**
     * 全局路由配置
     */
    private function route() {
        $login = config('admin_login_name');
        $index = config('default_home_module');
        $rules = [
            // 前台默认首页
            'index' => $index,
            // 后台登录页
            $login  => 'user/login/index',
        ];
        app('route')->rules($rules);
    }
    
    /**
     * 安装模块路由配置
     */
    private function installRoute() {
        $rules = [
            'index'         => 'install/index/index',
            'step2'         => 'install/index/step2',
            'step3'         => 'install/index/step3',
            'step4'         => 'install/index/step4',
            'step5'         => 'install/index/step5',
            'step6'         => 'install/index/step6',
            'complete'      => 'install/index/complete',
            'dbInfoCheck'   => 'install/index/dbInfoCheck',
            'userInfoCheck' => 'install/index/userInfoCheck'
        ];
        app('route')->rules($rules);
    }
    
    /**
     * 注册插件钩子
     */
    private function setPlugins() {
        $pluginCacheName = 'system_plugin';
        if (cache($pluginCacheName) === false) {
            $where = ['enable' => 1];
            $field = 'name,hook';
            $pluginList = db('sys_plugin')
                ->where($where)
                ->field($field)
                ->order('order', 'asc')
                ->select();
            cache($pluginCacheName, $pluginList, 0);
        } else {
            $pluginList = cache($pluginCacheName);
        }
        $plugins = format_array($pluginList, 'name', 'hook');
        if (!$plugins || count($plugins) < 1) {
            return;
        }
        foreach ($plugins as $plugin => $hooks) {
            $hooks = explode(',', $hooks);
            foreach ($hooks as $hook) {
                hook_add($hook, 'plugins\\' . hump_to_underline($plugin) . '\\' . underline_to_hump($plugin, true));
            }
        }
    }
}