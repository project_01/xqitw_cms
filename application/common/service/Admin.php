<?php
/**
 *  ==================================================================
 *        文 件 名: Admin.php
 *        概    要: 后台服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/11 23:34
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\service;

use app\user\model\SysUser;
use authcode\AuthCode;
use tree\Tree;

/**
 * Class Admin - 后台服务层
 * @package app\common\service
 */
class Admin extends Base {
    
    /**
     * 设置模板继承路径
     * @param string $tempName - 模板名
     * @return array
     */
    public function setAdminViewPath($tempName = 'skcloud_layout') {
        $common_view = config('common_view');
        $common_view_layout = $common_view . $tempName;
        $assign = [
            'layout_base'        => $common_view_layout . '/layout_base.html',
            'layout_panel'       => $common_view_layout . '/layout_panel.html',
            'layout_tabs'        => $common_view_layout . '/layout_tabs.html',
            'layout_detail'      => $common_view_layout . '/layout_detail.html',
            'layout_left_menu'   => $common_view_layout . '/common/left_menu.html',
            'layout_right_aside' => $common_view_layout . '/common/right_aside.html',
            'layout_top_bar'     => $common_view_layout . '/common/top_bar.html',
            'layout_breadcrumbs' => $common_view_layout . '/common/breadcrumbs.html',
            'layout_setting'     => $common_view_layout . '/common/layout_setting.html',
            'layout_head_core'   => $common_view_layout . '/common/head_core.html',
        ];
        if (!defined('LAYOUT_PANEL')) {
            define('LAYOUT_PANEL', $assign['layout_panel']);
        }
        if (!defined('LAYOUT_TABS')) {
            define('LAYOUT_TABS', $assign['layout_tabs']);
        }
        if (!defined('LAYOUT_DETAIL')) {
            define('LAYOUT_DETAIL', $assign['layout_detail']);
        }
        return $assign;
    }
    
    /**
     * 获取当前登录用户昵称和头像用于菜单栏显示
     * @param int $uid 用户ID
     * @return array|bool
     */
    public function getUserSimpleInfo($uid) {
        $cacheName = 'nickname_' . $uid;
        if (cache($cacheName) === false) {
            $userInfo = SysUser::getUserSimpleInfo($uid);
            cache($cacheName, $userInfo, 0);
        }
        return cache($cacheName);
    }
    
    /**
     * 获取后台布局配置
     * @param int $uid - 当前登录用户ID
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getSysLayout($uid) {
        $db = db('sys_layout');
        $where = [
            ['uid', 'EQ', $uid]
        ];
        $sysLayout = $db->where($where)->field(true)->find();
        return $sysLayout ? $sysLayout : [];
    }
    
    /**
     * 获取要展示的模块
     * @param int $limit - 指定获取的数量
     * @return mixed
     */
    public function getActiveModule($limit = 6) {
        $limit = intval($limit) < 1 ? 6 : $limit;
        $cacheName = 'system_module_limit_' . $limit;
        if (cache($cacheName) === false) {
            $db = db('sys_module');
            $system_module = $db->where(['show' => 1, 'enable' => 1])
                ->field('id,name,title,des,sub_name,ico')
                ->limit($limit)
                ->order('order', 'asc')
                ->select();
            cache($cacheName, $system_module, 0);
        }
        return cache($cacheName);
    }
    
    /**
     * 获取全站模块数量
     * @return mixed
     */
    public function getModuleNum() {
        $cacheName = 'system_module_num';
        if (cache($cacheName) === false) {
            $num = db('sys_module')->field('id')->count();
            cache($cacheName, $num, 0);
        }
        return cache($cacheName);
    }
    
    /**
     * 获取后台菜单数据
     * @param $rid - 当前用户角色ID
     * @param $defaultRid - 默认管理员角色ID
     * @param $uid - 当前用户ID
     * @param $defaultUid - 默认管理员ID
     * @param $mid - 当前模块ID
     * @param $controller - 当前控制器名，用于激活当前菜单
     * @param $action - 当前方法名，用于激活当前菜单
     * @return string
     */
    public function getMenuData($rid, $defaultRid, $uid, $defaultUid, $mid, $controller, $action) {
        $cacheName = 'system_admin_menu_' . $rid . '_' . $uid . '_' . $mid . '_' . $controller . '_' . $action;
        $powerModel = db('sys_power');
        if (cache($cacheName) === false) {
            $menuWhere = [
                ['enable', 'EQ', 1],
                ['show', 'EQ', 1],
                ['mid', 'EQ', $mid],
            ];
            $debug = config('app_debug');
            if ($debug == false) {
                $menuWhere[] = ['debug', 'NEQ', 1];
            }
            $menuField = 'id,pid,name,ico,mid,controller,action';
            $menu = $powerModel
                ->where($menuWhere)
                ->field($menuField)
                ->order('order', 'ASC')
                ->select();
            if (!$menu) {
                $menu = [];
            } else {
                $roleModel = db('sys_role');
                foreach ($menu as $k => $v) {
                    $_controller = explode('-', strtolower($v['controller']));
                    $_controller = isset($_controller[2]) ? $_controller[1] . '.' . $_controller[2] : $_controller[1];
                    $menu[$k]['controller'] = $_controller;
                    $check = $this->checkMenuPower($uid, $defaultUid, $rid, $defaultRid, $v, $roleModel, $powerModel);
                    if (!$check) {
                        unset($menu[$k]);
                    }
                }
                $treeConfig = ['title' => 'name'];
                $menu = Tree::config($treeConfig)->toLayer($menu);
            }
            cache($cacheName, $menu, 0);
        }
        return cache($cacheName);
    }
    
    /**
     * 页面刷新同时刷新登录信息
     */
    public function loginLifeTime() {
        /* 获取登录超时时间 */
        $lifeTime = intval(config('session_life_time'));
        /* 获取session解密后的数组*/
        $admin_session = AuthCode::decrypt(session('admin'));
        /* 设置session */
        session('admin', AuthCode::encrypt($admin_session, $lifeTime));
    }
    
    /**
     * 检查当前角色是否拥有指定菜单的权限
     * @param $uid - 当前用户ID
     * @param $defaultUid -  默认超级管理员用户ID
     * @param $rid - 当前角色ID
     * @param $defaultRid - 默认超级管理员角色ID
     * @param $menu - 当前菜单信息
     * @param $roleModel - 实例化角色表
     * @param $powerModel - 实例化权限表
     * @return bool
     */
    protected function checkMenuPower($uid, $defaultUid, $rid, $defaultRid, $menu, $roleModel, $powerModel) {
        if ($rid == $defaultRid && $uid == $defaultUid) {
            return true;
        }
        $power = $roleModel->where([['id', 'EQ', $rid]])->value('power');
        if (!$power) {
            return false;
        }
        $powerArr = explode(',', $power);
        $checkWhere = [
            ['mid', 'EQ', intval($menu['mid'])],
            ['controller', 'EQ', strtolower($menu['controller'])],
            ['action', 'EQ', strtolower($menu['action'])]
        ];
        $id = $powerModel->where($checkWhere)->value('id');
        if (!$id) {
            return false;
        } else if (!in_array($id, $powerArr)) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * 通过当前模块、控制器、方法，匹配对应的权限ID
     * @param $controller - 控制器名称
     * @param $action - 方法名称
     * @param int $mid - 模块ID
     * @param string $module - 模块名称
     * @return bool | int
     */
    public function getPowerId($controller, $action, $mid = 1, $module = 'admin') {
        if (strpos($controller, '.')) {
            $controllerArr = explode('.', $controller);
            $controller = strtolower($controllerArr[0]) . '-' . ucfirst($controllerArr[1]);
        }
        $controller = $module . '-' . $controller;
        $where = [
            ['mid', 'EQ', $mid],
            ['controller', 'EQ', $controller],
            ['action', 'EQ', $action]
        ];
        $debug = config('app_debug');
        if ($debug == false) {
            $where[] = ['debug', 'NEQ', 1];
        }
        $db = db('sys_power');
        $info = $db->where($where)->field('id,enable,debug')->find();
        return !$info ? false : $info;
    }
}