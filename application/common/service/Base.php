<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 服务层基类
 *        作    者: IT小强
 *        创建时间: 2017/9/15 15:00
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\service;

use ueditor\UEditor;

/**
 * Class Base - 服务层基类
 * @package app\common\service
 */
class Base {
    
    /**
     * @var array - 树形类配置
     */
    protected $treeConfig = [
        'id'    => 'id',    // id名称
        'pid'   => 'pid',   // pid名称
        'title' => 'name', // 标题名称
        'child' => 'child', // 子元素键名
        'html'  => ' ┣ ',   // 层级标记
        'step'  => 6,       // 层级步进数量
    ];
    
    /**
     * @var string - 返回信息
     */
    protected $message = '';
    
    /**
     * 获取反馈信息
     * @return string
     */
    public function getMsg() {
        return $this->message;
    }
    
    /**
     * 设置模板继承路径
     * @param string $tempName - 模板名
     * @return array
     */
    public function setBaseViewPath($tempName = 'skcloud_layout') {
        $common_view = config('common_view');
        $common_view_layout = $common_view . $tempName;
        $assign = [
            'layout_head_core' => $common_view_layout . '/common/head_core.html',
        ];
        return $assign;
    }
    
    /**
     * 强制使用url伪静态后缀
     * @param $urlHtmlSuffix - 当前后缀
     * @return bool
     */
    public function urlHtmlSuffixCheck($urlHtmlSuffix) {
        $requestUrl = request()->url();
        $baseUrl = HIDE_APP ? BASE_URL : str_replace(PUBLIC_NAME . '/', '', BASE_URL);
        if ($requestUrl === $baseUrl) {
            return true;
        }
        $allow = config('url_html_suffix');
        if ($allow === false) {
            if (strpos(PAGE_URL, '.?') >= 10
                || substr(PAGE_URL, -1, 1) === '.'
                || !empty($urlHtmlSuffix)
            ) {
                $this->message = '不允许使用伪静态后缀名';
                return false;
            }
            return true;
        }
        if (!$urlHtmlSuffix || empty($urlHtmlSuffix)) {
            $this->message = '伪静态后缀名不能为空';
            return false;
        }
        $allow = explode('|', config('url_html_suffix'));
        if (is_array($allow) && count($allow) >= 1 && !in_array($urlHtmlSuffix, $allow)) {
            $this->message = '伪静态后缀名错误';
            return false;
        }
        return true;
    }
    
    /**
     * 检查当前模块是否启用
     * @param string $module - 模块名称，为空时自动获取
     * @param int $mid - 模块ID，为0时自动获取
     * @return bool - 返回值为布尔
     */
    public function moduleOpenCheck($module = '', $mid = 0) {
        /* 获取当前模块名称 */
        $module = strtolower(empty($module) ? request()->module() : $module);
        
        if ($module === 'index' || $module === 'install') {
            return true;
        }
        /* 获取当前模块id */
        $where = [
            ['name', 'EQ', $module],
            ['enable', 'EQ', 1],
        ];
        $mid = ($mid < 1) ? intval(db('sys_module')->where($where)->value('id')) : $mid;
        if ($mid < 1) {
            $this->message = '模块未启用';
            return false;
        }
        return true;
    }
    
    /**
     * 检查静态页面是否存在
     * @param bool $is_bool - 是否返回布尔值
     * @param string $param - 额外的参数
     * @param int $cacheTime - 缓存时间（秒，0表示永久缓存）
     * @return array|bool - 返回信息数组/或者布尔值
     */
    public function checkCache($is_bool = true, $param = '', $cacheTime = 0) {
        $module = request()->module();
        $controller = request()->controller();
        $action = request()->action();
        if (!empty($param)) {
            $action .= '_' . strval($param);
        }
        $path = config('html_cache_path') . strtolower($module) . '/' . strtolower($controller) . '/';
        $file = $path . strtolower($action) . '.html';
        $code = (is_file($file) && ($cacheTime == 0 || (time() - filemtime($file) < $cacheTime))) ? 1 : 0;
        if ($is_bool === true) {
            return boolval($code);
        }
        return ['path' => $path, 'file' => $file, 'code' => $code];
    }
    
    /**
     * HTML页面压缩
     * @param $string - 原始HTML页面内容
     * @return string - 压缩后的页面内容
     */
    public function compressHtml($string) {
        $pattern = '/<pre.*?>.*?<\/pre.*?>/si';
        $check = preg_match($pattern, $string, $match);
        if (!$check) {
            return $this->pregCompress($string);
        }
        $end = '<pre>compress</pre>';
        $string = $string . $end;
        $pattern = "/(.*?)(<pre.*?\>.*?<\/pre.*?>)/si";
        $string = preg_replace_callback($pattern, function ($matches) {
            $matches[1] = $this->pregCompress($matches[1]);
            return $matches[1] . $matches[2];
        }, $string);
        $string = str_replace($end, '', $string);
        return $string;
    }
    
    /**
     * 利用正则压缩页面
     * @param $string
     * @return mixed
     */
    protected function pregCompress($string) {
        // 清除单行注释
        $string = preg_replace('/[^\:]\/\/.*/', ' ', $string);
        // 清除多行注释
        $string = preg_replace('/\/\*.*?\*\//s', ' ', $string);
        // 清除HTML注释
        $string = preg_replace('/<!--.*?-->/s', ' ', $string);
        // 清除换行符和制表符
        $string = str_replace(["\r\n", "\n", "\t"], [' ', ' ', ' '], $string);
        // 清除多余的空白
        $string = preg_replace('/[\s]+/s', ' ', $string);
        return $string;
    }
    
    /**
     * 缓存生成静态页面
     * @param string $data - 待缓存的元素HTML
     * @param array $cache - 缓存文件信息
     * @param bool $isCompress - 是否压缩HTML页面
     * @return string - 返回缓存好的页面
     */
    public function cacheHtml($data, $cache, $isCompress = true) {
        if (isset($cache['code']) && $cache['code'] === 1) {
            return file_get_contents($cache['file']);
        }
        if (!is_dir($cache['path'])) {
            mkdir($cache['path'], 0777, true);
        }
        if ($isCompress === true) {
            $data = $this->compressHtml($data);
        }
        file_put_contents($cache['file'], $data);
        return file_get_contents($cache['file']);
    }
    
    /**
     * 解析搜索条件，拼装where数组
     * @param array $search - 表单提交的搜索项
     * @param string $allowSearchField - 允许搜索的字段
     * @param array $config - 字段配置
     * @return array
     */
    public function getSearchParams($search = [], $allowSearchField = '', $config = []) {
        $defaultConfig = [
            'like' => ['name', 'en_name', 'describe', 'nickname', 'n_name'],
            'in'   => ['status', 'pid', 'mid'],
        ];
        $config = array_merge($defaultConfig, $config);
        
        if (!$search || !is_array($search) || count($search) < 1) {
            return [];
        }
        if (!is_array($allowSearchField)) {
            if (is_string($allowSearchField) && !empty($allowSearchField)) {
                $allowSearchField = explode(',', $allowSearchField);
            } else {
                $allowSearchField = [];
            }
        }
        $where = [];
        foreach ($search as $item => $value) {
            $valueCheck = ($value === '' || $value === '_search_get_all' || $value === false || $value === NULL);
            $allowCheck = (count($allowSearchField) < 1) ? false : !in_array($item, $allowSearchField);
            if ($valueCheck || $allowCheck) {
                continue;
            } elseif (in_array($item, $config['like'])) {
                $where[] = [$item, 'LIKE', '%' . $value . '%'];
            } else if (in_array($item, $config['in'])) {
                if (is_string($value)) {
                    $value = explode(',', $value);
                }
                if (is_array($value) && count($value) >= 1) {
                    $where[] = [$item, 'IN', $value];
                }
            } else if (preg_match('/^(.*?)_start$/', $item, $match)) {
                $startTime = $value ? strtotime($value) : 0;
                $where[] = [$match[1], 'GT', $startTime];
            } else if (preg_match('/^(.*?)_end$/', $item, $match)) {
                $endTime = $value ? strtotime($value) : time();
                if (isset($where[$match[1]][1])) {
                    $startTime = $where[$match[1]][1];
                    $where[] = [$match[1], 'BETWEEN', [$startTime, $endTime]];
                } else {
                    $where[] = [$match[1], 'LT', $endTime];
                }
            } else {
                $where[] = [$item, 'EQ', $value];
            }
        }
        return $where;
    }
    
    /**
     * 单文件上传
     * @param string $img_name - input name
     * @param array $config - 上传配置
     * @return bool|array
     */
    public function upload($img_name, $config = []) {
        $img_name = strip_tags(strval($img_name));
        if (!$img_name) {
            $this->message = '参数错误';
            return false;
        }
        $file = request()->file($img_name);
        if (!$file) {
            $this->message = '未获取到图片';
            return false;
        }
        $pathName = 'uploads';
        $path = BASE_ROOT . PUBLIC_NAME . DIRECTORY_SEPARATOR . $pathName;
        $info = $file->move($path);
        if (!$info) {
            $this->message = $file->getError();
            return false;
        }
        $saveName = str_replace('\\', '/', $info->getSaveName());
        $path = '__UPLOADS__' . '/' . $saveName;
        $oldInfo = $file->getInfo();
        $id = $this->addFileInfo($oldInfo, $path, $path, 'local');
        if ($id < 1) {
            @unlink($path . DIRECTORY_SEPARATOR . $saveName);
            $this->message = '文件上传失败';
            return false;
        }
        return [
            'code'     => 1,
            'msg'      => '上传成功',
            'original' => get_array_data('name', $oldInfo),
            'title'    => $saveName,
            'type'     => strtolower(pathinfo($saveName, PATHINFO_EXTENSION)),
            'size'     => get_array_data('size', $oldInfo),
            'id'       => $id,
            'path'     => $path
        ];
    }
    
    /**
     * WebUpload 多图上传接口
     * @param array $config - 上传配置
     * @return array|bool
     */
    public function webUpload($config = []) {
        $upload = $this->upload('file', $config);
        if (!$upload) {
            abort(403, $this->message);
            return false;
        }
        return $upload;
    }
    
    /**
     * UEditor 请求接口
     */
    public function ue() {
        set_time_limit(0);
        $ueAction = request()->get('action');
        $ueCallback = request()->get('callback');
        $ueConfig = get_config_level_one('ueditor');
        /* 判断请求类型 */
        switch ($ueAction) {
            case 'config':
                $result = $this->getReturnUeConfig($ueConfig);
                break;
            /* 上传图片 */
            case 'uploadimage':
                /* 上传涂鸦 */
            case 'uploadscrawl':
                /* 上传视频 */
            case 'uploadvideo':
                /* 上传文件 */
            case 'uploadfile':
                $result = $this->ueUpload();
                break;
            /* 列出图片 */
            case 'listimage':
                /* 列出文件 */
            case 'listfile':
                $result = $this->ueList();
                break;
            /* 抓取远程文件 */
            case 'catchimage':
                $result = $this->ueCrawler();
                break;
            default:
                $result = ['state' => '请求无效'];
                break;
        }
        
        /* 返回请求结果 */
        if (empty($ueCallback)) {
            return json_encode($result);
        } else if (preg_match("/^[\w_]+$/", $ueCallback)) {
            return htmlspecialchars($ueCallback) . '(' . $result . ')';
        } else {
            return json_encode(['state' => 'callback参数不合法']);
        }
    }
    
    /**
     * UEditor上传
     * @return array
     */
    protected function ueUpload() {
        $ueAction = request()->get('action');
        $ueConfig = get_config_level_one('ueditor');
        switch ($ueAction) {
            case 'uploadimage':
                $fieldName = $ueConfig['image_field_name'];
                break;
            case 'uploadscrawl':
                $fieldName = $ueConfig['scrawl_field_name'];
                break;
            case 'uploadvideo':
                $fieldName = $ueConfig['video_field_name'];
                break;
            case 'uploadfile':
                $fieldName = $ueConfig['file_field_name'];
                break;
            default:
                $fieldName = $ueConfig['file_field_name'];
                break;
        }
        $upload = $this->upload($fieldName);
        $code = get_array_data('code', $upload, 0);
        $url = get_array_data('path', $upload, '');
        return [
            "state"    => $code ? 'SUCCESS' : $this->message,
            "url"      => str_replace('__UPLOADS__', BASE_URL . 'uploads', $url),
            "title"    => get_array_data('title', $upload, ''),
            "original" => get_array_data('original', $upload, ''),
            "type"     => '.' . get_array_data('type', $upload, 'png'),
            "size"     => get_array_data('size', $upload, 0),
        ];
    }
    
    /**
     * UEditor 文件列表
     * @return array
     */
    protected function ueList() {
        $ueAction = request()->get('action');
        $ueConfig = get_config_level_one('ueditor');
        switch ($ueAction) {
            /* 列出文件 */
            case 'listfile':
                $allowFiles = $ueConfig['file_manager_allow_files'];
                $listSize = $ueConfig['file_manager_list_size'];
                $path = $ueConfig['file_manager_list_path'];
                break;
            /* 列出图片 */
            case 'listimage':
            default:
                $allowFiles = $ueConfig['image_manager_allow_files'];
                $listSize = $ueConfig['image_manager_list_size'];
                $path = $ueConfig['image_manager_list_path'];
                break;
        }
        $allowFiles = substr(str_replace('.', '|', join('', $allowFiles)), 1);
        /* 获取参数 */
        $size = isset($_GET['size']) ? htmlspecialchars($_GET['size']) : $listSize;
        $start = isset($_GET['start']) ? htmlspecialchars($_GET['start']) : 0;
        $end = $start + $size;
        
        /* 获取文件列表 */
        $path = request()->server('DOCUMENT_ROOT') . (substr($path, 0, 1) == "/" ? "" : "/") . $path;
        $files = $this->getFiles($path, $allowFiles);
        if (!count($files)) {
            return [
                "state" => "no match file",
                "list"  => array(),
                "start" => $start,
                "total" => count($files)
            ];
        }
        
        /* 获取指定范围的列表 */
        $len = count($files);
        for ($i = min($end, $len) - 1, $list = array(); $i < $len && $i >= 0 && $i >= $start; $i--) {
            $list[] = $files[$i];
        }
        /* 返回数据 */
        $result = [
            "state" => "SUCCESS",
            "list"  => $list,
            "start" => $start,
            "total" => count($files)
        ];
        return $result;
    }
    
    /**
     * UEditor 抓取远程文件
     * @return array
     */
    protected function ueCrawler() {
        $ueConfig = get_config_level_one('ueditor');
        /* 上传配置 */
        $config = [
            "pathFormat" => $ueConfig['catcher_path_format'],
            "maxSize"    => $ueConfig['catcher_max_size'],
            "allowFiles" => $ueConfig['catcher_allow_files'],
            "oriName"    => "remote.png"
        ];
        $fieldName = $ueConfig['catcher_field_name'];
        
        /* 抓取远程图片 */
        $list = [];
        if (isset($_POST[$fieldName])) {
            $source = $_POST[$fieldName];
        } else {
            $source = $_GET[$fieldName];
        }
        foreach ($source as $imgUrl) {
            $item = new UEditor($imgUrl, $config, "remote");
            $info = $item->getFileInfo();
            $temp = [
                "state"    => $info["state"],
                "url"      => $info["url"],
                "size"     => $info["size"],
                "title"    => htmlspecialchars($info["title"]),
                "original" => htmlspecialchars($info["original"]),
                "source"   => htmlspecialchars($imgUrl)
            ];
            $list[] = $temp;
        }
        return [
            'state' => count($list) ? 'SUCCESS' : 'ERROR',
            'list'  => $list
        ];
    }
    
    /**
     * 遍历获取目录下的指定类型的文件
     * @param $path
     * @param $allowFiles
     * @param array $files
     * @return array
     */
    protected function getFiles($path, $allowFiles, &$files = array()) {
        if (!is_dir($path)) return null;
        if (substr($path, strlen($path) - 1) != '/') $path .= '/';
        $handle = opendir($path);
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..') {
                $path2 = $path . $file;
                if (is_dir($path2)) {
                    $this->getfiles($path2, $allowFiles, $files);
                } else {
                    if (preg_match("/\.(" . $allowFiles . ")$/i", $file)) {
                        $files[] = [
                            'url'   => substr($path2, strlen(request()->server('DOCUMENT_ROOT'))),
                            'mtime' => filemtime($path2)
                        ];
                    }
                }
            }
        }
        return $files;
    }
    
    /**
     * 获取用于返回前端的配置信息
     * @param $config - 原始配置信息
     * @return array - 转换后的配置信息
     */
    protected function getReturnUeConfig($config) {
        if (!is_array($config) || count($config) < 1) {
            return [];
        }
        $re = [];
        foreach ($config as $k => $v) {
            $re[underline_to_hump($k)] = $v;
        }
        return $re;
    }
    
    /**
     * 上传文件信息写入数据表
     * @param $info - 文件元素信息
     * @param $path - 文件路径
     * @param string $thumb -缩略图路径
     * @param string $driver - 上传驱动
     * @param int $uid - 上传用户Id
     * @param null $dbName - 数据表名（不带前缀）
     * @return int|string - 文件Id
     */
    public function addFileInfo($info, $path, $thumb = '', $driver = 'local', $uid = 0, $dbName = NULL) {
        $dbName = ($dbName == NULL) ? 'sys_attachment' : $dbName;
        $db = db($dbName);
        $t = time();
        $data = [
            'uid'    => $uid,
            'name'   => get_array_data('name', $info),
            'path'   => $path,
            'thumb'  => $thumb,
            'module' => request()->module(),
            'ext'    => pathinfo($path, PATHINFO_EXTENSION),
            'size'   => get_array_data('size', $info),
            'driver' => $driver,
            'atime'  => $t,
            'utime'  => $t,
        ];
        $id = $db->insertGetId($data);
        if (!$id) {
            $this->message = '文件信息写入数据库失败';
            return false;
        }
        return intval($id);
    }
    
    /**
     * 保存Base64图像
     * @param $base64Img - 原始Base64图像
     * @param $savePath - 保存路径
     * @param $saveName -保存文件名
     * @return bool
     */
    public function saveBase64Img($base64Img, $savePath, $saveName) {
        if (empty($base64Img)) {
            $this->message = '请选择要上传的图片';
            return false;
        }
        if (!preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64Img, $result)) {
            $this->message = '图片参数错误';
            return false;
        }
        $search = $result[1];
        $extension = '.' . $result[2];
        /* 解码 base64 */
        $img = base64_decode(str_replace($search, '', $base64Img));
        /* 创建目录 */
        if (!is_dir($savePath)) {
            if (!mkdir($savePath, 0700, true)) {
                $this->message = '创建文件目录失败';
                return false;
            }
        }
        /* 保存图片 */
        $input = file_put_contents($savePath . $saveName . $extension, $img);
        if (!$input) {
            $this->message = '图片保存失败';
            return false;
        }
        $this->message = $extension;
        return true;
    }
}