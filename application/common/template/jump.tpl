<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?php
        $space = config('web_title_delimiter_space');
        $delimiter = config('web_title_delimiter');
        if ($space == 1) {
            $delimiter = ' ' . $delimiter . ' ';
        }
        $title = '跳转提示' . $delimiter . config('sys_name');
        $title .= ' V' . config('sys_version');
        echo $title;
        ?>
    </title>
    <!-- Font-awesome Stylesheet [ REQUIRED ]-->
    <link rel="stylesheet" type="text/css" href="__STATIC__/assets/font-awesome/css/font-awesome.min.css">

    <!-- Bootstrap Stylesheet [ REQUIRED ]-->
    <link rel="stylesheet" type="text/css" href="__STATIC__/assets/bootstrap/css/bootstrap.min.css">

    <!-- SKCloud Stylesheet [ REQUIRED ]-->
    <link rel="stylesheet" type="text/css" href="__STATIC__/assets/css/skcloud.min.css">
</head>
<body>
<div id="container" class="cls-container">

    <div class="cls-header">
        <div class="cls-brand">
            <a class="box-inline" href="{:config('sys_link')}">
                <span class="brand-title">
                    {:config('sys_name')}
                    <span class="text-thin"> V{:config('sys_version')}</span>
                </span>
            </a>
        </div>
    </div>

    <div class="cls-content">
        <?php switch ($code) { ?>
        <?php case 1: ?>
        <h1 class="error-code text-success"><i class="fa fa-check-circle"></i></h1>
        <p class="text-success text-semibold text-lg text-uppercase">
            <i class="fa fa-check-circle"></i>
            <?php echo(strip_tags($msg)); ?>
        </p>
        <?php break; ?>
        <?php case 0: ?>
        <h1 class="error-code text-danger"><i class="fa fa-times"></i></h1>
        <p class="text-danger text-semibold text-lg text-uppercase">
            <i class="fa fa-times"></i>
            <?php echo(strip_tags($msg)); ?>
        </p>
        <?php break; ?>
        <?php } ?>
        <div class="pad-btm text-muted">
            <p>
                页面正在自动
                <a id="href" href="<?php echo($url); ?>">跳转</a>
                等待时间：
                <b class="text-info" id="wait"><?php echo($wait); ?></b>
                秒
            </p>
        </div>
        <div class="row mar-ver">
            <form class="col-xs-12 col-sm-10 col-sm-offset-1"
                  method="get" action="{:url('admin/search/index')}">
                <input name="keyword" type="text"
                       placeholder="搜索..." class="form-control input-lg error-search">
            </form>
        </div>
        <hr class="new-section-sm bord-no">
        <div class="pad-top">
            <a class="btn btn-success" href="<?php echo($url); ?>">
                <i class="fa fa-rocket"></i>
                立即跳转
            </a>
            <button class="btn btn-danger" onclick="stop();">
                <i class="fa fa-ban"></i>
                停止跳转
            </button>
            <a class="btn btn-primary" href="{:url(config('default_admin_module'))}">
                <i class="fa fa-home"></i>
                回到首页
            </a>
        </div>
    </div>

    <p>
        <a href="{:config('sys_link')}">
            {:config('sys_copy')} All rights reserved.
        </a>
    </p>
</div>
<script type="text/javascript">
    (function () {
        var wait = document.getElementById('wait'),
            href = document.getElementById('href').href;
        var interval = setInterval(function () {
            var time = --wait.innerHTML;
            if (time <= 0) {
                location.href = href;
                clearInterval(interval);
            }
        }, 1000);
        /* 禁止跳转 */
        window.stop = function () {
            clearInterval(interval);
        }
    })();
</script>
</body>
</html>