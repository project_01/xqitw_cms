<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>500 - Error - 服务器错误</title>
    <style type="text/css">
        [class^="col-"]:not(.pad-no) {
            padding-left: 7.5px;
            padding-right: 7.5px;
        }

        .row {
            margin: 0 -7.5px;
        }

        .h1, .h2, .h3, h1, h2, h3 {
            margin-top: 20px;
            margin-bottom: 10px;
        }

        .col-xs-12 {
            width: 100%;
        }

        body {
            margin: 0;
            display: block;
            background-color: #ecf0f5;
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 13px;
            font-weight: normal;
            font-variant: normal;
            color: #758697;
            min-width: 290px;
            line-height: 1.42857143;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased !important;
        }

        #container {
            min-height: 100vh;
            height: auto;
            position: relative;
            min-width: 290px;
            overflow: hidden;
        }

        .cls-container {
            color: #647484;
            background-color: #ecf0f5;
            text-align: center;
        }

        .cls-header {
            position: relative;
            background-color: rgba(0, 0, 0, 0.03);
        }

        .cls-brand {
            display: inline-block;
            padding: 5px 0;
        }

        .brand-title {
            background-color: transparent;
            color: inherit;
            float: none;
            display: inline-block;
            line-height: 22px;
            font-size: 20px;
            font-weight: 600;
            padding: 5px;
            vertical-align: middle;
            width: auto;
        }

        .cls-content {
            padding: 50px 15px 15px;
            padding-top: 10vh;
            position: relative;
        }

        .cls-content .error-code {
            font-size: 120px;
            font-weight: 400;
            margin-bottom: 50px;
        }

        .text-info, a.text-info:hover, a.text-info:focus {
            color: #008fa1;
        }

        .text-5x, .text-4x, .text-5x, .text-2x, .text-lg, .text-sm, .text-xs {
            line-height: 1.25;
            font-size: 4em;
        }

        .text-semibold {
            font-weight: 600;
        }

        .text-main, a.text-main:hover, a.text-main:focus {
            color: #2b425b;
        }

        .text-uppercase {
            text-transform: uppercase;
        }

        p {
            margin: 0 0 10px;
        }

        .text-lg {
            font-size: 1.2em;
        }

        .pad-top {
            padding-top: 15px;
        }

        a {
            text-decoration: none;
            color: #758697;
            outline: 0;
        }

        .btn {
            display: inline-block;
            cursor: pointer;
            background-color: transparent;
            color: inherit;
            padding: 6px 12px;
            border-radius: 0;
            border: 1px solid transparent;
            font-size: 13px;
            line-height: 1.42857;
            vertical-align: middle;
            transition: all 0.25s;
        }

        .btn-primary-basic, .btn-primary, .btn-primary:focus, .btn-primary.disabled:hover, .btn-primary:disabled:hover, .btn-hover-primary:hover, .btn-hover-primary:active, .btn-hover-primary.active, .btn.btn-active-primary:active, .btn.btn-active-primary.active, .dropdown.open > .btn.btn-active-primary, .btn-group.open .dropdown-toggle.btn.btn-active-primary {
            background-color: #38a0f4;
            border-color: #42a5f5;
            color: #fff;
        }
        .text-thin {
            font-weight: 300;
        }
    </style>
</head>
<body>
<?php $config = include BASE_ROOT.'config'.DIRECTORY_SEPARATOR.'app.php'; ?>
<div id="container" class="cls-container">

    <div class="cls-header">
        <div class="cls-brand">
            <a class="box-inline" href="<?php echo $config['sys_link'];?>">
                <span class="brand-title">
                    <?php echo $config['sys_name'];?>
                    <span class="text-thin"> V<?php echo $config['sys_version'];?></span>
                </span>
            </a>
        </div>
    </div>

    <div class="cls-content">
        <h1 class="error-code text-info">500</h1>
        <p class="text-main text-semibold text-lg text-uppercase">
            <?php echo htmlentities($message); ?>
        </p>
        <div class="pad-top" style="overflow: hidden;height: auto;">
            <a class="btn btn-primary" href="<?php echo BASE_URL;?>">
                回到首页
            </a>
        </div>
    </div>
    <p class="pad-top">
        <a href="<?php echo $config['sys_link'];?>">
            <?php echo $config['sys_copy'];?> All rights reserved.
        </a>
    </p>
</div>
</body>
</html>