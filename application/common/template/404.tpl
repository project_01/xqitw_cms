<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 - Error - 页面不见鸟</title>
    <!-- Font-awesome Stylesheet [ REQUIRED ]-->
    <link rel="stylesheet" type="text/css" href="__STATIC__/assets/font-awesome/css/font-awesome.min.css">

    <!-- Bootstrap Stylesheet [ REQUIRED ]-->
    <link rel="stylesheet" type="text/css" href="__STATIC__/assets/bootstrap/css/bootstrap.min.css">

    <!-- SKCloud Stylesheet [ REQUIRED ]-->
    <link rel="stylesheet" type="text/css" href="__STATIC__/assets/css/skcloud.min.css">
</head>
<body>
<div id="container" class="cls-container">

    <div class="cls-header">
        <div class="cls-brand">
            <a class="box-inline" href="<{:config('sys_link')}>">
                <span class="brand-title">
                    <{:config('sys_name')}>
                    <span class="text-thin"> V<{:config('sys_version')}></span>
                </span>
            </a>
        </div>
    </div>

    <div class="cls-content">
        <h1 class="error-code text-info">404</h1>
        <p class="text-main text-semibold text-lg text-uppercase">
            您要查找的页面不存在，试试搜索吧！
        </p>
        <div class="row mar-ver">
            <form class="col-xs-12 col-sm-10 col-sm-offset-1"
                  method="get" action="<{:url('admin/search/index')}>">
                <input name="keyword" type="text"
                       placeholder="搜索..." class="form-control input-lg error-search">
            </form>
        </div>
        <div class="pad-top">
            <a class="btn btn-primary" href="<{:url(config('default_admin_module'))}>">
                <i class="fa fa-home"></i>回到首页
            </a>
        </div>
    </div>
    <p class="pad-top">
        <a href=" <{:config('sys_link')}>">
            <{:config('sys_copy')}> All rights reserved.
        </a>
    </p>
</div>
</body>
</html>

