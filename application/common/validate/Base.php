<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 验证器基类
 *        作    者: IT小强
 *        创建时间: 2017/9/13 17:58
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\validate;

use captcha\Captcha;
use think\Validate;

abstract class Base extends Validate {
    /**
     * @var string - 错误提示信息前缀
     */
    protected $errPreFix = '<i class="fa fa-close"></i> ';
    
    /**
     * 数据表字段唯一性验证
     * @param $value - 验证数据
     * @param $rule - 验证规则（数据表名称）
     * @param $data - 全部数据（数组）
     * @param $field - 字段名
     * @return bool
     */
    protected function uniqueCheck($value, $rule, $data, $field) {
        $model = db($rule);
        $pk = $model->getPk();
        $info = request()->param();
        $id = isset($info[$pk]) ? intval($info[$pk]) : 0;
        $where = [[$field, 'EQ', $value]];
        if ($id !== 0) {
            $where[] = [$pk, 'NEQ', $id];
        }
        $check = $model->where($where)->field($pk)->value($pk);
        return ($check >= 1) ? ':attribute已存在' : true;
    }
    
    protected function pkCheck($value, $rule, $data, $field) {
        $model = db($rule);
        $pk = $model->getPk();
        $where = [[$pk, 'EQ', $value]];
        $check = intval($model->where($where)->field($pk)->value($pk));
        return ($check >= 1) ? ':attribute不存在' : true;
    }
    
    
    /**
     * 验证码验证
     * @param $value - 验证数据
     * @param $rule - 验证规则
     * @return bool
     */
    protected function captcha($value, $rule) {
        $captcha = new Captcha(get_config_level_one('captcha'));
        return $captcha->check($value, $rule);
    }
    
    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError() {
        return str_replace(':errPreFix', $this->errPreFix, $this->error);
    }
}