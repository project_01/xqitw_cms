<?php
/**
 *  ==================================================================
 *        文 件 名: Home.php
 *        概    要: 前台控制器基类
 *        作    者: IT小强
 *        创建时间: 2017/9/11 23:32
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\controller;

/**
 * Class Home - 前台控制器基类
 * @package app\common\controller
 */
abstract class Home extends Base {
    
    /**
     * 初始化
     */
    protected function initialize() {
        parent::initialize();
        /* 检测站点是否开启 */
        $index_on_off = config('index_on_off');
        if (!$index_on_off) {
            $this->redirect('index/close/index');
            exit();
        }
        /* 设置当前模块模板路径 */
        $this->setViewPath();
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\common\service\Home();
    }
    
    /**
     * 加载模板输出
     * @param string $template - 模板文件名
     * @param bool|int $isCache - 是否生成静态页面 false表示不生成，当值为int时表示静态页面有效时间
     * @param array $cache - 检查页面缓存是否存在,数组信息
     * @param bool $isCompress - 是否压缩HTML页面
     * @param array $config 更多参数
     * @return mixed
     */
    protected function fetch($template = '', $isCache = false, $cache = [], $isCompress = true, $config = []) {
        // 模板根目录
        $viewPath = config('template.view_path');
        // 模板文件后缀名（html）
        $viewSuffix = config('template.view_suffix');
        // 当前控制器名
        $controller = hump_to_underline($this->request->controller());
        $controller = str_replace('.', DIRECTORY_SEPARATOR, $controller);
        // 当前方法名
        $action = hump_to_underline($this->request->action());
        // 拼装当前目标完整路径
        $template = empty($template) ? $action : $template;
        $template = $viewPath . $controller . DIRECTORY_SEPARATOR . $template . '.' . $viewSuffix;
        return parent::fetch($template, $isCache, $cache, $isCompress, $config);
    }
    
    /**
     * 设置当前模块模板路径
     */
    protected function setViewPath() {
        $module = request()->module();
        $templatePath = APP_PATH . $module . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR;
        $template = config('cms_template_name');
        $templateArr = array_values(get_template_dir($templatePath, 'admin'));
        if (!in_array($template, $templateArr)) {
            $template = 'default';
        }
        $viewPath = $templatePath . $template . DIRECTORY_SEPARATOR;
        config('template.view_path', $viewPath);
        $view_replace_str = config('view_replace_str');
        $view_replace_str['__' . strtoupper($template) . '__'] = BASE_URL . 'static/' . $template;
        config('view_replace_str', $view_replace_str);
    }
}