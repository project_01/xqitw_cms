<?php
/**
 *  ==================================================================
 *        文 件 名: Plugin.php
 *        概    要: 插件基类
 *        作    者: IT小强
 *        创建时间: 2017/9/21 15:26
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\controller;

use builder\KeFormBuilder;
use think\Controller;
use think\Exception;

/**
 * Class Plugin - 插件基类
 * @package plugins
 */
abstract class Plugin extends Controller {
    
    /**
     * @var string - 插件配置文件
     */
    public $pluginConfigFile = '';
    
    /**
     * @var string - 插件路径
     */
    protected $pluginPath = '';
    
    /**
     * @var string - 插件数据表名
     */
    protected $pluginDbName = 'sys_plugin';
    
    /**
     * @var string - 插件配置数据表名
     */
    protected $pluginConfigDbName = 'sys_plugincon';
    
    /**
     * 错误信息
     * @var string
     */
    protected $errorInfo = '';
    
    /**
     * 初始化
     */
    protected function initialize() {
        parent::initialize();
        // 获取插件配置信息
        $this->pluginPath = config('plugin_path') . hump_to_underline($this->getName()) . DIRECTORY_SEPARATOR;
        if (is_file($this->pluginPath . 'config.php')) {
            $this->pluginConfigFile = $this->pluginPath . 'config.php';
        }
    }
    
    /**
     * 获取插件名称
     * @return string
     */
    final protected function getName() {
        $class = get_class($this);
        return substr($class, strrpos($class, '\\') + 1);
    }
    
    /**
     * 渲染插件模板
     * @param string $template 模板或直接解析内容
     * @param array $vars 模板输出变量
     * @param array $replace 替换内容
     * @param array $config 模板参数
     * @throws Exception
     * @return mixed
     */
    protected function fetch($template = '', $vars = [], $replace = [], $config = []) {
        // 模板文件后缀名（html）
        $viewSuffix = config('template.view_suffix');
        // 当前方法名
        $action = hump_to_underline($this->request->action());
        // 拼装当前目标完整路径
        $template = empty($template) ? $action : $template;
        $template = $this->pluginPath . 'view' . DIRECTORY_SEPARATOR . $template . '.' . $viewSuffix;
        return parent::fetch($template, array_merge($this->getPluginConfig(), $vars), $replace, $config);
    }
    
    /**
     * 获取插件配置信息
     * @param string $name - 插件名
     * @return array
     */
    public function getPluginConfig($name = '') {
        $name = hump_to_underline(empty($name) ? ($this->getName()) : $name);
        // 如果插件已缓存，则冲缓存中获取配置信息
        $cacheName = 'system_plugin_' . $name;
        if (cache($cacheName) !== false) {
            return cache($cacheName);
        }
        // 如果插件已经安装，则从数据库获取差价配置
        $where = [['name', 'EQ', $name]];
        $id = db($this->pluginDbName)->where($where)->field('id')->value('id');
        $config = db($this->pluginConfigDbName)->where([['pid', 'EQ', $id]])->field(true)->select();
        if (!empty($config)) {
            $config = $this->getPluginConfigVar($config);
            cache($cacheName, $config);
            return $config;
        }
        // 无缓存且未安装时从自带文件中获取
        $config = $this->getPluginConfigVarByFile();
        if ($config) {
            $config = $this->getPluginConfigVar($config['var']);
            cache($cacheName, $config);
            return $config;
        }
        return [];
    }
    
    /**
     * 提取插件配置变量
     * @param array $var - 原始变量数组
     * @return array - 转换后的 键->值 数组
     */
    protected function getPluginConfigVar($var = []) {
        $config = [];
        if (!is_array($var) || count($var) < 1) {
            return $config;
        }
        foreach ($var as $k => $v) {
            $type = get_array_data('type', $v, false);
            $name = get_array_data('name', $v, false);
            if (!$type || !$name) {
                continue;
            }
            $value = get_array_data('value', $v, '');
            $config[$name] = $value;
        }
        return $config;
    }
    
    /**
     * 判断插件是否安装
     * @param string $name - 插件标识
     * @return bool
     */
    protected function isInstall($name = '') {
        $name = hump_to_underline(empty($name) ? ($this->getName()) : $name);
        $where = [['name', 'EQ', $name]];
        $isInstall = db($this->pluginDbName)->where($where)->value('id');
        return ($isInstall >= 1) ? true : false;
    }
    
    /**
     * 获取配置文件中变量
     * @return bool|array
     */
    protected function getPluginConfigVarByFile() {
        if (!is_file($this->pluginConfigFile)) {
            return false;
        }
        $config = include_once $this->pluginConfigFile;
        if ($config && isset($config['var']) && is_array($config['var']) && count($config['var']) >= 1) {
            return $config['var'];
        } else {
            return false;
        }
    }
    
    /**
     * 配置表单页面生成
     * @param string $url - 表单提交地址
     * @param string $successUrl - 提交成功跳转地址
     * @param null $data - 表单默认数据
     * @return string
     */
    public function getConfigForm($url, $successUrl, $data = NUll) {
        $formData = $this->getPluginConfigVarByFile();
        if (!$formData) {
            return '';
        }
        $submitBtn = '修改配置信息';
        $form = KeFormBuilder::makeForm($url, 2, $data)
            ->addItems($formData, true, $successUrl, $submitBtn);
        return $form;
    }
    
    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError() {
        return $this->errorInfo;
    }
    
    /**
     * 必须实现安装方法
     * @return mixed
     */
    abstract public function install();
    
    /**
     * 必须实现卸载方法
     * @return mixed
     */
    abstract public function uninstall();
}