<?php
/**
 *  ==================================================================
 *        文 件 名: Admin.php
 *        概    要: 后台控制器基类
 *        作    者: IT小强
 *        创建时间: 2017/9/11 23:32
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\controller;

use app\user\model\SysPower;
use authcode\AuthCode;

/**
 * Class Admin - 后台控制器基类
 * @package app\common\controller
 */
abstract class Admin extends Base {
    
    /**
     * @var \app\common\service\Admin - 服务层
     */
    protected $service = null;
    
    /**
     * @var - 默认超级管理员UID
     */
    protected $defaultUid;
    
    /**
     * @var - 默认超级管理员角色ID
     */
    protected $defaultRid;
    
    /**
     * @var - 当前登录管理员UID
     */
    protected $uid = '';
    
    /**
     * @var - 当前登录管理角色ID
     */
    protected $rid = '';
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = '';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = '';
    
    /**
     * @var string|bool - 新增、修改时的允许字段
     */
    protected $allowField = true;
    
    /**
     * @var array - 不允许被操作的记录主键
     */
    protected $disablePk = [];
    
    /**
     * 初始化
     */
    protected function initialize() {
        parent::initialize();
        /* 获取管理员信息 */
        $this->getAdminInfo();
        /* 管理员登录验证 */
        $this->adminLoginCheck();
        /* 权限检查 */
        if (!$this->powerCheck($this->mid, $this->controller, $this->action, $this->module)) {
            $this->error('<i class="fa fa-times"></i> 权限不足');
            exit();
        }
        /* 获取面包屑导航 */
        $this->getBreadcrumb($this->mid, $this->module, $this->controller, $this->action);
        /* 页面刷新同时刷新登录信息 */
        $this->service->loginLifeTime();
        $assign = [
            /* 当前模块 */
            'module'       => $this->module,
            /* 当前控制器 */
            'controller'   => $this->controller,
            /* 当前操作 */
            'action'       => $this->action,
            /* 获取后台菜单 */
            'menu_list'    => $this->service->getMenuData($this->rid, $this->defaultRid, $this->uid, $this->defaultUid, $this->mid, $this->controller, $this->action),
            /* 当前登录用户信息 */
            'userInfo'     => $this->service->getUserSimpleInfo($this->uid),
            /* 获取启用且展示的模块 */
            'activeModule' => $this->service->getActiveModule(config('active_module')),
            /* 获取模块数量 */
            'moduleNum'    => $this->service->getModuleNum(),
            /* 当前页链接 */
            'page_url'     => request()->url(),
            /* 获取后台布局配置 */
            'sys_layout'   => $this->service->getSysLayout($this->uid)
        ];
        // 设置后台模板文件变量
        $assign = array_merge($assign, $this->service->setAdminViewPath());
        $this->assign($assign);
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\common\service\Admin();
    }
    
    /**
     * 获取管理员信息
     */
    protected function getAdminInfo() {
        // 默认超级管理员uid
        $this->defaultUid = config('administrators_uid');
        // 默认超级管理员rid
        $this->defaultRid = config('administrators_rid');
        // 解密session内容
        $adminUserSession = AuthCode::decrypt(session('admin'));
        // 获取当前登录用户的uid
        $this->uid = isset($adminUserSession['uid']) ? $adminUserSession['uid'] : '';
        // 获取当前登录用户的rid
        $this->rid = isset($adminUserSession['rid']) ? $adminUserSession['rid'] : '';
    }
    
    /**
     * 管理员登录验证
     */
    protected function adminLoginCheck() {
        
        /* 判断是否登录，未登录时重定向到登录页 */
        if (!$this->uid || !$this->rid) {
            $url = request()->url();
            cookie('login_call_back_url', $url, 3600 * 24 * 15);
            if (!request()->isAjax()) {
                if (config('is_hiden_admin_login_path')) {
                    abort(404, '当前页面不是登录页面!');
                } else {
                    $this->redirect('user/login/index');
                }
            } else {
                $this->error('<i class="fa fa-times"></i> 您还没登录或者登录已过期');
            }
            exit();
        }
        
        /* 判断是否为管理员 如果为普通用户 重定向到前台首页 */
        if ($this->rid == config('member_rid')) {
            $this->redirect('index/index/index');
            exit();
        }
    }
    
    /**
     * 管理员权限检查
     * @param $mid - 当前模块id
     * @param $controller - 当前控制器名称
     * @param $action - 获取当前方法名称
     * @param $module - 获取当前模块名称
     * @return bool,验证通过返回true
     */
    protected function powerCheck($mid, $controller, $action, $module) {
        $mid = intval($mid);
        /* 匹配当前模块->控制器->方法所对应的权限ID */
        $powerInfo = $this->service->getPowerId($controller, $action, $mid, $module);
        $debug = intval(get_array_data('debug', $powerInfo, 1));
        if (config('app_debug') == false && $debug == 1) {
            // 非调试模式下debug权限不启用
            return false;
        }
        if ($this->uid == $this->defaultUid && $this->rid == $this->defaultRid) {
            // 超级管理员不验证权限
            return true;
        }
        if (!$powerInfo) {
            // 未查询到对应权限
            return false;
        }
        $enable = intval(get_array_data('enable', $powerInfo, 0));
        if ($enable === 2) {
            // 未启用的权限不进行检查
            return true;
        }
        /* 获取当前角色的权限列表 */
        $where = [['id', 'EQ', $this->rid]];
        $power = db('sys_role')->where([$where])->value('power');
        if (!$power) {
            // 未查询到该角色的权限列表
            return false;
        }
        $powerArr = explode(',', $power);
        $pid = intval(get_array_data('id', $powerInfo, 0));
        /* 判断权限ID 是否在权限列表内 */
        if (!in_array($pid, $powerArr)) {
            return false;
        }
        return true;
    }
    
    /**
     * 输入当前控制器名和方法名及图标，用于显示面包屑导航和title
     * @param $mid - 当前模块id
     * @param $module - 当前模块名
     * @param $controller - 当前控制器名称
     * @param $action - 获取当前方法名称
     */
    protected function getBreadcrumb($mid, $module, $controller, $action) {
        $moduleCacheName = 'breadcrumb_' . $mid;
        if (cache($moduleCacheName) === false) {
            $moduleName = db('sys_module')
                ->where([['id', 'EQ', $mid]])
                ->field('title,sub_name,ico')
                ->find();
            cache($moduleCacheName, $moduleName, 0);
        }
        $moduleName = cache($moduleCacheName);
        if ($moduleName) {
            $this->assign('moduleName', $moduleName['title']);
            $this->assign('moduleIco', $moduleName['ico']);
            $this->assign('sub_module_name', $moduleName['sub_name']);
        }
        
        $curMenuCacheName = 'breadcrumb_cur_menu_' . $mid . '_' . $controller . '_' . $action;
        if (cache($curMenuCacheName) === false) {
            $breadcrumb = SysPower::getCurMenu($mid, $module, $controller, $action);
            cache($curMenuCacheName, $breadcrumb, 0);
        }
        $breadcrumb = cache($curMenuCacheName);
        $title = self::$sysName . ' v' . self::$sysVersion;
        if ($breadcrumb) {
            if (isset($breadcrumb['controller']['name'])) {
                $title = $breadcrumb['controller']['name'] . self::$delimiter . $title;
                $controllerAssign = [
                    'controllerName' => $breadcrumb['controller']['name'],
                    'controllerIco'  => $breadcrumb['controller']['ico']
                ];
                $this->assign($controllerAssign);
            }
            if (isset($breadcrumb['action']['name'])) {
                $title = $breadcrumb['action']['name'] . self::$delimiter . $title;
                $actionAssign = [
                    'actionName' => $breadcrumb['action']['name'],
                    'actionIco'  => $breadcrumb['action']['ico']
                ];
                $this->assign($actionAssign);
            }
        }
        $this->assign('title', $title);
    }
    
    /**
     * 快速修改字段值
     * @param int $pk - 主键ID的值
     * @param string $name - 修改字段名
     * @param string $value - 修改字段值
     * @return bool|array|string
     */
    public function updateField($pk, $name, $value = '') {
        if (!$pk || !$name || !request()->isAjax()) {
            return false;
        }
        $data = [$name => $value];
        $validate = validate($this->validate);
        $check = $validate->scene($name)->check($data);
        if (!$check) {
            echo '错误：';
            $msg = strval($validate->getError());
            return strip_tags($msg);
        }
        if (count($this->disablePk) >= 1 && in_array($pk, $this->disablePk)) {
            $msg = '此项内容不允许被修改';
            echo '错误：';
            return $msg;
        }
        $where = [['id', 'EQ', $pk]];
        $update = db($this->db)->where($where)->field($name)->setField($name, $value);
        if (!$update) {
            $msg = '修改失败';
            echo '错误：';
            return $msg;
        }
        return ['code' => 1, 'msg' => '修改成功'];
    }
    
    /**
     * 后台表格专用分页方法
     * @param string $model - 要操作的数据表
     * @param array $where - 查询条件
     * @param string $field - 查询字段
     * @param int $offset - 偏移量
     * @param int $limit - 每页显示数量
     * @param string $sort - 排序字段
     * @param string $order - 排序类型（ASC/DESC）
     * @return array|bool
     */
    protected function getPaginate($model = '', $where = [], $field = '', $offset = 0, $limit = 0, $sort = 'id', $order = 'ASC') {
        $model = empty($model) ? $this->db : $model;
        $offset = intval($offset);
        $limit = intval($limit);
        $page = ($offset / $limit) + 1;
        $config = $config = ['page' => $page, 'list_rows' => $limit];
        $data = db($model)
            ->where($where)
            ->field($field)
            ->order($sort, $order)
            ->paginate($config);
        if (!$data) {
            return ['rows' => [], 'total' => 0];
        }
        $returnData = $data->getCollection()->toArray();
        if (!is_array($returnData) || count($returnData) < 1) {
            return ['rows' => [], 'total' => 0];
        }
        return ['rows' => $returnData, 'total' => $data->total()];
    }
}