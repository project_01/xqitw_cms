<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 公共模块公共控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/11 22:33
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\controller;

use think\Controller;

/**
 * Class Base - 公共模块公共控制器
 * @package app\common\controller
 */
abstract class Base extends Controller {
    /**
     * @var string - 系统名称
     */
    protected static $sysName = '';
    
    /**
     * @var string - 系统版本号
     */
    protected static $sysVersion = '';
    
    /**
     * @var string - 标题分隔符
     */
    protected static $delimiter = ' - ';
    
    /**
     * @var \app\common\service\Base - 服务层
     */
    protected $service = null;
    
    /**
     * @var string - 当前模块名
     */
    protected $module = 'index';
    
    /**
     * @var int - 当前模块ID
     */
    protected $mid = 0;
    
    /**
     * @var string - 当前控制器
     */
    protected $controller = 'Index';
    
    /**
     * @var string - 当前操作
     */
    protected $action = 'index';
    
    /**
     * @var string - 当前URL伪静态后缀
     */
    protected $urlHtmlSuffix = '';
    
    /**
     * @var array - 树形类配置
     */
    protected $treeConfig = [
        'id'    => 'id',    // id名称
        'pid'   => 'pid',   // pid名称
        'title' => 'name', // 标题名称
        'child' => 'child', // 子元素键名
        'html'  => ' ┣ ',   // 层级标记
        'step'  => 6,       // 层级步进数量
    ];
    
    /**
     * 初始化
     */
    protected function initialize() {
        
        parent::initialize();
        
        /* 导入服务层类 */
        $this->importService();
        
        /* 获取模块、控制器、操作信息 */
        $this->getRequestInfo();
        
        /* 检查当前URL后缀是否正确 */
        if (!$this->service->urlHtmlSuffixCheck($this->urlHtmlSuffix)) {
            abort(404, $this->service->getMsg());
            exit();
        }
        
        /* 检测当前模块是否启用 */
        if (!$this->service->moduleOpenCheck($this->module, $this->mid)) {
            abort(404, $this->service->getMsg());
            exit();
        }
        // 设置模板变量
        $this->assign($this->service->setBaseViewPath());
        /* 获取系统信息 */
        $this->getSysInfo();
    }
    
    /**
     * UEditor 请求接口
     * @return string
     */
    public function ue() {
        return $this->service->ue();
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\common\service\Base();
    }
    
    /**
     * 获取系统信息
     */
    protected function getSysInfo() {
        // 获取系统名称
        self::$sysName = config('sys_name');
        // 获取系统版本
        self::$sysVersion = config('sys_version');
        // 获取标题分隔符
        $isSpace = config('web_title_delimiter_space');
        $delimiter = config('web_title_delimiter');
        $delimiter = empty($delimiter) ? '-' : $delimiter;
        self::$delimiter = ($isSpace == true) ? (' ' . $delimiter . ' ') : $delimiter;
    }
    
    /**
     * 获取模块、控制器、操作信息
     */
    protected function getRequestInfo() {
        // 获取当前URL的访问后缀
        $this->urlHtmlSuffix = $this->request->ext();
        /* 获取当前控制器名称 */
        $this->controller = $this->request->controller();
        /* 获取当前方法名称 */
        $this->action = $this->request->action();
        /* 获取当前模块名称 */
        $this->module = $this->request->module();
        /* 获取当前模块id */
        if ($this->module !== 'index' && $this->module !== 'install') {
            $where = [
                ['name', 'EQ', $this->module],
                ['enable', 'EQ', 1]
            ];
            $this->mid = intval(db('sys_module')->where($where)->field('id')->value('id'));
        }
    }
    
    /**
     * 加载模板输出
     * @param string $template - 模板文件名
     * @param bool|int $isCache - 是否生成静态页面 false表示不生成，当值为int时表示静态页面有效时间
     * @param array $cache - 检查页面缓存是否存在,数组信息
     * @param bool $isCompress - 是否压缩HTML页面
     * @param array $config 更多参数
     * @return mixed
     */
    protected function fetch($template = '', $isCache = false, $cache = [], $isCompress = true, $config = []) {
        $vars = get_array_data('vars', $config, []);
        $replace = get_array_data('replace', $config, []);
        $fetchConfig = get_array_data('config', $config, []);
        if ($isCache !== false) {
            $fetch = '';
            if (!isset($cache['code']) || $cache['code'] === 0) {
                $fetch = parent::fetch($template, $vars, $replace, $fetchConfig);
            }
            return $this->service->cacheHtml($fetch, $cache, $isCompress);
        }
        if ($isCompress === true) {
            $fetch = parent::fetch($template, $vars, $replace, $fetchConfig);
            return $this->service->compressHtml($fetch);
        }
        return parent::fetch($template, $vars, $replace, $fetchConfig);
    }
}