<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 数据模型基类
 *        作    者: IT小强
 *        创建时间: 2017/9/12 11:55
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\common\model;

use think\Model;

/**
 * Class Base - 公共模块模型基类
 * @package app\common\model
 */
abstract class Base extends Model {
    // 定义时间戳字段名
    protected $autoWriteTimestamp = true;
    protected $createTime = 'atime';
    protected $updateTime = 'utime';
}