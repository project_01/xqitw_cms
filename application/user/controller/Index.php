<?php
/**
 *  ==================================================================
 *        文 件 名: Index.php
 *        概    要: 用户中心控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/15 15:38
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\controller;


use app\user\service\User;

/**
 * Class Index - 用户中心控制器
 * @package app\user\controller
 */
class Index extends Base {
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_user';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'user';
    
    /**
     * @var \app\user\service\User - 服务层
     */
    protected $service = null;
    
    /**
     * @var string - 允许修改的个人信息字段
     */
    private $allowUserInfo = 't_name,n_name,sex,age,birthday,qq,address,describe_subject,describe_content';
    
    /**
     * @var - 当前登录用户的信息数组
     */
    private $userInfo = [];
    
    /**
     * 初始化
     */
    protected function initialize() {
        parent::initialize();
        $this->userInfo = $this->service->getUserInfo(['sys_user.id' => $this->uid]);
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new User();
    }
    
    /**
     * 个人中心
     * @return mixed
     */
    public function index() {
        hook_add('admin_js', function () {
            echo print_script('table.js');
        });
        hook_add('admin_css', function () {
            echo print_script('table.css');
        });
        $assign = [
            'user_info'           => $this->userInfo,
            'psd_form'            => $this->service->getPasswordForm(),
            'info_form'           => $this->service->getUserInfoForm($this->userInfo),
            'add_bootstrap_table' => true
        ];
        $this->assign($assign);
        return $this->fetch();
    }
    
    /**
     *  AJAX(POST) 快速修改字段值
     * @param int $pk - 主键ID的值
     * @param string $name - 修改字段名
     * @param string $value - 修改字段值
     * @return bool|array|string
     */
    public function updateField($pk = 0, $name = '', $value = '') {
        $pk = $this->uid;
        $update = parent::updateField($pk, $name, $value);
        cache('nickname_' . $this->uid, null);
        return $update;
    }
    
    /**
     * AJAX(POST) 修改用户信息
     * @return bool
     */
    public function saveUserInfo() {
        if (!request()->isAjax()) {
            return false;
        }
        $data = request()->post();
        $data = get_allow_data($this->allowUserInfo, $data);
        $validate = validate($this->validate);
        $check = $validate->scene('save_user_info')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        $where = [['id', 'EQ', $this->uid]];
        if (isset($data['address']) && !empty($data['address'])) {
            $data['address'] = serialize($data['address']);
        }
        if (isset($data['birthday']) && !empty($data['birthday'])) {
            $data['birthday'] = strtotime($data['birthday']);
        }
        unset($data['id']);
        $update = db($this->db)->where($where)->field(true)->update($data);
        if (!$update) {
            $this->error('修改个人信息失败');
        }
        $cacheName = 'nickname_' . $this->uid;
        cache($cacheName, null);
        $this->success('修改个人信息成功', url('user/index/index'));
        return true;
    }
    
    /**
     * AJAX(POST) 修改用户密码
     * @return bool
     */
    public function savePassword() {
        if (!request()->isAjax()) {
            return false;
        }
        $data = request()->post();
        $validate = validate($this->validate);
        $check = $validate->scene('edit_password')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        if (!isset($data['old_password']) || !check_password($data['old_password'], $this->userInfo['password'])) {
            $this->error('原密码不正确');
            return false;
        }
        $password = password_hash($data['password'], PASSWORD_DEFAULT);
        $where = [['id', 'EQ', $this->uid]];
        $update = db($this->db)->where($where)->field('password')->setField('password', $password);
        if (!$update) {
            $this->error('修改密码失败');
            return false;
        }
        $this->success('修改密码成功,请重新登录', url('user/login/logout'));
        return true;
    }
    
    /**
     * AJAX (POST) 用户头像上传
     * @return bool
     */
    public function updateAvatar() {
        if (!request()->isAjax()) {
            $this->error('非法请求');
            return false;
        }
        // 获取参数
        $data = request()->post();
        if (!isset($data['action']) || $data['action'] !== 'avatar') {
            $this->error('提交参数错误');
            return false;
        }
        // url解码
        $img = isset($data['img']) ? urldecode($data['img']) : '';
        
        /* 设置头像保存路径 */
        $public = HIDE_APP ? '' : PUBLIC_NAME . '/';
        $tempDir = ROOT . $public . 'uploads/temp/';
        $uploadDir = ROOT . $public . 'uploads/avatars/';
        $saveName = 'avatar_' . time() . '_' . $this->uid;
        /* 保存图像 */
        $save = $this->service->saveBase64Img($img, $tempDir, $saveName);
        if (!$save) {
            $this->error($this->service->getMsg());
            return false;
        }
        $saveName = $saveName . $this->service->getMsg();
        /* 图像裁剪 */
        $thumb = $this->service->thumb($tempDir, $uploadDir, $saveName);
        if (!$thumb) {
            $this->error($this->service->getMsg());
            return false;
        }
        /* 删除临时文件 */
        $del = unlink($tempDir . $saveName);
        if (!$del) {
            $this->error('删除临时文件失败');
        }
        /* 删除用户旧头像 */
        $db = db($this->db);
        $oldImg = $db->where([['id', 'EQ', $this->uid]])->field('portrait')->value('portrait');
        if (!empty($oldImg)) {
            @unlink($uploadDir . $oldImg);
        }
        /* 更新数据库头像信息 */
        $where = [['id', 'EQ', $this->uid]];
        $update = db($this->db)->where($where)->field('portrait')->setField('portrait', $saveName);
        if (!$update) {
            @unlink($uploadDir . $saveName);
            $this->error('头像上传失败');
            return false;
        }
        cache('nickname_' . $this->uid, null);
        $this->success('头像上传成功');
        return true;
    }
}