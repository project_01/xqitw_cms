<?php
/**
 *  ==================================================================
 *        文 件 名: User.php
 *        概    要: 用户管理控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/21 11:48
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\controller;

/**
 * Class User - 用户管理控制器
 * @package app\user\controller
 */
class User extends Base {
    
    /**
     * @var \app\user\service\User - 服务层
     */
    protected $service = null;
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_user';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'user';
    
    /**
     * @var array - 不允许被操作的记录主键
     */
    protected $disablePk = [1];
    
    /**
     * @var string - 新增、修改时的允许字段
     */
    protected $allowField = 'password,username,rid,n_name';
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\user\service\User();
    }
    
    /**
     * 用户列表
     * @param string $sort
     * @param string $order
     * @param int $offset
     * @param int $limit
     * @param string $search
     * @return bool|mixed|\think\response\Json
     */
    public function index($sort = 'order', $order = 'desc', $offset = 0, $limit = 10, $search = '') {
        if (!request()->isAjax()) {
            $table = $this->service->getTable();
            $this->assign('panel', $table);
            return $this->fetch(LAYOUT_PANEL);
        }
        $where = [];
        $allowSearchField = '';
        $where = array_merge($where, $this->service->getSearchParams($search, $allowSearchField));
        $field = 'id,rid,n_name,add_time,last_time,email,show,enable,order';
        $list = $this->getPaginate($this->db, $where, $field, $offset, $limit, $sort, $order);
        return $list ? json($list) : false;
    }
    
    /**
     * 添加用户
     * @return bool|mixed
     */
    public function add() {
        if (!request()->isAjax()) {
            $form = $this->service->getForm(url('add'), url('index'));
            $assign = ['panel' => $form];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        $data = get_allow_data($this->allowField, request()->post());
        $validate = validate($this->validate);
        $check = $validate->scene('add')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        $data['add_time'] = time();
        $data['last_time'] = time();
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        $add = db($this->db)->field(true)->insertGetId($data);
        if (!$add) {
            $this->error('添加用户失败');
        }
        $this->success('添加用户成功', url('index'));
        return true;
    }
    
    /**
     * 编辑用户
     * @param int $id - 用户ID
     * @return bool|mixed
     */
    public function edit($id = 0) {
        $id = intval($id);
        if ($id == 0) {
            $this->redirect('add');
        }
        if (in_array($id, $this->disablePk)) {
            $this->error('该用户不允许被修改');
            return false;
        }
        $editUrl = url('edit', ['id' => $id]);
        if (!request()->isAjax()) {
            $where = [['id', 'EQ', $id]];
            $field = 'username,age,qq,rid,n_name,t_name,phone,qq,email,show,enable,order';
            $data = db($this->db)->where($where)->field($field)->find();
            
            $form = $this->service->getForm($editUrl, $editUrl, $data);
            $assign = ['panel' => $form];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        // Ajax修改用户信息
        $this->allowField = 'password,rid,n_name';
        $data = get_allow_data($this->allowField, request()->post());
        $validate = validate($this->validate);
        $check = $validate->scene('edit')->check($data);
        if (!$check) {
            $this->error($validate->getError());
            return false;
        }
        if (isset($data['password'])) {
            if (!empty($data['password'])) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            } else {
                unset($data['password']);
            }
        }
        $where = [['id', 'EQ', $id]];
        $update = db($this->db)->where($where)->field(true)->update($data);
        if (!$update) {
            $this->error('修改用户信息失败');
        }
        $this->success('修改用户信息成功', $editUrl);
        return true;
    }
    
    /**
     * AJAX(POST) - 删除用户
     * @return bool|\think\response\Json
     */
    public function delete() {
        if (!request()->isAjax()) {
            return false;
        }
        $data = request()->post();
        if (isset($data['data']) && is_array($data['data']) && count($data) >= 1) {
            $id = $data['data'];
        } else if (isset($data['id'])) {
            $id = [intval($data['id'])];
        } else {
            $this->error('参数错误');
            return false;
        }
        foreach ($id as $idValue) {
            if (in_array($idValue, $this->disablePk)) {
                $this->error('所选用户中包含不允许删除的用户');
                return false;
            }
        }
        $where = [['id', 'IN', $id]];
        $del = db($this->db)->where($where)->delete();
        if (!$del) {
            $this->error('用户删除失败');
        } else {
            $this->success('用户删除成功');
        }
        return true;
    }
}