<?php
/**
 *  ==================================================================
 *        文 件 名: Role.php
 *        概    要: 角色管理控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/21 11:15
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\controller;

/**
 * Class Role - 角色管理控制器
 * @package app\user\controller
 */
class Role extends Base {
    
    /**
     * @var \app\user\service\Role - 服务层
     */
    protected $service = null;
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_role';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'role';
    
    /**
     * @var string - 新增、修改时的允许字段
     */
    protected $allowField = 'name,describe,power,pid,order,show,enable';
    
    /**
     * @var array - 不允许被操作的记录主键
     */
    protected $disablePk = [1, 2];
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\user\service\Role();
    }
    
    /**
     * 角色列表
     * @param string $sort
     * @param string $order
     * @param int $offset
     * @param int $limit
     * @param string $search
     * @return bool|mixed|\think\response\Json
     */
    public function index($sort = 'order', $order = 'desc', $offset = 0, $limit = 10, $search = '') {
        if (!request()->isAjax()) {
            $table = $this->service->getTable($this->db);
            $this->assign('panel', $table);
            return $this->fetch(LAYOUT_PANEL);
        }
        $where = [];
        $allowSearchField = 'name,pid,atime_start,atime_end,utime_start,utime_end,describe';
        $where = array_merge($where, $this->service->getSearchParams($search, $allowSearchField));
        $field = 'id,pid,name,atime,utime,describe,show,enable,order';
        $list = $this->getPaginate($this->db, $where, $field, $offset, $limit, $sort, $order);
        return $list ? json($list) : false;
    }
    
    /**
     * 添加角色
     * @return bool|mixed
     */
    public function add() {
        if (!request()->isAjax()) {
            $form = $this->service->getForm(url('add'), url('index'));
            $assign = ['panel' => $form];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        $data = get_allow_data($this->allowField, request()->post());
        if (isset($data['power']) && is_array($data['power']) && count($data['power']) >= 1) {
            $data['power'] = join(',', $data['power']);
        }
        $validate = validate($this->validate);
        $check = $validate->scene('add')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        $add = db($this->db)->field($this->allowField)->insertGetId($data);
        if (!$add) {
            $this->error('添加角色失败');
        }
        $this->success('添加角色成功', url('index'));
        return true;
    }
    
    /**
     * 修改角色信息
     * @param int $id - 角色ID
     * @return bool|mixed
     */
    public function edit($id = 0) {
        $id = intval($id);
        if ($id == 0) {
            $this->redirect('add');
        }
        if (count($this->disablePk) >= 1 && in_array($id, $this->disablePk)) {
            $this->error('该角色不允许被修改');
        }
        $editUrl = url('edit', ['id' => $id]);
        if (!request()->isAjax()) {
            $where = [['id', 'EQ', $id]];
            $field = 'id,name,pid,describe,power,order,show,enable';
            $data = db($this->db)->where($where)->field($field)->find();
            $form = $this->service->getForm($editUrl, $editUrl, $data);
            $assign = ['panel' => $form];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        // Ajax修改角色
        $data = get_allow_data($this->allowField, request()->post());
        if (isset($data['power']) && is_array($data['power']) && count($data['power']) >= 1) {
            $data['power'] = join(',', $data['power']);
        }
        $validate = validate($this->validate);
        $check = $validate->scene('edit')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        $where = [['id', 'EQ', $id]];
        $update = db($this->db)->where($where)->field($this->allowField)->update($data);
        if (!$update) {
            $this->error('修改角色失败');
        }
        $this->success('修改角色成功', $editUrl);
        return true;
    }
    
    /**
     * AJAX(POST) - 删除角色
     * @return bool|\think\response\Json
     */
    public function delete() {
        if (!request()->isAjax()) {
            return false;
        }
        $data = request()->post();
        if (isset($data['data']) && is_array($data['data']) && count($data) >= 1) {
            $id = $data['data'];
        } else if (isset($data['id'])) {
            $id = [intval($data['id'])];
        } else {
            $this->error('参数错误');
            return false;
        }
        foreach ($id as $idValue) {
            if (in_array($idValue, $this->disablePk)) {
                $this->error('所选角色中包含不允许删除的角色');
                return false;
            }
            if (db('sys_user')->where([['rid', 'EQ', $idValue]])->field('id')->find()) {
                $this->error('所选角色中包含有用户，请先删除用户再操作');
                return false;
            }
        }
        $where = [['id', 'IN', $id]];
        $del = db($this->db)->where($where)->delete();
        if (!$del) {
            $this->error('角色删除失败');
        } else {
            $this->success('角色删除成功');
        }
        return true;
    }
}