<?php
/**
 *  ==================================================================
 *        文 件 名: Power.php
 *        概    要: 权限管理控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/16 16:30
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\controller;

use app\user\model\SysPower;
use builder\KeFormBuilder;
use files\File;
use tree\Tree;

/**
 * Class Power - 权限管理控制器
 * @package app\user\controller
 */
class Power extends Base {
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_power';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'power';
    
    /**
     * @var array - 不允许被操作的记录主键
     */
    protected $disablePk = [];
    
    /**
     * @var string - 权限缓存标识
     */
    private $system_power_index_cache = 'system_power_index';
    
    /**
     * @var string - 添加/修改时的允许字段
     */
    protected $allowField = 'name,pid,mid,ico,controller,action,order,type,show,enable,debug';
    
    /**
     * @var \app\user\service\Power - 服务层
     */
    protected $service = null;
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\user\service\Power();
    }
    
    /**
     * 权限列表
     * @param $mid - 模块ID
     * @param $loop - 分类层数
     * @return bool|mixed
     */
    public function index($mid = 1, $loop = 0) {
        // 获取模块ID
        $mid = intval($mid);
        if (!$mid) {
            $this->redirect('user/index/index');
        }
        // 获取权限显示层数
        $loop = intval($loop);
        $cacheName = $this->system_power_index_cache . '_' . $mid . '_' . $loop;
        if (cache($cacheName) === false) {
            $moduleWhere = [
                ['show', 'EQ', 1],
                ['enable', 'EQ', 1]
            ];
            $moduleField = 'title,ico,id';
            $module_list = db('sys_module')->where($moduleWhere)->field($moduleField)->select();
            $assign = [
                // 当前模块ID
                'this_mid'    => $mid,
                // 当前层数
                'this_loop'   => $loop,
                // 获取权限<ol></ol>列表
                'list'        => $this->service
                    ->createPowerOlList(SysPower::getAllPowerList(0, $mid, $loop), 'son'),
                // 获取模块列表
                'module_list' => $module_list
            ];
            cache($cacheName, $assign, 0);
        } else {
            $assign = cache($cacheName);
        }
        $this->assign($assign);
        return $this->fetch();
    }
    
    /**
     * 添加权限
     * @param int $pid - 父级权限ID
     * @param int $mid - 所属模块ID
     * @return mixed
     */
    public function add($pid = 0, $mid = 1) {
        if (!request()->isAjax()) {
            $form = $this->_getForm(url('add'), url('index'), intval($mid), intval($pid));
            $this->assign('panel', $form);
            return $this->fetch(LAYOUT_PANEL);
        }
        // Ajax添加权限操作
        $data = get_allow_data($this->allowField, request()->post());
        $validate = validate('power');
        $check = $validate->scene('add')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        $isWhere = [
            ['mid', 'EQ', $data['mid']],
            ['controller', 'EQ', $data['controller']],
            ['action', 'EQ', $data['action']]
        ];
        $db = db($this->db);
        $is_set = $db->where($isWhere)->field('id')->count();
        if ($is_set >= 1) {
            $this->error('该权限已经存在,请勿重复添加');
        }
        $add = $db->field($this->allowField)->insertGetId($data);
        if (!$add) {
            $this->error('添加权限失败');
        }
        $this->success('添加权限成功');
        return true;
    }
    
    /**
     * 修改权限
     * @param $id - 权限ID
     * @return mixed
     */
    public function edit($id) {
        $id = intval($id);
        if ($id == 0) {
            $this->redirect('add');
        }
        // 非Ajax请求时输入模板
        if (!request()->isAjax()) {
            $where = [['id', 'EQ', $id]];
            $field = 'id,pid,mid,ico,name,controller,action,order,type,show,enable,debug';
            $data = db($this->db)->where($where)->field($field)->find();
            $url = url('edit', ['id' => $id]);
            $form = $this->_getForm($url, $url, 1, 0, $data);
            $this->assign('panel', $form);
            return $this->fetch(LAYOUT_PANEL);
        }
        // Ajax修改权限操作
        $data = get_allow_data($this->allowField, request()->post());
        $validate = validate('power');
        $check = $validate->scene('edit')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        $update = db($this->db)->where([['id', 'EQ', $id]])->field($this->allowField)->update($data);
        if (!$update) {
            $this->error('修改权限失败');
        }
        $this->success('修改权限成功');
        return true;
    }
    
    /**
     * 删除权限
     */
    public function delete() {
        if (!request()->isAjax()) {
            return false;
        }
        $data = request()->post();
        if (isset($data['del']) && $data['del'] == 'all') {
            // 删除多个
            $id = $data['data'];
        } else {
            // 删除单个
            $id = [$data['id']];
        }
        // 检查是否是否有下级权限
        foreach ($id as $idValue) {
            if (in_array($idValue, $this->disablePk)) {
                $this->error('所选权限中包含不允许删除的权限');
                return false;
            }
            if (db($this->db)->where([['pid', 'EQ', $idValue]])->field('id')->value('id')) {
                $this->error('删除权限失败：所选权限中含有下级权限');
                return false;
            }
        }
        // 执行删除
        $where = [['type', 'NEQ', 1], ['id', 'IN', $id]];
        $del = db($this->db)->where($where)->delete();
        if (!$del) {
            $this->error('删除权限失败：所选权限中包含系统权限');
            return false;
        }
        $this->success('删除权限成功');
        return true;
    }
    
    /**
     * 导出权限
     * @return mixed
     */
    public function export() {
        if (!$this->request->isAjax()) {
            $form = $this->service->getExportForm(url('export'), '');
            $this->assign('panel', $form);
            return $this->fetch(LAYOUT_PANEL);
        }
        $mid = intval($this->request->post('mid'));
        if ($mid < 1) {
            $this->error('所选模块不存在');
            return false;
        }
        $name = db('sys_module')->where([['id', 'EQ', $mid]])->field('name')->value('name');
        if (!$name) {
            $this->error('所选模块不存在');
            return false;
        }
        $where = ['mid' => $mid];
        $filed = 'id,pid,name,ico,controller,action,order,type,show,enable,debug';
        $data = db('sys_power')->field($filed)->where($where)->order('order', 'asc')->select();
        if (!$data || !is_array($data) || count($data) < 1) {
            $this->error('该模块下没有权限');
            return false;
        }
        $info = Tree::config($this->treeConfig)->toLayer($data);
        $path = APP_PATH . $name . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
        if (!is_dir($path)) {
            if (!mkdir($path, 0777, true)) {
                $this->error('创建目录失败');
                return false;
            }
        }
        $info = json_encode($info);
        $put = file_put_contents($path . 'menu.json', $info);
        if (!$put) {
            $this->error('导出权限失败');
            return false;
        }
        $this->success('导出权限成功');
        return true;
    }
    
    /**
     * 权限列表排序
     * @return bool
     */
    public function order() {
        if (!request()->isAjax()) {
            return false;
        }
        $data = request()->post();
        if (!isset($data['action']) || $data['action'] != 'order') {
            $this->error('非法请求');
        }
        if (!isset($data['list']) || !is_array($data['list']) || count($data['list']) < 1) {
            $this->error('参数错误');
        }
        $order = SysPower::orderById($data['list'], 0);
        if (!$order) {
            $this->error('排序失败');
        }
        $cacheName = $this->system_power_index_cache . '_' . $data['mid'] . '_' . $data['loop'];
        cache($cacheName, null);
        $this->success('排序成功');
        return true;
    }
    
    /**
     * 获取指定模块下的控制器列表
     * @param $mid - 模块ID
     * @return array
     */
    public function getController($mid = 1) {
        $mid = (request()->isAjax()) ? request()->post('data') : $mid;
        $moduleInfo = db('sys_module')->where([['id', 'EQ', $mid]])->field('name,sub_name')->find();
        $moduleName = isset($moduleInfo['name']) ? $moduleInfo['name'] : 'admin';
        $moduleOdd = isset($moduleInfo['sub_name']) ? $moduleInfo['sub_name'] : '';
        $controllerArr = get_controller_name($moduleName, $moduleOdd);
        $controllerList = format_array($controllerArr, 'value', 'desc');
        if (!request()->isAjax()) {
            return $controllerList;
        }
        if (!$controllerArr || !is_array($controllerArr) || count($controllerArr) < 1) {
            $data = ['code' => -1, 'msg' => 'error'];
        } else {
            $data = ['code' => 1, 'data' => $controllerArr];
        }
        return json($data);
    }
    
    /**
     * 获取指定控制器下的方法列表
     * @param string $controllerName - 控制器名
     * @return array
     */
    public function getAction($controllerName = 'admin-Index') {
        $controllerName = (request()->isAjax()) ? request()->post('data') : $controllerName;
        $controllerName = empty($controllerName) ? 'admin-Index' : $controllerName;
        $data = explode('-', $controllerName);
        $module = $data[0];
        if (isset($data[2])) {
            $controller = $data[1] . '/' . $data[2];
        } else {
            $controller = $data[1];
        }
        $fileContent = File::read_file(APP_PATH . $module . '/controller/' . $controller . '.php');
        $fileContent = preg_match_all('/function(.*?)\(/', $fileContent, $match);
        $returnData = [];
        $ajaxData = [];
        if (!$fileContent) {
            return $returnData;
        }
        foreach ($match[1] as $k => $v) {
            if (!empty($v)) {
                $item = str_replace(' ', '', $v);
                $returnData[$item] = $item;
                $ajaxData[] = ['value' => $item, 'desc' => $item];
            }
        }
        $returnData['default'] = 'default';
        $returnData['updateField'] = 'updateField';
        $ajaxData[] = ['value' => 'default', 'desc' => 'default'];
        $ajaxData[] = ['value' => 'updateField', 'desc' => 'updateField'];
        return (request()->isAjax()) ? json(['code' => 1, 'data' => $ajaxData]) : $returnData;
    }
    
    /**
     * 获取指定模块下的权限列表，作为上级权限作为权限编辑、新增时的选项
     * @param int $mid - 模块ID
     * @return array|\think\response\Json
     */
    public function getPowerList($mid = 1) {
        $mid = (request()->isAjax()) ? request()->post('data') : $mid;
        // 拉取权限列表
        $powerArr = db($this->db)
            ->where(['enable' => 1, 'mid' => $mid])
            ->field('id,pid,name')
            ->select();
        $powerArr = Tree::config($this->treeConfig)->toList($powerArr);
        $returnData = [];
        $returnData['0'] = '顶级权限';
        $ajaxData = [];
        $ajaxData[] = ['value' => '0', 'desc' => '顶级权限'];
        foreach ($powerArr as $k => $v) {
            $returnData[$v['id']] = $v['title_display'];
            $ajaxData[] = ['value' => $v['id'], 'desc' => $v['title_display']];
        }
        return (request()->isAjax()) ? json(['code' => 1, 'data' => $ajaxData]) : $returnData;
    }
    
    /**
     * 添加/编辑页面表单统一生成
     * @param string $url - 表单提交地址
     * @param string $successUrl - 提交成功跳转地址
     * @param int $mid - 模块ID
     * @param int $pid - 权限父级ID
     * @param null $data - 权限数据
     * @return mixed
     */
    private function _getForm($url, $successUrl, $mid = 1, $pid = 0, $data = NUll) {
        $submitBtn = $data == NULL ? '添加权限' : '修改权限';
        // 处理字段
        $mid = isset($data['mid']) ? intval($data['mid']) : $mid;
        $pid = isset($data['pid']) ? intval($data['pid']) : $pid;
        $controller = isset($data['controller']) ? $data['controller'] : '';
        // 拉取模块列表
        $moduleArr = db('sys_module')->where(['enable' => 1])->field('id,title')->select();
        $moduleList = format_array($moduleArr, 'id', 'title');
        // 拉取控制器列表
        $controllerList = $this->getController($mid);
        // 拉取方法列表
        $actionList = $this->getAction($controller);
        // 拉取权限列表
        $powerArr = db($this->db)
            ->where(['enable' => 1, 'mid' => $mid])
            ->field('id,pid,name')
            ->select();
        $powerArr = Tree::config($this->treeConfig)->toList($powerArr);
        $powerList = cm_array_merge(['0' => '顶级权限'], format_array($powerArr, 'id', 'title_display'));
        // name字段验证
        $nameValidate = [
            'notEmpty' => ['message' => '权限名称不能为空'],
        ];
        // controller字段验证
        $controllerValidate = [
            'notEmpty' => ['message' => '控制器名不能为空'],
            'regexp'   => [
                'regexp'  => '/^[a-zA-Z0-9_-]+$/',
                'message' => '控制器名只能由字母、数字和下划线及破折号组成'
            ],
        ];
        // action字段验证
        $actionValidate = [
            'notEmpty' => ['message' => '方法名称不能为空'],
            'regexp'   => [
                'regexp'  => '/^[a-zA-Z0-9_-]+$/',
                'message' => '方法名称只能由字母、数字和下划线组成'
            ],
        ];
        // order字段验证
        $orderValidate = [
            'notEmpty' => ['message' => '排序数值不能为空'],
            'numeric'  => ['message' => '排序数值只能为数字'],
        ];
        // 创建表单
        $form = KeFormBuilder::makeForm($url, 2, $data)
            ->addSelect('mid', $mid, $moduleList, '所属模块')
            ->addSelect('pid', $pid, $powerList, '上级权限')
            ->addText('name', '', '权限名称', $nameValidate)
            ->addIco('ico', '', '权限图标')
            ->addLinkage('mid', 'pid', url('getPowerList'))
            ->addLinkage('mid', 'controller', url('getController'))
            ->addLinkage('controller', 'action', url('getAction'))
            ->addSelect('controller', $controller, $controllerList, '控制器名', $controllerValidate)
            ->addSelect('action', '', $actionList, '方法名称', $actionValidate)
            ->addText('order', 0, '排序数值', $orderValidate)
            ->addRadio('type', 1, ['1' => '系统权限', '2' => '用户添加', '3' => '其他类型'], '权限类型')
            ->addSwitch('show', 1, [1, 2], '是否展示')
            ->addSwitch('enable', 1, [1, 2], '是否启用')
            ->addSwitch('debug', 1, [1, 2], '是否仅调试模式启用')
            ->addSubmitBtn($submitBtn)
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
}