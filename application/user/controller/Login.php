<?php
/**
 *  ==================================================================
 *        文 件 名: Login.php
 *        概    要: 用户登录控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/13 12:09
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\controller;

use authcode\AuthCode;

/**
 * Class Login - 用户登录控制器
 * @package app\user\controller
 */
class Login extends \app\common\controller\Base {
    /**
     * @var - 当前登录管理员UID
     */
    private $uid = '';
    
    /**
     * @var - 当前登录管理角色ID
     */
    private $rid = '';
    
    /**
     * @var bool - 是否开启后台注册
     */
    private $is_admin_reg = false;
    
    /**
     * @var string - 要操作的数据表名称
     */
    private $db = 'sys_user';
    
    /**
     * @var string - 要操作的验证器类
     */
    private $validate = 'user';
    
    /**
     * @var string - 登录成功后的回跳地址
     */
    private $loginCallBackUrl = '';
    
    /**
     * @var \app\user\service\Login - 服务层
     */
    protected $service = null;
    
    /**
     * 初始化操作
     */
    protected function initialize() {
        parent::initialize();
        // 解密session内容
        $adminUserSession = AuthCode::decrypt(session('admin'));
        // 获取当前登录用户的uid
        $this->uid = isset($adminUserSession['uid']) ? $adminUserSession['uid'] : '';
        // 获取当前登录用户的rid
        $this->rid = isset($adminUserSession['rid']) ? $adminUserSession['rid'] : '';
        $this->is_admin_reg = config('is_admin_reg');
        // 获取登录成功后的回跳地址
        $cookieUrl = cookie('login_call_back_url');
        $this->loginCallBackUrl = !empty($cookieUrl) ? $cookieUrl : url(config('default_admin_module'));
        $assign = [
            'is_admin_reg'        => $this->is_admin_reg,
            'login_call_back_url' => $this->loginCallBackUrl,
        ];
        $this->assign($assign);
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\user\service\Login();
    }
    
    /**
     * 登录页面
     * @return mixed
     */
    public function index() {
        if ($this->uid >= 1 && $this->rid >= 1) {
            $this->redirect($this->loginCallBackUrl);
            return false;
        }
        $title = '管理员登录' . self::$delimiter . self::$sysName . ' V' . self::$sysVersion;
        
        $oauthType = hook('oauth_type', null, true);
        if (!$oauthType || !is_array($oauthType) || count($oauthType) < 1) {
            $oauthType = [];
        }
        $assign = [
            'title'            => $title,
            'oauth'            => db('sys_plugin')->where('name', 'Oauth')->value('enable'),
            'oauth_type'       => $oauthType,
            'is_login_captcha' => config('is_login_captcha'),
            'psd_form'         => $this->service->passwordReminderForm()
        ];
        $this->assign($assign);
        return $this->fetch('login');
    }
    
    /**
     * AJAX(POST) 登录验证
     * @return bool|\think\response\Json
     */
    public function login() {
        if (!request()->isAjax()) {
            return false;
        }
        if ($this->uid >= 1 && $this->rid >= 1) {
            return false;
        }
        $captchaCheck = config('is_login_captcha') == '1' ? true : false;
        $check = app()->model($this->db)->loginCheck($captchaCheck);
        Hook('admin_login');
        return json($check);
    }
    
    /**
     * AJAX(POST) 用户登录时检测用户名是否存在
     * @return bool|\think\response\Json
     */
    public function loginUsernameCheck() {
        if (!request()->isAjax()) {
            return false;
        }
        if ($this->uid >= 1 && $this->rid >= 1) {
            return false;
        }
        $username = request()->post('username');
        if (empty($username)) {
            return false;
        }
        $where = [
            ['username', 'EQ', $username]
        ];
        $db = db($this->db);
        $check = $db->where($where)->field('id')->value('id');
        $check = ($check >= 1) ? true : false;
        $return = ['valid' => boolval($check)];
        return json($return);
    }
    
    /**
     * AJAX(POST) 获取短信/邮箱验证码用于找回密码功能
     * @return bool
     */
    public function getCaptcha() {
        // 判断提交方式是否为AJAX
        if (!request()->isAjax()) {
            $this->error('提交方式有误');
            return false;
        }
        if ($this->uid >= 1 && $this->rid >= 1) {
            return false;
        }
        // 找回密码允许的类型
        $allowType = ['email,phone'];
        // 获取POST提交的数据
        $data = request()->post();
        // 判断提交的验证方式是否存在
        if (!isset($data['type']) || in_array($data['type'], $allowType)) {
            $this->error('暂未开放此验证方式');
            return false;
        }
        $type = $data['type'];
        // 验证数据
        $validate = validate('user');
        $check = $validate->scene('send_code_' . $type)->check($data);
        if (!$check) {
            $this->error($validate->getError());
            return false;
        }
        $xTime = 30;
        $captchaData = [
            'code'   => cm_round(6, 'num'),
            's_time' => time(),
            'x_time' => 60 * $xTime
        ];
        $msg = '验证码发送成功,' . $xTime . '分钟内有效，请及时验证！';
        if ($type == 'email') {
            $email = $data['email'];
            $issetCheck = db('user')->where(['email' => ['EQ', $email]])->value('id');
            if (!$issetCheck || $issetCheck < 1) {
                $this->error('您输入的Email不存在');
                return false;
            }
            $content = '您正在使用Email找回密码功能,您的验证码是[' . $captchaData['code'] . ']' . $xTime . '分钟内有效,请及时验证！';
            // $send = Email::send($email, '找回密码 - 小强IT屋', $content);
        } else if ($type == 'phone') {
            $phone = $data['phone'];
            $issetCheck = db($this->db)->where([['phone', 'EQ', $phone]])->value('id');
            if (!$issetCheck || $issetCheck < 1) {
                $this->error('您输入的手机号不存在');
                return false;
            }
            $send = true;
        } else {
            $send = false;
        }
        if (!$send) {
            $this->error('验证码发送失败');
            return false;
        }
        $this->success($msg, '', ($captchaData['x_time'] / 60));
        return true;
    }
    
    /**
     * 退出登录，清除session
     */
    public function logout() {
        /* 删除缓存 */
        session('user_' . $this->uid, null);
        /* 清空session */
        session(null);
        /* 页面跳转 */
        $referrer = request()->server('HTTP_REFERER');
        $redirect_url = $referrer ? $referrer : 'admin/index/index';
        $this->redirect($redirect_url);
    }
    
    /**
     * 第三方登录
     * @param string $type - 登录方式
     * @return mixed
     */
    public function oauthLogin($type = '') {
        // 判断用户是否已经登录
        if ($this->uid >= 1 && $this->rid >= 1) {
            $this->redirect($this->loginCallBackUrl);
            return false;
        }
        // 参数错误时跳转到账号密码登录页进行普通登录
        if (empty($type)) {
            $this->redirect('user/login/index');
            return false;
        }
        // 调用第三方登录
        return hook('oauth_login', $type, true);
    }
    
    /**
     * 第三方登录授权回调地址
     * @param null $type - 登录方式
     * @param null $code - 回传code参数
     */
    public function oauthCallback($type = null, $code = null) {
        // 判断用户是否已经登录
        if ($this->uid >= 1 && $this->rid >= 1) {
            $this->redirect($this->loginCallBackUrl);
            exit();
        }
        $params = [
            'type' => $type,
            'code' => $code,
        ];
        $info = hook('oauth_callback', $params, true);
        dump($info);
        // TODO 进行登录处理
    }
}