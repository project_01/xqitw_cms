<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 用户模块基类控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/15 14:45
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\controller;

use app\common\controller\Admin;

/**
 * Class Base - 用户模块基类控制器
 * @package app\user\controller
 */
abstract class Base extends Admin {
    
}