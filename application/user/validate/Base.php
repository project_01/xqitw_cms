<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 用户模块验证器基类
 *        作    者: IT小强
 *        创建时间: 2017/9/14 15:18
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\validate;

/**
 * Class Base - 用户模块验证器基类
 * @package app\user\validate
 */
abstract class Base extends \app\common\validate\Base {
    
}