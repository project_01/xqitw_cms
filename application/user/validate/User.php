<?php
/**
 *  ==================================================================
 *        文 件 名: User.php
 *        概    要: sys_user 数据表验证器
 *        作    者: IT小强
 *        创建时间: 2017/9/14 15:19
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\validate;

/**
 * Class User - sys_user 数据表验证器
 * @package app\user\validate
 */
class User extends Base {
    /**
     * @var array - 验证规则
     */
    protected $rule = [
        'username'         => 'require|alphaDash|uniqueCheck:sys_user',
        '__token__'        => 'require|token:__token__',
        'email'            => 'require|email|uniqueCheck:sys_user',
        'captcha'          => 'require|captcha',
        'n_name'           => 'chsDash',
        't_name'           => 'require|chsDash',
        'birthday'         => 'alphaDash',
        'age'              => 'integer|between:1,300',
        'qq'               => 'max:20|uniqueCheck:sys_user',
        'phone'            => [
            'number',
            'length:11',
            'regex' => '/^1[3|4|5|6|7|8|9][0-9]{9}$/',
            'require',
            'uniqueCheck:sys_user'
        ],
        // 'address'          => 'chsDash',
        'password'         => 'require|min:2|max:18',
        'old_password'     => 'require|min:2|max:18',
        'confirm_password' => 'require|min:2|max:18|confirm:password'
    ];
    
    /**
     * @var array - 验证提示信息
     */
    protected $message = [
        // 用户名
        'username.require'   => ':errPreFix用户名不得为空',
        'username.alphaDash' => ':errPreFix用户名只允许字母、数字和下划线 破折号',
        'username.unique'    => ':errPreFix用户名已经被注册',
        // 密码
        'password.require'   => ':errPreFix密码不得为空',
        'password.min'       => ':errPreFix密码最小长度为2个字符',
        'password.max'       => ':errPreFix密码最大长度为18个字符',
        // 邮箱
        'email.require'      => ':errPreFixEmail不得为空',
        'email.email'        => ':errPreFixEmail格式有误，请重新填写',
        'email.unique'       => ':errPreFixEmail已经被注册',
        // 验证码
        'captcha.require'    => ':errPreFix验证码不能为空',
        'captcha.captcha'    => ':errPreFix验证码错误',
        // 昵称
        'n_name'             => ':errPreFix昵称只能是汉字、字母、数字和_及-',
        // 真实姓名
        't_name.require'     => ':errPreFix真实姓名不得为空',
        't_name.chsDash'     => ':errPreFix姓名只能是汉字、字母、数字和_及-',
        // 出生日期
        'birthday'           => ':errPreFix格式错误',
        // 年龄
        'age'                => ':errPreFix年龄只能为1-300之间的纯数字',
        // QQ
        // 'qq'                 => ':errPreFixQQ号只能为纯数字',
        // 联系电话
        'phone'              => ':errPreFix手机号码只能为11位纯数字',
        // 联系地址
        'address'            => ':errPreFix地址只能为汉字、字母、数字和_及-',
    ];
    
    /**
     * @var array - 验证场景
     */
    protected $scene = [
        //发送邮箱验证码时验证邮箱
        'send_code_email' => ['email' => 'require|email'],
        //发送手机验证码时验证手机
        'send_code_phone' => [
            'phone' => [
                'number',
                'length:11',
                'regex' => '/^1[3|4|5|6|7|8|9][0-9]{9}$/',
                'require',
            ]
        ],
        // 邮箱验证码验证
        'email_captcha'   => ['email' => 'require|email'],
        // 普通登录验证场景
        'login'           => ['username' => 'require|alphaDash', 'password'],
        // 验证码登录验证场景
        'login_captcha'   => ['username' => 'require|alphaDash', 'password', 'captcha'],
        // 保存个人信息
        'save_user_info'  => ['n_name', 'sex', 'age', 'birthday', 'qq', 'describe_subject', 'describe_content'],
        // 注册验证
        'reg'             => ['username', 'password', 'email', 'captcha'],
        // 添加用户验证
        'add'             => ['username', 'password'],
        // 编辑用户验证
        'edit'            => ['username', 'password'],
        // 修改密码验证
        'edit_password'   => ['password', 'password2', 'old_password'],
        'n_name'          => ['n_name'],
        't_name'          => ['t_name'],
        'email'           => ['email'],
        'age'             => ['age'],
        'qq'              => ['qq'],
        'phone'           => ['phone'],
        'address'         => ['address'],
        'birthday'        => ['birthday'],
        'password'        => ['password'],
    ];
}