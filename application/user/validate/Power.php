<?php
/**
 *  ==================================================================
 *        文 件 名: Power.php
 *        概    要: sys_power 数据表验证器
 *        作    者: IT小强
 *        创建时间: 2017/9/21 11:39
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\validate;

/**
 * Class Power - sys_power 数据表验证器
 * @package app\user\validate
 */
class Power extends Base {
    /**
     * @var array - 验证规则
     */
    protected $rule = [
        'pid'        => 'require|integer',
        'mid'        => 'require|integer',
        'name'       => 'require|chsDash',
        'controller' => 'require|alphaDash',
        'action'     => 'require|alphaDash',
        'order'      => 'integer',
        'type'       => 'require|integer',
        'show'       => 'require|integer|between:1,2',
        'enable'     => 'require|integer|between:1,2',
        'debug'      => 'require|integer|between:1,2'
    ];
    
    /**
     * @var array - 验证提示信息
     */
    protected $message = [
        // 权限名称
        'name.require'         => ':errPreFix权限名称不能为空',
        'name.chsDash'         => ':errPreFix权限名称只能是汉字、字母、数字和_及-',
        
        // 控制器名称
        'controller.require'   => ':errPreFix控制器名不能为空',
        'controller.alphaDash' => ':errPreFix控制器名只允许字母、数字和下划线 破折号',
        
        // 方法名
        'action.require'       => ':errPreFix方法名不能为空',
        'action.alphaDash'     => ':errPreFix方法名只允许字母、数字和下划线 破折号',
    ];
    
    /**
     * @var array - 验证场景
     */
    protected $scene = [
        'add'        => ['name', 'mid', 'pid', 'ico', 'controller', 'action', 'order', 'type', 'show', 'enable', 'debug'],
        'edit'       => [
            'name' => 'require|chsDash',
            'mid', 'pid', 'ico', 'controller', 'action', 'order', 'type', 'show', 'enable', 'debug'
        ],
        'name'       => ['name'],
        'pid'        => ['pid'],
        'mid'        => ['mid'],
        'controller' => ['controller'],
        'action'     => ['action'],
        'order'      => ['order'],
        'type'       => ['type'],
        'show'       => ['show'],
        'enable'     => ['enable'],
        'debug'      => ['debug']
    ];
}