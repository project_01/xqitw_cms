<?php
/**
 *  ==================================================================
 *        文 件 名: Role.php
 *        概    要: sys_role 数据表验证器
 *        作    者: IT小强
 *        创建时间: 2017/9/21 11:38
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\validate;

/**
 * Class Role - sys_role 数据表验证器
 * @package app\user\validate
 */
class Role extends Base {
    
    /**
     * @var array - 验证规则
     */
    protected $rule = [
        'pid'      => 'integer',
        'name'     => 'require|chsDash|uniqueCheck:sys_role',
        'describe' => 'chsDash',
        'power'    => ['regex' => '/^[1-9]{1}[0-9,]*([0-9]){1}$/'],
        'order'    => 'integer',
        'show'     => 'require|integer|in:1,2',
        'enable'   => 'require|integer|in:1,2'
    ];
    
    /**
     * @var array - 验证提示信息
     */
    protected $message = [
        // 角色名称
        'name.require'     => ':errPreFix角色名称不能为空',
        'name.chsDash'     => ':errPreFix角色名称只能是汉字、字母、数字和_及-',
        'name.unique'      => ':errPreFix角色名称已存在',
        
        // 角色描述
        'describe.chsDash' => ':errPreFix角色描述只能是汉字、字母、数字和_及-',
        
        //角色权限
        'power.regex'      => ':errPreFix角色权限格式不正确',
    ];
    
    /**
     * @var array - 验证场景
     */
    protected $scene = [
        'add'      => ['name', 'describe', 'power', 'pid', 'order', 'show', 'enable'],
        'edit'     => [
            'name' => 'require|chsDash',
            'describe', 'power', 'pid', 'order', 'show', 'enable'
        ],
        'name'     => ['name'],
        'pid'      => ['pid'],
        'describe' => ['describe'],
        'power'    => ['power'],
        'order'    => ['order'],
        'show'     => ['show'],
        'enable'   => ['enable']
    ];
}