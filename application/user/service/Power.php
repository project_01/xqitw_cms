<?php
/**
 *  ==================================================================
 *        文 件 名: Power.php
 *        概    要: 权限控制服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/16 16:58
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\service;
use builder\KeFormBuilder;

/**
 * Class Power - 权限控制服务层
 * @package app\user\service
 */
class Power extends Base {
    
    /**
     * 递归生成权限列表树（<ol></ol>）
     * @param $data - 原始数组
     * @param string $son - 子代名称
     * @return string
     */
    public function createPowerOlList($data, $son = 'son') {
        $html = '';
        if (count($data) < 0) {
            return $html;
        }
        $html .= '<ol class="dd-list">';
        foreach ($data as $key => $value) {
            $id = $value['id'];
            $check_box = '<input class="magic-checkbox" id="son_' . $id . '" name="son[]" type="checkbox" value="' . $id . '">';
            $check_box .= '<label for="son_' . $id . '" class="pointer"></label>';
            
            $add_url = url('add', ['pid' => $id, 'mid' => $value['mid']]);
            $edit_url = url('edit', ['id' => $id]);
            $html .= '    <li class="dd-item dd3-item" data-id="' . $id . '">';
            $html .= '        <div class="dd-handle dd3-handle"></div>';
            $html .= '        <div class="dd3-content">';
            $html .= $check_box;
            $html .= '            &nbsp;<i class="fa-fw ' . $value['ico'] . '"></i>&nbsp;';
            $html .= '            <span>' . $value['name'] . '</span>';
            $html .= '            <button data-id="' . $id . '" type="button" class="hidden-xs btn btn-xs btn-danger del-btn pull-right">';
            $html .= '                <i class="fa-fw fa fa-trash"></i>';
            $html .= '                <span class="hidden-xs">删除权限</span>';
            $html .= '            </button>';
            $html .= '            <a href="' . $edit_url . '" class="btn btn-info btn-xs pull-right">';
            $html .= '                <i class="fa-fw fa fa-edit"></i>';
            $html .= '                <span class="hidden-xs">编辑权限</span>';
            $html .= '            </a>';
            $html .= '            <a href="' . $add_url . '" class="hidden-xs btn btn-success btn-xs pull-right">';
            $html .= '                <i class="fa-fw fa fa-send"></i>';
            $html .= '                <span class="hidden-xs">添加子项</span>';
            $html .= '            </a>';
            $html .= '        </div>';
            if (isset($value['son'])) {
                $html .= self::createPowerOlList($value['son'], $son);
            }
            $html .= '    </li>';
        }
        $html .= '</ol>';
        return $html;
    }
    
    /**
     * 获取导出权限列表的表单
     * @param $url - 表单提交地址
     * @param $successUrl - 表单回跳地址
     * @return string
     */
    public function getExportForm($url, $successUrl) {
        $where = [
            ['enable', 'EQ', 1]
        ];
        $moduleList = db('sys_module')->where($where)->field('id,title')->select();
        $moduleList = format_array($moduleList, 'id', 'title');
        $form = KeFormBuilder::makeForm($url, 2)
            ->addSelect('mid', '', $moduleList, '所属模块')
            ->addSubmitBtn('开始导出权限列表')
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
}