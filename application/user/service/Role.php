<?php
/**
 *  ==================================================================
 *        文 件 名: Role.php
 *        概    要: 角色管理服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/21 11:20
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\service;

use builder\KeFormBuilder;
use builder\KeTableBuilder;
use tree\Tree;

/**
 * Class Role - 角色管理服务层
 * @package app\user\service
 */
class Role extends Base {
    
    /**
     * 列表页表格生成
     * @param $db - 当前表名
     * @param array $config - 表格配置信息
     * @return mixed
     */
    public function getTable($db, $config = []) {
        // 拉取角色列表（用于选择父级角色）
        $roleArr = db($db)->field('id,pid,name')->select();
        $roleList = Tree::config($this->treeConfig)->toList($roleArr);
        $pidSelect = [
            'type'   => 'select',
            'source' => get_select_list($roleList, 'id', 'title_display', '顶级角色'),
            'array'  => cm_array_merge(['0' => '顶级角色'], format_array($roleList, 'id', 'title_display'))
        ];
        
        // 生成表格
        $table = KeTableBuilder::makeTable(url('index'), url('updateField'), $config)
            ->addCheckbox()
            ->addTextColumn('id', 'ID', 'text-center')
            ->addTextColumn('name', '角色名称', 'text-center', ['type' => 'text'], 'true')
            ->addTextColumn('pid', '上级角色', 'text-center', $pidSelect, 'true')
            ->addTextColumn('describe', '角色描述', 'text-center', ['type' => 'textarea'], 'true')
            ->addTextColumn('atime', '添加时间', 'text-center', [], 'true', 'getTime')
            ->addTextColumn('utime', '更新时间', 'text-center', [], 'true', 'getTime')
            ->addTextColumn('order', '排序值', 'text-center', ['type' => 'text'], 'true')
            ->addSwitchColumn('show', url('updateField'), '是否展示', 'hidden-xs')
            ->addSwitchColumn('enable', url('updateField'), '是否启用', 'hidden-xs')
            ->addEditColumn('id', url('edit'), url('delete'), '编辑', '确定要删除该角色吗?')
            ->addLinkBtn(url('add'), '添加', 'edit', 'btn-success', '角色')
            ->addAjaxAllBtn('删除', url('delete'), url('index'), '角色', '确定要删除所选角色吗?')
            ->addTextSearch('name', '角色名称', '按角色名称搜索')
            ->addTextSearch('describe', '角色描述', '按角色描述搜索')
            ->addSelectSearch('pid', '上级角色', $pidSelect['array'], true)
            ->addTimeSearch('atime', '添加时间', 'yyyy-mm-dd', 2)
            ->addTimeSearch('utime', '更新时间', 'yyyy-mm-dd', 2)
            ->returnTable();
        return $table;
    }
    
    /**
     * 添加/编辑页面表单统一生成
     * @param string $url - 表单提交地址
     * @param string $successUrl - 提交成功跳转地址
     * @param null $data - 角色数据
     * @return mixed
     */
    public function getForm($url, $successUrl, $data = NUll) {
        $submitBtn = $data == NULL ? '添加角色' : '修改角色';
        // 拉取角色列表
        $roleWhere = ['enable' => 1];
        $roleField = 'id,name,pid';
        $roleArr = db('sys_role')->where($roleWhere)->field($roleField)->select();
        $roleArr = Tree::config($this->treeConfig)->toList($roleArr);
        $roleList = format_array($roleArr, 'id', 'title_display');
        $roleList[0] = '顶级角色';
        
        // 拉取权限列表
        $midList = db('sys_module')->where(['enable' => 1])->field('id,name,title')->select();
        $powerList = [];
        foreach ($midList as $k => $v) {
            $powerSubArr = db('sys_power')
                ->where(['enable' => 1, 'mid' => $v['id']])
                ->field('pid,id,name,ico')
                ->select();
            $powerSubArr = Tree::config($this->treeConfig)->toList($powerSubArr);
            $powerSubList = [];
            $_pid = 0;
            foreach ($powerSubArr as $powerK => $powerV) {
                $__pid = $powerV['pid'];
                if ($__pid == 0 && intval($powerK) != 0) {
                    $powerSubList[$powerV['id'] . '_hr'] = 'hr';
                }
                if ($__pid != $_pid && $__pid != 0) {
                    $powerSubList[$powerV['id'] . '_br'] = 'br';
                }
                $powerSubList[$powerV['id']] = '<i class="' . $powerV['ico'] . '"></i> ' . $powerV['name'];
                
                $_pid = $__pid;
            }
            $tempArr = ['title' => $v['title'], 'list' => $powerSubList];
            $powerList[$k] = $tempArr;
        }
        // name字段验证
        $nameValidate = [
            'notEmpty' => ['message' => '角色名称不能为空'],
        ];
        // order字段验证
        $orderValidate = [
            'notEmpty' => ['message' => '排序数值不能为空'],
            'numeric'  => ['message' => '排序数值只能为数字'],
        ];
        // $describe字段验证
        $describeValidate = [];
        $form = KeFormBuilder::makeForm($url, 2, $data)
            ->addText('name', '', '角色名称', $nameValidate)
            ->addSelect('pid', '', $roleList, '所属角色')
            ->addTextArea('describe', '', '角色描述', 4, $describeValidate)
            ->addCheckBoxGroup('power', '', $powerList, '角色权限')
            ->addText('order', 0, '排序数值', $orderValidate)
            ->addSwitch('show', 1, [1, 2], '是否展示')
            ->addSwitch('enable', 1, [1, 2], '是否启用')
            ->addSubmitBtn($submitBtn)
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
}