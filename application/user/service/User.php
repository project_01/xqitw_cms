<?php
/**
 *  ==================================================================
 *        文 件 名: User.php
 *        概    要: 用户相关服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/20 17:41
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\service;

use builder\KeFormBuilder;
use builder\KeTableBuilder;
use think\Image;
use tree\Tree;

/**
 * Class User - 用户相关服务层
 * @package app\user\service
 */
class User extends Base {
    
    /**
     * 列表页表格生成
     * @param array $config - 表格配置信息
     * @return mixed
     */
    public function getTable($config = []) {
        // 拉取角色列表（用于选择用户角色）
        $roleArr = db('sys_role')->field('id,pid,name')->select();
        $roleList = Tree::config($this->treeConfig)->toList($roleArr);
        $source = get_select_list($roleList, 'id', 'name');
        $ridSelect = [
            'type'   => 'select',
            'source' => $source,
            'array'  => format_array($roleList, 'id', 'title_display')
        ];
        // 生成表格
        $table = KeTableBuilder::makeTable(url('index'), url('updateField'), $config)
            ->addCheckbox()
            ->addTextColumn('id', 'ID', 'text-center')
            ->addTextColumn('n_name', '用户昵称', 'text-center', ['type' => 'text'], 'true')
            ->addTextColumn('rid', '所属角色', 'text-center', $ridSelect, 'true')
            ->addTextColumn('email', '邮箱', 'text-center', ['type' => 'email'], 'true')
            ->addTimeColumn('add_time', '注册时间', 'text-center', [], 'true')
            ->addTimeColumn('last_time', '最近登录', 'text-center', [], 'true')
            ->addTextColumn('order', '排序值', 'text-center', ['type' => 'text'], 'true')
            ->addSwitchColumn('enable', url('updateField'), '是否启用', 'hidden-xs')
            ->addEditColumn('id', url('edit'), url('delete'), '编辑', '确定要删除该用户吗?')
            ->addLinkBtn(url('add'), '添加', 'edit', 'btn-success', '用户')
            ->addToolBtn('删除', 'trash', 'btn-danger', '用户')
            ->addSelectSearch('rid', '所属角色', $ridSelect['array'], true)
            ->addTextSearch('username', '账号', '按账号搜索')
            ->addTextSearch('n_name', '昵称', '按昵称搜索')
            ->addTextSearch('email', '邮箱', '按邮箱搜索')
            ->addTextSearch('phone', '手机', '按手机搜索')
            ->addTextSearch('t_name', '真实姓名', '按姓名搜索')
            ->addTimeSearch('add_time', '注册时间', 'yyyy-mm-dd', 2)
            ->addTimeSearch('last_time', '最近登录时间', 'yyyy-mm-dd', 2)
            ->returnTable();
        return $table;
    }
    
    /**
     * 添加/编辑页面表单统一生成
     * @param string $url - 表单提交地址
     * @param string $successUrl - 提交成功跳转地址
     * @param null $data - 用户数据
     * @return mixed
     */
    public function getForm($url, $successUrl, $data = NUll) {
        $submitBtn = $data == NULL ? '添加用户' : '修改用户';
        // 拉取角色列表
        $roleWhere = ['enable' => 1];
        $roleField = 'id,name,pid';
        $roleArr = db('sys_role')->where($roleWhere)->field($roleField)->select();
        $roleArr = Tree::config($this->treeConfig)->toList($roleArr);
        $roleList = format_array($roleArr, 'id', 'title_display');
        $usernameValidate = [
            'notEmpty'     => ['message' => '登录账号不能为空'],
            'stringLength' => [
                'min'     => 4,
                'max'     => 20,
                'message' => '用户名长度为4-20个字符'
            ],
            'regexp'       => [
                'regexp'  => '/^[a-z0-9_-]+$/',
                'message' => '用户名只能由小写字母、数字和下划线破折号组成'
            ]
        ];
        $passwordValidate = [
            'stringLength' => [
                'min'     => 2,
                'max'     => 20,
                'message' => '密码长度为2-20个字符'
            ],
            'different'    => [
                'field'   => 'username',
                'message' => '密码不能和用户名相同'
            ]
        ];
        $password2Validate = [
            'identical' => [
                'field'   => 'password',
                'message' => '两次密码输入不一致'
            ]
        ];
        $form = KeFormBuilder::makeForm($url, 2, $data)
            ->addText('username', '', '登录账号', $usernameValidate)
            ->addText('n_name', '', '用户昵称')
            ->addSelect('rid', '', $roleList, '所属角色')
            ->addPassword('password', '', '密码', $passwordValidate)
            ->addPassword('password2', '', '重复密码', $password2Validate)
            ->addSubmitBtn($submitBtn)
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
    
    /**
     * 获取用户全部信息
     * @param array $where
     * @param string $filed
     * @return array
     */
    public function getUserInfo($where = [], $filed = '') {
        $db = db();
        $userInfo = $db->field($filed)->where($where)
            ->view('sys_user', true)
            ->view('sys_role', ['id' => 'rid', 'name' => 'r_name'], 'sys_user.rid = sys_role.id', 'LEFT')
            ->find();
        return $userInfo ? $userInfo : [];
    }
    
    /**
     * 图像裁剪
     * @param $tempDir - 原图目录
     * @param $saveDir - 保存目录
     * @param $saveName - 文件名
     * @param $width - 宽
     * @param $height - 高
     * @return bool
     */
    public function thumb($tempDir, $saveDir, $saveName, $width = 150, $height = 150) {
        if (!file_exists($saveDir)) {
            if (!mkdir($saveDir, 0700, true)) {
                $this->message = '创建文件保存目录失败';
                return false;
            }
        }
        $thumb = Image::open($tempDir . $saveName)
            ->thumb($width, $height, Image::THUMB_SCALING)
            ->save($saveDir . $saveName);
        if (!$thumb) {
            $this->message = '图像裁剪失败';
            return false;
        }
        $this->message = '图像裁剪成功';
        return true;
    }
    
    /**
     * 修改密码表单
     * @return mixed
     */
    public function getPasswordForm() {
        $url = url('savePassword');
        $successUrl = url('user/login/logout');
        $validate_0 = [
            'notEmpty' => ['message' => '原密码不能为空']
        ];
        $validate_1 = [
            'notEmpty'  => ['message' => '新密码不能为空'],
            'different' => [
                'field'   => 'old_password',
                'message' => '新密码不能和原密码相同'
            ]
        ];
        $validate_2 = [
            'notEmpty'  => ['message' => '重复新密码不能为空'],
            'identical' => [
                'field'   => 'password',
                'message' => '两次密码输入不一致'
            ]
        ];
        $form = KeFormBuilder::makeForm($url, 2, [], 'psd-form')
            ->addPassword('old_password', '', '原密码', $validate_0)
            ->addPassword('password', '', '新密码', $validate_1)
            ->addPassword('confirm_password', '', '重复新密码', $validate_2)
            ->addSubmitBtn('确认修改')
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
    
    /**
     * 修改个人信息表单
     * @param $data - 表单默认数据
     * @return mixed
     */
    public function getUserInfoForm($data) {
        $url = url('user/index/saveUserInfo');
        $successUrl = url('user/index/index');
        
        $defaultAddress = [
            'province' => '贵州省',
            'city'     => '贵阳市',
            'dist'     => '南明区',
        ];
        if (isset($data['address']) && !empty($data['address'])) {
            $defaultAddress = unserialize($data['address']);
        }
        $birthday = date('Y-m-d', $data['birthday']);
        $form = KeFormBuilder::makeForm($url, 2, $data, 'info-form')
            ->addHidden('id')
            ->addText('t_name', '', '真实姓名')
            ->addRadio('sex', '', ['1' => '保密', '2' => '男', '3' => '女'], '性别')
            ->addText('age', '', '年龄')
            ->addTime('birthday', $birthday, '生日', 'yyyy-mm-dd', 2)
            ->addText('qq', '', 'QQ')
            ->addDistPicker('联系地址', $defaultAddress, 'address[province]', 'address[city]', 'address[dist]')
            ->addSubmitBtn('确认修改')
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
}