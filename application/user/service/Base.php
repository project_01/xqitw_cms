<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 用户模块服务层基类
 *        作    者: IT小强
 *        创建时间: 2017/9/15 15:02
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\service;

use app\common\service\Admin;

/**
 * Class Base - 用户模块服务层基类
 * @package app\user\service
 */
class Base extends Admin {
    
}