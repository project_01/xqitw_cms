<?php
/**
 *  ==================================================================
 *        文 件 名: Login.php
 *        概    要: 登录服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/15 14:47
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\service;

use authcode\AuthCode;
use builder\KeFormBuilder;

/**
 * Class Login - 登录服务层
 * @package app\user\service
 */
class Login extends Base {
    
    /**
     * 登录回调
     * @param $info - 用户信息
     * @param string $model - 用户数据表名
     */
    public static function loginCallBack($info, $model = 'sys_user') {
        // 获取需要的登录信息
        $info = [
            'uid'      => get_array_data('uid', $info, 0),
            'rid'      => get_array_data('rid', $info, 0),
            'n_name'   => get_array_data('n_name', $info, ''),
            'username' => get_array_data('username', $info, ''),
        ];
        
        // 拼装查询条件
        $where = [
            ['id', 'EQ', $info['uid']],
            ['enable', 'EQ', 1],
            ['del', 'EQ', 1]
        ];
        
        // 记录本次登录时间
        $last_time = 'last_time';
        db($model)->where($where)->field($last_time)->setField($last_time, time());
        
        // 设置session
        $lifeTime = intval(config('session_life_time'));
        session('admin', AuthCode::encrypt($info, $lifeTime));
        
        // 记录登录日志
        $details = $info['n_name'] . '(' . $info['username'] . ') 登录成功';
        add_action_log('user_login_success', $model, $info['uid'], $info['uid'], $details);
    }
    
    /**
     * 找回密码表单
     * @return mixed
     */
    public function passwordReminderForm() {
        $url = url('getCaptcha');
        $successUrl = url('user/login/index');
        $validate_email = [
            'notEmpty' => ['message' => 'Email不能为空']
        ];
        $validate_phone = [
            'notEmpty' => ['message' => '手机号不能为空']
        ];
        $validate_1 = [
            'notEmpty' => ['message' => '新密码不能为空']
        ];
        $validate_2 = [
            'notEmpty'  => ['message' => '重复新密码不能为空'],
            'identical' => [
                'field'   => 'password',
                'message' => '两次密码输入不一致'
            ]
        ];
        $btn = '<button type="button" id="get-captcha" class="btn btn-info">获取验证码</button>';
        $form = KeFormBuilder::makeForm($url, 2, [], 'psd-form')
            ->addRadio('type', '', ['email' => '邮件验证码', 'phone' => '手机验证码'], '选择验证方式')
            ->addText('email', '', '已绑定Email', $validate_email)
            ->addText('phone', '', '已绑定手机号', $validate_phone)
            ->addInputBtnGroup('captcha', '', '输入验证码', $btn)
            ->addPassword('password', '', '填写新密码', $validate_1)
            ->addPassword('confirm_password', '', '重复新密码', $validate_2)
            ->addShowHide('type', 'email', 'email')
            ->addShowHide('type', 'phone', 'phone')
            ->addSubmitBtn('确认修改')
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
}