<?php
/**
 *  ==================================================================
 *        文 件 名: SysUser.php
 *        概    要: sys_user 数据模型类
 *        作    者: IT小强
 *        创建时间: 2017/9/14 15:10
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\model;

use app\user\service\Login;

/**
 * Class SysUser - sys_user 数据模型类
 * @package app\user\model
 */
class SysUser extends Base {
    
    /**
     * @var bool - 关闭自动写入时间戳
     */
    protected $autoWriteTimestamp = false;
    
    /**
     * 通过用户UID查询用户昵称、头像
     * @param $id - 用户UID
     * @return array
     */
    public static function getUserSimpleInfo($id) {
        $db = new static();
        $info = $db->where([['id', 'EQ', $id]])->field('username,portrait,n_name,email')->find();
        if (is_object($info)) {
            $info = $info->toArray();
        }
        $data = [
            'nickname' => get_array_data('n_name', $info, false),
            'portrait' => get_array_data('portrait', $info, false),
            'email'    => get_array_data('email', $info, false),
        ];
        if (!$data['nickname'] || empty($data['nickname'])) {
            $data['nickname'] = $data['username'];
        }
        return $data;
    }
    
    /**
     * 登录验证
     * @param bool $check_captcha 是否需要验证码（默认需要）
     * @return array
     */
    public static function loginCheck($check_captcha = true) {
        /* 获取提交的用户名和密码 */
        $data = get_allow_data('username,password,captcha,__token__', request()->post());
        /* 字段验证 */
        $validate = validate('user');
        $scene = $check_captcha ? 'login_captcha' : 'login';
        $check = $validate->scene($scene)->check($data);
        if (!$check) {
            $errorInfo = strval($validate->getError());
            add_action_log('user_login_error', 'user', 0, 0, $data['username'] . ' 登录失败（' . strip_tags($errorInfo) . '）');
            return ['code' => -1, 'msg' => $errorInfo];
        }
        
        /* 数据库查询 */
        $where = [
            ['username', 'EQ', $data['username']],
            ['enable', 'EQ', 1],
            ['del', 'EQ', 1]
        ];
        $db = new static();
        $result = $db
            ->field('username,password,salt,n_name,id as uid,rid')
            ->where($where)
            ->find();
        if (!$result) {
            add_action_log('user_login_error', 'user', 0, 0, $data['username'] . ' 登录失败（用户名不存在或者已被禁用）');
            return ['code' => -2, 'msg' => '<i class="fa fa-times"></i> 用户名不存在或者已被禁用'];
        }
        $result = $result->toArray();
        if (!check_password($data['password'], $result['password'])) {
            add_action_log('user_login_error', 'user', $result['uid'], $result['uid'], $data['username'] . ' 登录失败（用户名和密码不匹配）');
            return ['code' => -2, 'msg' => '<i class="fa fa-times"></i> 用户名和密码不匹配'];
        }
        //登录回调处理
        Login::loginCallBack($result);
        return ['code' => 1, 'msg' => '<i class="fa fa-check"></i> 登录成功'];
    }
    
}