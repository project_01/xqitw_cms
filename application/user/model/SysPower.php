<?php
/**
 *  ==================================================================
 *        文 件 名: SysPower.php
 *        概    要: sys_power（权限） 数据模型类
 *        作    者: IT小强
 *        创建时间: 2017/9/14 12:54
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\user\model;

/**
 * Class SysPower - sys_power（权限） 数据模型类
 * @package app\user\model
 */
class SysPower extends Base {
    
    /**
     * 获取当前页面的模块、控制器、方法名称，用于展示面包屑导航和title
     * @param $mid - 当前模块ID
     * @param $module - 当前模块名
     * @param $controller - 当前控制器名
     * @param $action - 当前操作名
     * @return array|bool
     */
    public static function getCurMenu($mid, $module, $controller, $action) {
        $db = new static();
        $_controller = explode('.', $controller);
        $_c1 = '-' . (isset($_controller[1]) ? strtolower($_controller[0]) : ucfirst(strtolower($_controller[0])));
        $_c2 = isset($_controller[1]) ? '-' . ucfirst(strtolower($_controller[1])) : '';
        $_controller = strtolower($module) . $_c1 . $_c2;
        $where = [
            'mid'        => $mid,
            'controller' => $_controller,
            'action'     => strtolower($action),
            'enable'     => 1
        ];
        $field = 'id,pid,name,ico';
        $actionName = $db->where($where)->field($field)->find();
        if (!$actionName) {
            return false;
        }
        $where = [
            'id'     => $actionName['pid'],
            'show'   => 1,
            'enable' => 1
        ];
        $returnData = [
            'action' => $actionName,
        ];
        $controllerName = $db->where($where)->field($field)->find();
        if ($controllerName) {
            $returnData['controller'] = $controllerName;
        }
        return $returnData;
    }
    
    /**
     * 权限列表排序
     * @param $data - 原始数据
     * @param int $pid - pid
     * @return bool
     */
    public static function orderById($data, $pid = 0) {
        if (!$data || !is_array($data) || count($data) < 1) {
            return false;
        }
        $updateData = ['pid' => intval($pid)];
        $db = new static();
        $i = 1;
        foreach ($data as $key => $value) {
            $updateData['order'] = $i;
            $id = intval($value['id']);
            $db->where([['id', 'EQ', $id]])->field('pid,order')->update($updateData);
            if (isset($value['children']) && is_array($value['children']) && count($value['children']) >= 1) {
                $db->orderById($value['children'], $id);
            }
            $i++;
        }
        return true;
    }
    
    /**
     * 递归获取所有权限列表数据
     * @param int $pid ，父id
     * @param int $mid ，模块ID
     * @param int $loopTimes ，要获取的层数
     * @param int $countNum ，循环计算，无需赋值，默认即可
     * @return array
     */
    public static function getAllPowerList($pid = 0, $mid = 0, $loopTimes = 0, $countNum = 0) {
        $db = new static();
        $field = 'id,pid,mid,name,ico,controller,action,show,enable';
        $where = [
            ['pid', 'EQ', (int)$pid]
        ];
        if ($mid != 0) {
            $where[] = ['mid', 'EQ', (int)$mid];
        }
        $powerArray = [];
        $power = $db->field($field)->order('order', 'ASC')->where($where)->select()->toArray();
        $countNum++;
        if (!$power || !is_array($power) || count($power) < 1) {
            return $powerArray;
        }
        foreach ($power as $item) {
            $itemArray = $item;
            $itemArray['loop'] = $countNum;
            if ($loopTimes == 0 || $countNum < $loopTimes) {
                $powerSon = self::getAllPowerList($itemArray['id'], $mid, $loopTimes, $countNum);
                if ($powerSon) {
                    $itemArray['son'] = $powerSon;
                }
            }
            $powerArray[] = $itemArray;
        }
        return $powerArray;
    }
}