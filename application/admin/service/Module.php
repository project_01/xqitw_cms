<?php
/**
 *  ==================================================================
 *        文 件 名: Module.php
 *        概    要: 模块管理服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/29 11:38
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\service;

use database\Sql;
use files\File;

/**
 * Class Module - 模块管理服务层
 * @package app\admin\service
 */
class Module extends Base {
    
    /**
     * 执行模块自带的SQL文件
     * @param $name - 模块标识
     * @param $info - 模块信息
     * @param $type - SQL类型（install/uninstall）
     * @return bool - 返回成功、失败
     */
    public function runModuleSQL($name, $info, $type = 'install') {
        $sqlFile = realpath(APP_PATH . $name . '/data/' . $type . '.sql');
        if (!is_file($sqlFile)) {
            // SQL文件不存在时无需写入
            return true;
        }
        $databasePrefix = get_array_data('database_prefix', $info, '');
        if (!empty($databasePrefix)) {
            $replace = [$databasePrefix => config('database.prefix')];
            $sqlStatement = Sql::getSqlFromFile($sqlFile, false, $replace);
        } else {
            $sqlStatement = Sql::getSqlFromFile($sqlFile);
        }
        if (empty($sqlStatement)) {
            // SQL文件为空时无需写入
            return true;
        }
        foreach ($sqlStatement as $value) {
            $exec = db()->execute($value);
            if ($exec === false) {
                $this->message = '模块自带安装SQL执行失败';
                return false;
            }
        }
        return true;
    }
    
    /**
     * 安装模块自带的后台菜单
     * @param $name - 模块标识
     * @param $mid - 模块ID
     * @return bool - 返回成功、失败
     */
    public function installModuleMenu($name, $mid) {
        $menuFile = realpath(APP_PATH . $name . '/data/menu.json');
        if (!is_file($menuFile)) {
            // 菜单文件不存在时无需写入
            return true;
        }
        $menu = file_get_contents($menuFile);
        if (empty($menu)) {
            // 菜单文件为空时无需写入
            return true;
        }
        $menu = json_decode($menu, true);
        if (!is_array($menu) && count($menu) < 1) {
            // 未获取到菜单信息时无需写入
            return true;
        }
        $insertPower = $this->insertPower($menu, $mid, 0, 'child');
        if (!$insertPower) {
            $this->message = '模块菜单写入失败';
            return false;
        }
        return true;
    }
    
    /**
     * 安装模块自带的行为数据
     * @param $name - 模块标识
     * @param $mid - 模块ID
     * @return bool - 返回成功、失败
     */
    public function installModuleAction($name, $mid) {
        $actionFile = realpath(APP_PATH . $name . '/data/action.json');
        if (!is_file($actionFile)) {
            // 行为文件不存在时无需写入
            return true;
        }
        $action = file_get_contents($actionFile);
        if (empty($action)) {
            // 行为文件为空时无需写入
            return true;
        }
        $action = json_decode($action, true);
        if (!is_array($action) && count($action) < 1) {
            // 未获取到行为信息时无需写入
            return true;
        }
        $insertAction = $this->insertAction($action, $mid);
        if (!$insertAction) {
            $this->message = '模块行为写入失败';
            return false;
        }
        return true;
    }
    
    /**
     * 获取模块信息
     * @param string $db - 数据表名
     * @return false|\PDOStatement|array|\think\Collection
     */
    public function getModuleInfo($db = 'sys_module') {
        $info = [];
        $module = $this->getModule();
        if (count($module) < 1) {
            return $info;
        }
        $db = db($db);
        $where = [];
        $info = $db->where($where)->field(true)->select();
        $info = $info ? $info : [];
        $moduleNameList = [];
        foreach ($info as $v) {
            $moduleNameList[] = $v['name'];
        }
        foreach ($module as $key => $value) {
            if (in_array($value, $moduleNameList)) {
                continue;
            }
            $_info = $this->getModuleInfoByFile($value);
            if ($_info['code'] = 1) {
                $_info['info']['enable'] = 3;
                $_info['info']['order'] = 0;
                $info[] = $_info['info'];
            }
        }
        return $info;
    }
    
    /**
     * 根据模块启/禁用状态查找模块
     * @param $status - 状态
     * @param $info - 所有模块
     * @return array
     */
    public function getModuleInfoByStatus($status, $info) {
        $allowStatus = [1, 2, 3];
        if (!in_array($status, $allowStatus)) {
            return $info;
        }
        $returnInfo = [];
        foreach ($info as $k => $v) {
            if (isset($v['enable']) && $v['enable'] == $status) {
                $returnInfo[] = $info[$k];
            }
        }
        return $returnInfo;
    }
    
    /**
     * 从文件自带的配置文件中获取模块信息
     * @param $name - 模块名称
     * @return array - 返回信息数组
     */
    public function getModuleInfoByFile($name) {
        $infoFile = APP_PATH . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'info.php';
        if (!is_file($infoFile)) {
            return ['code' => -1, 'msg' => '该模块配置文件不存在'];
        }
        $info = include $infoFile;
        if (!is_array($info) || count($info) < 1) {
            return ['code' => -2, 'msg' => '模块配置文件不正确'];
        }
        return ['code' => 1, 'msg' => '模块信息获取成功', 'info' => $info];
    }
    
    /**
     * 写入权限（用于模块初始化安装）
     * @param array $power - 权限
     * @param int $mid - 模块ID
     * @param int $pid - 起始父ID
     * @param string $child - 子代下标
     * @return bool
     */
    private function insertPower($power, $mid, $pid = 0, $child = 'child') {
        if (!is_array($power) && count($power) < 1) {
            return false;
        }
        $t = time();
        foreach ($power as $k => $v) {
            $data = [
                'pid'        => intval($pid),
                'mid'        => intval($mid),
                'name'       => $v['name'],
                'ico'        => $v['ico'],
                'controller' => $v['controller'],
                'action'     => $v['action'],
                'order'      => isset($v['order']) ? $v['order'] : 1,
                'type'       => isset($v['type']) ? $v['type'] : 1,
                'show'       => isset($v['show']) ? $v['show'] : 1,
                'enable'     => isset($v['enable']) ? $v['enable'] : 1,
                'debug'      => isset($v['debug']) ? $v['debug'] : 1,
                'atime'      => $t,
                'utime'      => $t
            ];
            $id = db('sys_power')->field(true)->insertGetId($data);
            if (!$id) {
                return false;
            }
            if (isset($v[$child]) && is_array($v[$child]) && count($v[$child]) >= 1) {
                $this->insertPower($v[$child], $mid, $id, $child);
            }
        }
        return true;
    }
    
    /**
     * 写入行为 （用于模块初始化安装）
     * @param array $action - 行为数组
     * @param int $mid - 模块ID
     * @return bool
     */
    private function insertAction($action, $mid) {
        if (!is_array($action) && count($action) < 1) {
            return false;
        }
        $t = time();
        foreach ($action as $k => $v) {
            $data = [
                'mid'    => intval($mid),
                'name'   => get_array_data('name', $v, ''),
                'title'  => get_array_data('title', $v, ''),
                'remark' => get_array_data('remark', $v, ''),
                'rule'   => get_array_data('rule', $v, ''),
                'log'    => get_array_data('log', $v, ''),
                'order'  => get_array_data('order', $v, 1),
                'enable' => get_array_data('enable', $v, 1),
                'atime'  => $t,
                'utime'  => $t
            ];
            $insert = db('sys_action')->field(true)->insert($data);
            if (!$insert) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 获取 application 下的 所有模块
     * @return array
     */
    private function getModule() {
        $dir = File::get_dirs(APP_PATH);
        if (!$dir || !isset($dir['dir'])) {
            return [];
        }
        $unsetModule = ['common', 'lang', 'install', 'index', 'admin', 'user', 'api'];
        $plugin = [];
        foreach ($dir['dir'] as $v) {
            if ($v != '.' && $v != '..' && !in_array($v, $unsetModule)) {
                $plugin[] = $v;
            }
        }
        return $plugin;
    }
}