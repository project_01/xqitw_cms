<?php
/**
 *  ==================================================================
 *        文 件 名: Plugin.php
 *        概    要: 插件管理服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/21 15:54
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\service;

use builder\KeFormBuilder;
use files\File;

/**
 * Class Plugin - 插件管理服务层
 * @package app\admin\service
 */
class Plugin extends Base {
    
    /**
     * 根据插件启/禁用状态查找插件
     * @param $status - 状态
     * @param $info - 所有插件
     * @return array
     */
    public function getPluginInfoByStatus($status, $info) {
        $returnInfo = [];
        foreach ($info as $k => $v) {
            if (isset($v['enable']) && $v['enable'] == $status) {
                $returnInfo[] = $info[$k];
            }
        }
        return $returnInfo;
    }
    
    /**
     * 获取插件信息,用于输出插件列表
     * @param $db - 插件数据表名
     * @param $pluginPath - 插件目录
     * @return array
     */
    public function getPluginInfo($db, $pluginPath) {
        $info = [];
        $plugin = $this->getPlugin($pluginPath);
        if (count($plugin) < 1) {
            return $info;
        }
        $where = [];
        $field = true;
        $db = db($db);
        $info = $db->where($where)->field($field)->select();
        $pluginNameList = $db->where($where)->field('name')->column('name');
        foreach ($plugin as $key => $name) {
            if (in_array($name, $pluginNameList)) {
                continue;
            }
            $_info = $this->getPluginInfoByFile($pluginPath, $name);
            if ($_info['code'] = 1) {
                $_info['info']['enable'] = 3;
                $_info['info']['order'] = 0;
                $info[] = $_info['info'];
            }
        }
        return $info;
    }
    
    /**
     * 从文件自带的配置文件中获取插件信息
     * @param $pluginPath - 插件目录
     * @param $name - 插件名称
     * @return array - 返回信息数组
     */
    public function getPluginInfoByFile($pluginPath, $name) {
        // 获取插件配置文件位置
        $configFile = $pluginPath . hump_to_underline($name) . '/config.php';
        if (!is_file($configFile)) {
            return ['code' => -1, 'msg' => '该插件的配置文件不存在'];
        }
        // 获取插件文件配置信息
        $config = include $configFile;
        if (!is_array($config) || count($config) < 1) {
            return ['code' => -2, 'msg' => '插件配置文件不正确'];
        }
        if (!isset($config['info']) || !is_array($config['info']) || count($config['info']) < 1) {
            return ['code' => -3, 'msg' => '插件配置基础信息有误'];
        }
        $returnData = ['code' => 1, 'msg' => '获取插件信息成功', 'info' => $config['info']];
        if (isset($config['var']) && is_array($config['var']) && count($config['var']) >= 1) {
            $returnData['var'] = $config['var'];
        }
        if (isset($config['tab']) && is_array($config['tab']) && count($config['tab']) >= 1) {
            $returnData['tab'] = $config['tab'];
        }
        return $returnData;
    }
    
    /**
     * 获取插件列表
     * @param $pluginPath - 插件目录
     * @return array - 返回插件列表（数组）
     */
    public function getPlugin($pluginPath) {
        $dir = File::get_dirs($pluginPath);
        if (!$dir || !isset($dir['dir'])) {
            return [];
        }
        $plugin = [];
        foreach ($dir['dir'] as $v) {
            if ($v != '.' && $v != '..' && $v != 'common') {
                $plugin[] = $v;
            }
        }
        return $plugin;
    }
    
    /**
     * 配置编辑页面表单生成
     * @param string $name - 插件标识
     * @param string $url - 表单提交地址
     * @param string $successUrl - 提交成功跳转地址
     * @param null $data - 表单默认数据
     * @return mixed
     */
    public function getForm($name, $url, $successUrl, $data = NUll) {
        $plugin = get_plugin_class($name);
        $form = $plugin->getConfigForm($url, $successUrl, format_array($data, 'name', 'value'));
        if (!empty($form)) {
            return $form;
        }
        $form = KeFormBuilder::makeForm($url, 2)
            ->addItems($data, true, $successUrl, '修改配置信息');
        return $form;
    }
}