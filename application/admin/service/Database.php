<?php
/**
 *  ==================================================================
 *        文 件 名: Database.php
 *        概    要: 数据库管理服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/26 22:24
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\service;

use builder\KeTableBuilder;
use files\File;

/**
 * Class Database - 数据库管理服务层
 * @package app\admin\service
 */
class Database extends Base {
    
    /**
     * 数据备份页表格
     * @param $url - 数据获取地址
     * @param bool $edit_url - 快速修改地址
     * @param array $config - 表格配置
     * @return string - 表格HTML
     */
    public function getBackupTable($url, $edit_url = false, $config = []) {
        $table = KeTableBuilder::makeTable($url, $edit_url, $config)
            ->addCheckbox()
            ->addTextColumn('name', '表名', 'text-left')
            ->addTextColumn('rows', '行数', 'text-center', [], true)
            ->addTextColumn('data_length', '大小', 'text-center', [], true, 'formatBytes')
            ->addTextColumn('data_free', '冗余', 'text-center', [], true, 'formatBytes')
            ->addTextColumn('create_time', '创建日期', 'text-center', [], true)
            ->addTextColumn('collation', '字符集', 'text-center')
            ->addTextColumn('engine', '引擎', 'text-center')
            ->addTextColumn('comment', '备注', 'text-left', [], true)
            ->addTextColumn('name', '操作', 'text-center', [], false, 'editDatabase')
            ->addLinkBtn(url('reply'), '还原', 'reply', 'btn-success', '数据')
            ->addToolBtn('备份', 'copy', 'btn-danger export-all', '数据')
            ->addToolBtn('优化', 'cogs', 'btn-info optimize-all', '表')
            ->addToolBtn('修复', 'wrench', 'btn-success repair-all', '表')
            ->returnTable();
        return $table;
    }
    
    /**
     * 数据还原页表格
     * @param $url - 数据获取地址
     * @param bool $edit_url - 快速修改地址
     * @param array $config - 表格配置
     * @return string - 表格HTML
     */
    public function getReplyTable($url, $edit_url = false, $config = []) {
        $table = KeTableBuilder::makeTable($url, $edit_url, $config)
            ->addCheckbox()
            ->addTextColumn('name', '文件名称', 'text-left')
            ->addTextColumn('compress', '压缩类型', 'text-center')
            ->addTextColumn('part', '分卷', 'text-center', [], true)
            ->addTextColumn('size', '文件大小', 'text-center', [], true, 'formatBytes')
            ->addTextColumn('create_time', '创建日期', 'text-center', [], true)
            ->addTextColumn('name', '操作', 'text-center', [], false, 'replyDatabase')
            ->addLinkBtn(url('index'), '备份', 'copy', 'btn-success', '数据')
            ->addToolBtn('删除', 'trash', 'btn-danger del-all', '文件')
            ->returnTable();
        return $table;
    }
    
    /**
     * 获取数据表名
     * @param null $data
     * @return bool|mixed|null|string
     */
    public function getTableName($data = NULL) {
        $data = (($data == NULL) ? (request()->post()) : $data);
        $tables = isset($data['data']) ? $data['data'] : false;
        if (!$tables) {
            return false;
        }
        if (is_array($tables) && count($tables) >= 1) {
            $tables = implode('`,`', $tables);
        }
        return $tables;
    }
    
    /**
     * 获取备份文件信息
     * @param $path - 文件路径
     * @return array|bool
     */
    public function getBackupDataInfo($path) {
        $backupData = [];
        if (!is_dir($path)) {
            return $backupData;
        }
        $dir = File::get_dirs($path);
        if (!isset($dir['file']) || !is_array($dir['file']) || count($dir['file']) < 1) {
            return $backupData;
        }
        foreach ($dir['file'] as $k => $v) {
            $backupData[] = $this->getFormatFileInfo($path, $v);
        }
        return array_reverse($backupData);
    }
    
    /**
     * 获取格式化后的备份文件信息
     * @param $path - 文件路径
     * @param $fileName - 文件名
     * @return array
     */
    public function getFormatFileInfo($path, $fileName) {
        $l1 = strrpos($fileName, '-') + 1;
        $l2 = strrpos($fileName, '.sql');
        $part = substr($fileName, $l1, $l2 - $l1);
        $compress = substr($fileName, strrpos($fileName, '.') + 1);
        $compress = strtolower($compress) == 'sql' ? '未压缩' : strtoupper($compress);
        $filePath = $path . DIRECTORY_SEPARATOR . $fileName;
        $info = [
            'name'        => $fileName,
            'path'        => $filePath,
            'size'        => filesize($filePath),
            'create_time' => date('Y-m-d H:i:s', filectime($filePath)),
            'compress'    => $compress,
            'part'        => $part,
        ];
        return $info;
    }
}