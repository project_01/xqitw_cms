<?php
/**
 *  ==================================================================
 *        文 件 名: Log.php
 *        概    要: 系统日志服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/27 13:38
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\service;

use builder\KeTableBuilder;

/**
 * Class Log - 系统日志服务层
 * @package app\admin\service
 */
class Log extends Base {
    
    /**
     * 列表页表格生成
     * @param array $config - 表格配置信息
     * @return mixed
     */
    public function getTable($config = []) {
        // 拉取模块列表（用于选择所属模块）
        $moduleArr = db('sys_module')->field('id,title')->select();
        $moduleSource = get_select_list($moduleArr, 'id', 'title');
        $moduleSelect = [
            'type'   => 'select',
            'source' => $moduleSource,
            'array'  => format_array($moduleArr, 'id', 'title')
        ];
        // 拉取用户列表（用于执行者选择）
        $userArr = db('sys_user')->field('id,n_name')->select();
        $userSource = get_select_list($userArr, 'id', 'n_name');
        $userSelect = [
            'type'   => 'select',
            'source' => $userSource,
            'array'  => format_array($userArr, 'id', 'n_name')
        ];
        // 拉取行为列表（用于行为名称选择）
        $actionArr = db('sys_action')->field('id,title')->select();
        $actionSource = get_select_list($actionArr, 'id', 'title');
        $actionSelect = [
            'type'   => 'select',
            'source' => $actionSource,
            'array'  => format_array($actionArr, 'id', 'title')
        ];
        // 生成表格
        $table = KeTableBuilder::makeTable(url('index'), false, $config)
            ->addCheckbox()
            ->addTextColumn('id', 'ID', 'text-center')
            ->addTextColumn('action_name', '行为名称', 'text-left', [], 'true')
            ->addTextColumn('title', '所属模块 ', 'text-center', [], 'true')
            ->addTextColumn('n_name', '执行人', 'text-center', [], 'true')
            ->addTextColumn('aip', '执行IP', 'text-center', [], 'true')
            ->addTextColumn('log_time', '执行时间', 'text-center', [], 'true', 'getTime')
            ->addTextColumn('remark', '日志详情', 'text-left', [], 'true')
            ->addEditColumn('id', url('detail'), url('delete'), '操作', '确定要删除该日志吗?')
            ->addAjaxAllBtn('删除', url('delete'), url('index'), '系统日志', '确定要删除所选日志吗?')
            ->addSelectSearch('a.id', '行为名称', $actionSelect['array'], true, '按行为名称搜索')
            ->addSelectSearch('m.id', '所属模块', $moduleSelect['array'], true, '按模块搜索')
            ->addSelectSearch('u.id', '执行人', $userSelect['array'], true, '按执行人搜索')
            ->addTimeSearch('l.atime', '执行时间', 'yyyy-mm-dd', 2)
            ->addTextSearch('aip', '执行IP', '按执行IP搜索')
            ->returnTable();
        return $table;
    }
    
    /**
     * 后台表格专用分页方法
     * @param string $model - 要操作的数据表
     * @param array $where - 查询条件
     * @param string $field - 查询字段
     * @param int $offset - 偏移量
     * @param int $limit - 每页显示数量
     * @param string $sort - 排序字段
     * @param string $order - 排序类型（ASC/DESC）
     * @return array|bool
     */
    public function getPaginate($model = '', $where = [], $field = '', $offset = 0, $limit = 0, $sort = 'l.id', $order = 'desc') {
        $offset = intval($offset);
        $limit = intval($limit);
        $page = ($offset / $limit) + 1;
        $config = $config = ['page' => $page, 'list_rows' => $limit];
        $data = db($model)->alias('l')
            ->join('__SYS_USER__ u', 'l.uid = u.id', 'LEFT')
            ->join('__SYS_ACTION__ a', 'l.aid = a.id', 'LEFT')
            ->join('__SYS_MODULE__ m', 'a.mid = m.id', 'LEFT')
            ->where($where)
            ->field($field)
            ->order($sort, $order)
            ->paginate($config);
        if (!$data) {
            return ['rows' => [], 'total' => 0];
        }
        $returnData = $data->getCollection()->toArray();
        if (!is_array($returnData) || count($returnData) < 1) {
            return ['rows' => [], 'total' => 0];
        }
        return ['rows' => $returnData, 'total' => $data->total()];
    }
}