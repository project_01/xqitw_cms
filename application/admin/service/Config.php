<?php
/**
 *  ==================================================================
 *        文 件 名: Config.php
 *        概    要: 配置管理服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/15 16:26
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\service;

use builder\KeFormBuilder;
use builder\KeTableBuilder;

/**
 * Class Config - 配置管理服务层
 * @package app\admin\service
 */
class Config extends Base {
    
    /**
     * 配置列表页表格
     * @param $url - 数据获取地址
     * @param bool $edit_url - 快速修改地址
     * @param array $config - 表格配置
     * @return string - 表格HTML
     */
    public function getTable($url, $edit_url = false, $config = []) {
        $table = KeTableBuilder::makeTable($url, $edit_url, $config)
            ->addCheckbox()
            ->addTextColumn('id', 'ID', 'text-center')
            ->addTextColumn('describe', '配置名称', 'text-left')
            ->addTextColumn('name', '配置标识', 'text-left')
            ->addTextColumn('value', '配置值', 'text-left')
            ->addTextColumn('cate', '配置分类', 'text-center')
            ->addTextColumn('type', '配置类型', 'text-center')
            ->addTextColumn('list', '配置可选值', 'text-center')
            ->addTextColumn('tip', '提示信息', 'text-center')
            ->addEditColumn('id', url('edit'), url('delete'), '编辑', '确定要删除该配置吗?')
            ->addLinkBtn(url('add'), '新增', 'send-o', 'btn-success', '配置')
            ->addLinkBtn(url('lists'), '修改', 'wrench', 'btn-info', '配置')
            ->addAjaxAllBtn('删除', url('delete'), url('index'), '配置', '确定要删除所选配置吗?')
            ->returnTable();
        return $table;
    }
    
    /**
     * 新增、修改配置表单
     * @param string $url - 表单提交地址
     * @param string $successUrl - 成功后的回跳地址
     * @param null|array $data - 表单默认数据
     * @return string - 表单HTML
     */
    public function getForm($url, $successUrl, $data = null) {
        $submitBtn = $data == null ? '添加配置' : '修改配置';
        $cate_where = [['enable', 'EQ', 1]];
        $cate_field = 'id,name';
        $categoryList = db('sys_concate')->where($cate_where)->field($cate_field)->select();
        $categoryList = format_array($categoryList, 'id', 'name');
        $nameValidate = [
            'notEmpty' => ['message' => '配置项名称不能为空']
        ];
        $describeValidate = [
            'notEmpty' => ['message' => '配置项描述不能为空']
        ];
        $cidValidate = [
            'notEmpty' => ['message' => '请选择配置所属分类'],
            'numeric'  => ['message' => '分类ID只能为数字'],
            'remote'   => [
                'url'     => url('idUniqueCheck'),
                'message' => '分类ID只能为数字',
                'delay'   => 1000,
                'type'    => 'post'
            ]
        ];
        $form = KeFormBuilder::makeForm($url, 2, $data)
            ->addText('name', '', '配置名称', $nameValidate, '英文/数字/下划线')
            ->addText('describe', '', '配置描述', $describeValidate, '支持中文描述')
            ->addSelect('cid', '', $categoryList, '配置分类', $cidValidate)
            ->addSelect('type', '', get_config_level_one('form_type'), '配置类型')
            ->addTags('value', '', '配置项值')
            ->addTags('list', '', '可选列表', [], '适应于多选、下拉', 'block')
            ->addText('tip', '', '提示语')
            ->addText('order', 0, '排序数值')
            ->addSwitch('show', 1, [1, 2], '是否展示')
            ->addSwitch('enable', 1, [1, 2], '是否启用')
            ->addSubmitBtn($submitBtn)
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
}