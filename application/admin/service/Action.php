<?php
/**
 *  ==================================================================
 *        文 件 名: Action.php
 *        概    要: 行为管理服务层
 *        作    者: IT小强
 *        创建时间: 2017/9/27 17:54
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\service;

use builder\KeFormBuilder;
use builder\KeTableBuilder;

/**
 * Class Action - 行为管理服务层
 * @package app\admin\service
 */
class Action extends Base {
    
    /**
     * 添加/编辑页面表单统一生成
     * @param string $url - 表单提交地址
     * @param string $successUrl - 提交成功跳转地址
     * @param null $data - 数据
     * @return mixed
     */
    public function getForm($url, $successUrl, $data = NUll) {
        $submitBtn = $data == NULL ? '添加行为' : '编辑行为';
        // 拉取模块列表
        $where = ['enable' => 1];
        $field = 'id,title';
        $arr = db('sys_module')->where($where)->field($field)->select();
        $moduleList = format_array($arr, 'id', 'title');
        
        // name字段验证
        $nameValidate = [
            'notEmpty' => ['message' => '行为唯一标示不能为空'],
        ];
        $titleValidate = [
            'notEmpty' => ['message' => '行为名称不能为空'],
        ];
        // order字段验证
        $orderValidate = [
            'notEmpty' => ['message' => '排序数值不能为空'],
            'numeric'  => ['message' => '排序数值只能为数字'],
        ];
        // $describe字段验证
        $describeValidate = [
            'notEmpty' => ['message' => '行为描述不能为空'],
        ];
        // $log字段验证
        $logValidate = [
            'notEmpty' => ['message' => '日志规则不能为空'],
        ];
        $form = KeFormBuilder::makeForm($url, 2, $data)
            ->addSelect('mid', '', $moduleList, '所属模块')
            ->addText('name', '', '行为标识', $nameValidate, '行为唯一标示,由数字、字母、下划线、破折号组成')
            ->addText('title', '', '行为名称', $titleValidate, '行为名称,由汉子、数字、字母、下划线、破折号组成')
            ->addTextArea('remark', '', '行为描述', 4, $describeValidate)
            ->addTextArea('rule', '', '行为规则', 4)
            ->addTextArea('log', '', '日志规则', 4, $logValidate)
            ->addText('order', 0, '排序数值', $orderValidate)
            ->addSwitch('enable', 1, [1, 2], '是否启用')
            ->addSubmitBtn($submitBtn)
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
    
    /**
     * 列表页表格生成
     * @param array $config - 表格配置信息
     * @return mixed
     */
    public function getTable($config = []) {
        // 拉取模块列表（用于选择所属模块）
        $arr = db('sys_module')->where([['enable', 'EQ', 1]])->field('id,title')->select();
        $source = get_select_list($arr, 'id', 'title');
        $select = [
            'type'   => 'select',
            'source' => $source,
            'array'  => format_array($arr, 'id', 'title')
        ];
        
        // 生成表格
        $table = KeTableBuilder::makeTable(url('index'), url('updateField'), $config)
            ->addCheckbox()
            ->addTextColumn('id', 'ID', 'text-center')
            ->addTextColumn('name', '行为标识', 'text-left', ['type' => 'text'], 'true')
            ->addTextColumn('title', '行为名称', 'text-left', ['type' => 'text'], 'true')
            ->addTextColumn('mid', '所属模块 ', 'text-center', $select, 'true')
            ->addTextColumn('order', '排序值', 'text-center', ['type' => 'text'], 'true')
            ->addSwitchColumn('enable', url('updateField'), '是否启用', 'hidden-xs')
            ->addEditColumn('id', url('edit'), url('delete'), '编辑', '确定要删除该行为吗?')
            ->addLinkBtn(url('add'), '添加', 'edit', 'btn-success', '行为')
            ->addAjaxAllBtn('删除', url('delete'), url('index'), '行为', '确定要删除所选行为吗?')
            ->addTextSearch('name', '行为标识', '按行为标识搜索')
            ->addTextSearch('title', '行为名称', '按行为名称搜索')
            ->addSelectSearch('mid', '所属模块', $select['array'], true)
            ->addTimeSearch('atime', '添加时间', 'yyyy-mm-dd', 2)
            ->addTimeSearch('utime', '更新时间', 'yyyy-mm-dd', 2)
            ->returnTable();
        return $table;
    }
    
    /**
     * 获取导出权限列表的表单
     * @param $url - 表单提交地址
     * @param $successUrl - 表单回跳地址
     * @return string
     */
    public function getExportForm($url, $successUrl) {
        $where = [
            ['enable', 'EQ', 1]
        ];
        $moduleList = db('sys_module')->where($where)->field('id,title')->select();
        $moduleList = format_array($moduleList, 'id', 'title');
        $form = KeFormBuilder::makeForm($url, 2)
            ->addSelect('mid', '', $moduleList, '所属模块')
            ->addSubmitBtn('开始导出行为')
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
}