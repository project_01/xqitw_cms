<?php
/**
 *  ==================================================================
 *        文 件 名: Action.php
 *        概    要: sys_action 数据表验证器
 *        作    者: IT小强
 *        创建时间: 2017/9/27 17:49
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\validate;

/**
 * Class Action - sys_action 数据表验证器
 * @package app\admin\validate
 */
class Action extends Base {
    
    /**
     * @var array - 验证规则
     */
    protected $rule = [
        'name|行为唯一标识' => 'require|alphaDash|uniqueCheck:sys_action',
        'title|行为名称'  => 'require|chsDash|uniqueCheck:sys_action',
        'mid|模块ID'    => 'integer|egt:0',
        'remark|行为描述' => 'chsDash',
        'order|排序数值'  => 'integer',
        'enable|是否启用'  => 'require|integer|in:1,2'
    ];
    
    /**
     * @var array - 验证场景
     */
    protected $scene = [
        'add'    => ['name', 'title', 'mid', 'remark', 'order', 'enable'],
        'edit'   => ['name', 'title', 'mid', 'remark', 'order', 'enable'],
        'name'   => ['name'],
        'title'  => ['title'],
        'mid'    => ['mid'],
        'remark' => ['remark'],
        'order'  => ['order'],
        'enable' => ['enable']
    ];
}