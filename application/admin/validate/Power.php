<?php
/**
 *  ==================================================================
 *        文 件 名: Power.php
 *        概    要: sys_power 数据表验证器
 *        作    者: IT小强
 *        创建时间: 2017/9/27 17:51
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\validate;

/**
 * Class Power - sys_power 数据表验证器
 * @package app\admin\validate
 */
class Power extends \app\user\validate\Power {
    
}