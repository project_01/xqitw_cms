<?php
/**
 *  ==================================================================
 *        文 件 名: Module.php
 *        概    要: sys_module 数据表验证器
 *        作    者: IT小强
 *        创建时间: 2017/9/29 13:56
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\validate;

/**
 * Class Module - sys_module 数据表验证器
 * @package app\admin\validate
 */
class Module extends Base {
    /**
     * @var array - 验证规则
     */
    protected $rule = [
        'name|模块标识'       => 'require|alphaDash|uniqueCheck:sys_module',
        'title|模块名称'      => 'require|chsDash',
        'sub_name|二级模块名称' => 'chsDash',
        'order|排序数值'      => 'integer',
        'show|是否显示'       => 'require|integer|in:1,2',
        'enable|是否启用'     => 'require|integer|in:1,2'
    ];
    
    /**
     * @var array - 验证场景
     */
    protected $scene = [
        'install' => ['name', 'title', 'sub_name'],
        'order'   => ['order']
    ];
}