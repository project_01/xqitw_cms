<?php
/**
 *  ==================================================================
 *        文 件 名: Plugin.php
 *        概    要: sys_plugin 数据表验证器
 *        作    者: IT小强
 *        创建时间: 2017/10/1 12:47
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\validate;

/**
 * Class Plugin - sys_plugin 数据表验证器
 * @package app\admin\validate
 */
class Plugin extends Base {
    /**
     * @var array - 验证规则
     */
    protected $rule = [
        'name|插件标识'   => 'require|alphaDash|uniqueCheck:sys_plugin',
        'title|插件名称'  => 'require|chsDash',
        'order|排序数值'  => 'integer',
        'show|是否显示'   => 'require|integer|in:1,2',
        'enable|是否启用' => 'require|integer|in:1,2'
    ];
    
    /**
     * @var array - 验证场景
     */
    protected $scene = [
        'install' => ['name', 'title'],
        'order'   => ['order']
    ];
}