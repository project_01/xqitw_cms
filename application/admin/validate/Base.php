<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 系统模块验证器基类
 *        作    者: IT小强
 *        创建时间: 2017/9/27 17:48
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\validate;

/**
 * Class Base - 系统模块验证器基类
 * @package app\admin\validate
 */
abstract class Base extends \app\common\validate\Base {
    
}