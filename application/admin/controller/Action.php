<?php
/**
 *  ==================================================================
 *        文 件 名: Action.php
 *        概    要: 行为管理
 *        作    者: IT小强
 *        创建时间: 2017/9/27 17:53
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

/**
 * Class Action - 行为管理
 * @package app\admin\controller
 */
class Action extends Base {
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_action';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'action';
    
    /**
     * @var string - 新增、修改时的允许字段
     */
    protected $allowField = 'mid,name,title,remark,rule,log,order,enable';
    
    /**
     * @var array - 不允许被操作的记录主键
     */
    protected $disablePk = [];
    
    /**
     * @var \app\admin\service\Action - 服务层
     */
    protected $service = null;
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\admin\service\Action();
    }
    
    /**
     * 行为列表
     * @param string $sort
     * @param string $order
     * @param int $offset
     * @param int $limit
     * @param string $search
     * @return bool|mixed|\think\response\Json
     */
    public function index($sort = 'id', $order = 'desc', $offset = 0, $limit = 10, $search = '') {
        if (!$this->request->isAjax()) {
            $table = $this->service->getTable();
            $this->assign('panel', $table);
            return $this->fetch(LAYOUT_PANEL);
        }
        $where = [];
        $allowSearchField = '';
        $where = array_merge($where, $this->service->getSearchParams($search, $allowSearchField));
        $field = 'id,mid,name,title,enable,order';
        return $this->getPaginate($this->db, $where, $field, $offset, $limit, $sort, $order);
    }
    
    /**
     * 添加行为
     * @return bool|mixed
     */
    public function add() {
        if (!$this->request->isAjax()) {
            $form = $this->service->getForm(url('add'), url('index'));
            $assign = ['panel' => $form];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        $data = get_allow_data($this->allowField, $this->request->post());
        $validate = validate($this->validate);
        $check = $validate->scene('add')->check($data);
        if (!$check) {
            $this->error($validate->getError());
            return false;
        }
        $data['atime'] = time();
        $data['utime'] = $data['atime'];
        $add = db($this->db)->field($this->allowField)->insertGetId($data);
        if (!$add) {
            $this->error('添加行为失败');
            return false;
        }
        $this->success('添加行为成功', url('index'));
        return true;
    }
    
    /**
     * 编辑行为
     * @param int $id - 行为ID
     * @return bool|mixed
     */
    public function edit($id = 0) {
        $id = intval($id);
        if ($id == 0) {
            $this->redirect('add');
            return false;
        }
        if (count($this->disablePk) >= 1 && in_array($id, $this->disablePk)) {
            $this->error('该行为不允许被修改');
            return false;
        }
        $editUrl = url('edit', ['id' => $id]);
        if (!$this->request->isAjax()) {
            $where = [['id', 'EQ', $id]];
            $field = '';
            $data = db($this->db)->where($where)->field($field)->find();
            $form = $this->service->getForm($editUrl, $editUrl, $data);
            $assign = ['panel' => $form];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        // Ajax修改行为信息
        $data = get_allow_data($this->allowField, $this->request->post());
        $validate = validate($this->validate);
        $check = $validate->scene('edit')->check($data);
        if (!$check) {
            $this->error($validate->getError());
            return false;
        }
        $data['utime'] = time();
        $where = [['id', 'EQ', $id]];
        $update = db($this->db)->where($where)->field($this->allowField)->update($data);
        if (!$update) {
            $this->error('编辑行为失败');
            return false;
        }
        $this->success('编辑行为成功', $editUrl);
        return true;
    }
    
    /**
     * AJAX(POST) - 删除行为
     * @return bool|\think\response\Json
     */
    public function delete() {
        if (!$this->request->isAjax()) {
            return false;
        }
        $data = $this->request->post();
        if (isset($data['data']) && is_array($data['data']) && count($data) >= 1) {
            $id = $data['data'];
        } else if (isset($data['id'])) {
            $id = [intval($data['id'])];
        } else {
            $this->error('参数错误');
            return false;
        }
        foreach ($id as $idValue) {
            if (in_array($idValue, $this->disablePk)) {
                $this->error('所选行为中包含不允许删出的行为');
                return false;
            }
        }
        $where = [['id', 'IN', $id]];
        $del = db($this->db)->where($where)->delete();
        if (!$del) {
            $this->error('行为删除失败');
        } else {
            $this->success('行为删除成功');
        }
        return true;
    }
    
    /**
     * 导出行为
     * @return mixed
     */
    public function export() {
        if (!$this->request->isAjax()) {
            $form = $this->service->getExportForm(url('export'), '');
            $this->assign('panel', $form);
            return $this->fetch(LAYOUT_PANEL);
        }
        $mid = intval($this->request->post('mid'));
        if ($mid < 1) {
            $this->error('所选模块不存在');
            return false;
        }
        $name = db('sys_module')->where([['id', 'EQ', $mid]])->field('name')->value('name');
        if (!$name) {
            $this->error('所选模块不存在');
            return false;
        }
        $where = ['mid' => $mid];
        $filed = 'id,mid,name,title,remark,rule,log,order,enable';
        $data = db('sys_action')->field($filed)->where($where)->order('order', 'asc')->select();
        if (!$data || !is_array($data) || count($data) < 1) {
            $this->error('该模块下没有行为');
            return false;
        }
        $path = APP_PATH . $name . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
        if (!is_dir($path)) {
            if (!mkdir($path, 0777, true)) {
                $this->error('创建目录失败');
                return false;
            }
        }
        $info = json_encode($data);
        $put = file_put_contents($path . 'action.json', $info);
        if (!$put) {
            $this->error('导出行为失败');
            return false;
        }
        $this->success('导出行为成功');
        return true;
    }
}