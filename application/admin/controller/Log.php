<?php
/**
 *  ==================================================================
 *        文 件 名: Log.php
 *        概    要: 系统日志控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/27 13:30
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

/**
 * Class Log - 系统日志控制器
 * @package app\admin\controller
 */
class Log extends Base {
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_log';
    
    /**
     * @var \app\admin\service\Log - 服务层
     */
    protected $service = null;
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\admin\service\Log();
    }
    
    /**
     * 系统日志列表
     * @param string $sort - 排序字段
     * @param string $order - 排序类型（asc/desc）
     * @param int $offset - 偏移量
     * @param int $limit - 每页条数
     * @param string $search - 搜索条件
     * @return bool|mixed|\think\response\Json
     */
    public function index($sort = 'l.id', $order = 'desc', $offset = 0, $limit = 10, $search = '') {
        if (!request()->isAjax()) {
            $table = $this->service->getTable();
            $this->assign('panel', $table);
            return $this->fetch('table');
        }
        $where = [];
        $allowSearchField = '';
        $where = array_merge($where, $this->service->getSearchParams($search, $allowSearchField));
        $field = 'l.id,a.title as action_name,u.n_name,l.aip,l.atime as log_time,m.title,l.remark';
        return $this->service->getPaginate($this->db, $where, $field, $offset, $limit, $sort, $order);
    }
    
    /**
     * 系统日志详情
     * @param int $id - 日志ID
     * @return mixed
     */
    public function detail($id = 0) {
        $id = intval($id);
        if ($id < 1) {
            $this->redirect('index');
        }
        $where = [[$this->db . '.id', 'EQ', $id]];
        $data = db()->view($this->db, true)
            ->view('sys_user', ['n_name' => 'user_name'], $this->db . '.uid = sys_user.id', 'LEFT')
            ->view('sys_action', ['title' => 'action_name', 'id' => 'action_id'], $this->db . '.aid = sys_action.id', 'LEFT')
            ->view('sys_module', ['title' => 'module_name', 'id' => 'module_id'], 'sys_action.mid = sys_module.id', 'LEFT')
            ->where($where)
            ->find();
        if (!$data || !is_array($data) || count($data) < 1) {
            $detail = [];
        } else {
            $detail = [
                '详情描述' => $data['remark'],
                '触发行为' => $data['action_name'] . '(行为ID:' . $data['action_id'] . ')',
                '所属模块' => $data['module_name'] . '(模块ID:' . $data['module_id'] . ')',
                '目标模型' => config('database.prefix') . $data['model'] . '(目标ID:' . $data['rid'] . ')',
                '执行者'  => $data['user_name'] . '(用户ID:' . $data['uid'] . ')',
                '执行时间' => date('Y-m-d H:i:s', $data['atime']),
                '执行IP' => $data['aip']
            ];
        }
        $assign = ['detail' => $detail];
        $this->assign($assign);
        return $this->fetch(LAYOUT_DETAIL);
    }
    
    /**
     * AJAX(POST) - 删除日志
     * @return bool|\think\response\Json
     */
    public function delete() {
        if (!$this->request->isAjax()) {
            return false;
        }
        $data = $this->request->post();
        if (isset($data['data']) && is_array($data['data']) && count($data) >= 1) {
            $id = $data['data'];
        } else if (isset($data['id'])) {
            $id = [intval($data['id'])];
        } else {
            $this->error('参数错误');
            return false;
        }
        foreach ($id as $idValue) {
            if (in_array($idValue, $this->disablePk)) {
                $this->error('所选日志中包含不允许删除的日志');
                return false;
            }
        }
        $where = [['id', 'IN', $id]];
        $del = db($this->db)->where($where)->delete();
        if (!$del) {
            $this->error('日志删除失败');
        } else {
            $this->success('日志删除成功');
        }
        return true;
    }
}