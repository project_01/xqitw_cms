<?php
/**
 *  ==================================================================
 *        文 件 名: Menu.php
 *        概    要: 菜单管理
 *        作    者: IT小强
 *        创建时间: 2017/9/27 17:26
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

use builder\KeFormBuilder;
use builder\KeTableBuilder;
use files\File;
use tree\Tree;

/**
 * Class Menu - 菜单管理
 * @package app\admin\controller
 */
class Menu extends Base {
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_power';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'power';
    
    /**
     * @var string - 添加/修改时的允许字段
     */
    protected $allowField = 'name,pid,mid,ico,controller,action,order,type,show,enable,debug';
    
    /**
     * @var array - 不允许被操作的记录主键
     */
    protected $disablePk = [];
    
    /**
     * 菜单列表
     * @return bool|mixed|\think\response\Json
     */
    public function index() {
        if (!$this->request->isAjax()) {
            $url = url('index');
            $editUrl = url('updateField');
            $config = ['search' => 'true', 'side_pagination' => 'client'];
            $table = $this->_getTable($url, $editUrl, $config);
            $this->assign('panel', $table);
            return $this->fetch(LAYOUT_PANEL);
        }
        $field = 'id,mid,pid,name,controller,show,enable,order,action,debug';
        $list = db($this->db)->field($field)->select();
        $list = Tree::config($this->treeConfig)->toList($list);
        $data = ['data' => $list];
        return $list ? json($data) : false;
    }
    
    /**
     * 添加菜单
     * @param int $pid - 父级菜单ID
     * @param int $mid - 所属模块ID
     * @return mixed
     */
    public function add($pid = 0, $mid = 1) {
        if (!$this->request->isAjax()) {
            $form = $this->_getForm(url('add'), url('index'), intval($mid), intval($pid));
            $this->assign('panel', $form);
            return $this->fetch(LAYOUT_PANEL);
        }
        // Ajax添加菜单操作
        $data = get_allow_data($this->allowField, $this->request->post());
        $validate = validate($this->validate);
        $check = $validate->scene('add')->check($data);
        if (!$check) {
            $this->error($validate->getError());
            return false;
        }
        $isWhere = [
            ['mid', 'EQ', $data['mid']],
            ['controller', 'EQ', $data['controller']],
            ['action', 'EQ', $data['action']]
        ];
        $is_set = db($this->db)->where($isWhere)->field('id')->count();
        if ($is_set >= 1) {
            $this->error('该菜单已经存在,请勿重复添加');
            return false;
        }
        $add = db($this->db)->field($this->allowField)->insertGetId($data);
        if (!$add) {
            $this->error('添加菜单失败');
            return false;
        }
        $this->success('添加菜单成功', url('index'));
        return true;
    }
    
    /**
     * 修改菜单
     * @param $id - 菜单ID
     * @return mixed
     */
    public function edit($id = 0) {
        $id = intval($id);
        if ($id == 0) {
            $this->redirect('add');
            return false;
        }
        // 非Ajax请求时输入模板
        if (!$this->request->isAjax()) {
            $where = [['id', 'EQ', $id]];
            $field = 'id,pid,mid,ico,name,controller,action,order,type,show,enable,debug';
            $data = db($this->db)->where($where)->field($field)->find();
            $url = url('edit', ['id' => $id]);
            $form = $this->_getForm($url, $url, 0, 0, $data);
            $this->assign('panel', $form);
            return $this->fetch(LAYOUT_PANEL);
        }
        // Ajax修改菜单操作
        $data = get_allow_data($this->allowField, $this->request->post());
        $validate = validate($this->validate);
        $check = $validate->scene('edit')->check($data);
        if (!$check) {
            $this->error($validate->getError());
        }
        $isWhere = [
            ['mid', 'EQ', $data['mid']],
            ['controller', 'EQ', $data['controller']],
            ['action', 'EQ', $data['action']],
            ['id', 'NEQ', $id]
        ];
        $is_set = db($this->db)->where($isWhere)->field('id')->count();
        if ($is_set >= 1) {
            $this->error('该菜单已经存在,请勿重复添加');
            return false;
        }
        $where = [['id', 'EQ', $id]];
        $update = db($this->db)->where($where)->update($data);
        if (!$update) {
            $this->error('修改菜单失败');
            return false;
        }
        $this->success('修改菜单成功');
        return true;
    }
    
    /**
     * AJAX(POST) - 删除菜单
     * @return bool|\think\response\Json
     */
    public function delete() {
        if (!$this->request->isAjax()) {
            return false;
        }
        $data = $this->request->post();
        if (isset($data['data']) && is_array($data['data']) && count($data) >= 1) {
            $id = $data['data'];
        } else if (isset($data['id'])) {
            $id = [intval($data['id'])];
        } else {
            $this->error('参数错误');
            return false;
        }
        $db = db($this->db);
        // 检查是否是否有下级菜单
        foreach ($id as $idValue) {
            if (in_array($idValue, $this->disablePk)) {
                $this->error('所选菜单中包含不允许删除的菜单');
                return false;
            }
            if ($db->where([['pid', 'EQ', $idValue]])->field('id')->value('id')) {
                $this->error('删除菜单失败：所选菜单中含有下级菜单');
                return false;
            }
        }
        $where = [['type', 'NEQ', 1], ['id', 'IN', $id]];
        $del = db($this->db)->where($where)->delete();
        if (!$del) {
            $this->error('菜单删除失败：所选菜单包含系统菜单，无法删除');
            return false;
        }
        $this->success('菜单删除成功');
        return true;
    }
    
    /**
     * 根据模块ID获取菜单列表
     * @param int $mid
     * @return array|\think\response\Json
     */
    public function getMenuListByMid($mid = 1) {
        $mid = ($this->request->isAjax()) ? $this->request->post('data') : $mid;
        $menuArr = db($this->db)
            ->where(['enable' => 1, 'mid' => $mid])
            ->field('id,pid,name')
            ->select();
        $menuArr = Tree::config($this->treeConfig)->toList($menuArr);
        $returnData = ['0' => '顶级菜单'];
        $ajaxData = [['value' => 0, 'desc' => '顶级菜单']];
        foreach ($menuArr as $k => $v) {
            $returnData[$v['id']] = $v['title_display'];
            $ajaxData[] = ['value' => $v['id'], 'desc' => $v['title_display']];
        }
        return ($this->request->isAjax()) ? json(['code' => 1, 'data' => $ajaxData]) : $returnData;
    }
    
    /**
     * 获取指定模块下的控制器列表
     * @param $mid - 模块ID
     * @return array
     */
    public function getController($mid = 1) {
        $mid = ($this->request->isAjax()) ? $this->request->post('data') : $mid;
        $moduleInfo = db('sys_module')->where([['id', 'EQ', $mid]])->field('name,sub_name')->find();
        $moduleName = isset($moduleInfo['name']) ? $moduleInfo['name'] : 'admin';
        $moduleOdd = isset($moduleInfo['sub_name']) ? $moduleInfo['sub_name'] : '';
        $controllerArr = get_controller_name($moduleName, $moduleOdd);
        $controllerList = format_array($controllerArr, 'value', 'desc');
        if (!$this->request->isAjax()) {
            return $controllerList;
        }
        if (!$controllerArr || !is_array($controllerArr) || count($controllerArr) < 1) {
            $data = ['code' => -1, 'msg' => 'error'];
        } else {
            $data = ['code' => 1, 'data' => $controllerArr];
        }
        return json($data);
    }
    
    /**
     * 获取指定控制器下的方法列表
     * @param string $controllerName - 控制器名
     * @return array
     */
    public function getAction($controllerName = 'admin-Index') {
        $controllerName = ($this->request->isAjax()) ? $this->request->post('data') : $controllerName;
        $controllerName = empty($controllerName) ? 'admin-Index' : $controllerName;
        $data = explode('-', $controllerName);
        $module = $data[0];
        if (isset($data[2])) {
            $controller = $data[1] . '/' . $data[2];
        } else {
            $controller = $data[1];
        }
        $fileContent = File::read_file(APP_PATH . $module . '/controller/' . $controller . '.php');
        $fileContent = preg_match_all('/function(.*?)\(/', $fileContent, $match);
        $returnData = [];
        $ajaxData = [];
        if (!$fileContent) {
            return $returnData;
        }
        foreach ($match[1] as $k => $v) {
            if (!empty($v)) {
                $item = str_replace(' ', '', $v);
                $returnData[$item] = $item;
                $ajaxData[] = ['value' => $item, 'desc' => $item];
            }
        }
        $returnData['default'] = 'default';
        $returnData['updateField'] = 'updateField';
        $ajaxData[] = ['value' => 'default', 'desc' => 'default'];
        $ajaxData[] = ['value' => 'updateField', 'desc' => 'updateField'];
        return ($this->request->isAjax()) ? json(['code' => 1, 'data' => $ajaxData]) : $returnData;
    }
    
    /**
     * 添加/编辑页面表单统一生成
     * @param string $url - 表单提交地址
     * @param string $successUrl - 提交成功跳转地址
     * @param int $mid - 模块ID
     * @param int $pid - 权限父级ID
     * @param null $data - 权限数据
     * @return mixed
     */
    private function _getForm($url, $successUrl, $mid = 1, $pid = 0, $data = NUll) {
        $submitBtn = $data == NULL ? '添加菜单' : '修改菜单';
        // 处理字段
        $mid = isset($data['mid']) ? intval($data['mid']) : $mid;
        $pid = isset($data['pid']) ? intval($data['pid']) : $pid;
        $controller = isset($data['controller']) ? $data['controller'] : '';
        // 拉取控制器列表
        $controllerList = $this->getController($mid);
        // 拉取方法列表
        $actionList = $this->getAction($controller);
        
        // 拉取模块列表
        $moduleArr = db('sys_module')
            ->where(['enable' => 1])
            ->field('id,title')
            ->select();
        $moduleList = format_array($moduleArr, 'id', 'title');
        // 拉取菜单列表
        $menuArr = db($this->db)
            ->where(['enable' => 1, 'mid' => $mid])
            ->field('id,pid,name')
            ->select();
        $menuArr = Tree::config($this->treeConfig)->toList($menuArr);
        $menuList = cm_array_merge(['0' => '顶级菜单'], format_array($menuArr, 'id', 'title_display'));
        // name字段验证
        $nameValidate = [
            'notEmpty' => ['message' => '菜单名称不能为空'],
        ];
        // controller字段验证
        $controllerValidate = [
            'notEmpty' => ['message' => '控制器名不能为空'],
            'regexp'   => [
                'regexp'  => '/^[a-zA-Z0-9_\-\.]+$/',
                'message' => '控制器名只能由字母、数字、下划线、破折号和点组成'
            ],
        ];
        // action字段验证
        $actionValidate = [
            'notEmpty' => ['message' => '方法名称不能为空'],
            'regexp'   => [
                'regexp'  => '/^[a-zA-Z0-9_]+$/',
                'message' => '方法名称只能由字母、数字和下划线组成'
            ],
        ];
        // order字段验证
        $orderValidate = [
            'notEmpty' => ['message' => '排序数值不能为空'],
            'numeric'  => ['message' => '排序数值只能为数字'],
        ];
        // 创建表单
        $form = KeFormBuilder::makeForm($url, 2, $data)
            ->addSelect('mid', $mid, $moduleList, '所属模块')
            ->addSelect('pid', $pid, $menuList, '上级菜单')
            ->addLinkage('mid', 'pid', url('getMenuListByMid'))
            ->addLinkage('mid', 'controller', url('getController'))
            ->addLinkage('controller', 'action', url('getAction'))
            ->addText('name', '', '菜单名称', $nameValidate)
            ->addIco('ico', '', '菜单图标')
            ->addSelect('controller', $controller, $controllerList, '控制器名', $controllerValidate)
            ->addSelect('action', '', $actionList, '方法名称', $actionValidate)
            ->addText('order', 0, '排序数值', $orderValidate)
            ->addRadio('type', 1, ['1' => '系统菜单', '2' => '用户添加', '3' => '其他类型'], '菜单类型')
            ->addSwitch('show', 1, [1, 2], '是否展示')
            ->addSwitch('enable', 1, [1, 2], '是否启用')
            ->addSwitch('debug', 1, [1, 2], '是否仅调试模式显示')
            ->addSubmitBtn($submitBtn)
            ->addResetBtn()
            ->validateForm($successUrl)
            ->returnForm();
        return $form;
    }
    
    /**
     * 获取菜单页表格
     * @param $url
     * @param bool $editable_url
     * @param array $config
     * @return mixed
     */
    private function _getTable($url, $editable_url = false, $config = []) {
        
        // 拉取模块列表
        $moduleList = db('sys_module')->field('id,name,title')->select();
        $midSelect = [
            'type'   => 'select',
            'source' => get_select_list($moduleList, 'id', 'title')
        ];
        // 创建表格
        $table = KeTableBuilder::makeTable($url, $editable_url, $config)
            ->addCheckbox()
            ->addTextColumn('id', 'ID', 'text-center')
            ->addTextColumn('title_display', '菜单名称', 'text-left', [], 'true')
            ->addTextColumn('mid', '所属模块', 'text-center', $midSelect, 'true')
            ->addTextColumn('controller', '控制器', 'text-left', ['type' => 'text'], 'true')
            ->addTextColumn('action', '操作名', 'text-left', ['type' => 'text'], 'true')
            ->addTextColumn('order', '排序值', 'text-center', ['type' => 'text'], 'true')
            ->addSwitchColumn('show', url('updateField'), '是否展示', 'hidden-xs')
            ->addSwitchColumn('enable', url('updateField'), '是否启用', 'hidden-xs')
            ->addSwitchColumn('debug', url('updateField'), '是否仅调试模式显示', 'hidden-xs')
            ->addEditColumn('id', url('edit'), url('delete'), '编辑', '确定要删除该菜单吗?')
            ->addLinkBtn(url('add'), '添加', 'edit', 'btn-success', '菜单')
            ->addAjaxAllBtn('删除', url('delete'), url('index'), '菜单', '确定要删除所选菜单吗?')
            ->returnTable();
        return $table;
    }
}