<?php
/**
 *  ==================================================================
 *        文 件 名: Database.php
 *        概    要: 数据库管理控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/26 22:20
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

/**
 * Class Database - 数据库管理控制器
 * @package app\admin\controller
 */
class Database extends Base {
    
    /**
     * @var string - 数据库备份路径
     */
    protected static $dbBackupPath = '';
    
    /**
     * @var \app\admin\service\Database - 服务层
     */
    protected $service = null;
    
    /**
     * 初始化
     */
    protected function initialize() {
        parent::initialize();
        self::$dbBackupPath = BASE_ROOT . 'data' . DIRECTORY_SEPARATOR . 'sql';
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\admin\service\Database();
    }
    
    /**
     *  数据表信息列表
     * @return mixed|\think\response\Json
     */
    public function index() {
        if ($this->request->isAjax()) {
            $data_list = db()->query("SHOW TABLE STATUS");
            $data_list = array_map('array_change_key_case', $data_list);
            $data = [
                'data'  => $data_list,
                'total' => count($data_list)
            ];
            return json($data);
        }
        $url = url('index');
        $config = ['search' => 'true', 'id_field' => 'name', 'side_pagination' => 'client'];
        $table = $this->service->getBackupTable($url, false, $config);
        $assign = ['panel' => $table];
        $this->assign($assign);
        return $this->fetch('table');
    }
    
    /**
     * 备份文件信息列表
     * @return mixed|\think\response\Json
     */
    public function reply() {
        if ($this->request->isAjax()) {
            $rows = $this->service->getBackupDataInfo(self::$dbBackupPath);
            $data = ['data' => $rows, 'total' => count($rows)];
            return json($data);
        }
        $url = url('reply');
        $config = ['search' => 'true', 'id_field' => 'name', 'side_pagination' => 'client'];
        $table = $this->service->getReplyTable($url, false, $config);
        $assign = ['panel' => $table];
        $this->assign($assign);
        return $this->fetch('table');
    }
    
    /**
     * 备份数据表
     * @param int $start - 起始行数
     * @return bool
     */
    public function export($start = 0) {
        if (!$this->request->isAjax()) {
            return false;
        }
        
        // 获取数据表
        $data = $this->request->post();
        $tables = isset($data['data']) ? $data['data'] : false;
        if (!$tables) {
            $this->error('请选择要备份的数据表');
            return false;
        }
        if (is_string($tables)) {
            $tables = explode(',', $tables);
        }
        
        // 初始化
        $path = self::$dbBackupPath;
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        
        // 读取备份配置
        $part_size = config('data_backup_part_size');
        $compress = config('data_backup_compress');
        $compress_level = config('data_backup_compress_level');
        $config = array(
            'path'     => realpath($path) . DIRECTORY_SEPARATOR,
            'part'     => $part_size ? $part_size : '20971520',
            'compress' => ($compress == 1) ? true : false,
            'level'    => $compress_level ? $compress_level : 1,
        );
        
        // 检查是否有正在执行的任务
        $lock = $config['path'] . 'backup.lock';
        if (is_file($lock)) {
            $this->error('检测到有一个备份任务正在执行，请稍后再试！');
            return false;
        }
        
        // 创建锁文件
        file_put_contents($lock, $this->request->time());
        
        // 检查备份目录是否可写
        if (!is_writeable($config['path'])) {
            $this->error('备份目录不存在或不可写，请检查后重试！');
            return false;
        }
        
        // 生成备份文件信息
        $file = [
            'name' => date('Y_m_d_His', $this->request->time()),
            'part' => 1
        ];
        
        // 创建备份文件
        $db = new \database\Database($file, $config);
        if (false === $db->create()) {
            $this->error('初始化失败，备份文件创建失败！');
            return false;
        }
        $export = $db->backupAll($tables, $start);
        if (!$export) {
            $this->error('备份出错，请稍后再试');
            return false;
        }
        // 备份完成，删除锁定文件
        @unlink($lock);
        $this->success('数据表备份完成！');
        return true;
    }
    
    /**
     * 还原数据表
     * @return bool
     */
    public function import() {
        if (!$this->request->isAjax()) {
            return false;
        }
        // 获取文件
        $data = $this->request->post();
        $files = isset($data['data']) ? $data['data'] : false;
        if (!$files) {
            $this->error('请选择要还原的文件');
        }
        if (is_string($files)) {
            $files = explode(',', $files);
        }
        $path = self::$dbBackupPath . DIRECTORY_SEPARATOR;
        $fileList = [];
        foreach ($files as $v) {
            $info = $this->service->getFormatFileInfo($path, $v);
            $temp = [
                'path'     => $info['path'],
                'compress' => ($info['compress'] == '未压缩') ? false : true
            ];
            $fileList[] = $temp;
        }
        $db = new \database\Database([], []);
        $import = $db->importAll($fileList);
        if (!$import) {
            $this->error('还原失败，请稍后再试');
        }
        $this->success('还原成功');
        return true;
    }
    
    /**
     * 优化数据表
     * @return bool
     */
    public function optimize() {
        if (!$this->request->isAjax()) {
            return false;
        }
        $tables = $this->service->getTableName();
        if (!$tables) {
            $this->error("请选择要优化的表！");
        }
        $list = db()->query("OPTIMIZE TABLE `{$tables}`");
        if ($list) {
            $this->success("数据表优化完成！");
        } else {
            $this->error("数据表优化出错请重试！");
        }
        return true;
    }
    
    /**
     * 修复数据表
     * @return bool
     */
    public function repair() {
        if (!$this->request->isAjax()) {
            return false;
        }
        $tables = $this->service->getTableName();
        if (!$tables) {
            $this->error("请选择要修复的表！");
        }
        $list = db()->query("REPAIR TABLE `{$tables}`");
        if ($list) {
            $this->success("数据表修复完成！");
        } else {
            $this->error("数据表修复出错！");
        }
        return true;
    }
    
    /**
     * 删除备份文件
     * @return bool
     */
    public function delete() {
        if (!$this->request->isAjax()) {
            return false;
        }
        $data = $this->request->post();
        $files = isset($data['data']) ? $data['data'] : false;
        if (!$files) {
            $this->error('请选择要删除的文件');
        }
        if (is_string($files)) {
            $files = explode(',', $files);
        }
        if (!is_array($files) || count($files) < 1) {
            $this->error('参数错误');
        }
        $add = 0;
        $less = 0;
        $path = self::$dbBackupPath;
        foreach ($files as $item) {
            if (@unlink($path . DIRECTORY_SEPARATOR . $item)) {
                $add++;
            } else {
                $less++;
            }
        }
        if ($less == 0) {
            $this->success('删除备份文件成功');
        } else if ($add == 0) {
            $this->error('删除备份文件失败');
        } else {
            $this->success('共' . $add . '个删除成功 ' . $less . '个删除失败');
        }
        return true;
    }
    
    /**
     * 下载备份的数据表文件
     * @param file - 指定要下载的sql文件名（GET）
     * @return bool
     */
    public function download() {
        $file = input('param.file', '');
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $file = str_replace('.' . $ext, '', $file);
        //var_dump($ext,$file);exit();
        try {
            download_file($file, self::$dbBackupPath, $ext);
        } catch (\Exception $exception) {
            $this->error('下载失败');
            return false;
        }
        return true;
    }
}