<?php

/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 系统模块基类控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/13 11:50
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

use app\common\controller\Admin;

/**
 * Class Base - 系统模块基类控制器
 * @package app\admin\controller
 */
abstract class Base extends Admin {

}