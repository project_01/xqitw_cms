<?php
/**
 *  ==================================================================
 *        文 件 名: Plugin.php
 *        概    要: 插件管理控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/21 15:49
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

use app\admin\model\SysPlugincon;
use builder\KeTableBuilder;
use database\Sql;

/**
 * Class Plugin - 插件管理控制器
 * @package app\admin\controller
 */
class Plugin extends Base {
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_plugin';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'plugin';
    
    /**
     * @var string - 插件目录
     */
    protected $pluginPath = '';
    
    /**
     * @var \app\admin\service\Plugin - 服务层
     */
    protected $service = null;
    
    /**
     * 初始化
     */
    protected function initialize() {
        parent::initialize();
        $this->pluginPath = config('plugin_path');
    }
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\admin\service\Plugin();
    }
    
    /**
     * 插件列表页
     * @param string $group - 插件分组
     * @param int $enable - 插件状态
     * @return mixed|\think\response\Json
     */
    public function index($group = 'local', $enable = 0) {
        $enable = intval($enable);
        switch ($group) {
            case 'local':
                $data_list = $this->service->getPluginInfo($this->db, $this->pluginPath);
                break;
            case 'online':
                $data_list = [];
                break;
            default:
                $data_list = $this->service->getPluginInfo($this->db, $this->pluginPath);
        }
        $all_num = count($data_list);
        $enable_num = 0;
        $disable_num = 0;
        $uninstall_num = 0;
        foreach ($data_list as $item) {
            $status = isset($item['enable']) ? intval($item['enable']) : 0;
            switch ($status) {
                case 1:
                    $enable_num++;
                    break;
                case 2:
                    $disable_num++;
                    break;
                case 3:
                    $uninstall_num++;
            }
        }
        if (request()->isAjax()) {
            switch ($enable) {
                case 1:
                    $list = $this->service->getPluginInfoByStatus(1, $data_list);
                    break;
                case 2:
                    $list = $this->service->getPluginInfoByStatus(2, $data_list);
                    break;
                case 3:
                    $list = $this->service->getPluginInfoByStatus(3, $data_list);
                    break;
                default:
                    $list = $data_list;
            }
            $data = ['data' => $list, 'total' => count($list)];
            return json($data);
        }
        $url = url('index', ['group' => $group, 'enable' => $enable]);
        $config = ['search' => 'true', 'id_field' => 'name', 'side_pagination' => 'client'];
        $all_url = url('index', ['group' => $group, 'enable' => 0]);
        $enable_url = url('index', ['group' => $group, 'enable' => 1]);
        $disable_url = url('index', ['group' => $group, 'enable' => 2]);
        $uninstall_url = url('index', ['group' => $group, 'enable' => 3]);
        $table = KeTableBuilder::makeTable($url, url('updateField'), $config)
            ->addTextColumn('title', '插件名称')
            ->addTextColumn('name', '插件标识')
            ->addTextColumn('describe', '插件描述')
            ->addTextColumn('author', '插件作者', 'text-center')
            ->addTextColumn('version', '插件版本', 'text-center')
            ->addTextColumn('order', '插件排序', 'text-center', ['type' => 'text'], true)
            ->addTextColumn('name', '操作', 'text-center', [], false, 'editPlugins')
            ->addLinkBtn(url('upload'), '上传', 'upload', 'btn-default', '插件')
            ->addLinkBtn($all_url, '全部(' . $all_num . ')', '', 'btn-success')
            ->addLinkBtn($enable_url, '已启用(' . $enable_num . ')', '', 'btn-info')
            ->addLinkBtn($disable_url, '已禁用(' . $disable_num . ')', '', 'btn-danger')
            ->addLinkBtn($uninstall_url, '未安装(' . $uninstall_num . ')', '', 'btn-warning')
            ->returnTable();
        $groupList = [
            ['id' => 'local', 'name' => '本地插件'],
            ['id' => 'online', 'name' => '插件商城'],
        ];
        $assign = [
            'tabs_param'   => 'group',
            'tabs_id'      => 'id',
            'tabs_name'    => 'name',
            'tabs_url'     => 'admin/plugin/index',
            'tabs_this_id' => $group,
            'tabs'         => $groupList,
            'panel'        => $table,
        ];
        $this->assign($assign);
        return $this->fetch('tabs');
    }
    
    public function upload() {
        // TODO 上传本地插件功能待完成
    }
    
    /**
     * 修改插件配置信息
     * @param $name - 插件标识
     * @return bool|mixed
     */
    public function edit($name) {
        $where = [['name', 'EQ', $name]];
        $db = db($this->db);
        $check = $db->where($where)->field('id,title')->find();
        $id = get_array_data('id', $check, 0);
        $title = get_array_data('title', $check, 0);
        if (intval($id) < 1) {
            $this->error('插件不存在或者未安装');
            return false;
        }
        if (!request()->isAjax()) {
            $editUrl = url('edit', ['name' => $name]);
            $data = db('sys_plugincon')->where([['pid', 'EQ', $id]])->field(true)->select();
            if (!is_array($data) || count($data) < 1) {
                $this->error('该插件无配置信息');
                return false;
            }
            $form = $this->service->getForm($name, $editUrl, $editUrl, $data);
            $assign = [
                'plugin_title' => $title,
                'panel'        => $form
            ];
            $this->assign($assign);
            return $this->fetch();
        }
        $data = request()->post();
        if (!is_array($data) || count($data) < 1) {
            $this->error('参数错误');
            return false;
        }
        cache('system_plugin_' . $name, null);
        $update = SysPlugincon::saveAllConfig($id, $data);
        return $update;
    }
    
    /**
     * 插件管理
     * @param $name - 插件名称
     * @param string $controller - 执行控制器名
     * @param string $action - 执行方法名
     * @param string $param - 额外的参数
     * @return mixed
     */
    public function manage($name, $controller = 'Index', $action = 'index', $param = '') {
        $where = [['name', 'EQ', $name]];
        $db = db($this->db);
        $check = $db->where($where)->field('id,title')->find();
        $id = get_array_data('id', $check, 0);
        $title = get_array_data('title', $check, 0);
        if (intval($id) < 1) {
            $this->error('插件不存在或者未安装');
            return false;
        }
        // 拼装插件管理控制器
        $pluginClass = 'plugins\\' . hump_to_underline($name) . '\\controller\\' . $controller;
        // 检查控制器是否存在
        if (!class_exists($pluginClass)) {
            $this->error('控制器[' . $pluginClass . ']不存在');
            return false;
        }
        // 实例化控制器
        $plugin = new $pluginClass(request(), app());
        // 检测要执行的操作是否存在
        if (!method_exists($plugin, $action)) {
            $this->error('执行操作[' . $action . ']不存在');
            return false;
        }
        
        // 执行AJAX请求
        if (request()->isAjax()) {
            if (!empty($param)) {
                return $plugin->$action($param);
            } else {
                return $plugin->$action();
            }
        }
        // 执行非AJAX请求
        $assign = [
            'plugin_title'    => $title,
            'this_name'       => $name,
            'this_controller' => $controller,
            'this_action'     => $action,
            'this_param'      => $param,
        ];
        // 获取插件管理页面菜单项
        $tabGroup = $this->service->getPluginInfoByFile($this->pluginPath, $name);
        if ($tabGroup['code'] == 1 && isset($tabGroup['tab'])) {
            $assign['group'] = $tabGroup['tab'];
        }
        if (!empty($param)) {
            $assign['content'] = $plugin->$action($param);
        } else {
            $assign['content'] = $plugin->$action();
        }
        $this->assign($assign);
        return $this->fetch();
    }
    
    /**
     * 插件详情详情
     * @param string $name - 插件名称
     * @return mixed
     */
    public function detail($name) {
        $install = true;
        $where = [['name', 'EQ', $name]];
        $db = db($this->db);
        $data = $db->where($where)->field(true)->find();
        if (!$data || !is_array($data) || count($data) < 1) {
            $data = $this->service->getPluginInfoByFile($this->pluginPath, $name);
            $install = false;
            if ($data['code'] != 1) {
                $this->error('获取插件信息失败');
                return false;
            }
            $data = $data['info'];
        }
        $author_url = get_array_data('author_url', $data);
        $update_url = get_array_data('update_url', $data);
        $admin = get_array_data('admin', $data, 2);
        $enable = get_array_data('admin', $data, 2);
        $detail = [
            '插件标识'   => get_array_data('name', $data),
            '插件名称'   => get_array_data('title', $data),
            '插件图标'   => '<i class="' . get_array_data('ico', $data) . '"></i>',
            '插件描述'   => get_array_data('describe', $data),
            '插件版本'   => get_array_data('version', $data),
            '插件作者'   => get_array_data('author', $data),
            '作者主页'   => [
                'href'  => $author_url,
                'value' => $author_url,
            ],
            '插件主页'   => [
                'href'  => $update_url,
                'value' => $update_url,
            ],
            '插件钩子'   => get_array_data('hook', $data),
            '是否有配置项' => $admin == 1 ? '有配置项' : '无配置项',
        ];
        if ($install) {
            $detail['是否启用'] = $enable == 1 ? '启用' : '禁用';
            $detail['安装时间'] = date('Y-m-d H:i:s', get_array_data('atime', $data, 0));
            $detail['更新时间'] = date('Y-m-d H:i:s', get_array_data('utime', $data, 0));
        }
        $assign = ['detail' => $detail];
        $this->assign($assign);
        return $this->fetch(LAYOUT_DETAIL);
    }
    
    /**
     * （AJAX）启用插件
     * @return bool
     */
    public function enable() {
        if (!request()->isAjax()) {
            return false;
        }
        $name = input('param.name', '');
        $where = [['name', 'EQ', $name]];
        $data = ['utime' => time(), 'enable' => 1];
        $update = db($this->db)->where($where)->field('utime,enable')->update($data);
        if (!$update) {
            $this->error('插件 [' . $name . '] 启用失败');
            return false;
        }
        cache('system_plugin', null);
        cache('system_plugin_' . $name, null);
        $this->success('插件 [' . $name . '] 启用成功');
        return true;
    }
    
    /**
     * （AJAX）禁用插件
     * @return bool
     */
    public function disable() {
        if (!request()->isAjax()) {
            return false;
        }
        $name = input('param.name', '');
        $where = [['name', 'EQ', $name]];
        $data = ['utime' => time(), 'enable' => 2];
        $update = db($this->db)->where($where)->field('utime,enable')->update($data);
        if (!$update) {
            $this->error('插件 [' . $name . '] 禁用失败');
            return false;
        }
        cache('system_plugin', null);
        cache('system_plugin_' . $name, null);
        $this->success('插件 [' . $name . '] 禁用成功');
        return true;
    }
    
    /**
     * 安装插件
     * @return bool
     */
    public function install() {
        $name = input('param.name', '');
        if (!$this->request->isAjax() || empty($name)) {
            $this->error('非法请求');
            return false;
        }
        if (in_array($name, $this->disablePk)) {
            $this->error('插件[' . $name . ']不允许被操作');
            return false;
        }
        
        // 获取插件信息
        $pluginInfo = $this->service->getPluginInfoByFile($this->pluginPath, $name);
        if ($pluginInfo['code'] != 1) {
            $this->error($pluginInfo['msg']);
            return false;
        }
        
        // 模块依赖检查
        $needModule = get_array_data('need_module', $pluginInfo, false);
        if (is_array($needModule) && count($needModule) >= 1) {
            $checkNeedModule = checkDependence('module', $needModule);
            if ($checkNeedModule !== true) {
                $this->error($checkNeedModule['msg']);
                return false;
            }
        }
        
        // 插件依赖检查
        $needPlugin = get_array_data('need_plugin', $pluginInfo, false);
        if (is_array($needPlugin) && count($needPlugin) >= 1) {
            $checkNeedPlugin = checkDependence('plugin', $needPlugin);
            if ($checkNeedPlugin !== true) {
                $this->error($checkNeedPlugin['msg']);
                return false;
            }
        }
        
        // 获取插件类
        $plugin_class = get_plugin_class($name, false);
        if (!class_exists($plugin_class)) {
            $this->error('插件不存在！');
            return false;
        }
        
        // 数据验证
        $validate = validate($this->validate);
        $check = $validate->scene('install')->check(get_array_data('info', $pluginInfo, []));
        if (!$check) {
            $this->error($validate->getError());
            return false;
        }
        
        // 实例化插件
        $plugin = new $plugin_class(request(), app());
        // 插件预安装
        if (!$plugin->install()) {
            $this->error('插件预安装失败!原因：' . $plugin->getError());
            return false;
        }
        
        // 执行安装插件sql文件
        $sql_file = realpath(config('plugin_path') . hump_to_underline($name) . '/sql/install.sql');
        if (is_file($sql_file)) {
            if (isset($plugin->database_prefix) && $plugin->database_prefix != '') {
                $sql_statement = Sql::getSqlFromFile($sql_file, false, [$plugin->database_prefix => config('database.prefix')]);
            } else {
                $sql_statement = Sql::getSqlFromFile($sql_file);
            }
            if (!empty($sql_statement)) {
                foreach ($sql_statement as $value) {
                    $exec = db()->execute($value);
                    if ($exec === false) {
                        $this->error('插件自带安装SQL执行失败');
                        return false;
                    }
                }
            }
        }
        
        // 写入插件信息
        $data = $pluginInfo['info'];
        $data['atime'] = time();
        $data['utime'] = $data['atime'];
        $data['enable'] = 2;
        $id = db($this->db)->field(true)->insertGetId($data);
        if ($id < 1) {
            $this->error('插件 [' . $name . '] 安装失败');
            return false;
        }
        
        // 写入插件的配置信息
        if (isset($pluginInfo['var']) && !empty($pluginInfo['var'])) {
            if (!SysPlugincon::installPluginConfig($id, $pluginInfo['var'])) {
                db($this->db)->where([['id', 'EQ', $id]])->delete();
                $this->error('插件 [' . $name . '] 安装失败');
                return false;
            }
        }
        
        cache('system_plugin', null);
        cache('system_plugin_' . $name, null);
        $this->success('插件 [' . $name . '] 安装成功');
        return true;
    }
    
    /**
     * 卸载插件
     * @return bool
     */
    public function uninstall() {
        $name = input('param.name', '');
        if (!$this->request->isAjax() || empty($name)) {
            $this->error('非法请求');
            return false;
        }
        if (in_array($name, $this->disablePk)) {
            $this->error('插件[' . $name . ']不允许被操作');
            return false;
        }
        $where = [['name', 'EQ', $name]];
        $check = db($this->db)->where($where)->field('id,enable')->find();
        if (!$check) {
            $this->error('插件不存在');
            return false;
        }
        $id = $check['id'];
        $enable = $check['enable'];
        if ($enable == 1) {
            $this->error('请在禁用插件后再卸载');
            return false;
        }
        
        // 获取插件类
        $plugin_class = get_plugin_class($name, false);
        if (!class_exists($plugin_class)) {
            $this->error('插件不存在！');
            return false;
        }
        // 实例化插件
        $plugin = new $plugin_class(request(), app());
        // 插件预卸载
        if (!$plugin->uninstall()) {
            $this->error('插件预卸载失败!原因：' . $plugin->getError());
            return false;
        }
        // 执行安装插件sql文件
        $sql_file = realpath(config('plugin_path') . $name . '/sql/uninstall.sql');
        if (is_file($sql_file)) {
            if (isset($plugin->database_prefix) && $plugin->database_prefix != '') {
                $sql_statement = Sql::getSqlFromFile($sql_file, false, [$plugin->database_prefix => config('database.prefix')]);
            } else {
                $sql_statement = Sql::getSqlFromFile($sql_file);
            }
            if (!empty($sql_statement)) {
                foreach ($sql_statement as $value) {
                    $exec = db()->execute($value);
                    if ($exec === false) {
                        $this->error('插件自带卸载SQL执行失败');
                        return false;
                    }
                }
            }
        }
        
        $delConfig = db('sys_plugincon')->where([['pid', 'EQ', $id]])->delete();
        $delPlugin = db($this->db)->where([['id', 'EQ', $id]])->delete();
        if (!$delPlugin || !$delConfig) {
            $this->error('插件 [' . $name . '] 卸载失败');
        }
        cache('system_plugin', null);
        cache('system_plugin_' . $name, null);
        $this->success('插件 [' . $name . '] 卸载成功');
        return true;
    }
    
    /**
     * AJAX（POST）快速修改字段值
     * @param int $pk - 模块标识
     * @param string $name - 修改字段名
     * @param string $value - 修改字段值
     * @return bool|array|string
     */
    public function updateField($pk, $name, $value = '') {
        if (!$pk || !$name || !request()->isAjax()) {
            return false;
        }
        $allowField = ['order'];
        if (!in_array($name, $allowField)) {
            echo '错误：';
            $msg = '不允许修改此内容';
            return strip_tags($msg);
        }
        
        $data = [$name => $value];
        $validate = validate($this->validate);
        $check = $validate->scene($name)->check($data);
        if (!$check) {
            echo '错误：';
            $msg = strval($validate->getError());
            return strip_tags($msg);
        }
        
        $where = [['name', 'EQ', $pk]];
        $update = db($this->db)->where($where)->field($name)->setField($name, $value);
        if (!$update) {
            $msg = '修改失败';
            echo '错误：';
            return $msg;
        }
        cache('system_plugin', null);
        cache('system_plugin_' . $name, null);
        return ['code' => 1, 'msg' => '修改成功'];
    }
}