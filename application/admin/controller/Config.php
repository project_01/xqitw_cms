<?php
/**
 *  ==================================================================
 *        文 件 名: Config.php
 *        概    要: 配置管理
 *        作    者: IT小强
 *        创建时间: 2017/9/15 15:43
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

use app\admin\model\SysConfig;
use builder\KeFormBuilder;

/**
 * Class Config - 配置管理
 * @package app\admin\controller
 */
class Config extends Base {
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_config';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'config';
    
    /**
     * @var array - 不允许被操作的记录主键
     */
    protected $disablePk = [];
    
    /**
     * @var string - 添加/修改时的允许字段
     */
    protected $allowField = 'id,cid,name,value,describe,type,list,tip';
    
    /**
     * @var \app\admin\service\Config - 服务层
     */
    protected $service = null;
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\admin\service\Config();
    }
    
    /**
     * 配置列表
     * @return mixed
     */
    public function index() {
        if (!request()->isAjax()) {
            $url = url('index');
            $config = ['search' => 'true', 'side_pagination' => 'client'];
            $table = $this->service->getTable($url, false, $config);
            $assign = ['panel' => $table];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        // 获取配置类型
        $configType = get_config_level_one('form_type');
        // 查询配置分类列表
        $cate_where = [['enable', 'EQ', 1], ['pid', 'EQ', 0]];
        $cate_field = 'name,id';
        $configCategory = db('sys_concate')->where($cate_where)->field($cate_field)->select();
        $configCategory = format_array($configCategory, 'id', 'name');
        // 获取配置列表项
        $where = [['enable', 'EQ', 1]];
        $field = 'id,cid,name,value,describe,type,list,tip';
        $configList = db($this->db)->where($where)->field($field)->order('id','desc')->select();
        foreach ($configList as $k => $v) {
            $configList[$k]['cate'] = $configCategory[$v['cid']];
            $configList[$k]['type'] = $configType[$v['type']];
        }
        $data = [
            'data'  => $configList,
            'total' => count($configList)
        ];
        return json($data);
    }
    
    /**
     * 配置修改列表
     * @param int $cid - 配置分类ID
     * @return mixed
     */
    public function lists($cid = 1) {
        if (request()->isAjax()) {
            return SysConfig::saveAllConfig();
        }
        
        // 查询配置分类列表
        $cate_where = [['enable', 'EQ', 1], ['pid', 'EQ', 0]];
        $cate_field = 'name,id';
        $config_category = db('sys_concate')->where($cate_where)->field($cate_field)->select();
        
        // 获取默认cid
        $defaultCid = isset($config_category[0]['id']) ? $config_category[0]['id'] : 1;
        
        // 获取配置分类ID
        $cid = intval($cid) < 1 ? $defaultCid : intval($cid);
        
        // 获取配置列表项
        $where = [
            ['cid', 'EQ', $cid],
            ['enable', 'EQ', 1]
        ];
        $field = 'name,value,describe,type,list,tip';
        $configList = db($this->db)
            ->where($where)
            ->field($field)
            ->order('order', 'ASC')
            ->select();
        //配置列表项表单输出
        if ($configList) {
            $form = KeFormBuilder::makeForm(url('lists'), 2)
                ->addItems($configList)
                ->addsubmitBtn('修改配置')
                ->addResetBtn()
                ->validateForm()
                ->returnForm('form');
            $this->assign('panel', $form);
        }
        
        // 模板变量设置并输出模板
        $assign = [
            'tabs_param'   => 'cid',
            'tabs_id'      => 'id',
            'tabs_name'    => 'name',
            'tabs_url'     => 'admin/config/lists',
            'tabs_this_id' => $cid,
            'tabs'         => $config_category,
        ];
        $this->assign($assign);
        return $this->fetch(LAYOUT_TABS);
    }
    
    /**
     * 添加新的配置项
     * @return mixed|\think\response\Json
     */
    public function add() {
        if (request()->isAjax()) {
            return json(SysConfig::addConfig());
        }
        $form = $this->service->getForm(url('add'), url('lists'));
        $assign = ['panel' => $form];
        $this->assign($assign);
        return $this->fetch(LAYOUT_PANEL);
    }
    
    /**
     * 修改配置项
     * @param $id - 配置ID
     * @return mixed
     */
    public function edit($id = 0) {
        $id = intval($id);
        if ($id == 0) {
            $this->redirect('add');
        }
        // 非Ajax请求时输入模板
        if (!request()->isAjax()) {
            $where = [['id', 'EQ', $id]];
            $field = 'id,cid,name,value,describe,type,list,tip';
            $data = db($this->db)->where($where)->field($field)->find();
            $url = url('edit', ['id' => $id]);
            $form = $this->service->getForm($url, $url, $data);
            $assign = ['panel' => $form];
            $this->assign($assign);
            return $this->fetch(LAYOUT_PANEL);
        }
        return json(SysConfig::editConfig($id));
    }
    
    /**
     * AJAX(POST) - 删除配置
     * @return bool|\think\response\Json
     */
    public function delete() {
        if (!$this->request->isAjax()) {
            return false;
        }
        $data = $this->request->post();
        if (isset($data['data']) && is_array($data['data']) && count($data) >= 1) {
            $id = $data['data'];
        } else if (isset($data['id'])) {
            $id = [intval($data['id'])];
        } else {
            $this->error('参数错误');
            return false;
        }
        
        foreach ($id as $idValue) {
            if (in_array($idValue, $this->disablePk)) {
                $this->error('所选配置中包含不允许删除的配置');
                return false;
            }
        }
        $where = [['id', 'IN', $id]];
        $del = db($this->db)->where($where)->delete();
        if (!$del) {
            $this->error('配置删除失败');
            return false;
        }
        $this->success('配置删除成功');
        return true;
    }
    
    /**
     * AJAX(POST) - 检查选择的分类ID是否存在
     * @return bool|\think\response\Json
     */
    public function idUniqueCheck() {
        if (!request()->isAjax()) {
            return false;
        }
        $cid = request()->post('cid');
        $where = [['id', 'EQ', $cid]];
        $check = db('sys_concate')->where($where)->field('id')->value('id');
        $check = ($check >= 0) ? true : false;
        $return = ['valid' => boolval($check)];
        return json($return);
    }
}