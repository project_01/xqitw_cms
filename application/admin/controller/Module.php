<?php
/**
 *  ==================================================================
 *        文 件 名: Module.php
 *        概    要: 模块管理
 *        作    者: IT小强
 *        创建时间: 2017/9/29 11:37
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

use builder\KeFormBuilder;
use builder\KeTableBuilder;
use files\File;

/**
 * Class Module - 模块管理
 * @package app\admin\controller
 */
class Module extends Base {
    
    /**
     * @var string - 要操作的数据表名称
     */
    protected $db = 'sys_module';
    
    /**
     * @var string - 要操作的验证器类
     */
    protected $validate = 'module';
    
    /**
     * @var string - 添加/修改时的允许字段
     */
    protected $allowField = 'name,title,des,ico,version,author,author_url,update_url,sub_name';
    
    /**
     * @var array - 不允许被操作的模块
     */
    protected $disablePk = ['admin', 'user', 'install', 'api', 'common', 'index', 'lang'];
    
    /**
     * @var \app\admin\service\Module - 服务层
     */
    protected $service = null;
    
    /**
     * 加载服务层
     */
    protected function importService() {
        // 加载服务层
        $this->service = new \app\admin\service\Module();
    }
    
    /**
     * 模块列表页
     * @param string $group - 模块分组
     * @param int $enable - 模块状态
     * @return mixed|\think\response\Json
     */
    public function index($group = 'local', $enable = 0) {
        $enable = intval($enable);
        switch ($group) {
            case 'local':
                $data_list = $this->service->getModuleInfo();
                break;
            case 'online':
                $data_list = [];
                break;
            default:
                $data_list = $this->service->getModuleInfo();
        }
        $all_num = count($data_list);
        $enable_num = 0;
        $disable_num = 0;
        $uninstall_num = 0;
        foreach ($data_list as $item) {
            $status = isset($item['enable']) ? intval($item['enable']) : 0;
            switch ($status) {
                case 1:
                    $enable_num++;
                    break;
                case 2:
                    $disable_num++;
                    break;
                case 3:
                    $uninstall_num++;
            }
        }
        if ($this->request->isAjax()) {
            $list = $this->service->getModuleInfoByStatus($enable, $data_list);
            $data = ['data' => $list, 'total' => count($list)];
            return json($data);
        }
        $url = url('index', ['group' => $group, 'enable' => $enable]);
        $config = ['search' => 'true', 'id_field' => 'name', 'side_pagination' => 'client'];
        $all_url = url('index', ['group' => $group, 'enable' => 0]);
        $enable_url = url('index', ['group' => $group, 'enable' => 1]);
        $disable_url = url('index', ['group' => $group, 'enable' => 2]);
        $uninstall_url = url('index', ['group' => $group, 'enable' => 3]);
        $table = KeTableBuilder::makeTable($url, url('updateField'), $config)
            ->addTextColumn('title', '模块名称')
            ->addTextColumn('name', '模块标识')
            ->addTextColumn('des', '模块描述')
            ->addTextColumn('author', '模块作者')
            ->addTextColumn('version', '模块版本', 'text-center')
            ->addTextColumn('order', '模块排序', 'text-center', ['type' => 'text'], true)
            ->addTextColumn('name', '操作', 'text-center', [], false, 'editModule')
            ->addLinkBtn(url('upload'), '上传', 'upload', 'btn-default', '模块')
            ->addLinkBtn($all_url, '全部(' . $all_num . ')', '', 'btn-success')
            ->addLinkBtn($enable_url, '已启用(' . $enable_num . ')', '', 'btn-info')
            ->addLinkBtn($disable_url, '已禁用(' . $disable_num . ')', '', 'btn-danger')
            ->addLinkBtn($uninstall_url, '未安装(' . $uninstall_num . ')', '', 'btn-warning')
            ->returnTable();
        $groupList = [
            ['id' => 'local', 'name' => '本地模块'],
            ['id' => 'online', 'name' => '模块商城'],
        ];
        $assign = [
            'tabs_param'   => 'group',
            'tabs_id'      => 'id',
            'tabs_name'    => 'name',
            'tabs_url'     => 'admin/module/index',
            'tabs_this_id' => $group,
            'tabs'         => $groupList,
            'panel'        => $table,
        ];
        $this->assign($assign);
        return $this->fetch('tabs');
    }
    
    /**
     * 查看模块详情
     * @return bool|mixed
     */
    public function detail() {
        $name = input('param.name', '');
        if (empty($name)) {
            $this->error('非法请求');
            return false;
        }
        $install = true;
        $where = [['name', 'EQ', $name]];
        $db = db($this->db);
        $dbData = $db->where($where)->field(true)->find();
        if (!is_array($dbData) || count($dbData) < 1) {
            $install = false;
            $dbData = [];
        }
        $fileData = $this->service->getModuleInfoByFile($name);
        $fileData = ($fileData['code'] != 1) ? [] : $fileData['info'];
        $data = array_merge($fileData, $dbData);
        $author_url = get_array_data('author_url', $data);
        $update_url = get_array_data('update_url', $data);
        $enable = get_array_data('enable', $data, 3);
        $detail = [
            '模块标识' => get_array_data('name', $data),
            '模块名称' => get_array_data('title', $data),
            '模块图标' => '<i class="' . get_array_data('ico', $data) . '"></i>',
            '模块描述' => get_array_data('des', $data),
            '模块版本' => get_array_data('version', $data),
            '模块作者' => get_array_data('author', $data),
            '作者主页' => [
                'href'  => $author_url,
                'value' => $author_url,
            ],
            '模块主页' => [
                'href'  => $update_url,
                'value' => $update_url,
            ],
        ];
        if ($install) {
            $detail['是否启用'] = $enable == 1 ? '启用' : '禁用';
            $detail['安装时间'] = date('Y-m-d H:i:s', get_array_data('atime', $data, 0));
            $detail['更新时间'] = date('Y-m-d H:i:s', get_array_data('utime', $data, 0));
        }
        $assign = ['detail' => $detail];
        $this->assign($assign);
        return $this->fetch(LAYOUT_DETAIL);
    }
    
    /**
     * 上传本地模块
     * @return mixed
     */
    public function upload() {
        $form = KeFormBuilder::makeForm('')
            ->addFileUpload('module', '', '上传ZIP压缩包', url('upload'))
            ->returnForm();
        $this->assign('panel', $form);
        return $this->fetch(LAYOUT_PANEL);
    }
    
    /**
     * AJAX（POST）启用模块
     * @param string - $name
     * @return bool
     */
    public function enable() {
        $name = input('param.name', '');
        if (!$this->request->isAjax() || empty($name)) {
            $this->error('非法请求');
            return false;
        }
        if (in_array($name, $this->disablePk)) {
            $this->error('模块[' . $name . ']不允许被操作');
            return false;
        }
        $where = [['name', 'EQ', $name]];
        $update = db($this->db)->where($where)->field('enable')->setField('enable', 1);
        if (!$update) {
            $this->error('模块 [' . $name . '] 启用失败');
        } else {
            cache('system_module_limit_' . config('active_module'), null);
            $this->success('模块 [' . $name . '] 启用成功');
        }
        return true;
    }
    
    /**
     * AJAX（POST）禁用模块
     * @param string - $name
     * @return bool
     */
    public function disable() {
        $name = input('param.name', '');
        if (!$this->request->isAjax() || empty($name)) {
            $this->error('非法请求');
            return false;
        }
        if (in_array($name, $this->disablePk)) {
            $this->error('模块[' . $name . ']不允许被操作');
            return false;
        }
        $where = [['name', 'EQ', $name]];
        $update = db($this->db)->where($where)->field('enable')->setField('enable', 2);
        if (!$update) {
            $this->error('模块 [' . $name . '] 禁用失败');
        } else {
            cache('system_module_limit_' . config('active_module'), null);
            $this->success('模块 [' . $name . '] 禁用成功');
        }
        return true;
    }
    
    /**
     * AJAX（POST）模块安装
     * @param string - $name
     * @return bool
     */
    public function install() {
        $name = input('param.name', '');
        if (!$this->request->isAjax() || empty($name)) {
            $this->error('非法请求');
            return false;
        }
        if (in_array($name, $this->disablePk)) {
            $this->error('模块[' . $name . ']不允许被操作');
            return false;
        }
        
        // 获取模块信息
        $info = $this->service->getModuleInfoByFile($name);
        if ($info['code'] != 1) {
            $this->error('模块信息获取失败');
            return false;
        }
        $info = $info['info'];
        
        // 模块依赖检查
        $needModule = get_array_data('need_module', $info, false);
        if (is_array($needModule) && count($needModule) >= 1) {
            $checkNeedModule = checkDependence('module', $needModule);
            if ($checkNeedModule !== true) {
                $this->error($checkNeedModule['msg']);
                return false;
            }
        }
        
        // 插件依赖检查
        $needPlugin = get_array_data('need_plugin', $info, false);
        if (is_array($needPlugin) && count($needPlugin) >= 1) {
            $checkNeedPlugin = checkDependence('plugin', $needPlugin);
            if ($checkNeedPlugin !== true) {
                $this->error($checkNeedPlugin['msg']);
                return false;
            }
        }
        
        // 数据验证
        $validate = validate($this->validate);
        $check = $validate->scene('install')->check($info);
        if (!$check) {
            $this->error($validate->getError());
            return false;
        }
        
        // 安装模块自带SQL文件
        if (!$this->service->runModuleSQL($name, $info, 'install')) {
            $this->error($this->service->getMsg());
            return false;
        }
        
        // 数据入表
        $data = get_allow_data($this->allowField, $info);
        $t = time();
        $data['atime'] = $t;
        $data['utime'] = $t;
        $data['order'] = 0;
        $data['enable'] = 2;
        $db = db($this->db);
        $id = $db->field(true)->insertGetId($data);
        if ($id < 1) {
            $this->error('模块 [' . $name . '] 安装失败');
            return false;
        }
        
        
        // 安装模块自带的后台菜单
        if (!$this->service->installModuleMenu($name, $id)) {
            $this->error($this->service->getMsg());
            return false;
        }
        
        // 安装模块自带的后台行为
        if (!$this->service->installModuleAction($name, $id)) {
            $this->error($this->service->getMsg());
            return false;
        }
        
        // 静态资源处理
        $formDir = APP_PATH . $name . DIRECTORY_SEPARATOR . 'assets';
        $toDir = BASE_ROOT . PUBLIC_NAME . DIRECTORY_SEPARATOR . 'static';
        // 复制静态资源目录
        File::copy_dir($formDir, $toDir);
        // 删除静态资源目录
        File::del_dir($formDir);
        
        cache('system_module_limit_' . config('active_module'), null);
        $this->success('模块 [' . $name . '] 安装成功');
        return true;
    }
    
    /**
     * AJAX（POST）模块卸载
     * @param string - $name
     * @return bool
     */
    public function uninstall() {
        $name = input('param.name', '');
        if (!$this->request->isAjax() || empty($name)) {
            $this->error('非法请求');
            return false;
        }
        if (in_array($name, $this->disablePk)) {
            $this->error('模块[' . $name . ']不允许被操作');
            return false;
        }
        
        // 检查是否满足卸载条件
        $check = db($this->db)->where([['name', 'EQ', $name]])->field('id,enable')->find();
        if (!$check) {
            $this->error('模块[' . $name . ']不存在');
            return false;
        }
        $mid = $check['id'];
        $enable = $check['enable'];
        if ($enable == 1) {
            $this->error('请在禁用模块后再卸载');
            return false;
        }
        
        // 获取模块信息
        $info = $this->service->getModuleInfoByFile($name);
        if ($info['code'] != 1) {
            $this->error('模块信息获取失败');
            return false;
        }
        $info = $info['info'];
        
        // 执行模块自带卸载SQL文件
        if (!$this->service->runModuleSQL($name, $info, 'uninstall')) {
            $this->error($this->service->getMsg());
            return false;
        }
        
        // 删除模块自带的菜单
        db('sys_power')->where([['mid', 'EQ', $mid]])->delete();
        
        // 删除模块自带的行为
        db('sys_action')->where([['mid', 'EQ', $mid]])->delete();
        
        // 静态资源处理
        $formDir = BASE_ROOT . PUBLIC_NAME . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . $name;
        $toDir = APP_PATH . $name . DIRECTORY_SEPARATOR . 'assets';
        // 复制静态资源目录
        File::copy_dir($formDir, $toDir);
        // 删除静态资源目录
        File::del_dir($formDir);
        $where = [
            ['id', 'EQ', $mid],
            ['name', 'EQ', $name],
        ];
        
        // 删除模块信息
        $uninstall = db($this->db)->where($where)->delete();
        if (!$uninstall) {
            $this->error('模块 [' . $name . '] 卸载失败');
            return false;
        }
        
        cache('system_module_limit_' . config('active_module'), null);
        $this->success('模块 [' . $name . '] 卸载成功');
        return true;
    }
    
    /**
     * AJAX（POST）快速修改字段值
     * @param int $pk - 模块标识
     * @param string $name - 修改字段名
     * @param string $value - 修改字段值
     * @return bool|array|string
     */
    public function updateField($pk, $name, $value = '') {
        if (!$pk || !$name || !request()->isAjax()) {
            return false;
        }
        $allowField = ['order'];
        if (!in_array($name, $allowField)) {
            echo '错误：';
            $msg = '不允许修改此内容';
            return strip_tags($msg);
        }
        
        $data = [$name => $value];
        $validate = validate($this->validate);
        $check = $validate->scene($name)->check($data);
        if (!$check) {
            echo '错误：';
            $msg = strval($validate->getError());
            return strip_tags($msg);
        }
        
        $where = [['name', 'EQ', $pk]];
        $update = db($this->db)->where($where)->field($name)->setField($name, $value);
        if (!$update) {
            $msg = '修改失败';
            echo '错误：';
            return $msg;
        }
        cache('system_module_limit_' . config('active_module'), null);
        return ['code' => 1, 'msg' => '修改成功'];
    }
}