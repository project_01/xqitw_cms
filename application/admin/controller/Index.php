<?php
/**
 *  ==================================================================
 *        文 件 名: Index.php
 *        概    要: admin模块默认控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/13 11:52
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\controller;

use files\File;

/**
 * Class Index - admin模块默认控制器
 * @package app\admin\controller
 */
class Index extends Base {
    
    /**
     * admin模块首页
     * @return mixed
     */
    public function index() {
        return $this->fetch();
    }
    
    /**
     * 清理缓存
     */
    public function rmCache() {
        $path = BASE_ROOT . 'runtime/';
        if (!is_dir($path)) {
            $this->error('缓存目录不存在');
        }
        $cacheType = config('web_cache_type');
        if (!$cacheType || empty($cacheType)) {
            $this->error('请先设置删除缓存类型');
        }
        if (is_string($cacheType)) {
            $cacheType = unserialize($cacheType);
        }
        if (!is_array($cacheType) || count($cacheType) < 1) {
            $this->error('缓存设置有误');
        }
        $allowDir = ['log', 'cache', 'temp'];
        $add = 0;
        $less = 0;
        foreach ($cacheType as $v) {
            if (!in_array($v, $allowDir)) {
                continue;
            }
            if ($v == 'cache') {
                app('cache')->clear();
            }
            $rm = File::del_dir($path . $v);
            if ($rm) {
                $add++;
            } else {
                $less++;
            }
        }
        if ($add == 0) {
            $this->error('缓存清空失败');
        } else if ($less == 0) {
            $this->success('缓存清空成功');
        } else {
            $this->success('缓存部分清理成功');
        }
    }
    
    /**
     * 静态页面清理
     */
    public function rmHtml() {
        $path = config('html_cache_path');
        if (!is_dir($path)) {
            $this->error('静态文件路径不存在');
        }
        $dir = File::get_dirs($path);
        $dir = isset($dir['dir']) ? $dir['dir'] : [];
        if (count($dir) < 1) {
            $this->error('没有静态文件需要删除');
        }
        $add = 0;
        $less = 0;
        foreach ($dir as $v) {
            if ($v == '..' || $v == '..') {
                continue;
            }
            $rm = File::del_dir($path . $v);
            if (!$rm) {
                $less++;
            } else {
                $add++;
            }
        }
        if ($add == 0) {
            $this->error('静态文件删除失败');
        } else if ($less == 0) {
            $this->success('静态文件删除成功');
        } else {
            $this->success('静态文件部分删除成功');
        }
    }
    
    /**
     * 后台布局设置
     */
    public function setLayout() {
        $data = request()->post();
        $data = isset($data['data']) ? $data['data'] : NULL;
        $field = isset($data['field']) ? trim(strip_tags($data['field'])) : NULL;
        if (!$field) {
            $this->error('参数错误');
            return false;
        }
        $value = isset($data[$field]) ? trim(strip_tags($data[$field])) : NULL;
        if (!$value) {
            $this->error('参数错误');
        }
        $db = db('sys_layout');
        $checkWhere = [['uid', 'EQ', $this->uid]];
        $id = $db->where($checkWhere)->value('id');
        $t = time();
        $data = [
            'atime' => $t,
            'utime' => $t,
            'uid'   => $this->uid,
            $field  => $value
        ];
        if (!$id) {
            $set = $db->field('uid,atime,utime,' . $field)->insertGetId($data);
        } else {
            $where = [
                ['id', 'EQ', $id],
                ['uid', 'EQ', $this->uid]
            ];
            $data = [
                'utime' => $t,
                $field  => $value
            ];
            $set = $db->where($where)
                ->field('utime,' . $field)
                ->update($data);
        }
        if (!$set) {
            $this->error('设置失败');
        } else {
            $this->success('设置成功');
        }
        return true;
    }
}