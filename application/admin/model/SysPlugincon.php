<?php
/**
 *  ==================================================================
 *        文 件 名: SysPlugincon.php
 *        概    要: sys_plugincon 数据表模型类
 *        作    者: IT小强
 *        创建时间: 2017/8/27 12:25
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\model;

/**
 * Class SysPlugincon - sys_plugincon 数据表模型类
 * @package app\admin\model
 */
class SysPlugincon extends Base {
    
    /**
     * 保存当前插件全部配置
     * @param int $pid - 插件ID
     * @param array $data - 配置数据
     * @return array 返回操作结果数组
     */
    public static function saveAllConfig($pid, $data = NULL) {
        $db = new static();
        $data = ($data !== NULL) ? $data : (request()->post());
        $add = 0;
        $subtract = 0;
        foreach ($data as $k => $v) {
            $v = is_array($v) ? serialize($v) : $v;
            $update = $db->saveOneConfig($k, $pid, $v);
            if ($update) {
                $add++;
            } else {
                $subtract++;
            }
        }
        if ($add == 0) {
            return ['code' => -1, 'msg' => '未做修改无需保存'];
        } else if ($subtract) {
            return ['code' => 1, 'msg' => '配置项全部更新成功'];
        } else {
            return ['code' => 1, 'msg' => '共有' . $add . '项有更新'];
        }
    }
    
    /**
     * 安装插件配置项
     * @param $pid - 插件ID
     * @param $var - 插件配置项数组
     * @return bool - 返回值为布尔
     */
    public static function installPluginConfig($pid, $var) {
        $db = new static();
        foreach ($var as $k => $v) {
            $v['pid'] = $pid;
            $v['atime'] = time();
            $v['utime'] = $v['atime'];
            if (isset($v['list']) && is_array($v['list']) && count($v['list']) >= 1) {
                $v['list'] = serialize($v['list']);
            }
            if (isset($v['type']) && $v['type'] == 'switch' && !isset($v['list'])) {
                $v['list'] = serialize(['0' => '1', '1' => '2']);
            }
            if (!$db->field(true)->insert($v)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 修改配置项
     * @param string $name - 配置项名
     * @param int $pid - 配置所属插件ID
     * @param mixed $value - 配置项值
     * @return bool
     */
    private function saveOneConfig($name, $pid, $value) {
        $where = [
            ['name', 'EQ', $name],
            ['pid', 'EQ', $pid]
        ];
        $update = $this->where($where)->field('value')->setField('value', $value);
        return $update ? true : false;
    }
}