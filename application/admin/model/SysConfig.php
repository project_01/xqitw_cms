<?php
/**
 *  ==================================================================
 *        文 件 名: SysConfig.php
 *        概    要: sys_config 数据模型类
 *        作    者: IT小强
 *        创建时间: 2017/9/15 15:50
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\model;

/**
 * Class SysConfig - sys_config 数据模型类
 * @package app\admin\model
 */
class SysConfig extends Base {
    
    /**
     * 添加新配置项
     * @return array
     */
    public static function addConfig() {
        $data = request()->post();
        $configType = get_array_data('type', $data, 'text');
        $configList = get_array_data('list', $data, '');
        $configValue = get_array_data('value', $data, '');
        $_this = new static();
        switch ($configType) {
            case 'switch':
                $data['list'] = serialize(['0' => '1', '1' => '2']);
                break;
            case 'checkbox':
            case 'radio':
                $data['list'] = $_this->formatConfigData($configList);
                break;
            case 'select':
                $data['value'] = $_this->formatConfigData($configValue);
                $data['list'] = $_this->formatConfigData($configList);
                break;
            case 'images':
                $data['value'] = serialize($configValue);
                break;
            default:
                $data['list'] = $configList;
                break;
        }
        $data['atime'] = time();
        $data['utime'] = $data['atime'];
        $id = self::create($data);
        if (!$id) {
            $returnData = ['code' => 0, 'msg' => '添加配置失败'];
        } else {
            $returnData = ['code' => 1, 'msg' => '添加配置成功'];
        }
        cache('system_config', null);
        return $returnData;
    }
    
    /**
     * 修改配置项
     * @param $id - 配置项ID
     * @return array
     */
    public static function editConfig($id) {
        $data = request()->post();
        $configType = get_array_data('type', $data, 'text');
        $configList = get_array_data('list', $data, '');
        $configValue = get_array_data('value', $data, '');
        $_this = new static();
        switch ($configType) {
            case 'switch':
                $data['list'] = serialize(['0' => '1', '1' => '2']);
                break;
            case 'checkbox':
            case 'radio':
                $data['list'] = $_this->formatConfigData($configList);
                break;
            case 'select':
                $data['value'] = $_this->formatConfigData($configValue);
                $data['list'] = $_this->formatConfigData($configList);
                break;
            case 'images':
                $data['value'] = serialize($configValue);
                break;
            default:
                $data['list'] = $configList;
                break;
        }
        $data['utime'] = time();
        $where = [['id', 'EQ', $id]];
        $update = self::update($data, $where, true);
        if (!$update) {
            $returnData = ['code' => 0, 'msg' => '修改配置失败'];
        } else {
            $returnData = ['code' => 1, 'msg' => '修改配置成功'];
        }
        cache('system_config', null);
        return $returnData;
    }
    
    /**
     * 保存全部配置
     */
    public static function saveAllConfig() {
        $db = new static();
        $data = request()->post();
        $add = 0;
        $subtract = 0;
        foreach ($data as $k => $v) {
            $v = is_array($v) ? serialize($v) : $v;
            $update = $db->saveOneConfig($k, $v);
            if ($update) {
                $add++;
            } else {
                $subtract++;
            }
        }
        cache('system_config', null);
        if ($add == 0) {
            return ['code' => -1, 'msg' => '未做修改无需保存'];
        } else if ($subtract) {
            return ['code' => 1, 'msg' => '配置项全部更新成功'];
        } else {
            return ['code' => 1, 'msg' => '共有' . $add . '项有更新'];
        }
    }
    
    /**
     * 修改单个配置
     * @param $name - 配置项名
     * @param $value - 配置项值
     * @return bool
     */
    private function saveOneConfig($name, $value) {
        $db = new static();
        $where = [['name', 'EQ', $name]];
        $update = $db->where($where)->field('value')->setField('value', $value);
        return $update ? true : false;
    }
    
    /**
     * 格式化表达提交数据
     * @param $data - 表单提交的原始数据
     * @return array|mixed|string
     */
    private function formatConfigData($data) {
        if (empty($data)) {
            return '';
        }
        if (is_string($data)) {
            if (preg_match('/^({).*?(})$/', $data)) {
                $data = json_decode($data, true);
            } else if (preg_match('/^a:.*?(})$/', $data)) {
                $data = unserialize($data);
            } else {
                $data = format_array(cm_explode($data, ','));
            }
        }
        if (is_array($data) && count($data) >= 1) {
            $data = serialize($data);
        } else {
            $data = '';
        }
        return $data;
    }
}