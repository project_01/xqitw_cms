<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: 系统模块数据模型基类
 *        作    者: IT小强
 *        创建时间: 2017/9/15 15:51
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\admin\model;

/**
 * Class Base - 系统模块数据模型基类
 * @package app\admin\model
 */
abstract class Base extends \app\common\model\Base {
    
}