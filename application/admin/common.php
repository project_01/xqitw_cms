<?php

/**
 *  ==================================================================
 *        文 件 名: common.php
 *        概    要: 公共函数
 *        作    者: IT小强
 *        创建时间: 2017/9/29 12:34
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

/**
 * 检查依赖
 * @param string $type 类型：module（1）/plugin（2）
 * @param array $data 检查数据
 * @return array | bool 检测通过返回true，不满足时返回数组信息
 */
function checkDependence($type = '', $data = []) {
    foreach ($data as $key => $value) {
        if (!isset($value[2])) {
            $value[2] = '=';
        }
        // 当前版本
        if ($type === 'module' || $type === 1) {
            $dbName = 'sys_module';
            $typeName = '模块';
        } else if ($type === 'plugin' || $type === 2) {
            $dbName = 'sys_plugin';
            $typeName = '插件';
        } else {
            return ['code' => 0, 'msg' => '检查类型有误'];
        }
        $where = [['name', 'EQ', $value[0]]];
        $currVersion = db($dbName)->where($where)->field('version')->value('version');
        if (!$currVersion) {
            return ['code' => 0, 'msg' => $typeName . ' [' . $value[0] . '] 未安装'];
        }
        // 比对版本
        $compare = version_compare($currVersion, $value[1], $value[2]);
        if (!$compare) {
            return ['code' => 0, 'msg' => $typeName . ' [' . $value[0] . '] 版本要求：[' . $value[2] . $value[1] . '] 不满足'];
        }
    }
    return true;
}