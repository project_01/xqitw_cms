<?php
/**
 *  ==================================================================
 *        文 件 名: Close.php
 *        概    要: 站点关闭控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/12 10:36
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\index\controller;

/**
 * Class Close - 站点关闭控制器
 * @package app\index\controller
 */
class Close extends \app\common\controller\Base {
    
    /**
     * 站点关闭时的默认页面
     * @return mixed
     */
    public function index() {
        $index_on_off = config('index_on_off');
        if ($index_on_off) {
            $this->redirect('index/index/index');
            return false;
        }
        $isCache = 0;
        $isCompress = true;
        $checkCache = $this->service->checkCache(false, '', $isCache);
        if ($checkCache['code'] === 1) {
            return $this->fetch('index', $isCache, $checkCache, $isCompress);
        }
        $title = '网站正在维护中...';
        $welcome = '网站正在维护中...';
        $assign = ['title' => $title, 'welcome' => $welcome];
        $this->assign($assign);
        return $this->fetch('index', $isCache, $checkCache, $isCompress);
    }
    
    /**
     * 不存在的URL全部跳转到首页
     */
    public function _empty() {
        $this->redirect('index/close/index');
        exit();
    }
}