<?php
/**
 *  ==================================================================
 *        文 件 名: Base.php
 *        概    要: Index模块基类控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/12 9:47
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\index\controller;

use app\common\controller\Home;

/**
 * Class Base - Index模块基类控制器
 * @package app\index\controller
 */
abstract class Base extends Home {
    
}