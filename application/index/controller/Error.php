<?php
/**
 *  ==================================================================
 *        文 件 名: Error.php
 *        概    要: 空控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/14 11:20
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\index\controller;

/**
 * Class Error - 空控制器
 * @package app\install\controller
 */
class Error extends Base {
    
    /**
     * 不存在的URL全部跳转到首页
     */
    public function index() {
        $this->redirect('index/index/index');
        exit();
    }
    
    /**
     * 不存在的URL全部跳转到首页
     */
    public function _empty() {
        $this->redirect('index/index/index');
        exit();
    }
}