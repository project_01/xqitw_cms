<?php
/**
 *  ==================================================================
 *        文 件 名: Index.php
 *        概    要: 默认模块首页
 *        作    者: IT小强
 *        创建时间: 2017/9/12 10:36
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace app\index\controller;

/**
 * Class Index - 默认模块首页
 * @package app\index\controller
 */
class Index extends Base {
    
    /**
     * 默认首页
     */
    public function index() {
        $thisUrl = 'index/index/index';
        $defaultUrl = config('default_home_module');
        if (!empty($defaultUrl) && $thisUrl !== $defaultUrl) {
            $this->redirect($defaultUrl);
            exit();
        }
        $isCache = false;
        $isCompress = true;
        $checkCache = $this->service->checkCache(false, '', $isCache);
        if ($checkCache['code'] === 1) {
            return $this->fetch('index', $isCache, $checkCache, $isCompress);
        }
        $title = 'SKCloud安装成功' . self::$delimiter . self::$sysName . ' V' . self::$sysVersion;
        $assign = [
            'title'   => $title,
            'welcome' => $title
        ];
        $this->assign($assign);
        return $this->fetch('index', $isCache, $checkCache, $isCompress);
    }
    
    /**
     * 微信支付测试
     */
    public function pay() {
        $params = [
            // 设置商品或支付单简要描述
            'body'   => '校园E条街 - 账号充值',
            // 设置订单总金额，只能为整数，详见支付金额(单位：分)
            'fee'    => 1,
            // 设置附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
            'attach' => 'chongzhi',
            // 设置商品标记，代金券或立减优惠功能的参数，说明详见代金券或立减优惠
            'tag'    => '',
            // 设置支付类型 取值如下：JSAPI，NATIVE，APP，MWEB 详细说明见参数规定
            'type'   => '',
        ];
        hook('wx_pay_api', $params, true);
    }
    
    /**
     * 不存在的URL全部跳转到首页
     */
    public function _empty() {
        $this->redirect('index/index/index');
        exit();
    }
}
