<?php

// +----------------------------------------------------------------------
// | UEditor 上传设置
// +----------------------------------------------------------------------

$static = BASE_ROOT . PUBLIC_NAME . '/static/';
$uploads_path = BASE_ROOT . PUBLIC_NAME . '/uploads/';
$uploads_url = BASE_URL . 'uploads/';
return [
    /* 执行上传图片的action名称 */
    "image_action_name"          => "uploadimage",
    /* 提交的图片表单名称 */
    "image_field_name"           => "upfile",
    /* 上传大小限制，单位B */
    "image_max_size"             => 2048000,
    /* 上传图片格式显示 */
    "image_allow_files"          => [".png", ".jpg", ".jpeg", ".gif", ".bmp"],
    /* 是否压缩图片,默认是true */
    "image_compress_enable"      => true,
    /* 图片压缩最长边限制 */
    "image_compress_border"      => 1600,
    /* 插入的图片浮动方式 */
    "image_insert_align"         => "none",
    /* 图片访问路径前缀 */
    "image_url_prefix"           => '',
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    /* {filename} 会替换成原文件名,配置这项需要注意中文乱码问题 */
    /* {rand:6} 会替换成随机数,后面的数字是随机数的位数 */
    /* {time} 会替换成时间戳 */
    /* {yyyy} 会替换成四位年份 */
    /* {yy} 会替换成两位年份 */
    /* {mm} 会替换成两位月份 */
    /* {dd} 会替换成两位日期 */
    /* {hh} 会替换成两位小时 */
    /* {ii} 会替换成两位分钟 */
    /* {ss} 会替换成两位秒 */
    /* 非法字符 \ : * ? " < > | */
    /* 具请体看线上文档: fex.baidu.com/ueditor/#use-format_upload_filename */
    "image_path_format"          => $uploads_url . '{yyyy}{mm}{dd}/{time}{rand:6}',
    
    /* 执行上传涂鸦的action名称 */
    "scrawl_action_name"         => "uploadscrawl",
    /* 提交的图片表单名称 */
    "scrawl_field_name"          => "upfile",
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    "scrawl_path_format"         => $uploads_url . '{yyyy}{mm}{dd}/{time}{rand:6}',
    /* 上传大小限制，单位B */
    "scrawl_max_size"            => 2048000,
    /* 图片访问路径前缀 */
    "scrawl_url_prefix"          => "",
    /* 插入的图片浮动方式 */
    "scrawl_insert_align"        => "none",
    
    /* 执行上传截图的action名称 */
    "snapscreen_action_name"     => "uploadimage",
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    "snapscreen_path_format"     => $uploads_url . '{yyyy}{mm}{dd}/{time}{rand:6}',
    /* 图片访问路径前缀 */
    "snapscreen_url_prefix"      => "",
    /* 插入的图片浮动方式 */
    "snapscreen_insert_align"    => "none",
    
    /* 执行抓取远程图片的action名称 */
    "catcher_local_domain"       => ["127.0.0.1", "localhost", "img.baidu.com"],
    "catcher_action_name"        => "catchimage",
    /* 提交的图片列表表单名称 */
    "catcher_field_name"         => "source",
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    "catcher_path_format"        => $uploads_url . '{yyyy}{mm}{dd}/{time}{rand:6}',
    /* 图片访问路径前缀 */
    "catcher_url_prefix"         => "",
    /* 上传大小限制，单位B */
    "catcher_max_size"           => 2048000,
    /* 抓取图片格式显示 */
    "catcher_allow_files"        => [".png", ".jpg", ".jpeg", ".gif", ".bmp"],
    
    /* 执行上传视频的action名称 */
    "video_action_name"          => "uploadvideo",
    /* 提交的视频表单名称 */
    "video_field_name"           => "upfile",
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    "video_path_format"          => $uploads_url . '{yyyy}{mm}{dd}/{time}{rand:6}',
    /* 视频访问路径前缀 */
    "video_url_prefix"           => "",
    /* 上传大小限制，单位B，默认100MB */
    "video_max_size"             => 102400000,
    /* 上传视频格式显示 */
    "video_allow_files"          => [
        ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg", ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid"
    ],
    
    /* 执行上传视频的action名称 */
    "file_action_name"           => "uploadfile",
    /* 提交的文件表单名称 */
    "file_field_name"            => "upfile",
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    "file_path_format"           => $uploads_url . '{yyyy}{mm}{dd}/{time}{rand:6}',
    /* 文件访问路径前缀 */
    "file_url_prefix"            => "",
    /* 上传大小限制，单位B，默认50MB */
    "file_max_size"              => 51200000,
    /* 上传文件格式显示 */
    "file_allow_files"           => [
        ".png", ".jpg", ".jpeg", ".gif", ".bmp",
        ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
        ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid",
        ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso",
        ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml"
    ],
    
    /* 列出指定目录下的图片 */
    /* 执行图片管理的action名称 */
    "image_manager_action_name"  => "listimage",
    /* 指定要列出图片的目录 */
    "image_manager_list_path"    => $uploads_url,
    /* 每次列出文件数量 */
    "image_manager_list_size"    => 20,
    /* 图片访问路径前缀 */
    "image_manager_url_prefix"   => "",
    /* 插入的图片浮动方式 */
    "image_manager_insert_align" => "none",
    /* 列出的文件类型 */
    "image_manager_allow_files"  => [".png", ".jpg", ".jpeg", ".gif", ".bmp"],
    
    
    /* 列出指定目录下的文件 */
    /* 执行文件管理的action名称 */
    "file_manager_action_name"   => "listfile",
    /* 指定要列出文件的目录 */
    "file_manager_list_path"     => $uploads_url,
    /* 文件访问路径前缀 */
    "file_manager_url_prefix"    => "",
    /* 每次列出文件数量 */
    "file_manager_list_size"     => 20,
    /* 列出的文件类型 */
    "file_manager_allow_files"   => [
        ".png", ".jpg", ".jpeg", ".gif", ".bmp",
        ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
        ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid",
        ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso",
        ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml"
    ]
];