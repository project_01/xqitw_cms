<?php

// +----------------------------------------------------------------------
// | minify 压缩设置
// +----------------------------------------------------------------------

$static = BASE_ROOT . PUBLIC_NAME . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR;
return [
    'min_path'           => BASE_ROOT . PUBLIC_NAME . '/static/minify',
    'min_url'            => BASE_URL . 'static/minify',
    
    // 自定义js
    'script_js'          => [
        //  Pace - Page Load Progress Par
        'assets/pace/pace.min.js',
        //  select2 插件
        'assets/select2/js/select2.min.js',
        'assets/select2/js/i18n\zh-CN.js',
        // 全屏插件
        'assets/js/jquery.fullscreen.js',
        // 繁简体转换
        'assets/js/language.js',
        //  标签输入框 插件
        'assets/tags-input/jquery.tagsinput.min.js',
        //  滚动条优化
        'assets/js/jquery.nicescroll.min.js',
        //  ui-panels 插件
        'assets/js/ui-panels.js',
        //  表单序列化为json
        'assets/js/serializeJson.min.js',
        //  SKCloud JS
        'assets/js/skcloud.min.js',
        //  自定义 script
        'assets/js/script.min.js',
    ],
    // 核心js
    'layout_js'          => [
        //  jQuery 文件
        'assets/jquery/jquery-2.2.4.min.js',
        //  bootstrap 文件
        'assets/bootstrap/js/bootstrap.min.js',
        // layer 插件
        'assets/layer/layer.js'
    ],
    // 核心css
    'layout_css'         => [
        // 选项框美化
        'assets/css/bootstrap-checkbox.min.css',
        'assets/js/vendor/chosen/css/chosen.min.css',
        'assets/js/vendor/chosen/css/chosen-bootstrap.css',
        // tags输入框插件
        'assets/tags-input/jquery.tagsinput.min.css',
        // 弹框插件
        'assets/jquery-confirm/jquery-confirm.min.css',
        // 动画插件
        'assets/css/animate.min.css',
        // 滑动菜单插件
        'assets/js/vendor/mmenu/css/jquery.mmenu.all.css',
        // 视频背景插件
        'assets/js/vendor/videobackground/css/jquery.videobackground.css',
    ],
    // 表格js
    'table_js'           => [
        //  bootstrap 表格插件
        'assets/bootstrap-table/bootstrap-table.min.js',
        //  bootstrap 表格导出
        'assets/bootstrap-export/tableExport.js',
        'assets/bootstrap-table/extensions/export/bootstrap-table-export.min.js',
        //  bootstrap 表格编辑
        'assets/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js',
        'assets/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js',
        // 搜索框
        'assets/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js',
        //  bootstrap 表格汉化
        'assets/bootstrap-table/locale/bootstrap-table-zh-CN.min.js',
    ],
    // 表格css
    'table_css'          => [
        'assets/bootstrap-table/bootstrap-table.min.css',
        'assets/bootstrap-editable/css/bootstrap-editable.css',
    ],
    // 地区选择js
    'dist_picker_js'     => [
        // 地区选择数据源
        'assets/distpicker/distpicker.data.js',
        // 地区选择插件
        'assets/distpicker/distpicker.js',
    ],
    // 表单验证js
    'form_js'            => [
        // bootstrapvalidator 表单验证 插件
        'assets/bootstrapvalidator/js/bootstrapValidator.min.js',
        // 表单验证汉化
        'assets/bootstrapvalidator/js/language/zh_CN.js',
    ],
    // 表单验证css
    'form_css'           => [
        // bootstrapvalidator 表单验证 插件
        'assets/bootstrapvalidator/css/bootstrapValidator.min.css',
    ],
    // 时间日前选择控件js
    'datetimepicker_js'  => [
        // 时间日前选择控件
        'assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        'assets/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js',
    ],
    // 时间日前选择控件css
    'datetimepicker_css' => [
        // 时间日前选择控件
        'assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
    ],
    // markdown编辑器css
    'markdown_css'       => [
        'assets/bootstrap-markdown/css/bootstrap-markdown.min.css',
    ],
    // markdown编辑器js
    'markdown_js'        => [
        'assets/bootstrap-markdown/js/markdown.js',
        'assets/bootstrap-markdown/js/to-markdown.js',
        'assets/bootstrap-markdown/js/bootstrap-markdown.js',
    ],
    
    // editormd编辑器css
    'editormd_css'       => [
        'assets/editormd/css/editormd.min.css',
        // 'assets/editormd/css/editormd.preview.min.css',
    ],
    // editormd编辑器js
    'editormd_js'        => [
        'assets/editormd/editormd.min.js',
    ],
    // webuploader多图上传css
    'webuploader_css'    => [
        'assets/webuploader/webuploader.css'
    ],
    // webuploader多图上传js
    'webuploader_js'     => [
        'assets/webuploader/webuploader.min.js',
        'assets/webuploader/webuploadini.js'
    ],
    // ueditor编辑器js
    'ueditor_js'         => [
        'assets/ueditor/ueditor.config.js',
        'assets/ueditor/ueditor.all.min.js',
    ],
];