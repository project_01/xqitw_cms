<?php
/**
 *  ==================================================================
 *        文 件 名: index.php
 *        概    要: 应用入口文件
 *        作    者: IT小强
 *        创建时间: 2017-09-11 11:38
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

header('Content-type: text/html; charset=utf-8');

/** PHP版本检查  */
if (version_compare(PHP_VERSION, '5.6', '<')) {
    die('PHP版本过低，最少需要PHP5.6，请升级PHP版本！');
}

/** 是否隐藏项目文件 */
define('HIDE_APP', true);

/** 定义公共目录名称 */
define('PUBLIC_NAME', 'public');

/** 定义核心框架目录名称 */
define('CORE_NAME', 'thinkphp');

/** 根相对路径 */
define('BASE_URL', str_replace('\\', '/', substr(__DIR__, (preg_match("/\/$/", str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT'])) ? (strlen($_SERVER['DOCUMENT_ROOT']) - 1) : strlen($_SERVER['DOCUMENT_ROOT']))) . '/' . (HIDE_APP ? '' : PUBLIC_NAME . '/')));

/** 本地绝对路径 */
define('ROOT', realpath(str_replace('\\', '/', __DIR__) . '/') . DIRECTORY_SEPARATOR);

/** 定义框架根目录 */
define('BASE_ROOT', realpath(ROOT . (HIDE_APP ? '../' : '')) . DIRECTORY_SEPARATOR);

/** 定义应用目录 */
define('APP_PATH', BASE_ROOT . 'application' . DIRECTORY_SEPARATOR);

/** 加载自定义函数文件 */
require APP_PATH . 'helper.php';

/** 安装锁定文件路径 */
define('INSTALL_LOCK_PATH', BASE_ROOT . 'data' . DIRECTORY_SEPARATOR . 'install.lock');

/** 检查是否安装 */
if (!is_file(INSTALL_LOCK_PATH)) {
   define('BIND_MODULE', 'install');
}

/** 加载框架基础文件 */
require BASE_ROOT . CORE_NAME . DIRECTORY_SEPARATOR . 'base.php';

/** 执行应用并响应 */
\think\Container::get('app', [APP_PATH])
    ->run()
    ->send();