/* 后台布局设置 */
window.setSysLayout = function (set_layout_url, set_layout_type) {
    var data = {};


    var demoSetBody = $('#demo-set'),
        niftyContainer = $('#container'),
        niftyMainNav = $('#mainnav-container'),
        niftyAside = $('#aside-container');
    $('.mainnav-toggle').on('click', function () {
        data = {field: 'collapsed_mode'};
        if (niftyContainer.hasClass('mainnav-sm')) {
            data.collapsed_mode = 1;
        } else if (niftyContainer.hasClass('mainnav-lg')) {
            data.collapsed_mode = 2;
        } else {
            return false;
        }
        $.ajax({
            url: set_layout_url,
            type: set_layout_type,
            data: {data: data},
            success: function (re) {
            }
        });
    });

    if (demoSetBody.length) {
        function InitializeSettingWindow() {
            /* BOXED LAYOUT */
            var boxedLayoutCheckbox = document.getElementById('demo-box-lay'),
                boxedLayoutImgBtn = document.getElementById('demo-box-img'),
                boxedLayoutImgBox = $('#demo-bg-boxed'),
                boxedLayoutBtnClose = document.getElementById('demo-close-boxed-img'),
                blurredBgList = $('#demo-blurred-bg'),
                polygonBgList = $('#demo-polygon-bg'),
                abstractBgList = $('#demo-abstract-bg');

            /* Initialize */
            if (niftyContainer.hasClass('boxed-layout')) {
                boxedLayoutCheckbox.checked = true;
                boxedLayoutImgBtn.disabled = false;
            } else {
                boxedLayoutCheckbox.checked = false;
                boxedLayoutImgBtn.disabled = true;
            }

            function bg_thumb_template(cat) {
                var list = '';
                for (var i = 1; i < 17; i++) {
                    list += '<a href="javascript:void(0);" class="thumbnail box-inline"><img class="img-responsive" src="' + assetsUrl + 'img/premium/boxed-bg/' + cat + '/thumbs/' + i + '.jpg" alt="Background Image"></a>';
                }
                return list;
            }

            function add_bg_thumbs() {
                blurredBgList.append(bg_thumb_template('blurred'));
                polygonBgList.append(bg_thumb_template('polygon'));
                abstractBgList.append(bg_thumb_template('abstract'));

                var boxedBgthumb = boxedLayoutImgBox.find('.thumbnail');
                boxedBgthumb.on('click', function () {
                    boxedBgthumb.removeClass('selected');
                    var url = $(this).children('img').prop('src').replace('thumbs', 'bg');
                    $(this).addClass('selected');
                    niftyContainer.css({
                        'background-image': 'url(' + url + ')',
                        'background-repeat': 'no-repeat',
                        'background-size': 'cover'
                    });
                    data = {boxed_layout_bg: url};
                    data.field = 'boxed_layout_bg';
                    doAjax(set_layout_url, set_layout_type, data, false, false);
                });
            }

            /* Boxed Layout Checkbox （是否启用留白布局） */
            boxedLayoutCheckbox.onchange = function () {
                if (boxedLayoutCheckbox.checked) {
                    niftyContainer.addClass('boxed-layout');
                    niftyContainer.css({
                        'background-image': 'url("<{$sys_layout[\'boxed_layout_bg\']|default=""}>")',
                        'background-repeat': 'no-repeat',
                        'background-size': 'cover'
                    });
                    boxedLayoutImgBtn.disabled = false;
                    data = {boxed_layout: 1};
                } else {
                    niftyContainer.removeClass('boxed-layout').removeAttr('style');
                    boxedLayoutImgBtn.disabled = true;
                    boxedLayoutImgBox.removeClass('open').find('.thumbnail').removeClass('selected');
                    data = {boxed_layout: 2};
                }
                data.field = 'boxed_layout';
                doAjax(set_layout_url, set_layout_type, data, false, false);
                $(window).trigger('resize');
            };

            /* Image Buttons（背景图片选择） */
            boxedLayoutImgBtn.onclick = function () {
                if (!boxedLayoutImgBox.hasClass('open')) {
                    boxedLayoutImgBox.addClass('open');
                    if (!demoSetBody.hasClass('hasbgthumbs')) {
                        add_bg_thumbs();
                        demoSetBody.addClass('hasbgthumbs')
                    }
                } else {
                    boxedLayoutImgBox.removeClass('open');
                }
            };

            /* Close Button */
            boxedLayoutBtnClose.onclick = function () {
                boxedLayoutImgBox.removeClass('open');
            };
            /* TRANSITION EFFECTS */
            var effectList = 'easeInQuart easeOutQuart easeInBack easeOutBack easeInOutBack steps jumping rubber',
                animCheckbox = document.getElementById('demo-anim'),
                transitionVal = document.getElementById('demo-ease');

            /* Initialize */
            if (niftyContainer.hasClass('effect')) {
                animCheckbox.checked = true;
                transitionVal.disabled = false;
            } else {
                animCheckbox.checked = false;
                transitionVal.disabled = true;
            }

            /* Animations checkbox（是否开启动画） */
            animCheckbox.onchange = function () {
                if (animCheckbox.checked) {
                    transitionVal.disabled = false;
                    niftyContainer.addClass(transitionVal.options[transitionVal.selectedIndex].value);
                    data = {enable_animations: 1};
                } else {
                    niftyContainer.removeClass('effect ' + effectList);
                    transitionVal.disabled = true;
                    data = {enable_animations: 2};
                }
                data.field = 'enable_animations';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };

            /* Transition selectbox(动画过度效果) */
            var effectArray = effectList.split(" ");
            for (var i = 0; i < effectArray.length; i++) {
                if (niftyContainer.hasClass(effectArray[i])) {
                    transitionVal.value = effectArray[i];
                    break;
                }
            }
            transitionVal.onchange = function () {
                var valueSelected = this.options[this.selectedIndex].value;
                if (valueSelected) {
                    niftyContainer.removeClass(effectList).addClass(valueSelected);
                    data = {transitions: valueSelected};
                    data.field = 'transitions';
                    doAjax(set_layout_url, set_layout_type, data, false, false);
                }
            };


            /* NAVBAR */
            var navbarFixedCheckbox = document.getElementById('demo-navbar-fixed');

            /* Initialize */
            if (niftyContainer.hasClass('navbar-fixed')) {
                navbarFixedCheckbox.checked = true;
            } else {
                navbarFixedCheckbox.checked = false;
            }

            /* Fixed Position */
            navbarFixedCheckbox.onchange = function () {
                if (navbarFixedCheckbox.checked) {
                    niftyContainer.addClass('navbar-fixed');
                    data = {fixed_navbar: 1};
                } else {
                    niftyContainer.removeClass('navbar-fixed');
                    data = {fixed_navbar: 2};
                }
                data.field = 'fixed_navbar';
                doAjax(set_layout_url, set_layout_type, data, false, false);
                /* Refresh the aside, to enable or disable the "Bootstrap Affix" when the navbar in a "static position".*/
                niftyMainNav.niftyAffix('update');
                niftyAside.niftyAffix('update');
            };


            /* FOOTER(页脚) */
            var footerFixedCheckbox = document.getElementById('demo-footer-fixed');


            /* Initialize */
            if (niftyContainer.hasClass('footer-fixed')) {
                footerFixedCheckbox.checked = true;
            } else {
                footerFixedCheckbox.checked = false;
            }

            /*  Fixed Position */
            footerFixedCheckbox.onchange = function () {
                if (footerFixedCheckbox.checked) {
                    niftyContainer.addClass('footer-fixed');
                    data = {fixed_footer: 1};
                } else {
                    niftyContainer.removeClass('footer-fixed');
                    data = {fixed_footer: 2};
                }
                data.field = 'fixed_footer';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };


            /*  NAVIGATION */
            var collapsedCheckbox = document.getElementById('demo-nav-coll'),
                navFixedCheckbox = document.getElementById('demo-nav-fixed'),
                navProfileCheckbox = document.getElementById('demo-nav-profile'),
                navShortcutCheckbox = document.getElementById('demo-nav-shortcut'),
                navOffcanvasSB = document.getElementById('demo-nav-offcanvas'),
                navProfile = $('#mainnav-profile'),
                navShortcut = $('#mainnav-shortcut');


            /* Initialize */
            if (niftyContainer.hasClass('mainnav-fixed')) {
                navFixedCheckbox.checked = true;
            } else {
                navFixedCheckbox.checked = false;
            }

            /* Fixed Position */
            if (niftyContainer.hasClass('mainnav-fixed')) {
                navFixedCheckbox.checked = true;
            } else {
                navFixedCheckbox.checked = false;
            }
            navFixedCheckbox.onchange = function () {
                if (navFixedCheckbox.checked) {
                    $.niftyNav('fixedPosition');
                    data = {fixed_nav: 1};
                } else {
                    $.niftyNav('staticPosition');
                    data = {fixed_nav: 2};
                }
                data.field = 'fixed_nav';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };

            /* Profile */
            if (navProfile.hasClass('hidden')) {
                navProfileCheckbox.checked = false;
            } else {
                navProfileCheckbox.checked = true;
            }
            navProfileCheckbox.onchange = function () {
                navProfile.toggleClass('hidden');
                if (navProfile.hasClass('hidden')) {
                    data = {widget_profil: 2};
                } else {
                    data = {widget_profil: 1};
                }
                data.field = 'widget_profil';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };


            /* Shortcut Buttons */
            if (navShortcut.hasClass('hidden')) {
                navShortcutCheckbox.checked = false;
            } else {
                navShortcutCheckbox.checked = true;
            }
            navShortcutCheckbox.onchange = function () {
                navShortcut.toggleClass('hidden');
                if (navShortcut.hasClass('hidden')) {
                    data = {shortcut_buttons: 2};
                } else {
                    data = {shortcut_buttons: 1};
                }
                data.field = 'shortcut_buttons';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };


            /* Collapsing/Expanding Navigation */
            var _collapsed_mode_val = "<{$sys_layout['collapsed_mode']|default=2}>";
            if (parseInt(_collapsed_mode_val) === 1) {
                collapsedCheckbox.checked = true;
            } else {
                collapsedCheckbox.checked = false;
            }
            collapsedCheckbox.onchange = function () {
                if (collapsedCheckbox.checked) {
                    $.niftyNav('collapse');
                    data = {collapsed_mode: 1};
                } else {
                    $.niftyNav('expand');
                    data = {collapsed_mode: 2};
                }
                data.field = 'collapsed_mode';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };

            /* Offcanvas Navigation */
            var nav_mode = $('#off_canvas_value').val();
            navOffcanvasSB.onchange = function () {
                if (collapsedCheckbox.checked) {
                    collapsedCheckbox.checked = false;
                }
                var _off_canvas_value = this.options[this.selectedIndex].value;
                $('.mainnav-toggle').removeClass('push slide reveal none').addClass(_off_canvas_value);
                $('#off_canvas_value').val(_off_canvas_value);
                data = {off_canvas: _off_canvas_value};
                data.field = 'off_canvas';
                doAjax(set_layout_url, set_layout_type, data, "<{$page_url}>", false);
            };

            if (nav_mode === "push" || nav_mode === "slide" || nav_mode === "reveal" || nav_mode === "none") {
                $('.mainnav-toggle').removeClass('push slide reveal').addClass(nav_mode);
                navOffcanvasSB.value = nav_mode;
            } else {
                if (niftyContainer.hasClass('mainnav-sm')) {
                    collapsedCheckbox.checked = true;
                } else {
                    collapsedCheckbox.checked = false;
                }
            }


            /* ASIDE(有侧边栏布局) */
            var asdVisCheckbox = document.getElementById('demo-asd-vis'),
                asdFixedCheckbox = document.getElementById('demo-asd-fixed'),
                asdFloatCheckbox = document.getElementById('demo-asd-float'),
                asdPosCheckbox = document.getElementById('demo-asd-align'),
                asdThemeCheckbox = document.getElementById('demo-asd-themes');

            /* Visible */
            if (niftyContainer.hasClass('aside-in')) {
                asdVisCheckbox.checked = true;
            } else {
                asdVisCheckbox.checked = false;
            }
            asdVisCheckbox.onchange = function () {
                if (asdVisCheckbox.checked) {
                    $.niftyAside('show');
                    data = {aside_visible: 1};
                } else {
                    $.niftyAside('hide');
                    data = {aside_visible: 2};
                }
                data.field = 'aside_visible';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };


            /* Fixed Position */
            if (niftyContainer.hasClass('aside-fixed')) {
                asdFixedCheckbox.checked = true;
            } else {
                asdFixedCheckbox.checked = false;
            }
            asdFixedCheckbox.onchange = function () {
                if (asdFixedCheckbox.checked) {
                    $.niftyAside('fixedPosition');
                    data = {aside_fixed: 1};
                } else {
                    $.niftyAside('staticPosition');
                    data = {aside_fixed: 2};
                }
                data.field = 'aside_fixed';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };


            /* Floating Aside */
            if (niftyContainer.hasClass('aside-float')) {
                asdFloatCheckbox.checked = true;
            } else {
                asdFloatCheckbox.checked = false;
            }
            asdFloatCheckbox.onchange = function () {
                if (asdFloatCheckbox.checked) {
                    niftyContainer.addClass('aside-float');
                    data = {aside_float: 1};
                } else {
                    niftyContainer.removeClass('aside-float');
                    data = {aside_float: 2};
                }
                data.field = 'aside_float';
                doAjax(set_layout_url, set_layout_type, data, false, false);
                $(window).trigger('resize');
            };


            /*  Align */
            if (niftyContainer.hasClass('aside-left')) {
                asdPosCheckbox.checked = true;
            } else {
                asdPosCheckbox.checked = false;
            }
            asdPosCheckbox.onchange = function () {
                if (asdPosCheckbox.checked) {
                    $.niftyAside('alignLeft');
                    data = {aside_left: 1};
                } else {
                    $.niftyAside('alignRight');
                    data = {aside_left: 2};
                }
                data.field = 'aside_left';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };


            /*  Themes */
            if (niftyContainer.hasClass('aside-bright')) {
                asdThemeCheckbox.checked = false;
            } else {
                asdThemeCheckbox.checked = true;
            }
            asdThemeCheckbox.onchange = function () {
                if (asdThemeCheckbox.checked) {
                    $.niftyAside('darkTheme');
                    data = {aside_dark: 1};
                } else {
                    $.niftyAside('brightTheme');
                    data = {aside_dark: 2};
                }
                data.field = 'aside_dark';
                doAjax(set_layout_url, set_layout_type, data, false, false);
            };


            /* COLOR SCHEMES(配色方案) */
            var themeBtn = $('.demo-theme'),
                changeTheme = function (themeName, type) {
                    var themeCSS = $('#theme'),
                        fileext = '.min.css',
                        filename = assetsUrl + 'css/themes/type-' + type + '/' + themeName + fileext;
                    data = {color: filename};
                    data.field = 'color';
                    doAjax(set_layout_url, set_layout_type, data, false, false);
                    if (themeCSS.length) {
                        themeCSS.prop('href', filename);
                    } else {
                        themeCSS = '<link id="theme" href="' + filename + '" rel="stylesheet">';
                        $('head').append(themeCSS);
                    }
                };

            $('#demo-theme').on('click', '.demo-theme', function (e) {
                e.preventDefault();
                var el = $(this);
                if (el.hasClass('disabled')) {
                    return false;
                }
                changeTheme(el.attr('data-theme'), el.attr('data-type'));
                themeBtn.removeClass('disabled');
                el.addClass('disabled');
                return false;
            });
        }

        var nav_mode = $('#off_canvas_value').val();
        if (nav_mode === "push" || nav_mode === "slide" || nav_mode === "reveal" || nav_mode === 'none') {
            $('.mainnav-toggle').removeClass('push slide reveal').addClass(nav_mode);
            if (nav_mode !== 'none') {
                niftyContainer.removeClass('mainnav-lg mainnav-sm').addClass(nav_mode);
            }
        }
        demoSetBody = $('#demo-set-body');
        var demoSetBtn = $('#demo-set-btn');
        $('html').on('click', function (e) {
            if (demoSetBody.hasClass('in')) {
                if (!$(e.target).closest('#demo-set').length) {
                    demoSetBtn.trigger('click')
                }
            }
        });
        demoSetBtn.one('click', InitializeSettingWindow);
        $('#demo-btn-close-settings').on('click', function () {
            demoSetBtn.trigger('click')
        });
    }
};
/* editorMd编辑器主题设置 */
window.editorMdThemeSelect = function (id, themes, lsKey, callback) {
    var select = $("#" + id);

    for (var i = 0, len = themes.length; i < len; i++) {
        var theme = themes[i];
        var selected = (localStorage[lsKey] == theme) ? " selected=\"selected\"" : "";
        select.append("<option value=\"" + theme + "\"" + selected + ">" + theme + "</option>");
    }
    select.bind("change", function () {
        var theme = $(this).val();
        if (theme === "") {
            return false;
        }
        localStorage[lsKey] = theme;
        callback(select, theme);
    });

    return select;
};
/* 移动端检测 */
window.isMobile = function () {
    if (/android/i.test(navigator.userAgent)) {
        //document.write("This is Android'browser.");//这是Android平台下浏览器
        return true;
    }
    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
        //document.write("This is iOS'browser.");//这是iOS平台下浏览器
        return true;
    }
    if (/Linux/i.test(navigator.userAgent)) {
        //document.write("This is Linux'browser.");//这是Linux平台下浏览器
        return true;
    }
    if (/Linux/i.test(navigator.platform)) {
        //document.write("This is Linux operating system.");//这是Linux操作系统平台
        return true;
    }
    if (/MicroMessenger/i.test(navigator.userAgent)) {
        //document.write("This is MicroMessenger'browser.");//这是微信平台下浏览器
        return true;
    }

    return false;
};
/* 设置cookie */
window.setCookie = function (name, value, time) {
    var exp = new Date();
    exp.setTime(exp.getTime() + time * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ';path=/';
};
/* 读取cookie */
window.getCookie = function (name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
        return unescape(arr[2]);
    } else {
        return '';
    }
};
/* 删除cookie */
window.delCookie = function (name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != '' && cval != null) {
        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
    }
};
/* 设置表格布局样式 */
window.setDataCardView = function (id) {
    if ($(window).width() < 768) {
        $(id).bootstrapTable('toggleView');
    } else {
        $(id).bootstrapTable();
    }
};
/* 多图上传控件 删除当前图片操作 */
$(document).on('click', '.ke-form-upload-more > .item > span.del-img', function () {
    var _this = $(this);
    var _trueBtn = '<i class="fa fa-trash"></i>&nbsp;删除';
    var _falseBtn = '<i class="fa fa-remove"></i>&nbsp;取消';
    layer.msg('确认要删除此图片吗？', {
        time: 0,
        btn: [_trueBtn, _falseBtn],
        yes: function (index) {
            layer.close(index);
            _this.parent('span.item').remove();
        }
    });
});
/* 单文件上传控件中 删除当前图片操作 */
window.uploadDelImg = function (inputId, imgId) {
    var _trueBtn = '<i class="fa fa-trash"></i>&nbsp;删除';
    var _falseBtn = '<i class="fa fa-remove"></i>&nbsp;取消';
    layer.msg('确认要删除此图片吗？', {
        time: 0,
        btn: [_trueBtn, _falseBtn],
        yes: function (index) {
            layer.close(index);
            $('#' + inputId).val('');
            $('#' + imgId).hide();
            $('#' + imgId + '_delImg').hide();
        }
    });
};
/* 图标选择框搜索功能 */
window.searchIco = function (id) {
    var search_input_id = id + '_search_input';
    var keyWord = $(search_input_id).val();
    var reg = new RegExp(keyWord);
    $(id + ' .ico-item').each(function () {
        var _thisIco = $(this).attr('data-ico');
        if (_thisIco.match(reg)) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
};
/* 获取选中的CheckBox并以数组返回 */
window.getCheckboxSon = function (name) {
    var data = [];
    var childBox = $('input[name="' + name + '"]');
    childBox.each(function () {
        if ($(this).is(':checked')) {
            data.push($(this).val())
        }
    });
    return data;
};
/* 用于多选后发起ajax请求 */
window.doAJaxCheckALL = function (title, errTitle, url, ajaxCallbackUrl) {
    var data = getCheckboxSon("son[]");
    if (data.length < 1) {
        autoAlert(errTitle);
        return false;
    }
    var _trueBtn = '<i class="fa fa-check"></i>&nbsp;确定';
    var _falseBtn = '<i class="fa fa-remove"></i>&nbsp;取消';
    layer.msg(title, {
        time: 0,
        btn: [_trueBtn, _falseBtn],
        yes: function (index) {
            layer.close(index);
            doAjax(url, 'POST', data, ajaxCallbackUrl);
        }
    });
};
/* ajax请求 */
window.doAjax = function (ajaxUrl, ajaxType, ajaxData, ajaxCallbackUrl, isTable) {
    $.ajax({
        url: ajaxUrl,
        type: ajaxType,
        data: {data: ajaxData},
        dataType: 'json',
        success: function (re) {
            autoAlert(re.msg);
            if (re.code == 1 && ajaxCallbackUrl != false) {
                setTimeout(function () {
                    window.location.href = ajaxCallbackUrl;
                }, 2000)
            }
            if (re.code == 1 && isTable == 'true') {
                $('table').bootstrapTable('refresh');
            }
        },
        error: function (re) {
            $('.ajax.ajax-action').removeClass('ajax-action');
        }
    });
};
/* 格式化字节大小 */
window.formatBytes = function (size) {
    var units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    var unit = 0;
    for (var i = 0; size >= 1024 && i < 5; i++) {
        size /= 1024;
        unit = i;
    }
    return size.toFixed(2) + units[unit];
};
/* AJAX修改开关按钮值 */
window.checkToggler = function (url, type) {
    var obj = '.' + type + '-check-toggler';
    $(document).on('change', obj, function () {
        $(this).addClass('changeAction');
        var id = $(this).attr('data-id');
        var value = $(this).attr('data-value');
        var type = $(this).attr('data-type');
        var newValue = 1;
        if (parseInt(value) === 1) {
            newValue = 2;
        }
        var data = {pk: id, value: newValue, name: type};
        $.ajax({
            url: url, type: 'POST', dataType: 'json', data: data, success: function (re) {
                if ("undefined" === typeof re.code || re.code != '1') {
                    $('.toggle-switch.changeAction').removeClass('changeAction');
                } else {
                    $('.toggle-switch.changeAction').attr('data-value', newValue).toggleClass('checked').removeClass('changeAction');
                }
            }, error: function (re) {
                $('.toggle-switch.changeAction').removeClass('changeAction');
            }
        })
        ;
    });
};
/* 全局变量 PHP时间戳转为时间 */
window.getTime = function (value, row, index) {
    var data = new Date(value * 1000);
    var Y = data.getFullYear();
    var m = data.getMonth() + 1;
    var d = data.getDate();
    var H = data.getHours();
    var i = data.getMinutes();
    var s = data.getSeconds();
    return Y + '-' + m + '-' + d + ' ' + H + ':' + i + ':' + s;
};
/* 全局变量 返回 启用/禁用按钮 */
window.getCheckToggle = function (value, row, index, type) {
    var checked = value == 1 ? 'checked="checked"' : '';
    var html = '';
    html += '<input id="table-checkbox-' + type + '-' + row.id + '" type="checkbox" ' + checked;
    html += ' class="' + type + '-check-toggler toggle-switch"';
    html += ' data-id="' + row.id + '"';
    html += ' data-value="' + value + '"';
    html += ' data-index="' + index + '"';
    html += ' data-type="' + type + '">';
    html += '<label for="table-checkbox-' + type + '-' + row.id + '"></label>';
    return html;
};
window.editHtml = function (value, url, moreBtn) {
    if (!moreBtn || moreBtn === '' || moreBtn === null) {
        moreBtn = '';
    }
    moreBtn = moreBtn.replace(/___KEY___/g, value);
    var _url = url + '?id=' + value;
    var html = '';
    html += '<div class="btn-group btn-group-xs">';
    html += '   <a href="' + _url + '" class="btn btn-info">';
    html += '       <i class="fa fa-edit"></i>';
    html += '       <span>编辑</span>';
    html += '   </a>';
    html += moreBtn;
    html += '   <a href="javascript:void (0);" data-id="' + value + '" class="btn btn-danger del-btn">';
    html += '       <i class="fa fa-trash"></i>';
    html += '       <span>删除</span>';
    html += '   </a>';
    html += '</div>';
    return html;
};
window.cmEditHtml = function (value, btnHtml) {
    if (!btnHtml || btnHtml === '' || btnHtml === null) {
        btnHtml = '';
    }
    btnHtml = btnHtml.replace(/___ID___/g, value);
    var html = '';
    html += '<div class="btn-group btn-group-xs">';
    html += moreBtn;
    html += '</div>';
    return html;
};
/* 全局变量 返回搜索参数 */
window.queryParams = function (params, searchFormId) {
    params.search = $(searchFormId).serializeJson();
    return params;
};
/* 全局变量 刷新表格 */
window.tableRefresh = function (table) {
    $(table).bootstrapTable('refresh');
};
/* 全局变量 刷新表单 */
window.formRefresh = function (form) {
    $(form).bootstrapValidator('resetForm');
};
/* iCheck 实现全选反选 */
window.iCheckAll = function (checkId, inverseId, childName) {
    var checkAll = $(checkId);
    var inverse = $(inverseId);
    var childBox = $('input[name="' + childName + '"]');
    checkAll.on('ifChecked ifUnchecked', function (event) {
        inverse.prop('checked', false);
        inverse.iCheck('update');
        if (event.type == 'ifChecked') {
            childBox.iCheck('check');
        } else {
            childBox.iCheck('uncheck');
        }
    });
    inverse.on('ifChanged', function () {
        childBox.each(function () {
            if ($(this).is(':checked')) {
                $(this).iCheck('uncheck');
            } else {
                $(this).iCheck('check');
            }
        });
    });
    childBox.on('ifChanged', function () {
        if (childBox.filter(':checked').length == childBox.length) {
            checkAll.prop('checked', true);
        } else {
            checkAll.prop('checked', false);
        }
        checkAll.iCheck('update');
    });
};
/* jQ 实现全选反选 */
window.checkAll = function (checkId, child, inverseId) {
    $(checkId).click(function () {
        if (this.checked) {
            $(child).prop("checked", true);
        } else {
            $(child).prop("checked", false);
        }
    });
    $(child).click(function () {
        var chknum = $(child).length;//选项总个数
        var chk = 0;
        $(child).each(function () {
            if ($(this).prop("checked") === true) {
                chk++;
            }
        });
        if (chknum === chk) {//全选
            $(checkId).prop("checked", true);
        } else {//不全选
            $(checkId).prop("checked", false);
        }
    });
    $(inverseId).click(function () {
        var chknum = $(child).length;//选项总个数
        var chk = 0;
        $(child).each(function () {
            if ($(this).prop("checked") === true) {
                $(this).prop("checked", false);
            } else {
                $(this).prop("checked", true);
                chk++;
            }
        });
        if (chknum === chk) {//全选
            $(checkId).prop("checked", true);
        } else {//不全选
            $(checkId).prop("checked", false);
        }
    });
};
/**
 * 自定义提示框
 * @param type - 提示框类型（success|danger）
 * @param msg - 提示内容（文字或者html）
 * @return string - 提示框
 */
window.alertMessage = function (type, msg) {
    var alertType = (type == 'error' || type == 'red' || type == 'danger') ? 'red' : 'success';
    var html = '<div class="alert alert-' + alertType + ' alert-big text-center">';
    html += ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">';
    html += '×';
    html += ' </button>';
    html += msg;
    html += ' </div>';
    return html;
};
/** 自定义弹框 */
window.autoAlert = function (title) {
    layer.msg(title);
};
/** Ajax删除 */
window.del = function (data, url, title, successUrl, type) {
    var _trueBtn = '<i class="fa fa-check"></i>&nbsp;确定';
    var _falseBtn = '<i class="fa fa-remove"></i>&nbsp;取消';
    layer.msg(title, {
        time: 0,
        btn: [_trueBtn, _falseBtn],
        yes: function (index) {
            layer.close(index);
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (re) {
                    autoAlert(re.msg);
                    if (re.code === 1 && type === 'table') {
                        $('.del-btn.delAction').removeClass('delAction').parents('tr').remove();
                    }
                    if (re.code === 1 && successUrl) {
                        setTimeout(function () {
                            window.location.href = successUrl;
                        }, 2000)
                    }
                }
            });
        }
    });
};
/** Ajax联动 */
window.addLinkage = function (name, subName, url) {
    /*$(document).ready(function () {
        var _name = $('[name="' + name + '"]');
        var _this_val = _name.val();
        if (_name.is('input')) {
            _this_val = $('[name="' + name + '"]:checked').val();
        }
        ajaxSelect(url, _this_val, subName);
    });*/
    $(document).on('change', '[name="' + name + '"]', function () {
        var data = $(this).val();
        ajaxSelect(url, data, subName);
    });

    function ajaxSelect(url, data, subName) {
        $.ajax({
            url: url,
            type: 'POST',
            data: {data: data},
            dataType: 'json',
            success: function (re) {
                var html = '';
                if (re.code === 1 && re.data !== null && re.data !== '' && re.data.length >= 1) {
                    var data = re.data;
                    for (var i = 0; i < data.length; i++) {
                        var value = data[i].value;
                        var desc = data[i].desc;
                        html += '<option value="' + value + '">' + desc + '</option>'
                    }
                }
                var select = $('select[name="' + subName + '"]');
                select.html(html);
            }
        });
    }
};
/* Ajax 显示隐藏 */
window.showOrHide = function (controllerName, targetName, value) {
    value = value.split(",");
    var _name = $('[name="' + controllerName + '"]');
    var _this_val = _name.val();
    $(document).ready(function () {
        if (_name.is('input')) {
            _this_val = $('[name="' + controllerName + '"]:checked').val();
        }
        checkShowOrHide(_this_val, value, targetName);
    });
    $(document).on('change', '[name="' + controllerName + '"]', function () {
        checkShowOrHide($(this).val(), value, targetName);
    });

    function checkShowOrHide(_val, value, targetName) {
        var check = parseInt($.inArray(_val, value));
        var _target = $('[name="' + targetName + '"]');
        if (check === -1) {
            _target.attr('disabled', 'disabled');
            _target.parents('div.form-group').hide();
        } else {
            _target.removeAttr('disabled');
            _target.parents('div.form-group').show();
        }
    }
};
/** 图片预览 */
window.imgPreview = function (fileDom, id, hide_id, url, formId, imgName) {
    //判断是否支持FileReader
    if (window.FileReader) {
        var reader = new FileReader();
    } else {
        autoAlert("您的设备不支持图片预览功能，如需该功能请升级您的设备！");
    }
    //获取文件
    var file = fileDom.files[0];
    var imageType = /^image\//;
    //是否是图片
    var is_img = true;
    if (!imageType.test(file.type)) {
        is_img = false;
    }
    //读取完成
    reader.onload = function (e) {
        //获取图片dom
        // var img = document.getElementById(id);
        //图片路径设置为读取的图片
        if (e.target.result !== '') {
            uploadOne(hide_id, url, formId, imgName, id, is_img);
            if (is_img === true) {
                document.getElementById(id).style.display = 'inline-block';
                document.getElementById(id).src = e.target.result;
                document.getElementById(id).style.opacity = "1";
            }
        }
    };
    reader.readAsDataURL(file);
};
/** 单图上传 */
window.uploadOne = function (id, url, formId, imgName, imgId, is_img) {
    var pic = $('[name="' + imgName + '"]').get(0).files[0];
    var formData = new FormData();
    formData.append(imgName, pic);
    $.ajax({
        url: url + '?img_name=' + imgName,
        type: 'POST',
        xhr: function () {
            myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', function (e) {
                    if (e.lengthComputable) {
                        $('#' + imgId + '_progress').show();
                        var width = ((e.loaded / e.total) * 100) + '%';
                        $('#' + imgId + '_progress .progress .progress-bar').css({width: width});
                        $('#' + imgId + '_progress .progress-info .percent').text(width);
                        $('#' + imgId + '_progress .progress-info .msg').text('正在上传中...');
                    }
                }, false);
            }
            return myXhr;
        },
        data: formData,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.code == 1) {
                document.getElementById(id).value = response.id;
                if (is_img === true) {
                    document.getElementById(imgId + '_delImg').style.display = 'inline-block';
                } else {
                    document.getElementById(imgId + '_delImg').style.display = 'none';
                    document.getElementById(imgId).src = '';
                    document.getElementById(imgId).style.display = 'none';
                }
                $('#' + imgId + '_progress .progress-info .msg').text('上传成功,记得保存哦！');
                setTimeout(function () {
                    $('#' + imgId + '_progress').hide();
                }, 4000);
                document.getElementById('path_' + id).value = response.path;
                autoAlert(response.msg);
            } else {
                autoAlert(response.msg);
            }
        }
    });
};
/* 按钮添加AJAX */
$(document).on('click', 'button.ajax', function () {
    $(this).addClass('ajax-action');
    var ajaxType = $(this).attr('data-ajax-type');
    var ajaxData = $(this).attr('data-data');
    var ajaxUrl = $(this).attr('data-ajax-href');
    var ajaxTitle = $(this).attr('data-title');
    var ajaxCallbackUrl = $(this).attr('data-ajax-back-href');
    ajaxCallbackUrl = ajaxCallbackUrl ? ajaxCallbackUrl : false;
    var isTable = $(this).attr('data-ajax-table');
    isTable = isTable === 'true' ? 'true' : 'false';
    var confirm = $(this).attr('data-confirm');
    if (confirm === 'true') {
        var _trueBtn = '<i class="fa fa-check"></i>&nbsp;确定';
        var _falseBtn = '<i class="fa fa-remove"></i>&nbsp;取消';
        var title = ajaxTitle ? ajaxTitle : '确认执行该操作吗？';
        layer.msg(title, {
            time: 0,
            btn: [_trueBtn, _falseBtn],
            yes: function (index) {
                layer.close(index);
                doAjax(ajaxUrl, ajaxType, ajaxData, ajaxCallbackUrl, isTable);
            }
        });
    } else {
        doAjax(ajaxUrl, ajaxType, ajaxData, ajaxCallbackUrl, isTable);
    }
});
/* ajax开始/结束 */
$(document).ajaxStart(function () {
    $(".mask").show();
    $("#loader").show();
}).ajaxStop(function () {
    $(".mask").hide();
    $("#loader").hide();
});
$(document).ready(function () {
    $(".modal-open .modal,body,.modal-dialog .modal-body,.navbar-top-links>.mega-dropdown>.dropdown-menu.mega-dropdown-menu").niceScroll({
        cursorcolor: "#000000",
        zindex: 999999,
        bouncescroll: true,
        cursoropacitymax: 0.4,
        cursorborder: "",
        cursorborderradius: 7,
        cursorwidth: "2px",
        background: "rgba(0,0,0,.1)",
        autohidemode: false,
        railpadding: {top: 0, right: 2, left: 2, bottom: 0}
    });
});