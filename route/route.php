<?php
/**
 *  ==================================================================
 *        文 件 名: route.php
 *        概    要: 路由配置
 *        作    者: IT小强
 *        创建时间: 2017/9/27 21:02
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

return [
    // 站点关闭页
    'close'             => 'index/close/index',
    // 首页
    'index/index/index' => 'index/index/index',
    // 验证码输出
    'captcha/[:id]'     => "\\captcha\\CaptchaController@index",
    // 二维码输出
    'qrcode'            => "\\qrcode\\QrCodeController@index"
];