<?php

/**
 *  ==================================================================
 *        文 件 名: SystemInfo.php
 *        概    要: 系统信息插件
 *        作    者: IT小强
 *        创建时间: 2017/4/8 18:54
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace plugins\system_info;

use app\common\controller\Plugin;

/**
 * Class SystemInfo - 系统信息插件
 * @package plugins\system_info
 */
class SystemInfo extends Plugin {
    
    public function adminIndex() {
        $cacheName = 'db_version';
        if (cache($cacheName) === false) {
            $versionInfo = db()->query('select version() as version');
            $version = isset($versionInfo[0]['version']) ? $versionInfo[0]['version'] : '未知';
            cache($cacheName, $version, 0);
        }
        $var = ['db_version' => cache($cacheName)];
        echo $this->fetch('index', $var);
    }
    
    /**
 * 安装
 * @return bool
 */
    public function install() {
        return true;
    }
    
    /**
     * 卸载
     * @return bool
     */
    public function uninstall() {
        return true;
    }
}