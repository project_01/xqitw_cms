<?php

/**
 *  ==================================================================
 *        文 件 名: TeamInfo.php
 *        概    要: 产品团队
 *        作    者: IT小强
 *        创建时间: 2017/4/8 18:54
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace plugins\team_info;

use app\common\controller\Plugin;

/**
 * Class TeamInfo - 系统信息插件
 * @package plugins\team_info
 */
class TeamInfo extends Plugin {
    
    public function adminIndex() {
        echo $this->fetch();
    }
    
    /**
     * 安装
     * @return bool
     */
    public function install() {
        return true;
    }
    
    /**
     * 卸载
     * @return bool
     */
    public function uninstall() {
        return true;
    }
}