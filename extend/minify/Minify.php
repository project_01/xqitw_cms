<?php
/**
 *  ==================================================================
 *        文 件 名: Minify.php
 *        概    要:
 *        作    者: IT小强
 *        创建时间: 2017/4/11 10:06
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace minify;

use minify\cssmin\CSSMin;
use minify\htmlmin\HTMLMin;
use minify\jsmin\JSMin;

class Minify {
    
    /**
     * @var string - 压缩合并后文件存放根目录
     */
    protected static $minPath = '';
    
    /**
     * @var string - 压缩合并后文件存放根目录链接
     */
    protected static $minUrl = '';
    
    /**
     * @var string - 压缩的文件类型（js、css、html）
     */
    protected static $minType = '';
    
    /**
     * @var string
     */
    protected static $path = '';
    
    /**
     * @var string - 待压缩文件路径
     */
    protected static $filePath = '';
    
    /**
     * @var int - 缓存时间
     */
    protected static $cacheTime = 0;
    
    /**
     * @var array - 单例模式返回本类对象
     */
    protected static $instance = [];
    
    /**
     * 单利模式 - 返回本类对象
     * @return mixed|null
     */
    public static function instance() {
        $className = static::getClassName();
        $instance = isset(self::$instance[$className]) ? self::$instance[$className] : NULL;
        if (is_null($instance) || !$instance instanceof $className) {
            $instance = new $className();
        }
        return $instance;
    }
    
    /**
     * 获取以静态方法调用类的类名称
     * @return string 类名
     */
    final protected static function getClassName() {
        return get_called_class();
    }
    
    /**
     * 克隆防止继承
     */
    final protected function __clone() {
        // TODO:: 禁止克隆
    }
    
    /**
     * Minify 构造函数.
     */
    final protected function __construct() {
        // TODO::禁止主动实例化
    }
    
    /**
     * 设置配置文件
     * @param $config
     * @return static
     */
    public static function setConfig($config) {
        if (isset($config['file_path']) && is_dir($config['file_path'])) {
            self::$filePath = $config['file_path'];
        }
        if (isset($config['min_path']) && is_dir($config['min_path'])) {
            self::$minPath = $config['min_path'];
        } else {
            die('压缩根路径设置有误！！！');
        }
        if (isset($config['min_url']) && !empty($config['min_url'])) {
            self::$minUrl = $config['min_url'];
        } else {
            die('压缩根路径URL设置有误！！！');
        }
        if (isset($config['cache_time'])) {
            self::$cacheTime = intval($config['cache_time']);
        }
        return self::instance();
    }
    
    /**
     * 处理压缩文件并缓存
     * @param $name - 缓存的文件名
     * @param $file - 原始文件数组
     * @param string $type - 原始文件形式
     * @return bool|string
     */
    public function min($name, $file, $type = 'g') {
        $extension = pathinfo($name, PATHINFO_EXTENSION);
        switch ($extension) {
            case 'js':
                $subPath = 'js';
                break;
            case 'css':
                $subPath = 'css';
                break;
            case 'html':
                $subPath = 'html';
                break;
            default:
                $subPath = '';
        }
        if (empty($subPath)) {
            return false;
        }
        self::$minType = $subPath;
        if (!is_dir(self::$minPath . '/' . $subPath)) {
            mkdir(self::$minPath . '/' . $subPath, 0777, true);
        }
        self::$path = self::$minPath . '/' . $subPath . '/' . $name;
        $url = self::$minUrl . '/' . $subPath . '/' . $name;
        if (is_file(self::$path) && self::$cacheTime == 0) {
            return $url;
        }
        if (is_file(self::$path) && self::$cacheTime != 0) {
            $time = time() - filectime(self::$path);
            if ($time < self::$cacheTime) {
                return $url;
            }
        }
        switch ($type) {
            case 'g':
                $fileContent = $this->getGroupFileContent($file);
                break;
            default:
                $fileContent = '';
        }
        $minContent = $this->getMinContent($fileContent);
        file_put_contents(self::$path, $minContent);
        return $url;
    }
    
    /**
     * 解析文件数组，并合并文件内容
     * @param $filePath - 文件路径数组
     * @return string - 合并后的内容
     */
    protected function getGroupFileContent($filePath) {
        $fileContent = '';
        if (is_string($filePath)) {
            $filePath = explode(',', $filePath);
        }
        if (!is_array($filePath) || count($filePath) < 1) {
            return $fileContent;
        }
        foreach ($filePath as $v) {
            $file = self::$filePath . $v;
            if (is_file($file)) {
                $fileContent .= file_get_contents($file) . "\n";
            }
        }
        return $fileContent;
    }
    
    /**
     * 实例化压缩类，并输出压缩后的内容
     * @param $fileContent - 原始内容
     * @return bool|string - 压缩后的内容（失败返回false）
     */
    protected function getMinContent($fileContent) {
        switch (self::$minType) {
            case 'js':
                $class = JSMin::minify($fileContent);
                break;
            case 'css':
                $class = CSSMin::minify($fileContent);
                break;
            case 'html':
                $class = HTMLMin::minify($fileContent);
                break;
            default:
                $class = false;
        }
        return $class;
    }
}