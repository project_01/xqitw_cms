<?php
/**
 *  ==================================================================
 *        文 件 名: KeTableBuilder.php
 *        概    要: Table表格构建器
 *        作    者: IT小强
 *        创建时间: 2017/3/25 21:05
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace builder;

use think\Hook;


/**
 * Class KeTableBuilder
 * @package builder
 */
class KeTableBuilder extends KeBuilder {
    
    /**
     * @var string - 输出的表格Html
     */
    protected static $table = '';
    
    /**
     * @var string - 表格ID
     */
    protected static $tableId = 'bootstrapTable';
    
    /**
     * @var string - 表格搜索框ID
     */
    protected static $searchId = 'search';
    
    /**
     * @var string - 表格工具栏ID
     */
    protected static $toolId = 'toolbar';
    
    /**
     * @var string - 工具栏按钮
     */
    protected static $toolBtn = '';
    
    /**
     * @var string - 表格添加搜索
     */
    protected static $search = '';
    
    /**
     * @var string - queryParams方法名
     */
    protected static $queryParams = '';
    
    /**
     * @var string - 表格列
     */
    protected static $hr = '';
    
    /**
     * @var string - 额外的脚本
     */
    protected static $script = '';
    
    /**
     * 构建表格
     * @param $url - 数据获取地址
     * @param bool $editable_url - 字段修改地址
     * @param array $config - 配置信息
     * @return static
     */
    public static function makeTable($url, $editable_url = false, $config = []) {
        self::$tableId = 'bootstrapTable' . self::cm_round(6);
        self::$toolId = 'toolbar' . self::cm_round(6);
        self::$searchId = 'search' . self::cm_round(6);
        self::$queryParams = 'getQueryParams' . self::cm_round(6);
        $selectName = isset($config['select_name']) ? $config['select_name'] : 'son[]';
        $idField = isset($config['id_field']) ? $config['id_field'] : 'id';
        $class = isset($config['class']) ? $config['class'] : '';
        $pagination = isset($config['pagination']) ? $config['pagination'] : 'true';
        $search = isset($config['search']) ? $config['search'] : 'false';
        $method = isset($config['method']) ? $config['method'] : 'post';
        $cardView = isset($config['card_View']) ? $config['card_View'] : 'false';
        $paginationType = isset($config['side_pagination']) ? $config['side_pagination'] : 'server';
        $editable = ($editable_url == false) ? 'false' : 'true';
        $_all = ($paginationType == 'server') ? 'all' : 'all';
        $html = ' <div class="tile-body"><table id="' . self::$tableId . '" class="table-bordered" ' . $class;
        $html .= ' data-show-export="true"';
        $html .= ' data-advanced-search="' . $search . '"';
        $html .= ' data-id-table="advancedTable"';
        $html .= ' data-select-item-name="' . $selectName . '"';
        $html .= ' data-search="' . $search . '"';
        $html .= ' data-toggle="table"';
        $html .= ' data-url="' . $url . '"';
        $html .= ' data-method="' . $method . '"';
        $html .= ' data-pagination="' . $pagination . '"';
        $html .= ' data-page-size="10"';
        $html .= ' data-page-list="[10,20,50,100,500,' . $_all . ']"';
        $html .= ' data-side-pagination="' . $paginationType . '"';
        $html .= ' data-query-params="' . self::$queryParams . '"';
        $html .= ' data-toolbar="#' . self::$toolId . '"';
        $html .= ' data-show-refresh="true"';
        $html .= ' data-card-view="' . $cardView . '"';
        $html .= ' data-show-toggle="true"';
        $html .= ' data-show-columns="true"';
        $html .= ' data-showExport="true"';
        $html .= ' data-exportDataType="basic"';
        $html .= ' data-id-field="' . $idField . '"';
        $html .= ' data-editable="' . $editable . '"';
        $html .= ' data-editable-url="' . $editable_url . '">';
        self::$script .= ' function ' . self::$queryParams . '(params) {return queryParams(params, "#' . self::$searchId . '");} ';
        self::$table .= $html;
        return new static();
    }
    
    /**
     * 渲染并返回表格
     * @return string
     */
    public function returnTable() {
        $html = '';
        if (!empty(self::$search)) {
            self::$script .= ' $(document).on(\'click\', \'.search-btn\', function () {tableRefresh("#' . self::$tableId . '");}); ';
            $html .= $this->getSearchTool(self::$search, self::$searchId);
        }
        if (!empty(self::$toolBtn)) {
            $html .= $this->getTableTool(self::$toolBtn, self::$toolId);
        }
        if (!empty(self::$hr)) {
            $html .= self::$table . '<thead><tr>' . self::$hr . '</tr></thead></table></div>';
        } else {
            return $html;
        }
        if (!empty(self::$script)) {
            $_script = ' $(document).ready(function(){setDataCardView("#' . self::$tableId . '");}); ';
            $html .= '<script type="text/javascript"> ' . $_script . self::$script . ' </script>';
        }
        $assign = ['add_bootstrap_table' => true, 'table' => $html];
        hook_add('admin_js', function () {
            echo print_script('table.js');
        });
        hook_add('admin_css', function () {
            echo print_script('table.css');
        });
        return $html;
    }
    
    /**
     * 添加多项框
     * @param string $class
     * @return $this
     */
    public function addCheckbox($class = '') {
        $class = 'text-center ' . $class;
        $html = '<th';
        $html .= ' data-field="_checkbox_ke_table_" data-checkbox="true" ';
        $html .= $this->getContent($class, 'false', []);
        $html .= ' > ';
        $html .= ' </th>';
        self::$hr .= $html;
        return $this;
    }
    
    /**
     * 添加普通文本列
     * @param $field - 字段
     * @param string $title - 标题
     * @param string $class - class类
     * @param array $edit - 行内编辑
     * @param string $sort - 是否排序
     * @param string $formatter - 格式化数据
     * @return $this
     */
    public function addTextColumn($field, $title = '', $class = '', $edit = [], $sort = 'false', $formatter = '') {
        $title = empty($title) ? $field : $title;
        $html = '<th';
        $html .= ' data-field="' . $field . '"';
        if (!empty($formatter)) {
            $html .= ' data-formatter="' . $formatter . '"';
        }
        $html .= $this->getContent($class, $sort, $edit);
        $html .= ' > ';
        $html .= $title;
        $html .= ' </th>';
        self::$hr .= $html;
        return $this;
    }
    
    /**
     * 添加时间字段（用于将php时间戳转为时间）
     * @param $field - 字段
     * @param string $title - 标题
     * @param string $class - class类
     * @param array $edit - 行内编辑
     * @param string $sort - 是否排序
     * @return $this
     */
    public function addTimeColumn($field, $title = '', $class = '', $edit = [], $sort = 'false') {
        $title = empty($title) ? $field : $title;
        $formatter = 'getTime';
        $html = '<th';
        $html .= ' data-field="' . $field . '"';
        $html .= ' data-formatter="' . $formatter . '"';
        $html .= $this->getContent($class, $sort, $edit);
        $html .= ' > ';
        $html .= $title;
        $html .= ' </th>';
        self::$hr .= $html;
        return $this;
    }
    
    /**
     * 添加开关列
     * @param $field - 字段
     * @param $url - 修改地址
     * @param string $title - 标题
     * @param string $class - 额外的class类
     * @param string $width - 宽度
     * @return $this
     */
    public function addSwitchColumn($field, $url, $title = '', $class = '', $width = '90') {
        $title = empty($title) ? $field : $title;
        $formatter = 'getCheckToggle' . ucfirst($field) . '_' . self::cm_round(6);
        $html = ' <th';
        $html .= ' class="text-center ' . $class . '"';
        $html .= 'width="' . $width . '" data-field="' . $field . '" data-formatter="' . $formatter . '">';
        $html .= $title;
        $html .= '</th>';
        self::$hr .= $html;
        self::$status = true;
        $script = ' function ' . $formatter . '(value, row, index) { return getCheckToggle(value, row, index, "' . $field . '"); } ';
        $script .= ' checkToggler("' . $url . '","' . $field . '"); ';
        self::$script .= $script;
        return $this;
    }
    
    /**
     * 添加编辑列（编辑、删除）
     * @param $field - 字段
     * @param $url - 编辑地址
     * @param $delUrl - 删除地址
     * @param $successUrl - 删除成功回调地址
     * @param string $title - 标题
     * @param string $delTitle - 删除提示语
     * @param string $btn - 添加更多按钮html
     * @param string $class - 额外的class类
     * @param string $width - 宽度
     * @return $this
     */
    public function addEditColumn($field, $url, $delUrl, $title = '', $delTitle = '确认删除吗?', $btn = '', $successUrl = '', $class = '', $width = '180') {
        $title = empty($title) ? $field : $title;
        $formatter = 'edit' . ucfirst($field) . '_' . self::cm_round(6);
        $html = '<th class="text-center ' . $class . '" width="' . $width . '"';
        $html .= ' data-field="' . $field . '" data-formatter="' . $formatter . '">';
        $html .= $title;
        $html .= ' </th>';
        $script = ' function ' . $formatter . '(value) { return editHtml(value, "' . $url . '","' . $btn . '"); } ';
        $script .= ' $(document).on(\'click\',\'.del-btn\',function(){$(this).addClass(\'delAction\');var id=$(this).attr(\'data-id\');del({id:id},"' . $delUrl . '","' . $delTitle . '","' . $successUrl . '","table");}); ';
        self::$hr .= $html;
        self::$script .= $script;
        self::$status = true;
        return $this;
    }
    
    /**
     * 添加自定义编辑列（可自定义添加按钮）
     * @param $field - 字段
     * @param string $title - 标题
     * @param string $btn - 添加更多按钮html
     * @param string $class - 额外的class类
     * @param string $width - 宽度
     * @return $this
     */
    public function addCmEditColumn($field, $title = '', $btn = '', $class = '', $width = '180') {
        $title = empty($title) ? $field : $title;
        $formatter = 'cmEdit' . ucfirst($field) . '_' . self::cm_round(6);
        $html = '<th class="text-center ' . $class . '" width="' . $width . '"';
        $html .= ' data-field="' . $field . '" data-formatter="' . $formatter . '">';
        $html .= $title;
        $html .= ' </th>';
        $script = ' function ' . $formatter . '(value) { return cmEditHtml(value,"' . $btn . '"); } ';
        self::$hr .= $html;
        self::$script .= $script;
        self::$status = true;
        return $this;
    }
    
    /**
     * 添加工具栏链接按钮
     * @param $url - 链接地址
     * @param string $title - 标题
     * @param string $class -额外的class类
     * @param string $ico - fa字体图标
     * @param string $hiddenXs - 小屏幕时隐藏的文字
     * @return $this
     */
    public function addLinkBtn($url, $title = '', $ico = '', $class = 'btn-success', $hiddenXs = '') {
        $html = ' <a href="' . $url . '" class="btn ' . $class . '">';
        if (!empty($ico)) {
            $html .= '<i class="fa fa-' . $ico . '"></i> ';
        }
        if (!empty($title)) {
            $html .= $title;
        }
        if (!empty($hiddenXs)) {
            $html .= '<span class="hidden-xs">' . $hiddenXs . '</span>';
        }
        $html .= '</a>';
        self::$toolBtn .= $html;
        return $this;
    }
    
    /**
     * 添加工具栏普通按钮
     * @param $type - 按钮类型
     * @param string $title - 标题
     * @param string $class -额外的class类
     * @param string $ico - fa字体图标
     * @param string $hiddenXs - 小屏幕时隐藏的文字
     * @return $this
     */
    public function addToolBtn($title = '', $ico = '', $class = 'btn-success', $hiddenXs = '', $type = 'button') {
        $html = ' <button type="' . $type . '" class="btn ' . $class . '">';
        if (!empty($ico)) {
            $html .= '<i class="fa fa-' . $ico . '"></i> ';
        }
        if (!empty($title)) {
            $html .= $title;
        }
        if (!empty($hiddenXs)) {
            $html .= '<span class="hidden-xs">' . $hiddenXs . '</span>';
        }
        $html .= '</button>';
        self::$toolBtn .= $html;
        return $this;
    }
    
    /**
     * 添加工具栏AJAX操作全部按钮
     * @param string $title - 标题
     * @param string $url - AJAX提交地址
     * @param string $successUrl - AJAX完成跳转地址
     * @param string $confirmTitle - 确认信息
     * @param string $class -额外的class类
     * @param string $ico - fa字体图标
     * @param string $hiddenXs - 小屏幕时隐藏的文字
     * @return $this
     */
    public function addAjaxAllBtn($title, $url, $successUrl = '', $hiddenXs = '', $confirmTitle = '确定要执行此操作吗？', $ico = 'trash', $class = 'btn-danger') {
        $id = 'ajax_btn_' . self::cm_round(6);
        $html = ' <button id="' . $id . '" type="button" class="btn ' . $class . '">';
        if (!empty($ico)) {
            $html .= '<i class="fa fa-' . $ico . '"></i> ';
        }
        $html .= $title;
        if (!empty($hiddenXs)) {
            $html .= '<span class="hidden-xs">' . $hiddenXs . '</span>';
        }
        $html .= '</button>';
        self::$script .= ' $(document).on("click", "#' . $id . '", function () { doAJaxCheckALL("' . $confirmTitle . '", "当前未选择，请至少选择1项", "' . $url . '", "' . $successUrl . '");}); ';
        self::$toolBtn .= $html;
        return $this;
    }
    
    /**
     * 添加单行文本搜索项
     * @param $name - name
     * @param $title - 标题
     * @param $placeholder - 提示语
     * @return $this
     */
    public function addTextSearch($name, $title, $placeholder = '') {
        $placeholder = empty($placeholder) ? $title : $placeholder;
        $id = $name . self::cm_round(6, 'num');
        $content = '<input type="text" class="search form-control"';
        $content .= ' name="' . $name . '" id="' . $id . '" placeholder="' . $placeholder . '">';
        self::$search .= $this->addSearchItem($id, $title, $content);
        return $this;
    }
    
    /**
     * 添加时间段搜索
     * @param $name - name
     * @param $title - 标题
     * @param $format - 日期格式 (默认值: 'mm/dd/yyyy')
     * 日期格式， p, P, h, hh, i, ii, s, ss, d, dd, m, mm, M, MM, yy, yyyy 的任意组合。
     * p : meridian in lower case ('am' or 'pm') - according to locale file
     * P : meridian in upper case ('AM' or 'PM') - according to locale file
     * s : seconds without leading zeros
     * ss : seconds, 2 digits with leading zeros
     * i : minutes without leading zeros
     * ii : minutes, 2 digits with leading zeros
     * h : hour without leading zeros - 24-hour format
     * hh : hour, 2 digits with leading zeros - 24-hour format
     * H : hour without leading zeros - 12-hour format
     * HH : hour, 2 digits with leading zeros - 12-hour format
     * d : day of the month without leading zeros
     * dd : day of the month, 2 digits with leading zeros
     * m : numeric representation of month without leading zeros
     * mm : numeric representation of the month, 2 digits with leading zeros
     * M : short textual representation of a month, three letters
     * MM : full textual representation of a month, such as January or March
     * yy : two digit representation of a year
     * yyyy : full numeric representation of a year, 4 digits
     * @param $minView - (默认值：0, 'hour')
     * 日期时间选择器所能够提供的最精确的时间选择视图。
     * 如果设置minview:2;或minview:'month';，则选择日期后，不会再跳转去选择时分秒
     * @return $this
     */
    public function addTimeSearch($name, $title, $format = 'yyyy-mm-dd hh:ii:ss', $minView = 0) {
        $dateConfig = '{language:\'zh-CN\',autoclose:true,todayBtn:\'linked\',minView:\'' . $minView . '\'}';
        $id = str_replace('.', '-', $name . self::cm_round(6, 'num'));
        $content = '<div class="col-md-6" style="padding: 0 1px;">';
        $content .= '<input data-date-format="' . $format . '" type="text" class="search form-control"';
        $content .= ' name="' . $name . '_start" id="' . $id . '" placeholder="' . '开始时间' . '">';
        $content .= '</div>';
        $content .= '<div class="col-md-6" style="padding: 0 1px;">';
        $content .= '<input data-date-format="' . $format . '" type="text" class="search form-control"';
        $content .= ' name="' . $name . '_end" id="' . $id . '_end" placeholder="' . '结束时间' . '">';
        $content .= '</div>';
        $content .= '<script type="text/javascript"> $(\'#' . $id . '\').datetimepicker(' . $dateConfig . '); $(\'#' . $id . '_end\').datetimepicker(' . $dateConfig . ');</script>';
        self::$search .= $this->addSearchItem($id, $title, $content);
        hook_add('admin_js', function () {
            echo print_script('datetimepicker.js');
        });
        hook_add('admin_css', function () {
            echo print_script('datetimepicker.css');
        });
        return $this;
    }
    
    /**
     * 添加下拉框搜索项
     * @param $name - name
     * @param $title - 标题
     * @param array $list - 可选项
     * @param bool $is_multiple - 是否多选
     * @param $placeholder - 提示语
     * @return $this
     */
    public function addSelectSearch($name, $title, $list = [], $is_multiple = false, $placeholder = '') {
        $id = str_replace('.', '-', $name . self::cm_round(6, 'num'));
        $placeholder = empty($placeholder) ? $title : $placeholder;
        $multiple = ($is_multiple == true) ? ' multiple="multiple" ' : '';
        $content = '<select  id="' . $id . '"';
        $content .= ' name="' . $name . '"' . $multiple;
        $content .= ' class="chosen-select chosen-transparent search form-control">';
        if (is_array($list) && count($list) >= 1) {
            foreach ($list as $k => $v) {
                $content .= '<option value="' . $k . '">' . $v . '</option>';
            }
        }
        $content .= '</select>';
        $content .= '<script type="text/javascript"> $("#' . $id . '").select2({ placeholder: "' . $placeholder . '",allowClear: true});</script>';
        self::$search .= $this->addSearchItem($id, $title, $content);
        return $this;
    }
    
    /**
     * 拼装搜索项
     * @param $title - 标题
     * @param $id - id
     * @param $content - 内容
     * @return string
     */
    protected function addSearchItem($id, $title, $content) {
        $html = '';
        $html .= '<div class="form-group col-md-4" style="min-height: 40px;padding-left:8px;padding-right: 8px;margin: 5px 0;">';
        $html .= '    <label class="col-sm-4 control-label" for="' . $id . '">';
        $html .= $title;
        $html .= '    </label>';
        $html .= '    <div class="col-sm-8" style="padding: 0;">';
        $html .= $content;
        $html .= '    </div>';
        $html .= '</div>';
        return $html;
    }
    
    /**
     * 内容处理
     * @param string $class - 额外的class类
     * @param string $sort - 是否排序
     * @param array $edit - 编辑选项
     * @return string
     */
    protected function getContent($class = '', $sort = 'false', $edit = []) {
        $html = '';
        if (!empty($class)) {
            $html .= ' class="' . $class . '"';
        }
        if ($sort == 'true') {
            $html .= ' data-sortable="' . $sort . '"';
        }
        $isEdit = isset($edit['type']) ? 'true' : 'false';
        if ($isEdit == 'false') {
            return $html;
        }
        $editType = isset($edit['type']) ? $edit['type'] : 'text';
        $editSource = isset($edit['source']) ? $edit['source'] : '';
        if ($isEdit == 'true') {
            $html .= ' data-editable="' . $isEdit . '"';
            $html .= ' data-editable-type="' . $editType . '"';
            $html .= ' data-editable-source="' . $editSource . '"';
        }
        return $html;
    }
    
    /**
     * 获取表格工具栏
     * @param $btn -工具栏按钮列表
     * @param string $toolId - 工具栏ID
     * @return string
     */
    protected function getTableTool($btn, $toolId = 'toolbar') {
        $html = '';
        if (!empty($btn)) {
            $html .= '<div id="' . $toolId . '">';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-12">';
            $html .= '<div class="btn-group table-options">';
            $html .= $btn;
            $html .= '</div></div></div></div>';
        }
        return $html;
    }
    
    /**
     * 获取表格搜索栏
     * @param $search
     * @param $searchId
     * @return string
     */
    protected function getSearchTool($search, $searchId) {
        $html = '';
        if (empty($search)) {
            return $html;
        }
        $html .= '<div id="search-content">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="' . $searchId . '" method="post">
                                <div class="tile-body">
                                    <div class="form-horizontal">
                                        <div class="row">
                                           ' . $search . '
                                            <div class="form-group col-md-4" style="min-height: 40px;padding-left:8px;padding-right: 8px;margin: 5px 0;">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8 text-right" style="padding-right:0; ">
                                                <button type="button" class="btn btn-info search-btn">
                                                    <i class="fa fa-search"></i>
                                                    <span>查询</span>
                                                </button></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>';
        return $html;
    }
}