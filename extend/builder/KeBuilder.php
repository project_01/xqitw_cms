<?php
/**
 *  ==================================================================
 *        文 件 名: Builder.php
 *        概    要: 表单|表格构建器基类
 *        作    者: IT小强
 *        创建时间: 2017/3/24 9:18
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace builder;

abstract class KeBuilder {
    
    /**
     * @var bool - 状态值
     */
    protected static $status = false;
    
    /**
     * 生产随机字符串
     * @param int $length - 指定生产字符串的长度
     * @param string $type - 指定生产字符串的类型（all-全部，num-纯数字，letter-纯字母）
     * @return null|string
     */
    protected static function cm_round($length = 4, $type = 'all') {
        $str = '';
        $strUp = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $strLow = 'abcdefghijklmnopqrstuvwxyz';
        $number = '0123456789';
        switch ($type) {
            case 'num':
                $strPol = $number;
                break;
            case 'letter':
                $strPol = $strUp . $strLow;
                break;
            default:
                $strPol = $strUp . $number . $strLow;
        }
        $max = strlen($strPol) - 1;
        for ($i = 0; $i < $length; $i++) {
            $str .= $strPol[rand(0, $max)];
        }
        return $str;
    }
    
}