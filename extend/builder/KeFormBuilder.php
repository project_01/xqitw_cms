<?php
/**
 *  ==================================================================
 *        文 件 名: KeFormBuilder.php
 *        概    要: Form表单构建器
 *        作    者: IT小强
 *        创建时间: 2017/3/24 9:08
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace builder;

/**
 * Class KeFormBuilder - 表单构建器
 * @package builder
 */
class KeFormBuilder extends KeBuilder {
    
    /**
     * @var bool -表单验证状态
     */
    protected static $validate = false;
    
    /**
     * @var string - 表单验证脚本
     */
    protected static $validateHtml = '';
    
    /**
     * @var array - 表单验证项
     */
    protected static $validateConfig = [];
    
    /**
     * @var string - 输出的表单html
     */
    protected static $form = '';
    
    /**
     * @var string - 多级联动
     */
    protected static $linkage = '';
    
    /**
     * @var string - 表单提交按钮
     */
    protected static $submitBtn = '';
    
    /**
     * @var string - 表单重置按钮
     */
    protected static $resetBtn = '';
    
    /**
     * @var int - 表单label宽度
     */
    protected static $colWidth = 2;
    
    /**
     * @var string - 表单提交方式
     */
    protected static $method = 'post';
    
    /**
     * @var string - 表单ID
     */
    protected static $formID = 'validate-form';
    
    /**
     * @var array - 表单默认数据
     */
    protected static $formData = [];
    
    /**
     * 构建表单
     * @param $url - 表单提交地址
     * @param int $colWidth - 表单label宽度
     * @param array $formData - 表单默认数据，数组形式，可以直接传入数据库查询结果
     * @param string $formID - 指定表单ID
     * @param string $method - 指定表单提交方式
     * @return static
     */
    public static function makeForm($url, $colWidth = 2, $formData = [], $formID = 'validate-form', $method = 'post') {
        self::$validateConfig = [];
        self::$validateHtml = '';
        self::$form = '';
        self::$colWidth = intval($colWidth);
        self::$formData = $formData;
        self::$method = strval($method) == 'get' ? 'get' : 'post';
        self::$formID = $formID;
        $html = '<form enctype="multipart/form-data" action="' . $url . '" method="' . self::$method . '" class="form-horizontal" role="form" id="' . self::$formID . '">';
        self::$form .= $html;
        return new static();
    }
    
    /**
     * 添加多项表单
     * @param $list - 表单数据
     * @param bool $return - 是否接受返回表单
     * @param string $successUrl - 提交成功后的回跳地址
     * @param string $submitBtn - 提交按钮标题
     * @param string $resetBtn - 重置按钮标题
     * @return  mixed
     */
    public function addItems($list, $return = false, $successUrl = '', $submitBtn = '', $resetBtn = '重置表单') {
        if (!$list || !is_array($list) || count($list) < 1) {
            return $this;
        }
        foreach ($list as $k => $v) {
            $type = isset($v['type']) ? $v['type'] : 'text';
            $name = isset($v['name']) ? $v['name'] : '_name';
            $value = isset($v['value']) ? $v['value'] : '';
            $title = isset($v['describe']) ? $v['describe'] : '';
            $rows = isset($v['rows']) ? $v['rows'] : 4;
            $lists = isset($v['list']) ? $v['list'] : [];
            $validate = isset($v['validate']) ? $v['validate'] : [];
            $tip = isset($v['tip']) ? $v['tip'] : '';
            switch ($type) {
                case 'text':
                    $this->addText($name, $value, $title, $validate, $tip);
                    break;
                case 'hidden':
                    $this->addHidden($name, $value);
                    break;
                case 'password':
                    $this->addPassword($name, $value, $title, $validate, $tip);
                    break;
                case 'ico':
                    $this->addIco($name, $value, $title, $validate, $tip);
                    break;
                case 'textarea':
                    $this->addTextArea($name, $value, $title, $rows, $validate, $tip);
                    break;
                case 'radio':
                    $this->addRadio($name, $value, $lists, $title, $validate, $tip);
                    break;
                case 'checkbox':
                    $this->addCheckBox($name, $value, $lists, $title, $validate, $tip);
                    break;
                case 'checkboxgroup':
                    $this->addCheckBoxGroup($name, $value, $lists, $title, $validate, $tip);
                    break;
                case 'select':
                    $this->addSelect($name, $value, $lists, $title, $validate, $tip);
                    break;
                case 'switch':
                    $this->addSwitch($name, $value, $lists, $title, $validate, $tip);
                    break;
                case 'tags':
                    $this->addTags($name, $value, $title, $validate, $tip);
                    break;
                case 'file':
                    if (!$lists || is_array($lists) || empty($lists)) {
                        $lists = url('upload');
                    }
                    $this->addFileUpload($name, $value, $title, $lists, $tip);
                    break;
                case 'ueditor':
                    $this->addUEditor($name, $value, $title, $validate, 240, $tip);
                    break;
                case 'images':
                    if (!$lists || is_array($lists) || empty($lists)) {
                        $lists = url('webUpload');
                    }
                    $value = empty($value) ? $value : unserialize($value);
                    $this->addWebUpload($name, $value, $title, $lists, $tip);
                    break;
                case 'time':
                    $format = isset($v['format']) ? $v['format'] : 'yyyy-mm-dd hh:ii:ss';
                    $min = isset($v['min']) ? $v['min'] : 0;
                    $this->addTime($name, $value, $title, $format, $min, $validate, $tip);
                    break;
                default:
                    continue;
                    break;
            }
        }
        if ($return == false) {
            return $this;
        }
        if (!empty($submitBtn)) {
            $this->addSubmitBtn($submitBtn);
        }
        if (!empty($resetBtn)) {
            $this->addResetBtn($resetBtn);
        }
        $this->validateForm($successUrl);
        return $this->returnForm();
    }
    
    /**
     * 添加时间选择框
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param $format - 日期格式 (默认值: 'mm/dd/yyyy')
     * 日期格式， p, P, h, hh, i, ii, s, ss, d, dd, m, mm, M, MM, yy, yyyy 的任意组合。
     * p : meridian in lower case ('am' or 'pm') - according to locale file
     * P : meridian in upper case ('AM' or 'PM') - according to locale file
     * s : seconds without leading zeros
     * ss : seconds, 2 digits with leading zeros
     * i : minutes without leading zeros
     * ii : minutes, 2 digits with leading zeros
     * h : hour without leading zeros - 24-hour format
     * hh : hour, 2 digits with leading zeros - 24-hour format
     * H : hour without leading zeros - 12-hour format
     * HH : hour, 2 digits with leading zeros - 12-hour format
     * d : day of the month without leading zeros
     * dd : day of the month, 2 digits with leading zeros
     * m : numeric representation of month without leading zeros
     * mm : numeric representation of the month, 2 digits with leading zeros
     * M : short textual representation of a month, three letters
     * MM : full textual representation of a month, such as January or March
     * yy : two digit representation of a year
     * yyyy : full numeric representation of a year, 4 digits
     * @param $minView - (默认值：0, 'hour')
     * 日期时间选择器所能够提供的最精确的时间选择视图。
     * 如果设置minview:2;或minview:'month';，则选择日期后，不会再跳转去选择时分秒
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addTime($name, $value = '', $title = '', $format = 'yyyy-mm-dd hh:ii:ss', $minView = 0, $validate = [], $help = '', $helpType = 'block') {
        $dateConfig = '{language:\'zh-CN\',autoclose:true,todayBtn:\'linked\',minView:\'' . $minView . '\'}';
        if (empty($value)) {
            $value = $this->getFormData($name, $value);
        }
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<input data-date-format="' . $format . '" type="text" class="search form-control"';
        $content .= ' value="' . $value . '"';
        $content .= ' name="' . $name . '" id="' . $id . '" placeholder="' . '点击选择时间' . '">';
        $content .= '<script type="text/javascript"> $(\'#' . $id . '\').datetimepicker(' . $dateConfig . ');</script>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        hook_add('admin_js', function () {
            echo print_script('datetimepicker.js');
        });
        hook_add('admin_css', function () {
            echo print_script('datetimepicker.css');
        });
        return $this;
    }
    
    /**
     * 添加字体图标选择器
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addIco($name, $value = '', $title = '', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $search_input_id = 'modalDialog' . $id . '_search_input';
        $search_button_id = 'modalDialog' . $id . '_search_btn';
        $defaultIco = 'fa fa-camera-retro';
        $content = '<div class="input-group">';
        $content .= '<span class="input-group-addon btn-danger" id="input-left' . $id . '">';
        $content .= '<i class="' . (empty($value) ? $defaultIco : $value) . '"></i>';
        $content .= '</span>';
        $content .= '<input name="' . $name . '" value="' . $value . '" id="input' . $id . '" type="text" class="form-control"';
        $content .= ' placeholder="点击右侧按钮选择图标" readonly="readonly">';
        $content .= ' <a data-toggle="modal" href="#modalDialog' . $id . '" role="button" class="input-group-addon btn-success"><i class="fa fa-eye"></i> 图标选择</a></div>';
        $content .= $this->getIcoModalDialog('modalDialog' . $id);
        $content .= '<script type="text/javascript">';
        $content .= '$("#modalDialog' . $id . '").on("click", "span.ico-item", function () {var ico = $(this).attr("data-ico");$("input#input' . $id . '").val(ico);$("#input-left' . $id . '").html("<i class=\'" + ico + "\'></i>");});</script>';
        $content .= '<script type="text/javascript">';
        $content .= '$("#modalDialog' . $id . '").on("click", "#' . $search_button_id . '", function () {searchIco("#modalDialog' . $id . '");});</script>';
        $content .= '<script type="text/javascript">';
        $content .= '$("#modalDialog' . $id . '").on("change keyup","#' . $search_input_id . '",  function () {searchIco("#modalDialog' . $id . '");});</script>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加单行文本和按钮组合项
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param string $btn - 按钮
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addInputBtnGroup($name, $value = '', $title = '', $btn = '', $validate = [], $help = '', $helpType = 'block') {
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<div class="input-group">';
        $content .= '<input type="text" class="form-control" ';
        $content .= 'id="' . $id . '" ';
        $content .= 'name="' . $name . '" ';
        $content .= 'value="' . str_replace('"', '\'', $value) . '">';
        $content .= '<span class="input-group-btn">';
        $content .= $btn;
        $content .= '</span>';
        $content .= '</div>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加单行文本框
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addText($name, $value = '', $title = '', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<input type="text" class="form-control" ';
        $content .= 'id="' . $id . '" ';
        $content .= 'name="' . $name . '" ';
        $content .= 'value="' . str_replace('"', '\'', $value) . '">';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加hidden隐藏域
     * @param string $name - name
     * @param string $value - 文本框值
     * @return $this
     */
    public function addHidden($name, $value = '') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<input type="hidden" class="form-control" ';
        $content .= 'id="' . $id . '" ';
        $content .= 'name="' . $name . '" ';
        $content .= 'value="' . str_replace('"', '\'', $value) . '">';
        self::$form .= $content;
        return $this;
    }
    
    /**
     * 添加密码框
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addPassword($name, $value = '', $title = '', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<input type="text" class="form-control" ';
        $content .= 'id="' . $id . '" ';
        $content .= 'name="' . $name . '" ';
        $content .= 'onfocus="this.type=\'password\'" ';
        $content .= 'value="' . str_replace('"', '\'', $value) . '">';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加多行文本域
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param int $rows - 行数
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addTextArea($name, $value = '', $title = '', $rows = 4, $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<textarea class="form-control" name="' . $name . '" id="' . $id . '" rows="' . $rows . '">';
        $content .= $value;
        $content .= '</textarea>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加markdown编辑器
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param int $rows - 行数
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addMarkdown($name, $value = '', $title = '', $rows = 10, $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<textarea  data-provide="markdown" class="form-control" name="' . $name . '" id="' . $id . '" rows="' . $rows . '">';
        $content .= $value;
        $content .= '</textarea>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        hook_add('admin_js', function () {
            echo print_script('markdown.js');
        });
        hook_add('admin_css', function () {
            echo print_script('markdown.css');
        });
        return $this;
    }
    
    /**
     * 添加EditorMd编辑器
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param int $height - 编辑器高度
     * @param string $type - 初始值类型（html/code）
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addEditorMd($name, $value = '', $title = '', $height = 500, $type = 'html', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<div class="col-sm-4" style="padding-left:0;">
                        <select id="editormd-theme-select" class="form-control">
                            <option selected="selected" value="">编辑框样式</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                    <select id="editor-area-theme-select" class="form-control">
                        <option selected="selected" value="">内容区样式</option>
                    </select>
                    </div>
                    <div class="col-sm-4" style="padding-right:0;">
                    <select id="preview-area-theme-select" class="form-control">
                        <option selected="selected" value="">预览区样式</option>
                    </select>
                    </div>';
        $content .= '<div id="' . $id . '">';
        $content .= '<textarea class="editormd-markdown-textarea" name="' . $name . '[code]" placeholder="editormd" style="display:none;">' . $value . '</textarea>';
        $content .= '<textarea class="editormd-html-textarea" name="' . $name . '[html]" placeholder="editormd" style="display:none;">' . $value . '</textarea>';
        $content .= '</div>';
        $content .= '<script type="text/javascript">
        var ' . $id . 'editormd = editormd("' . $id . '", {
            width: "100%",
            autoHeight: false,
            height: ' . $height . ',
            syncScrolling: "single",
            theme: (localStorage.theme) ? localStorage.theme : "default",
            previewTheme: (localStorage.previewTheme) ? localStorage.previewTheme : "default",
            editorTheme: (localStorage.editorTheme) ? localStorage.editorTheme : "default",
            imageUpload : true,
            imageFormats : ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL : "' . url('web_upload_one') . '",
            path: "__STATIC__/assets/editormd/lib/",
            saveHTMLToTextarea : true,
            onload:function(){
               var _type="' . $type . '";
               if(_type==="html"){
                    this.setHTML("' . $value . '");
               }else{
                    this.setMarkdown("' . $value . '");
               }
            }
        });
         editorMdThemeSelect("editormd-theme-select", editormd.themes, "theme", function ($this, theme) {
            ' . $id . 'editormd.setTheme(theme);
        });

        editorMdThemeSelect("editor-area-theme-select", editormd.editorThemes, "editorTheme", function ($this, theme) {
            ' . $id . 'editormd.setCodeMirrorTheme(theme);
        });

        editorMdThemeSelect("preview-area-theme-select", editormd.previewThemes, "previewTheme", function ($this, theme) {
            ' . $id . 'editormd.setPreviewTheme(theme);
        });
</script>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        hook_add('admin_js', function () {
            echo print_script('editormd.js', false);
        });
        hook_add('admin_css', function () {
            echo print_script('editormd.css', false);
        });
        return $this;
    }
    
    /**
     * 添加单选按钮
     * @param $name - name
     * @param string $title - 标题
     * @param $list - 可选项列表
     * @param $value - 单选值
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addRadio($name, $value = '', $list = [], $title = '', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $list = $this->getListArray($list);
        if (!$list) {
            return $this;
        }
        $content = '';
        $list_key = array_keys($list);
        if (!in_array($value, $list_key)) {
            $value = $list_key[0];
        }
        foreach ($list as $k => $v) {
            $checked = ($k == $value) ? 'checked="checked"' : '';
            $id = $name . '_' . $k;
            $content .= '<input class="magic-radio" type="radio" name="' . $name . '" id="' . $id . '" value="' . $k . '" ' . $checked . '>';
            $content .= '<label class="magic-radio-label" for="' . $id . '"> ' . $v . ' </label>';
        }
        $id = $name . '_' . self::cm_round(6, 'num');
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加多选按钮
     * @param $name
     * @param array $value
     * @param array $list
     * @param string $title
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addCheckBox($name, $value = [], $list = [], $title = '', $validate = [], $help = '', $helpType = 'block') {
        $list = $this->getListArray($list);
        if (!$list) {
            return $this;
        }
        $value = $this->getListArray($this->getFormData($name, $value));
        if (!$list) {
            return $this;
        }
        $content = '';
        foreach ($list as $k => $v) {
            $checked = (in_array($k, $value)) ? 'checked="checked"' : '';
            $id = $name . '_' . $k;
            $content .= '<input class="magic-checkbox" id="' . $id . '" type="checkbox" name="' . $name . '[]" value="' . $k . '" ' . $checked . '>';
            $content .= '<label class="magic-checkbox-label" for="' . $id . '"> ' . $v . ' </label>';
        }
        $id = $name . '_' . self::cm_round(6, 'num');
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加分组多选按钮
     * @param $name
     * @param array $value
     * @param array $list
     * @param string $title
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addCheckBoxGroup($name, $value = [], $list = [], $title = '', $validate = [], $help = '', $helpType = 'block') {
        $list = $this->getListArray($list);
        if (!$list) {
            return $this;
        }
        $value = $this->getListArray($this->getFormData($name, $value));
        if (!$list) {
            return $this;
        }
        $content = '';
        foreach ($list as $k => $v) {
            $groupId = $name . '_group_' . $k;
            $groupIdP = $groupId . '_p';
            $content .= '<div id="' . $groupIdP . '" class="panel panel-mint">';
            $content .= '<div class="panel-heading">';
            $content .= '<h3 class="group-title panel-title">';
            $content .= '<input class="magic-checkbox" id="' . $groupId . '" type="checkbox">';
            $content .= '<label class="magic-checkbox-label-no" for="' . $groupId . '"> ' . $v['title'] . ' </label>';
            $content .= '</h3>';
            $content .= '</div>';
            $content .= '<div class="group-sub panel-body">';
            foreach ($v['list'] as $subK => $subV) {
                if ($subV == 'hr' && !is_numeric($subK)) {
                    $content .= '<hr style="margin: 5px 0;">';
                } else if ($subV == 'br' && !is_numeric($subK)) {
                    $content .= '<br>';
                } else {
                    $checked = (in_array($subK, $value)) ? 'checked="checked"' : '';
                    $id = $name . '_' . $k . '_' . $subK;
                    $content .= '<input class="magic-checkbox" id="' . $id . '" type="checkbox" name="' . $name . '[]" value="' . $subK . '" ' . $checked . '>';
                    $content .= '<label class="magic-checkbox-label" for="' . $id . '"> ' . $subV . ' </label>';
                }
            }
            $content .= '</div>';
            $content .= '<script type="text/javascript">';
            $content .= 'checkAll("#' . $groupId . '","#' . $groupIdP . ' .group-sub input:checkbox");';
            $content .= '</script>';
            $content .= '</div>';
        }
        $id = $name . '_' . self::cm_round(6, 'num');
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加下拉选项框
     * @param $name
     * @param string $value
     * @param array $list 多选列表
     * @param string $title
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addSelect($name, $value = '', $list = [], $title = '', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $list = $this->getListArray($list);
        if (!$list) {
            return $this;
        }
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<select class="form-control" name="' . $name . '" id="' . $id . '">';
        foreach ($list as $k => $v) {
            $selected = ($k == $value) ? 'selected' : '';
            $content .= '<option value="' . $k . '" ' . $selected . '> ' . $v . ' </option>';
        }
        $content .= '</select>';
        $content .= '<script type="text/javascript">$(document).ready(function() {$("#' . $id . '").select2({ placeholder: "' . $title . '",allowClear: true});}); </script>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加switch开关
     * @param $name - name值
     * @param string $title - 标题
     * @param int|string $value
     * @param $list - 可选项列表
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addSwitch($name, $value = 1, $list = ['0' => 1, '1' => 2], $title = '', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $list = $this->getListArray($list);
        if (!$list || !is_array($list) || count($list) < 2) {
            $list = ['0' => 1, '1' => 2];
        }
        $off = isset($list[1]) ? $list[1] : 'off';
        $on = isset($list[0]) ? $list[0] : 'on';
        $checked = ($value == $on) ? 'checked' : '';
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<input type="hidden" name="' . $name . '" id="hidden_' . $id . '" value="' . $off . '">';
        $content .= '<input type="checkbox" value="' . $on . '" name="' . $name . '" id="' . $id . '" ';
        $content .= 'class="toggle-switch" ' . $checked . '>';
        $content .= '<label class="toggle-switch-label" for="' . $id . '"></label>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加标签输入框
     * @param $name - name
     * @param string $title - 标题
     * @param string $value - 文本框值
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addTags($name, $value = '', $title = '', $validate = [], $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<input type="text" class="tags form-control" data-default="13213"';
        $content .= 'id="' . $id . '" ';
        $content .= 'name="' . $name . '" ';
        $content .= 'value="' . str_replace('"', '\'', $value) . '">';
        $_id = '#' . $id;
        $content .= '<script type="text/javascript">$("' . $_id . '").tagsInput({width: "auto"});</script>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        return $this;
    }
    
    /**
     * 添加文件上传控件
     * @param $name - name
     * @param string $value
     * @param $title - 标题
     * @param $url - 图片上传地址
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addFileUpload($name, $value = '', $title = '', $url = '', $help = '', $helpType = 'block') {
        $fileId = $this->getFormData($name, $value);
        $value = get_file_path($fileId);
        $url = empty($url) ? url('web_upload_one') : $url;
        $id = $name . '_' . self::cm_round(6, 'num');
        $imgName = $name . '_' . self::cm_round(6, 'num');
        $hiddenId = 'imgHidden' . self::cm_round(6);
        $imgId = 'imgSrc' . self::cm_round(6);
        $content = '<div class="input-group">';
        $content .= '<input id="path_' . $hiddenId . '" type="text" class="form-control" readonly="readonly" value="' . (empty($value) ? '暂无图片' : $value) . '">';
        $content .= '<input id="' . $hiddenId . '" type="hidden" name="' . $name . '" value="' . $fileId . '">';
        $content .= '<span class="input-group-btn">';
        $content .= '<button type="button" class="btn btn-success">';
        $content .= '<i class="fa fa-eye"></i> 选择</button>';
        $content .= '<span class="btn btn-primary btn-file">';
        $content .= '<i class="fa fa-upload"></i> 上传';
        $content .= '<input name="';
        $content .= $imgName;
        $content .= '" id="' . $id . '" type="file"';
        $content .= ' onchange="imgPreview(this';
        $content .= ',\'' . $imgId . '\',\'' . $hiddenId . '\',\'' . $url . '\',\'' . self::$formID . '\',\'' . $imgName . '\')">';
        $content .= '</span>';
        $content .= '</span>';
        $content .= '</div>';
        $content .= '<div id="' . $imgId . '_progress" style="display: none;">
                      <div class="progress-info">
                      <div class="msg">正在上传中...</div>
                       <div class="percent">0</div>
                      </div>
                      <div class="progress progress-striped progress-thin">
                        <div class="progress-bar progress-bar-red" role="progressbar" style="width:0"></div>
                      </div>
                    </div>';
        $imgType = ['gif', 'jpg', 'jpeg', 'png'];
        $valueExtension = pathinfo($value, PATHINFO_EXTENSION);
        $content .= '<div style="position: relative;">';
        $content .= '<i id="' . $imgId . '_delImg" style="display:' . (!in_array($valueExtension, $imgType) ? 'none' : 'inline-block') . '; cursor: pointer;position: absolute;margin-top:8px; right:0;" class="fa fa-trash fa-lg" title="删除图片" onclick=\'uploadDelImg("' . $hiddenId . '","' . $imgId . '");\'>删除</i>';
        $content .= ' <img src="' . (!in_array($valueExtension, $imgType) ? '' : $value) . '" id="' . $imgId . '" ';
        $content .= ' style="margin-top:25px;max-width:100%;max-height:100px;">';
        $content .= '</div>';
        self::addItem($id, $name, $content, $title, [], $help, $helpType);
        return $this;
    }
    
    /**
     * 添加多图上传控件
     * @param $name
     * @param string $value
     * @param $title
     * @param string $url
     * @param string $help
     * @param string $helpType
     * @return $this
     */
    public function addWebUpload($name, $value = '', $title, $url = '', $help = '', $helpType = 'block') {
        $value = $this->getFormData($name, $value);
        $url = empty($url) ? url('webUpload') : $url;
        $id = $name . '_' . self::cm_round(6, 'num');
        $valueData = empty($value) ? $this->getFormData($name, '') : $value;
        if (!is_array($valueData)) {
            $valueData = unserialize($valueData);
        }
        $content = '<input type="hidden" name="' . $name . '" value="">';
        $content .= '<div class="ke-form-upload-more">';
        if (is_array($valueData) && count($valueData) >= 1) {
            foreach ($valueData as $dv) {
                $content .= '<span class="item">';
                $content .= '<span class="del-img text-right"><i class="fa fa-lg fa-trash"></i></span>';
                $content .= '<img src="' . $dv . '" alt="' . $dv . '">';
                $content .= '<input type="hidden" name="' . $name . '[]" value="' . $dv . '">';
                $content .= '</span>';
            }
        }
        $content .= '</div>';
        $content .= '<div class="uploader-list-container">
                        <div class="queueList">
                            <div id="' . $id . 'dndArea" class="placeholder">
                                 <div id="' . $id . '-2"></div>
                                 <p>或将照片拖到这里，单次最多可选300张</p>
                            </div>
                        </div>
                        <div class="statusBar" style="display:none;">
                            <div class="progress"><span class="text">0%</span> <span class="percentage"></span></div>
                            <div class="info col-md-6"></div>
                            <div class="btns col-md-6 text-right">
                                  <div class="btn btn-info uploadBtn">
                                                <i class="fa fa-upload"></i>&nbsp;上传
                                            </div>
                                  <div id="' . $id . '2"></div>
                                  <div class="btn btn-success">
                                                <i class="fa fa-eye"></i>&nbsp;选择
                                            </div>
                            </div>
                        </div>
                    </div>';
        $content .= '<script type="text/javascript">var web_upload_path = "__STATIC__/assets/webuploader/";webUploadIni(web_upload_path,"' . $url . '","#' . $id . '","' . $name . '");</script>';
        self::addItem($id, $name, $content, $title, [], $help, $helpType);
        hook_add('admin_js', function () {
            echo print_script('webuploader_js');
        });
        hook_add('admin_css', function () {
            echo print_script('webuploader_css');
        });
        return $this;
    }
    
    /**
     * 添加UEditor编辑器
     * @param $name
     * @param string $value
     * @param string $title
     * @param array $validate - 字段验证
     * @param int $height -编辑器高度
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addUEditor($name, $value = '', $title = '', $validate = [], $height = 240, $help = '', $helpType = 'block') {
        $value = preg_replace(["/\n/", "/\n\r/", "/\s/"], [' ', ' ', ' '], $this->getFormData($name, $value));
        $id = $name . '_' . self::cm_round(6, 'num');
        $content = '<script id="' . $id . '" name="' . $name . '"></script>';
        $content .= '<script type="text/javascript"> var ' . $id . ' = UE.getEditor("' . $id . '", {ueUrl:assetsUrl+"ueditor/",initialFrameHeight: "' . $height . '",serverUrl:"' . url('ue') . '",autoFloatEnabled:false,topOffset:40}); var con_' . $id . '=\'' . $value . '\';' . $id . '.ready(function () { ' . $id . '.setContent(con_' . $id . '); }); </script>';
        self::addItem($id, $name, $content, $title, $validate, $help, $helpType);
        hook_add('admin_js', function () {
            echo print_script('ueditor.js', false);
        });
        return $this;
    }
    
    /**
     * 添加 表单提交按钮
     * @param string $btnTitle - 按钮标题
     * @param string $btnType - 按钮类型(submit/button)
     * @return $this
     */
    public function addSubmitBtn($btnTitle = '提交表单', $btnType = 'submit') {
        $id = $btnType . '_' . self::cm_round(6, 'num');
        $html = '<button id="' . $id . '" type="' . $btnType . '" class="btn btn-success">';
        $html .= $btnTitle;
        $html .= '</button>';
        self::$submitBtn = $html;
        return $this;
    }
    
    /**
     * 添加 表单重置按钮
     * @param string $btnTitle
     * @return $this
     */
    public function addResetBtn($btnTitle = '重置表单') {
        $id = 'reset_' . self::cm_round(6, 'num');
        $html = '<button id="' . $id . '" type="reset" class="btn btn-danger">';
        $html .= $btnTitle;
        $html .= '</button>';
        $html .= '<script type="text/javascript">$("#' . $id . '").on("click",function() {formRefresh("#' . self::$formID . '");});</script>';
        self::$resetBtn = $html;
        
        return $this;
    }
    
    /**
     * 添加下拉联动
     * @param $name - 父级下拉列表name
     * @param $subName - 子代父级下拉列表name
     * @param $url - ajax请求地址
     * @return $this
     */
    public function addLinkage($name, $subName, $url) {
        $html = ' addLinkage("' . $name . '", "' . $subName . '", "' . $url . '"); ';
        self::$linkage .= $html;
        return $this;
    }
    
    /**
     * 添加AJAX显示隐藏
     * @param $name - 父级下拉列表name
     * @param $subName - 子代父级下拉列表name
     * @param $value - 显示时的值，多个值以英文逗号分割
     * @return $this
     */
    public function addShowHide($name, $subName, $value) {
        $html = ' showOrHide("' . $name . '", "' . $subName . '", "' . $value . '"); ';
        self::$linkage .= $html;
        return $this;
    }
    
    /**
     * 添加中国省市区联动
     * @param string $title - 标题
     * @param array $default - 默认选择省份、市、区(不填写默认为北京市)
     * [
     * 'province'=>'贵州省',
     * 'city'=>'贵阳市',
     * 'dist'=>'南明区',
     * ]
     * @param string $provinceName - 省份下拉框name
     * @param string $cityName - 市下拉框name
     * @param string $districtName - 区下拉框name
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return $this
     */
    public function addDistPicker($title, $default = [], $provinceName = 'province', $cityName = 'city', $districtName = 'district', $help = '', $helpType = 'block') {
        $content = '';
        $province = isset($default['province']) ? $default['province'] : '北京市';
        $city = isset($default['city']) ? $default['city'] : '';
        $dist = isset($default['dist']) ? $default['dist'] : '';
        $distList = ['province' => ['name' => $provinceName, 'id' => $provinceName . self::cm_round(6)]];
        if (!empty($city)) {
            $distList['city'] = ['name' => $cityName, 'id' => $cityName . self::cm_round(6)];
        }
        if (!empty($dist)) {
            $distList['district'] = ['name' => $districtName, 'id' => $districtName . self::cm_round(6)];
        }
        $length = count($distList);
        $distId = 'distpicker' . self::cm_round(6);
        $width = 12 / $length;
        $content .= '<div id="' . $distId . '">';
        foreach ($distList as $k => $v) {
            $name = $v['name'];
            $id = $v['id'];
            $content .= '<div class="col-sm-' . $width . ' distpicker">';
            $content .= '<select name="' . $name . '" class="form-control" id="' . $id . '">';
            $content .= '</select>';
            $content .= '</div>';
        }
        $content .= '</div>';
        $content .= '<script type="text/javascript">';
        $content .= '$("#' . $distId . '")';
        $content .= '.distpicker({ province: "' . $province . '",city: "' . $city . '",district: "' . $dist . '" });';
        $content .= '</script>';
        self::addItem('dist' . self::cm_round(6), $title, $content, '', [], $help, $helpType);
        hook_add('admin_js', function () {
            echo print_script('dist_picker.js');
        });
        return $this;
    }
    
    /**
     * 返回构建好的表单
     * @return string
     */
    public function returnForm() {
        if (self::$status == false) {
            return '';
        }
        $html = token();
        if (!empty(self::$submitBtn) || !empty(self::$resetBtn)) {
            $html .= '<div class="form-group form-footer">';
            $html .= '<div class="col-md-12">';
            $html .= '<div class="pull-right">';
            $html .= self::$submitBtn . ' ';
            $html .= self::$resetBtn;
            $html .= '</div></div></div>';
        }
        $html .= '</form>';
        if (self::$validate == true) {
            hook_add('admin_js', function () {
                echo print_script('form.js');
            });
            hook_add('admin_css', function () {
                echo print_script('form.css');
            });
            $html .= self::$validateHtml;
        }
        self::$form .= $html;
        return self::$form;
    }
    
    /**
     * 构建表单Item样式
     * @param $id
     * @param $name
     * @param string $content
     * @param string $title
     * @param array $validate - 字段验证
     * @param string $help -提示语句
     * @param string $helpType -提示语句类型（block为块级元素）
     * @return string
     */
    protected function addItem($id, $name, $content = '', $title = '', $validate = [], $help = '', $helpType = 'block') {
        $title = empty($title) ? $name : $title;
        $html = ' <div class="form-group">';
        $html .= '<label for="' . $id . '" class="col-sm-' . self::$colWidth . ' control-label">';
        $html .= $title;
        $html .= '</label>';
        $html .= '<div class="col-sm-' . (12 - self::$colWidth) . '">';
        $html .= $content;
        $html .= '<span class="help-' . $helpType . '">' . $help . '</span>';
        $html .= '</div>';
        $html .= '</div>';
        self::$form .= $html;
        self::$status = true;
        if ($validate && is_array($validate) && count($validate) >= 1) {
            $this->addValidate($name, $validate);
        }
        return $html;
    }
    
    /**
     * 获取表单可选项数组
     * @param $list - 原始输入，可以为数组或者json
     * @return array
     */
    protected function getListArray($list) {
        if (empty($list)) {
            return [];
        }
        if (is_string($list)) {
            if (preg_match('/^({).*?(})$/', $list)) {
                $list = json_decode($list, true);
            } else if (preg_match('/^a:.*?(})$/', $list)) {
                $list = unserialize($list);
            } else {
                $list = explode(',', $list);
            }
        }
        if (!$list || !is_array($list) || count($list) < 1) {
            return [];
        } else {
            return $list;
        }
    }
    
    /**
     * 生成表单验证脚本
     * @param string $redirectUrl ,表单提交成功后的跳转地址
     * @return $this
     */
    public function validateForm($redirectUrl = '') {
        $formID = self::$formID;
        $method = self::$method;
        $linkage = self::$linkage;
        $validateConfig = json_encode(self::$validateConfig, JSON_UNESCAPED_UNICODE);
        $validateConfig = stripslashes($validateConfig);
        $patten = '/({"regexp":")(.*?)(",)/i';
        $replacement = "{\"regexp\":\$2,";
        $validateConfig = preg_replace($patten, $replacement, $validateConfig);
        $base_url_upload = urlencode(BASE_URL . 'uploads');
        $base_url_assets = urlencode(BASE_URL . 'static/assets');
        $base_url_static = urlencode(BASE_URL . 'static');
        $validateHtml = <<<"EOT"
<script type="text/javascript">(function ($) { var validate_config = {}; validate_config.message = '表单验证未通过'; validate_config.feedbackIcons = {valid: 'fa fa-check',invalid: 'fa fa-times',validating: 'fa fa-refresh'}; validate_config.fields = $validateConfig; $('#$formID').bootstrapValidator(validate_config).on('success.form.bv', function (e) { e.preventDefault(); var form = $(e.target); var formData=form.serialize();$.ajax({url: form.attr('action'), type: '$method', data: formData.replace(/$base_url_upload/ig, "___UPLOADS").replace(/$base_url_assets/ig, "___ASSETS").replace(/$base_url_static/ig, "___STATIC"), success: function (re) {autoAlert(re.msg);if (re.code == 1) {var redirectUrl=(!re.url || re.url===null ||re.url==='')?'$redirectUrl':re.url; if (redirectUrl!==''){setTimeout(function () {window.location.href = redirectUrl;}, 1000);}}formRefresh('#$formID');$('#$formID').bootstrapValidator('disableSubmitButtons', false);},error:function() { $('#$formID').bootstrapValidator('disableSubmitButtons', false);}});}); })(window.jQuery); $linkage </script>
EOT;
        self::$validateHtml = $validateHtml;
        self::$validate = $validateConfig ? true : false;
        return $this;
    }
    
    /**
     * 添加字段验证
     * @param $name
     * @param $config
     * @return $this
     */
    public function addValidate($name, $config) {
        if (!isset(self::$validateConfig[$name]['validators'])) {
            self::$validateConfig[$name]['validators'] = [];
        }
        self::$validateConfig[$name]['validators'] = array_merge(self::$validateConfig[$name]['validators'], $config);
        return $this;
    }
    
    /**
     * 生成图标选择模态框
     * @param string $id - 指定模态框ID
     * @param null $filePath -字体图标的css文件位置
     * @return string
     */
    protected function getIcoModalDialog($id = 'modalDialog', $filePath = NULL) {
        $defaultPath = BASE_ROOT . PUBLIC_NAME . '/static/assets/font-awesome/css/font-awesome.css';
        $filePath = $filePath == NULL ? $defaultPath : $filePath;
        $icoFile = file_get_contents($filePath);
        $search = '<div class="input-group margin-bottom-20"><input type="text" class="form-control" id="' . $id . '_search_input" placeholder="输入关键字搜索图标"><span class="input-group-addon btn-success" id="' . $id . '_search_btn" style="cursor: pointer;">图标搜索</span></div>';
        $ico = '';
        if (preg_match_all("/.fa-(.*?):before/", $icoFile, $icoList)) {
            $ico .= '<div style="text-align: center;">';
            foreach ($icoList[1] as $v) {
                $ico .= '<span data-ico="fa fa-' . $v . '" class="ico-item" ';
                $ico .= 'data-dismiss="modal" aria-hidden="true" style="display: inline-block;padding: 10px;margin-bottom:5px; border: solid 1px #ccc;width: 45px;height: 45px;text-align: center;vertical-align: middle; cursor: pointer;">';
                $ico .= ' <i class="fa fa-lg fa-' . $v . '"></i> ';
                $ico .= '</span>';
            }
            $ico .= '</div>';
        }
        $html = '<div class="modal fade" id="' . $id . '" tabindex="-1" role="dialog" aria-labelledby="modalDialogLabel" aria-hidden="true">';
        $html .= '<div class="modal-dialog" style="width: 80%!important;"><div class="modal-content">';
        $html .= '<div class="modal-header">';
        $html .= '<h3 class="modal-title" id="modalDialogLabel"><strong><i class="fa fa-camera-retro"></i> 图标</strong>选择器</h3>';
        $html .= '</div>';
        $html .= '<div class="modal-body">' . $search . $ico . '</div></div></div></div>';
        return $html;
    }
    
    /**
     * 自行获取表单默认数据
     * @param $name - name属性
     * @param string $default - 默认值
     * @return string
     */
    protected function getFormData($name, $default = '') {
        $data = self::$formData;
        $value = isset($data[$name]) ? $data[$name] : $default;
        return $value;
    }
}