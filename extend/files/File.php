<?php

/**
 *  ==================================================================
 *        文 件 名: File.php
 *        概    要: 文件处理类
 *        作    者: IT小强
 *        创建时间: 2017/3/21 20:10
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace files;

/**
 * Class File - 文件处理类
 * @package files
 */
class File {
    
    /**
     * 创建目录
     * @param $dir -目录名
     * @return boolean true 成功， false 失败
     */
    static public function mk_dir($dir) {
        $dir = rtrim($dir, '/') . '/';
        if (!is_dir($dir)) {
            if (mkdir($dir, 0700, true) == false) {
                return false;
            }
            return true;
        }
        return true;
    }
    
    /**
     * 基于数组创建目录和文件
     * @param array $files 文件名数组
     */
    static public function create_dir_or_files($files) {
        foreach ($files as $key => $value) {
            if (substr($value, -1) == '/') {
                mkdir($value, 0777, true);
            } else {
                @file_put_contents($value, '');
            }
        }
    }
    
    /**
     * 读取文件内容
     * @param $filename -文件名
     * @return string 文件内容
     */
    static public function read_file($filename) {
        $content = '';
        if (function_exists('file_get_contents')) {
            @$content = file_get_contents($filename);
        } else {
            if (@$fp = fopen($filename, 'r')) {
                @$content = fread($fp, filesize($filename));
                @fclose($fp);
            }
        }
        return $content;
    }
    
    /**
     * 写文件
     * @param $filename - 文件名
     * @param $writetext - 文件内容
     * @param $openmod - 打开方式
     * @return boolean true 成功, false 失败
     */
    static function write_file($filename, $writetext, $openmod = 'w') {
        if (@$fp = fopen($filename, $openmod)) {
            flock($fp, 2);
            fwrite($fp, $writetext);
            fclose($fp);
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 删除目录
     * @param $dirName -原目录
     * @return boolean true 成功, false 失败
     */
    static function del_dir($dirName) {
        if (!file_exists($dirName)) {
            return false;
        }
        
        $dir = opendir($dirName);
        while ($fileName = readdir($dir)) {
            $file = $dirName . '/' . $fileName;
            if ($fileName != '.' && $fileName != '..') {
                if (is_dir($file)) {
                    self::del_dir($file);
                } else {
                    unlink($file);
                }
            }
        }
        closedir($dir);
        return rmdir($dirName);
    }
    
    /**
     * 复制目录
     * @param $surDir - 原目录
     * @param $toDir - 目标目录
     * @return boolean true 成功, false 失败
     */
    static function copy_dir($surDir, $toDir) {
        $surDir = rtrim($surDir, '/') . '/';
        $toDir = rtrim($toDir, '/') . '/';
        if (!file_exists($surDir)) {
            return false;
        }
        
        if (!file_exists($toDir)) {
            self::mk_dir($toDir);
        }
        $file = opendir($surDir);
        while ($fileName = readdir($file)) {
            $file1 = $surDir . '/' . $fileName;
            $file2 = $toDir . '/' . $fileName;
            if ($fileName != '.' && $fileName != '..') {
                if (is_dir($file1)) {
                    self::copy_dir($file1, $file2);
                } else {
                    copy($file1, $file2);
                }
            }
        }
        closedir($file);
        return true;
    }
    
    /**
     * 列出目录
     * @param $dir -目录名
     * @param $is_del -是否删除当前目录和上级目录（. or ..）
     * @return array - 目录数组。列出文件夹下内容，返回数组 $dirArray['dir']:存文件夹；$dirArray['file']：存文件
     */
    static function get_dirs($dir, $is_del = true) {
        $dir = rtrim($dir, '/') . '/';
        $dirArray = [];
        if (false != ($handle = opendir($dir))) {
            $i = 0;
            $j = 0;
            while (false !== ($file = readdir($handle))) {
                if (is_dir($dir . $file)) { //判断是否文件夹
                    if ($is_del == false) {
                        $dirArray ['dir'] [$i] = $file;
                        $i++;
                    } else {
                        if ($file != '.' && $file != '..') {
                            $dirArray ['dir'] [$i] = $file;
                            $i++;
                        }
                    }
                } else {
                    $dirArray ['file'] [$j] = $file;
                    $j++;
                }
            }
            closedir($handle);
        }
        return $dirArray;
    }
    
    /**
     * 统计文件夹大小
     * @param $dir - 目录名
     * @return number 文件夹大小(单位 B)
     */
    static function get_size($dir) {
        $dirlist = opendir($dir);
        $dirsize = 0;
        while (false !== ($folderorfile = readdir($dirlist))) {
            if ($folderorfile != "." && $folderorfile != "..") {
                if (is_dir("$dir/$folderorfile")) {
                    $dirsize += self::get_size("$dir/$folderorfile");
                } else {
                    $dirsize += filesize("$dir/$folderorfile");
                }
            }
        }
        closedir($dirlist);
        return $dirsize;
    }
    
    /**
     * 检测是否为空文件夹
     * @param $dir - 目录名
     * @return boolean true 空， false 不为空
     */
    static function empty_dir($dir) {
        return (($files = @scandir($dir)) && count($files) <= 2);
    }
    
    /**
     * 解压zip格式的压缩文件
     * @param $zipFile - zip文件路径
     * @param string $toDir 解压路径
     * @param bool $mkNameDir - 是否以压缩文件命名
     * @param bool $overwrite -是否覆盖已有文件
     * @return bool
     */
    public static function unzip($zipFile, $toDir = '', $mkNameDir = true, $overwrite = true) {
        $zip = zip_open($zipFile);
        if (!$zip) {
            return false;
        }
        $toDir .= (substr($toDir, strlen($toDir) - 1, 1) != '/') ? '/' : '';
        if ($mkNameDir) {
            $start = (strrpos($zipFile, '/') === 0) ? 1 : intval(strrpos($zipFile, '/'));
            $end = strrpos($zipFile, '.') - 1;
            $toDir = $toDir . substr($zipFile, $start + 1, $end - $start) . "/";
        }
        // 如果不存在 创建目标解压目录
        $mkDir = self::mk_dir($toDir);
        if (!$mkDir) {
            return false;
        }
        // 对每个文件进行解压
        while ($zip_entry = zip_read($zip)) {
            // 文件不在根目录
            $pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
            if ($pos_last_slash !== false) {
                // 创建目录 在末尾带 /
                self::mk_dir($toDir . substr(zip_entry_name($zip_entry), 0, $pos_last_slash + 1));
            }
            
            // 打开包
            if (zip_entry_open($zip, $zip_entry, "r")) {
                
                // 文件名保存在磁盘上
                $file_name = $toDir . zip_entry_name($zip_entry);
                
                // 检查文件是否需要重写
                if ($overwrite === true || $overwrite === false && !is_file($file_name)) {
                    // 读取压缩文件的内容
                    $fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                    @file_put_contents($file_name, $fstream);
                    // 设置权限
                    @chmod($file_name, 0777);
                }
                // 关闭入口
                zip_entry_close($zip_entry);
            }
        }
        // 关闭压缩包
        zip_close($zip);
        return true;
    }
    
    /**
     * 文件缓存与文件读取
     * @param $name -  文件名
     * @param $value - 文件内容,为空则获取缓存
     * @param $path -  文件所在目录,默认是当前应用的DATA目录
     * @param $cached - 是否缓存结果,默认缓存
     * @return mixed
     */
    function cache($name, $value = '', $path = '', $cached = true) {
        static $_cache = array();
        $filename = $path . $name . '.php';
        if ('' !== $value) {
            if (is_null($value)) {
                // 删除缓存
                return false !== strpos($name, '*') ? array_map("unlink", glob($filename)) : unlink($filename);
            } else {
                // 缓存数据
                $dir = dirname($filename);
                // 目录不存在则创建
                if (!is_dir($dir))
                    mkdir($dir, 0755, true);
                $_cache[$name] = $value;
                return file_put_contents($filename, strip_whitespace("<?php\treturn " . var_export($value, true) . ";?>"));
            }
        }
        if (isset($_cache[$name]) && $cached == true) return $_cache[$name];
        // 获取缓存数据
        if (is_file($filename)) {
            $value = include $filename;
            $_cache[$name] = $value;
        } else {
            $value = false;
        }
        return $value;
    }
}
