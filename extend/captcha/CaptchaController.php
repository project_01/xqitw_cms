<?php
/**
 *  ==================================================================
 *        文 件 名: CaptchaController.php
 *        概    要: 验证码控制器类
 *        作    者: IT小强
 *        创建时间: 2017/9/18 17:57
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace captcha;

/**
 * 验证码控制器类
 * Class CaptchaController
 * @package captcha
 */
class CaptchaController {
    
    /**
     * 调用验证码
     * @param string $id
     * @return \think\Response
     */
    public function index($id = '') {
        $config = get_config_level_one('captcha');
        $captcha = new Captcha($config);
        return $captcha->entry($id);
    }
}