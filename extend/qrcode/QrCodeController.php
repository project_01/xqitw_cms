<?php

/**
 *  ==================================================================
 *        文 件 名: QrCodeController.php
 *        概    要: 二维码控制器
 *        作    者: IT小强
 *        创建时间: 2017/9/30 19:13
 *        修改时间:
 *        copyright (c)2016 admin@xqitw.com
 *  ==================================================================
 */

namespace qrcode;

class QrCodeController {
    
    /**
     * 二维码调用地址
     */
    public function index() {
        $data = request()->param();
        /* 二维码内容 */
        $text = $data['text'];
        /* 二维码大小 */
        $size = isset($data['size']) ? intval($data['size']) : 1000;
        if ($size < 200) {
            $size = 200;
        }
        /* 是否设置二维码边框 */
        $border = isset($data['border']) ? boolval($data['border']) : true;
        /* 二维码边距 */
        $padding = isset($data['padding']) ? intval($data['padding']) : 38;
        /* 二维码logo图 */
        $logo = isset($data['logo']) ? $data['logo'] : '';
        /* 二维码标签文字 */
        $label = isset($data['label']) ? $data['label'] : '';
        /* 要保存图片时，需设置二维码保存路径 */
        $saveName = isset($data['saveName']) ? $data['saveName'] : '';
        // 实例化二维码类
        $qrCode = new QrCode();
        // 设置生成二维码生成的各项参数
        /*
         * 设置版本号
         * QR码符号共有40种规格的矩阵，从21x21（版本1），到177x177（版本40）
         * 每一版本符号比前一版本 每边增加4个模块。
         */
        $qrCode->setVersion(6);
        //容错级别,2的容错率:30%容错级别：0：15%，1：7%，2：30%，3：25%
        $qrCode->setErrorCorrection(2);
        //设置QR码模块大小
        $qrCode->setModuleSize(2);
        //设置二维码保存类型
        $qrCode->setImageType('png');
        
        if (!empty($logo)) {
            //二维码中间的logo图片
            $qrCode->setLogo($logo);
            //设置logo大小
            $qrCode->setLogoSize($size / 5);
        }
        
        //二维码内容
        $qrCode->setText($text);
        //二维码生成后的大小
        $qrCode->setSize($size);
        //设置二维码的边框宽度，默认16
        $qrCode->setPadding($padding);
        //设置模块间距
        $qrCode->setDrawQuietZone(true);
        //给二维码加边框。。。
        $qrCode->setDrawBorder($border);
        if (!empty($label)) {
            //在生成的图片下面加上文字
            $qrCode->setLabel($label);
            //生成的文字大小、
            $createSize = $size / 18;
            $minSize = 14;
            $fontSize = ($createSize > $minSize) ? $createSize : $minSize;
            $qrCode->setLabelFontSize($fontSize);
            //设置标签字体
            $lablePath = BASE_ROOT . PUBLIC_NAME . '/static/assets/font-cn/simkai.ttf';
            $qrCode->setLabelFontPath($lablePath);
        }
        //生成的二维码的颜色
        $color_foreground = ['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0];
        $qrCode->setForegroundColor($color_foreground);
        //生成的图片背景颜色
        $color_background = ['r' => 241, 'g' => 241, 'b' => 241, 'a' => 0];
        $qrCode->setBackgroundColor($color_background);
        
        //生成二维码
        if (!empty($saveName)) {
            $qrCode->save($saveName);
        } else {
            header('Content-type: image/png');
            $qrCode->render();
        }
        exit();
    }
}